name = "501st Community Auxiliary Mod";
tooltipOwned="501st Aux";
picture = "RD501_Main\logo.paa";
logoSmall = "RD501_Main\logo.paa";
logo = "RD501_Main\logo.paa";
logoOver = "RD501_Main\logo.paa";
hideName = 0;
hidePicture = 0;
actionName = "Website";
action = "https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod";
description = "501st Aux Mod";
overview = "501st Legion Community Auxiliary Mod";