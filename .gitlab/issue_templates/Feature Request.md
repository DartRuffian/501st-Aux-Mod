<!-- Please remember that not all changes can be approved by the Aux Mod and most must be approved by your CoC. -->

# Description
A short description of the feature you would like

# Details
Full details of the feature you would like.
- Including
- More
- Details
- Is
- Better

# Notes
Additional notes.

/label ~pending-approval ~feature