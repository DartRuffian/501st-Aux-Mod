class cfgPatches
{
    class Aux501_Patch_Westar34L
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Westar34L"
        };
    };
};

class Mode_FullAuto;

class CowsSlot;
class MuzzleSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Westar34L: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Westar 34L";
        baseWeapon = "Aux501_Weaps_Westar34L";
        picture = "\OPTRE_Weapons\MG\icons\m247_icon.paa";
        model = "\OPTRE_Weapons\MG\M247.p3d";
        hiddenSelections[] = 
        {
            "mainbody1",
            "mainbody2",
            "magazine",
            "stock",
            "details",
            "barrel_sights"
        };
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Weapons\Mandalorians\Westar34L\Data\Texture\mainbody1.paa",
            "\Aux501\Weapons\Mandalorians\Westar34L\Data\Texture\mainbody2.paa",
            "\Aux501\Weapons\Mandalorians\Westar34L\Data\Texture\magazine.paa",
            "\Aux501\Weapons\Mandalorians\Westar34L\Data\Texture\stock.paa",
            "\Aux501\Weapons\Mandalorians\Westar34L\Data\Texture\details.paa",
            "\OPTRE_Weapons\MG\data\M247_barrel_sights_CO.paa"
        };
        handAnim[] = 
        {
            "OFP2_ManSkeleton",
            "\OPTRE_Weapons\MG\data\anim\OPTRE_M247_handanim.rtm"
        };
        recoil = "recoil_mxm";
        reloadAction = "WBK_LMG_Mag_RightSide_reload";
        fireLightDiffuse[] = {0.5,0.5,0.25};
        fireLightAmbient[] = {0.5,0.5,0.25};
        magazines[]=
        {
            "Aux501_Weapons_Mags_Westar34L150"
        };
        modes[] = {"FullAuto","aiclose","aimedium","aifar"};
        class FullAuto: Mode_FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\ls_sounds\weapons\shot\westar35s_1",+5db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\ls_sounds\weapons\shot\westar35s_1",+5db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            reloadTime = 0.1;
            dispersion = 0.00102;
            soundBurst = 0;
            ffCount = 1;
            soundContinuous = 0;
            minRange = 0;
            minRangeProbab = 0.9;
            midRange = 15;
            midRangeProbab = 0.7;
            maxRange = 30;
            maxRangeProbab = 0.1;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            showToPlayer = 1;
        };
        class aiclose: FullAuto
        {
            burst = 10;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 70;
            midRangeProbab = 0.7;
            maxRange = 150;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class aimedium: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 300;
            minRange = 75;
            minRangeProbab = 0.05;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.04;
        };
        class aifar: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 600;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 700;
            midRangeProbab = 0.6;
            maxRange = 1000;
            maxRangeProbab = 0.1;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "aux501_cows_holoscope"
                };
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] = 
                {
                    "bipod_01_f_blk"
                };
            };
        };    
    };

    class Aux501_Weaps_Westar34L_attach: Aux501_Weaps_Westar34L
    {
        class LinkedItems
        {
            class LinkedItemsOptic
            {
                item = "aux501_cows_holoscope";
                slot = "CowsSlot";
            };
            class LinkedItemsUnderBarrel
            {
                item = "bipod_01_f_blk";
                slot = "UnderBarrelSlot";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Westar35C100;

    class Aux501_Weapons_Mags_Westar34L150: Aux501_Weapons_Mags_Westar35C100
    {
        displayName = "[501st] Westar 34L Blaster Cell";
        displayNameShort = "Westar 34L Cell";
        picture = "\A3\Weapons_F_Exp\Data\UI\icon_150Rnd_556x45_Drum_Mag_F_ca.paa";
        count = 150;
        descriptionShort = "Westar 34L Medium Power Magazine";
        model = "\OPTRE_Weapons\MG\M247_Mag_Proxy.p3d";
        modelSpecial = "\OPTRE_Weapons\MG\M247_Mag_Proxy.p3d";
        modelSpecialIsProxy = 1;
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\optre_weapons\mg\data\m247_magazine_co.paa"};
    };
};