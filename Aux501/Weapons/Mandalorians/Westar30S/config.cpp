class cfgPatches
{
    class Aux501_Patch_Westar30S
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_Westar30S"
        };
    };
};

class CowsSlot;
class MuzzleSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    //class Aux501_Weaps_Westar30S: Aux501_rifle_base_stunless
    //{
        //scope = 2;
        //displayName = "[501st] Westar 30S";
        //baseWeapon = "Aux501_Weaps_Westar30S";
        //picture = "Aux501\Weapons\Mandalorians\Westar30S\iqa11_ui.paa";
        //model = "LF_Weapon_Unit\hi12\hi12.p3d";
        //handAnim[] = {"OFP2_ManSkeleton","LF_Weapon_Unit\hi12\anims\hi12_handanim.rtm"};
        //magazines[] =
        //{
            //"Aux501_Weapons_Mags_Westar30S15"
        //};
        //modes[] = {"Single","single_medium_optics1","single_far_optics2"};
        //class Single: Single
        //{
            //sounds[] = {"StandardSound"};
            //class BaseSoundModeType
            //{
                //weaponSoundEffect = "";
                //closure1[] = {};
                //closure2[] = {};
                //soundClosure[] = {};
            //};
            //class StandardSound: BaseSoundModeType
            //{
                //weaponSoundEffect = "";
                //begin1[] = {"LF_Weapon_Unit\main\sounds\Westar_1.ogg",1,1,1800};
                //soundBegin[] = {"begin1", 1};
            //};
            //reloadTime = 1.2;
            //dispersion = 0.00073;
            //minRange = 2;
            //minRangeProbab = 0.5;
            //midRange = 250;
            //midRangeProbab = 0.7;
            //maxRange = 450;
            //maxRangeProbab = 0.3;
        //};
        //class single_medium_optics1: Single
        //{
            //dispersion = 0.00073;
            //requiredOpticType = 1;
            //minRange = 2;
            //minRangeProbab = 0.2;
            //midRange = 450;
            //midRangeProbab = 0.7;
            //maxRange = 650;
            //maxRangeProbab = 0.2;
            //aiRateOfFire = 6;
            //aiRateOfFireDistance = 600;
        //};
        //class single_far_optics2: single_medium_optics1
        //{
            //dispersion = 0.00073;
            //requiredOpticType = 2;
            //minRange = 100;
            //minRangeProbab = 0.2;
            //midRange = 550;
            //midRangeProbab = 0.7;
            //maxRange = 1200;
            //maxRangeProbab = 0.05;
            //aiRateOfFire = 8;
            //aiRateOfFireDistance = 1200;
        //};
        //modelOptics = "LF_Weapon_Unit\Amban\Ambanscope.p3d";
        //class OpticsModes
        //{
            //class hi12scope_sights
            //{
                //opticsID = 1;
                //useModelOptics = 0;
                //opticsPPEffects[] = {"Default"};
                //opticsFlare = 0;
                //opticsDisablePeripherialVision = 0;
                //opticsZoomMin = 0.25;
                //opticsZoomMax = 1.25;
                //opticsZoomInit = 0.75;
                //memoryPointCamera = "eye";
                //visionMode[] = {};
                //distanceZoomMin = 200;
                //distanceZoomMax = 200;
                //cameraDir = "";
            //};

            //class hi12scope_scope: hi12scope_sights
            //{
                //opticsID = 1;
                //useModelOptics = 1;
                //opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur3"};
                //opticsZoomMin = 0.0625;
                //opticsZoomMax = 0.125;
                //opticsZoomInit = 0.125;
                //discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200};
                //discreteDistanceInitIndex = 1;
                //distanceZoomMin = 300;
                //distanceZoomMax = 1200;
                //discretefov[] = {"0.25/3","0.25/6","0.25/9","0.25/12","0.25/15","0.25/20"};
                //discreteInitIndex = 0;
                //memoryPointCamera = "eye";
                //visionMode[] = {"Normal","NVG","TI"};
                //opticsFlare = 1;
                //opticsDisablePeripherialVision = 1;
                //cameraDir = "";
            //};
        //};
        //class WeaponSlotsInfo: WeaponSlotsInfo
        //{
            //class CowsSlot: CowsSlot
            //{
                //displayName = "Optics Slot";
                //iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                //iconPinpoint = "Bottom";
                //iconPosition[] = {0.5,0.35};
                //iconScale = 0.2;
                //linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                //compatibleItems[] = {};
            //};
            //class MuzzleSlot: MuzzleSlot
            //{
                //linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                //displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                //compatibleItems[] =
                //{
                    //"Aux501_muzzle_flash"
                //};
            //};
        //};
    //};

    //class Aux501_Weaps_Westar30S_muzzled: Aux501_Weaps_Westar30S
    //{
        //class LinkedItems
        //{
            //class LinkedItemsMuzzle
            //{
                //item = "Aux501_muzzle_flash";
                //slot = "CowsSlot";
            //};
        //};
    //};
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_yellow;

    class Aux501_Weapons_Ammo_Westar30S_Blaster: Aux501_Weapons_Ammo_base_yellow
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
        model = "SWLW_main\Effects\laser_yellow.p3d";
        tracerscale = 1.5;
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_30mw10;

    class Aux501_Weapons_Mags_Westar30S15: Aux501_Weapons_Mags_30mw10
    {
        displayName = "[501st] Westar 30S Blaster Cell";
        displayNameShort = "15Rnd 30MW";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_mag_ui_ca.paa";
        count = 15;
        ammo = "Aux501_Weapons_Ammo_Westar30S_Blaster";
        descriptionShort = "Westar30S High Power Magazine";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
    };
};