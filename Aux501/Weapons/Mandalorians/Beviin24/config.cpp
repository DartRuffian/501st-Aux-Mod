class cfgPatches
{
    class Aux501_Patch_Beviin24
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_Westar35S"
        };
    };
};

class Mode_SemiAuto;

class CowsSlot;
class MuzzleSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Beviin24: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Beviin-24";
        baseWeapon = "Aux501_Weaps_Beviin24";
        picture = "\MDMR\ui\mandoDMRicon_ca.paa";
        model = "\MDMR\rifle\MandoDMR.p3d";
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        handAnim[] = {"OFP2_ManSkeleton","\MDMR\anims\MandoDMRHand.rtm"};
        fireLightDiffuse[] = {0.5,0.5,0.25};
        fireLightAmbient[] = {0.5,0.5,0.25};
        magazines[] =
        {
            "Aux501_Weapons_Mags_Beviin2415"
        };
        modes[] = {"Single","single_medium_optics1","single_far_optics2"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
            reloadTime = 1.2;
            dispersion = 0.00073;
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 450;
            maxRangeProbab = 0.3;
        };
        class single_medium_optics1: Single
        {
            dispersion = 0.00073;
            requiredOpticType = 1;
            minRange = 2;
            minRangeProbab = 0.2;
            midRange = 450;
            midRangeProbab = 0.7;
            maxRange = 650;
            maxRangeProbab = 0.2;
            aiRateOfFire = 6;
            aiRateOfFireDistance = 600;
        };
        class single_far_optics2: single_medium_optics1
        {
            dispersion = 0.00073;
            requiredOpticType = 2;
            minRange = 100;
            minRangeProbab = 0.2;
            midRange = 550;
            midRangeProbab = 0.7;
            maxRange = 1200;
            maxRangeProbab = 0.05;
            aiRateOfFire = 8;
            aiRateOfFireDistance = 1200;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = {"IBL_weapon_mandoDMR_scope"};
            };
        };
        class GunParticles
        {
            class FirstEffect
            {
                directionName = "Konec hlavne";
                effectName = "RifleAssaultCloud";
                positionName = "Usti hlavne";
            };
        };
    };

    class Aux501_Weaps_Beviin24_scoped: Aux501_Weaps_Beviin24
    {
        class LinkedItems
        {
            class LinkedItemsMuzzle
            {
                item = "IBL_weapon_mandoDMR_scope";
                slot = "CowsSlot";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_30mw10;

    class Aux501_Weapons_Mags_Beviin2415: Aux501_Weapons_Mags_30mw10
    {
        displayName = "[501st] Beviin-24 Blaster Cell";
        displayNameShort = "15Rnd 30MW";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_mag_ui_ca.paa";
        count = 15;
        ammo = "Aux501_Weapons_Ammo_Beviin24_Blaster";
        descriptionShort = "Beviin-24 High Power Magazine";
        model = "\MDMR\rifle\MandoDMRmag.p3d";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_yellow;

    class Aux501_Weapons_Ammo_Beviin24_Blaster: Aux501_Weapons_Ammo_base_yellow
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
    };
};