class cfgPatches
{
    class Aux501_Patch_Westar35S
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_Westar35S"
        };
    };
};

class Mode_SemiAuto;

class CowsSlot;
class MuzzleSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class FullAuto;
        class Single;
        class single_medium; 
        class single_far;
        class WeaponSlotsInfo;
    };
    class UGL_F;

    class Aux501_Weaps_Westar35S: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Westar 35S";
        baseWeapon = "Aux501_Weaps_Westar35S";
        picture = "\SWLW_merc_mando\rifles\westar35s\data\ui\SWLW_westar35s_ui.paa";
        model = "SWLW_merc_mando\rifles\westar35s\westar35s.p3d";
		handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_epa\LongRangeRifles\DMR_01\Data\Anim\dmr_01.rtm"};
        fireLightDiffuse[] = {0.5,0.5,0.25};
        fireLightAmbient[] = {0.5,0.5,0.25};
        magazines[] =
        {
            "Aux501_Weapons_Mags_Westar35S100"
        };
        muzzles[] =
        {
            "this",
            "Aux501_35s_Scatter_muzzle"
        };
        modes[] = {"FullAuto","Single","single_medium","single_far"};
        class FullAuto: FullAuto
        {
            minRange = 30;
            reloadTime = 0.096;
            dispersion = 0.002;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class Single: Single
        {
            minRange = 30;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class single_medium: single_medium
        {
            minRange = 30;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class single_far: single_far
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class Aux501_35s_Scatter_muzzle: UGL_F
        {
            displayName = "[501st] Underbarrel Shotgun";
            descriptionShort = "Underbarrel Shotgun Module for 35s";
            cursor = "";
			useModelOptics = 0;
			useExternalOptic = 1;
			magazineWell[] = {};
			cameraDir = "eye";
			memoryPointCamera = "";
			discreteDistance[] = {100};
			discreteDistanceCameraPoint[] = {"eye"};
			discreteDistanceInitIndex = 0;
			initSpeed = -1;
			muzzleEnd = "shotgun pos";
			muzzlePos = "shotgun dir";
			reloadMagazineSound[] = {"\SWLW_main\sounds\scatter_reload",0.56234133,1,30};
            magazines[] = {"Aux501_Weapons_Mags_SBB3_shotgun25"};
			class Single: Mode_SemiAuto
            {
                sounds[] = {"StandardSound"};
                class BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    closure1[] = {};
                    closure2[] = {};
                    soundClosure[] = {};
                };
                class StandardSound: BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    begin1[] = {"\MRC\JLTS\weapons\SBB3\sounds\SBB3_fire.wss",+3db,1,2200};
                    soundBegin[] = {"begin1",1};
                };
                reloadTime = 0.5;
                dispersion = 0.00073;
                minRange = 0;
                minRangeProbab = 0.5;
                midRange = 30;
                midRangeProbab = 0.7;
                maxRange = 60;
                maxRangeProbab = 0.3;
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = {"SWLW_Westar35S_scope"};
            };
        };
    };

    class Aux501_Weaps_Westar35S_scoped: Aux501_Weaps_Westar35S
    {
        class LinkedItems
        {
            class LinkedItemsMuzzle
            {
                item = "SWLW_Westar35S_scope";
                slot = "CowsSlot";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Westar35C100;

    class Aux501_Weapons_Mags_Westar35S100: Aux501_Weapons_Mags_Westar35C100
    {
        displayName = "[501st] Westar 35S Blaster Cell";
        displayNameShort = "Westar 35s Cell";
        descriptionShort = "Westar 35s Cell";
    };
};