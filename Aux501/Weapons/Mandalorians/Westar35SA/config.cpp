class cfgPatches
{
    class Aux501_Patch_Westar35SA
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Westar35SA"
        };
    };
};

class CowsSlot;

class cfgWeapons
{
    class hgun_P07_F;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Westar35SA: Aux501_pistol_base
    {
        scope = 2;
        displayName = "[501st] Westar 35SA";
        baseWeapon = "Aux501_Weaps_Westar35SA";
        picture = "\SWLW_merc_mando\pistols\westar35sa\data\ui\SWLW_westar35sa_ui.paa";
        model = "SWLW_merc_mando\pistols\westar35sa\westar35sa.p3d";
        magazines[] =
        {
            "Aux501_Weapons_Mags_Westar35SA30"
        };
        muzzles[] = {"this"};
        fireLightDiffuse[] = {0.5,0.5,0.25};
        fireLightAmbient[] = {0.5,0.5,0.25};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin2[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                begin3[] = {"SWLW_merc_mando\rifles\westar35s\sounds\westar35s",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = {""};
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Westar35C100;

    class Aux501_Weapons_Mags_Westar35SA30: Aux501_Weapons_Mags_Westar35C100
    {
        displayName = "[501st] Westar 35SA Cell";
        displayNameShort = "Westar 35SA Cell";
        count = 30;
        descriptionShort = "Medium Power Pistol magazine";
        mass = 4;
    };
};