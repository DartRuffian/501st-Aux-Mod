class cfgPatches
{
    class Aux501_Patch_IQA11
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_cows_iqa11_optic",
            "Aux501_Weaps_IQA11",
            "Aux501_Weaps_IQA11_scoped"
        };
    };
};

class CowsSlot;

class cfgWeapons
{
    class InventoryOpticsItem_Base_F;
    class ItemCore;
    class Aux501_cows_iqa11_optic: ItemCore
    {
        displayName = "IQA-11 scope";
        author = "501st Aux Team";
        picture = "\SWLW_merc_mando\rifles\sniper\data\ui\sniper_scope_ui.paa";
        model = "\SWLW_merc_mando\rifles\sniper\sniper_scope.p3d";
        scope = 2;
        descriptionShort = "";
        weaponInfoType = "RscWeaponZeroing";
        class ItemInfo: InventoryOpticsItem_Base_F
        {
            mass = 8;
            opticType = 1;
            optics = 1;
            modelOptics = "\SWLW_droids\pistols\rg4d\rg4d_optic.p3d";
            class OpticsModes
            {
                class Scope
                {
                    opticsID = 1;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = 0.01;
                    opticsZoomMax = 0.042;
                    opticsZoomInit = 0.042;
                    discretefov[] = {0.042,0.01};
                    discreteInitIndex = 0;
                    distanceZoomMin = 100;
                    distanceZoomMax = 1200;
                    discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200};
                    discreteDistanceInitIndex = 0;
                    memoryPointCamera = "opticView";
                    visionMode[] = {"Normal","NVG"};
                    opticsFlare = "true";
                    cameraDir = "";
                };
            };
        };
        inertia = 0.1;
    };

    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_IQA11: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName="[501st] IQA-11";
        baseWeapon="Aux501_Weaps_IQA11";
        model = "SWLW_merc_mando\rifles\sniper\sniper.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SWLW_merc_mando\rifles\sniper\data\sniper_ca.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\SWLW_clones\rifles\dc15x\anims\dc15x_handanim.rtm"};
        reloadAction = "ReloadMagazine";
        picture = "\SWLW_merc_mando\rifles\sniper\data\ui\sniper_ui.paa";
        selectionFireAnim = "zasleh";
        recoil="recoil_mx";
        magazines[]=
        {
            "Aux501_Weapons_Mags_IQA20"
        };
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        modes[] = {"Single","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single: Single
        {
            reloadTime = 1.2;
            dispersion = 0.00073;
            aiDispersionCoefY = 1.7;
            aiDispersionCoefX = 1.4;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\PDF\IQA11\data\sounds\iqa11_shot.wss",db+5,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\PDF\IQA11\data\sounds\iqa11_shot.wss",db+5,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            minRange = 25;
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            dispersion = 0.00073;
            minRange = 25;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 0.1;
            aiRateOfFireDistance = 50;
        };
        class aiclose: aicqb
        {
            minRange = 50;
            minRangeProbab = 0.5;
            midRange = 150;
            midRangeProbab = 1;
            maxRange = 250;
            maxRangeProbably = 0.5;
            aiRateOfFireDistance = 150;
        };
        class aimedium: aicqb
        {
            minRange = 150;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 1;
            maxRange = 350;
            maxRangeProbab = 0.1;
            aiRateOfFireDistance = 250;
            requiredOpticType = 0;
        };
        class aifar: aicqb
        {
            minRange = 250;
            minRangeProbab = 0.5;
            midRange = 350;
            midRangeProbab = 1;
            maxRange = 600;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 350;
            requiredOpticType = 0;
        };
        class aiopticmode1: aicqb
        {
            minRange = 400;
            minRangeProbab = 0.5;
            midRange = 500;
            midRangeProbab = 1;
            maxRange = 700;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 500;
            minRangeProbab = 0.5;
            midRange = 700;
            midRangeProbab = 1;
            maxRange = 900;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_iqa11_optic"
                };
            };
        };
    };
    class Aux501_Weaps_IQA11_scoped: Aux501_Weaps_IQA11
    {
        class LinkedItems
        {
            class LinkedItemsOptic
            {
                item = "Aux501_cows_iqa11_optic";
                slot = "CowsSlot";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_30mw10;

    class Aux501_Weapons_Mags_IQA20: Aux501_Weapons_Mags_30mw10
    {
        displayName = "[501st] IQA-11 Cell";
        displayNameShort = "IQA-11 Cell";
        picture = "\SWLW_merc_mando\rifles\sniper\data\ui\sniper_mag_ui.paa";
        model = "SWLW_merc_mando\rifles\sniper\sniper_mag.p3d";
        modelSpecial = "\SWLW_merc_mando\rifles\sniper\sniper_mag.p3d";
        modelSpecialIsProxy = 1;
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SWLW_merc_mando\rifles\sniper\data\sniper_mag_co.paa"};
        count = 20;
        ammo = "Aux501_Weapons_Ammo_IQA11";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;

    class Aux501_Weapons_Ammo_IQA11: Aux501_Weapons_Ammo_base_red
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
    };
};