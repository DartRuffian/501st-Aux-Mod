class cfgPatches
{
    class Aux501_Patch_explosives_Void3
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "A3_Weapons_F_Explosives",
            "ace_explosives"
        };
        units[] = 
        {
            "Aux501_Weapons_Explosives_void3"
        };
        weapons[] = 
        {
            "Aux501_Weaps_Void3_PutMuzzle"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Explosives_void3"
        };
    };
};

class CfgVehicles
{
    class ACE_Explosives_Place_DemoCharge;

    class Aux501_Weapons_Explosives_void3: ACE_Explosives_Place_DemoCharge
    {
        scope = 2;
        scopeCurator = 2;
        author = "501st Aux Team";
        displayName = "[501st] Void-3 Seismic Charge";
        model = "ls_weapons\explosives\demo\ls_explosives_demopack_defused";
        ammo = "Aux501_Weapons_Ammo_Explosives_void3";
    };
};

class CfgWeapons
{
    class Default;
    class Put: Default
    {
        muzzles[] += {"Aux501_Weaps_Void3_PutMuzzle"};
        
        class PutMuzzle: Default{};
        class ace_explosives_muzzle: PutMuzzle {};

        class Aux501_Weaps_Void3_PutMuzzle: ace_explosives_muzzle
        {
            magazines[] = {"Aux501_Weapons_Mags_Explosives_void3"};
        };
    };
};

class CfgMagazines
{
    class SatchelCharge_Remote_Mag;
    
    class Aux501_Weapons_Mags_Explosives_void3: SatchelCharge_Remote_Mag
    {
        scope = 2;
        author = "501st Aux Team";
        displayName = "[501st] Void-3 Seismic Charge";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_ctype_ui.paa";
        model = "ls_weapons\explosives\demo\ls_explosives_demoPack";
        ammo = "Aux501_Weapons_Ammo_Explosives_void3";
        descriptionShort = "High-powered explosive";
        mass = 60;
        ACE_Explosives_Placeable = 1;
        useAction = 0;
        ACE_Explosives_SetupObject = "Aux501_Weapons_Explosives_void3";
        ACE_Explosives_DelayTime = 1.5;
        allowedSlots[] = {901,701};
        class Library
        {
            libTextDesc = "";
        };
    };
};

class CfgAmmo
{
    class DemoCharge_Remote_Ammo;

    class Aux501_Weapons_Ammo_Explosives_void3: DemoCharge_Remote_Ammo
    {
        scope = 2;
        ace_explosives_Explosive = "Aux501_Weapons_Ammo_Explosives_void3";
        ace_explosives_magazine = "Aux501_Weapons_Mags_Explosives_void3";
        ACE_explodeOnDefuse = 0.02;
        ace_explosives_defuseObjectPosition[] = {-1.415,0,0.12};
        ace_explosives_size = 0;
        hit = 5500;
        indirectHit = 3500;
        indirectHitRange = 8;
        model = "ls_weapons\explosives\demo\ls_explosives_demoPack";
		mineModelDisabled = "ls_weapons\explosives\demo\ls_explosives_demopack_defused";
        soundHit[] = {"",3.1622777,1,1500};
        soundDeactivation[] = {"A3\Sounds_F\weapons\Mines\deactivate_mine_3a",1.4125376,1,20};
        SoundSetExplosion[] = {"Aux501_soundset_void3_exp","Explosion_Debris_SoundSet"};
        defaultMagazine = "Aux501_Weapons_Mags_Explosives_void3";
        ExplosionEffects = "MineNondirectionalExplosionSmall";
        CraterEffects = "MineNondirectionalCraterSmall";
        whistleDist = 10;
        mineInconspicuousness = 3;
        mineTrigger = "RemoteTrigger";
        triggerWhenDestroyed = 1;
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_void3_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Explosives\Void_3\data\sounds\seismic_charge.wss",1}
        };
        volume = 1.0;
        range = 2200;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_void3_midExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Explosives\Void_3\data\sounds\seismic_charge.wss",1}
        };
        volume = 1.0;
        range = 2200;
        rangeCurve[] = {{0,1},{100,1},{500,0},{2200,0}};
    };
};

class CfgSoundSets
{
    class Aux501_soundset_void3_exp
    {
        soundShaders[] = {"Aux501_SoundShader_void3_closeExp","Aux501_SoundShader_void3_midExp"};
        volumeFactor = 1.0;
        volumeCurve = "InverseSquare2Curve";
        spatial = 1;
        doppler = 0;
        loop = 0;
        sound3DProcessingType = "ExplosionLight3DProcessingType";
        distanceFilter = "explosionDistanceFreqAttenuationFilter";
    };
};