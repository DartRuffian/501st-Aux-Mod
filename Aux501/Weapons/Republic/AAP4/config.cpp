class cfgPatches
{
    class Aux501_Patch_AAP4
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_AAP4"
            "Aux501_Weaps_AAP4_carry"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_AAP4",
            "Aux501_Weapons_Mags_AAP4_csw"
        };
    };
};

class cfgWeapons
{
    class Launcher_Base_F;
    class ace_javelin_Titan_Static;

    class Aux501_Weaps_AAP4: ace_javelin_Titan_Static
    {
        author = "501st Aux Team";
        displayName = "AAP4 'Striker' Launcher";
        magazineReloadTime = 2;
        ace_javelin_enabled = 1;
        weaponInfoType = "ACE_RscOptics_javelin";
        modelOptics = "\z\ace\addons\javelin\data\reticle_titan.p3d";
        canLock = 0;  
        lockingTargetSound[] = {"", 0, 1};
        lockedTargetSound[] = {"", 0, 1};
        magazines[]=
        {
            "Aux501_Weapons_Mags_AAP4"
        };
    };
    class Aux501_Weaps_AAP4_carry: Launcher_Base_F
    {
        scope = 2;
        displayName = "[501st] AAP4 'Striker'";
        author = "501st Aux Team";
        model = "\ls_weapons\tertiary\plx1\ls_weapon_plx1.p3d";
        picture = "\Aux501\Weapons\Republic\AAP4\data\UI\aap4_carry_ui.paa";
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 1;
            pickupTime = 1;
            deploy = "RD501_stat_Striker";
        };
    };
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_AAP4_csw
    {
        Aux501_Weapons_Mags_AAP4 = 1;
    };
};

class CfgMagazines
{
    class 1Rnd_GAT_missiles;

    class Aux501_Weapons_Mags_AAP4: 1Rnd_GAT_missiles
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_battery.paa";
        displayName = "[501st] AAP4 'Striker' Pod";
        displayNameShort = "6Rnd 'Striker' Pod";
        count = 6;
        ammo = "ACE_Javelin_FGM148_Static";
        model = "\MRC\JLTS\weapons\PLX1\PLX1_mag.p3d";
        weaponpoolavailable = 1;
        mass = 50;	
    };
    class Aux501_Weapons_Mags_AAP4_csw: Aux501_Weapons_Mags_AAP4
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
};