class cfgPatches
{
    class Aux501_Patch_DC17M
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DC17M"
        };
    };
};

class Pointerslot;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC17M: Aux501_rifle_base
    {
        scope = 2;
        displayName = "[501st] DC-17M";
        baseWeapon = "Aux501_Weaps_DC17M";
        picture = "\Aux501\Weapons\Republic\DC17M\dc17m_ui.paa";
        model = "\3AS\3AS_Weapons\DC17M\3AS_DC17M_f";
        handAnim[] =
        {
            "OFP2_ManSkeleton",
            "3AS\3AS_Weapons\Data\Anim\DC17M.rtm"
        };
        reloadTime = 0.1;
        reloadAction = "3AS_GestureReload_DC17M";
        reloadMagazineSound[] = {"SWLW_clones_spec\sounds\DC17M_reload.wss",0.56234133,1,30};
        recoil = "recoil_SMG_03";
        magazines[] =
        {
            "Aux501_Weapons_Mags_10mw71"
        };
        class stun: Aux501_stun_muzzle{};
        modes[] = {"FullAuto","Single","close","short","medium","far_optic1","far_optic2"};
        class Single: Single
        {
            reloadTime = 0.047;
            dispersion = 0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",+3db,1,2200};
                begin2[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",+3db,1,2200};
                begin3[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class FullAuto: FullAuto
        {
            reloadTime = 0.047;
            dispersion = 0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",+3db,1,2200};
                begin2[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",+3db,1,2200};
                begin3[] = {"\SWLW_clones_spec\sounds\DC17M_blaster_fire.wss",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
            soundContinuous = 0;
            soundBurst = 0;
            minRange = 0;
            minRangeProbab = 0.9;
            midRange = 15;
            midRangeProbab = 0.7;
            maxRange = 30;
            maxRangeProbab = 0.1;
            aiRateOfFire = 1e-06;
            showToPlayer = 1;
        };
        class close: FullAuto
        {
            burst = 20;
            aiRateOfFire = 0.01;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 30;
            midRangeProbab = 0.7;
            maxRange = 50;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class short: close
        {
            burst = 15;
            aiRateOfFire = 0.2;
            aiRateOfFireDistance = 300;
            minRange = 50;
            minRangeProbab = 0.05;
            midRange = 150;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.04;
        };
        class medium: close
        {
            burst = 15;
            aiRateOfFire = 0.25;
            aiRateOfFireDistance = 600;
            minRange = 200;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.6;
            maxRange = 600;
            maxRangeProbab = 0.1;
        };
        class far_optic1: close
        {
            requiredOpticType = 1;
            showToPlayer = 0;
            burst = 3;
            aiRateOfFire = 9;
            aiRateOfFireDistance = 900;
            minRange = 350;
            minRangeProbab = 0.04;
            midRange = 550;
            midRangeProbab = 0.5;
            maxRange = 700;
            maxRangeProbab = 0.01;
        };
        class far_optic2: far_optic1
        {
            requiredOpticType = 2;
            autoFire = 0;
            burst = 1;
            minRange = 400;
            minRangeProbab = 0.05;
            midRange = 800;
            midRangeProbab = 0.5;
            maxRange = 1000;
            maxRangeProbab = 0.01;
        };
        modelOptics = "3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.25;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Scope: Ironsights
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.125;
                opticsZoomMax = 0.125;
                opticsZoomInit = 0.125;
                memoryPointCamera = "opticView";
                visionMode[] = {"Normal","NVG"};
                opticsFlare = "true";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
                cameraDir = "";
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {    
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";  
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_10mw50;

    class Aux501_Weapons_Mags_10mw71: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 71Rnd 10MW Cell";
        displayNameShort = "71Rnd 10MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15s.paa";
        count = 71;
        model = "SWLW_clones\smgs\westar_m5\WestarM5_mag.p3d";
        modelSpecial = "3AS\3AS_Weapons\DC17M\attachments\blaster_Barrel.p3d";
        modelSpecialIsProxy = 1;
        descriptionShort = "DC17M Medium Power Drum";
    };
};