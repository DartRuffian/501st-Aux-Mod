class cfgPatches
{
    class Aux501_Patch_Purg3
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_PURG3_flamer"
        };
    };
};

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class FullAuto;
    };

    class Aux501_Weaps_PURG3_flamer: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] PURG-3 Flamethrower";
        baseWeapon = "Aux501_Weaps_PURG3_flamer";
        picture = "\Aux501\Weapons\Republic\Flamer\data\purg3_ui.paa";
        model = "3AS\3AS_Weapons\X42\BX42.p3d";
        handAnim[] = {"OFP2_ManSkeleton","3AS\3AS_Weapons\Data\Anim\BX42.rtm"};
        reloadAction = "ReloadMagazine";
        fireLightDiffuse[] = {255,94,0};
        fireLightAmbient[] = {255,94,0};
        magazines[]=
        {
            "Aux501_Weapons_Mags_Flamethrower150"
        };
        modes[] = 
        {
            "FullAuto",
            "fullauto_medium",
            "Single",
            "single_medium_optics1",
            "single_far_optics2"
        };
        class FullAuto: FullAuto
        {
            aiDispersionCoefX = 2;
            aiDispersionCoefY = 3;
            aiRateOfFire = 1e-06;
            aiRateOfFireDispersion = 1;
            aiRateOfFireDistance = 500;
            artilleryCharge = 1;
            artilleryDispersion = 1;
            autoFire = 1;
            burst = 1;
            burstRangeMax = -1;
            canShootInWater = 0;
            dispersion = 0.00073;
            displayName = "Full";
            ffCount = 1;
            ffFrequency = 11;
            ffMagnitude = 0.5;
            flash = "gunfire";
            flashSize = 0.1;
            maxRange = 30;
            maxRangeProbab = 0.1;
            midRange = 15;
            midRangeProbab = 0.7;
            minRange = 0;
            minRangeProbab = 0.9;
            multiplier = 1;
            recoil = "recoil_auto_mx";
            recoilProne = "recoil_auto_prone_mx";
            reloadTime = 0.05;
            requiredOpticType = -1;
            showToPlayer = 1;
            sound[] = {"",10,1};
            soundBegin[] = {"sound",1};
            soundBeginWater[] = {"sound",1};
            soundBurst = 0;
            soundClosure[] = {"sound",1};
            soundContinuous = 1;
            soundEnd[] = {"sound",1};
            soundLoop[] = {};
            sounds[] = {"StandardSound"};
            textureType = "fullAuto";
            useAction = 0;
            useActionTitle = "";
            weaponSoundEffect = "";
            class BaseSoundModeType
            {
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
                weaponSoundEffect = "";
            };
            class StandardSound
            {
                begin1[] = {"3AS\3AS_Weapons\X42\SFX\flamer.ogg",+3db,1,2200};
                begin2[] = {"3AS\3AS_Weapons\X42\SFX\flamer.ogg",+3db,1,2200};
                begin3[] = {"3AS\3AS_Weapons\X42\SFX\flamer.ogg",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
                soundClosure[] = {};
                weaponSoundEffect = "";
            };
        };
        class fullauto_medium: FullAuto
        {
            dispersion = 0.00073;
            showToPlayer = 0;
            burst = 3;
            aiBurstTerminable = 1;
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 75;
            midRangeProbab = 0.7;
            maxRange = 150;
            maxRangeProbab = 0.05;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 200;
        };
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"3AS\3AS_Weapons\X42\SFX\flamer.ogg",1,1,1800};
                begin2[] = {"3AS\3AS_Weapons\X42\SFX\flamer.ogg",1,1,1800};
                begin3[] = {"3AS\3AS_Weapons\X42\SFX\flamer.ogg",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
            };
            reloadTime = 0.096;
            recoil = "recoil_single_mx";
            recoilProne = "recoil_single_prone_mx";
            dispersion = 0.00073;
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 450;
            maxRangeProbab = 0.3;
        };
        class single_medium_optics1: Single
        {
            showToPlayer = 0;
            dispersion = 0.00073;
            requiredOpticType = 1;
            minRange = 2;
            minRangeProbab = 0.2;
            midRange = 450;
            midRangeProbab = 0.7;
            maxRange = 650;
            maxRangeProbab = 0.2;
            aiRateOfFire = 6;
            aiRateOfFireDistance = 600;
        };
        class single_far_optics2: single_medium_optics1
        {
            dispersion = 0.00073;
            requiredOpticType = 2;
            minRange = 100;
            minRangeProbab = 0.2;
            midRange = 550;
            midRangeProbab = 0.7;
            maxRange = 800;
            maxRangeProbab = 0.05;
            aiRateOfFire = 8;
            aiRateOfFireDistance = 800;
        };
        irLaserPos = "laser pos";
        irLaserEnd = "laser dir";
        irDistance = 5;
    };
};

class CfgMagazines
{
    class CA_Magazine;
    class Aux501_Weapons_Mags_Flamethrower150: CA_Magazine
    {
        scope = 2;
        displayName = "[501st] PURG-3 fuel";
        displayNameShort = "PURG-3 fuel";
        author = "501st Aux Team";
        picture = "\MRC\JLTS\weapons\z6\data\ui\z6_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\z6\z6_mag.p3d";
        ammo = "Aux501_Weapons_Ammo_flamethrower";
        count = 150;
        initSpeed = 25;
        tracersEvery = 1;
        lastRoundsTracer = 999;
        descriptionShort = "PURG-3 Fuel";
    };
};

class CfgAmmo
{
    class BulletBase;
    class Aux501_Weapons_Ammo_flamethrower: BulletBase
    {
        submunitionAmmo = "Aux501_Weapons_Ammo_flamethrower_sub";
        submunitionConeAngle = 1;
        submunitionConeType[] = {"poissondisc",1};
        triggerTime = 0.05;
        hit = 10;
        indirectHit = 6.5;
        indirectHitRange = 2.5;
        cartridge = "";
        cost = 2;
        visibleFire = 32;
        audibleFire = 12;
        visibleFireTime = 20;
        airFriction = 0.1;
        timetolive = 2;
        maxSpeed = 25;
        typicalSpeed = 0.12;
        coefGravity = 0;
        deflecting = 0;
        explosive = 1;
        fuseDistance = 1;
        caliber = 0.5;
        tracerScale = 0.001;
        tracerStartTime = 0.01;
        tracerEndTime = 0.1;
        craterEffects = "";
        effectFly = "SmallFire";
        explosionEffects = "Flame_Explosion";
        model = "\A3\Weapons_f\Data\bullettracer\tracer_red";
    };
    class Aux501_Weapons_Ammo_flamethrower_sub: BulletBase
    {
        hit = 10;
        indirectHit = 5;
        indirectHitRange = 1;
        cartridge = "";
        cost = 2;
        visibleFire = 32;
        audibleFire = 12;
        visibleFireTime = 20;
        airFriction = 0;
        timetolive = 3.5;
        maxSpeed = 25;
        typicalSpeed = 0.12;
        coefGravity = 0.25;
        deflecting = 0;
        explosive = 1;
        fuseDistance = 1;
        caliber = 0.5;
        tracerScale = 0.01;
        tracerStartTime = 0.01;
        tracerEndTime = 0.1;
        craterEffects = "";
        effectFly = "SmallFire";
        explosionEffects = "Flame_Explosion";
    };
};