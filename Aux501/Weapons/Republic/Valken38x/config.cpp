class cfgPatches
{
    class Aux501_Patch_Valken38X
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Valken38Y"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    /* class Aux501_Weaps_Valken38X: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Valken38X";
        baseWeapon = "Aux501_Weaps_Valken38X";
        picture = "\Aux501\Weapons\Republic\Valken38x\valken_ui.paa";
        model = "327th_weapons\327th_valken\38X.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_Mark\LongRangeRifles\DMR_02\data\Anim\DMR_02.rtm"};
        recoil = "3AS_recoil_DC15A";
        modelOptics = "3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
        useModelOptics = 1;
        maxRecoilSway = 0;
        swayDecaySpeed = 0;
        inertia = 0.5;
        dexterity = 1.5;
        initSpeed = -1;
        maxZeroing = 2500;
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mwdp30", 
            "Aux501_Weapons_Mags_30mw10"
        };
        modes[] = {"Single","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single: Single
        {
            reloadTime = 0.2;		
            dispersion = 0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            dispersion = 0.00073;
            minRange = 25;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 0.1;
            aiRateOfFireDistance = 50;
        };
        class aiclose: aicqb
        {
            minRange = 50;
            minRangeProbab = 0.5;
            midRange = 150;
            midRangeProbab = 1;
            maxRange = 250;
            maxRangeProbably = 0.5;
            aiRateOfFireDistance = 150;
        };
        class aimedium: aicqb
        {
            minRange = 150;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 1;
            maxRange = 350;
            maxRangeProbab = 0.1;
            aiRateOfFireDistance = 250;
            requiredOpticType = 0;
        };
        class aifar: aicqb
        {
            minRange = 250;
            minRangeProbab = 0.5;
            midRange = 350;
            midRangeProbab = 1;
            maxRange = 600;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 350;
            requiredOpticType = 0;
        };
        class aiopticmode1: aicqb
        {
            minRange = 400;
            minRangeProbab = 0.5;
            midRange = 500;
            midRangeProbab = 1;
            maxRange = 700;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 500;
            minRangeProbab = 0.5;
            midRange = 700;
            midRangeProbab = 1;
            maxRange = 900;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.375;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Snip
            {
                useModelOptics = 1;
                opticsFlare = 1;
                opticsID = 2;
                cameraDir = "";
                memoryPointCamera = "op_eye";
                modelOptics = "\3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
                opticsPPEffects[] = {"OpticsCHAbera1"};
                weaponInfoType = "RscWeaponRangeZeroingFOV";
                opticsZoomMin = "0.25/12";
                opticsZoomMax = "0.25/6";
                opticsZoomInit = "0.25/6";
                discreteinitIndex = 0;
                discretefov[] = {"0.25/6","0.25/12"};
                discreteDistanceInitIndex = 1;
                distanceZoomMin = 300;
                distanceZoomMax = 1200;
                thermalMode[] = {0,1};
                visionMode[] = {"Normal","NVG","TI"};
                opticsDisplayName = "VALKEN";
                opticsDisablePeripherialVision = 0;
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "Aux501_cows_DMS_TI",
                    "Aux501_cows_DMS_2_TI",
                    "Aux501_cows_DMS_3_TI",
                    "Aux501_cows_DMS_4_TI"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
                compatibleItems[] =
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";
                compatibleItems[] = 
                {
                    "acc_flashlight","acc_pointer_IR"
                }; 
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] = 
                {
                    "bipod_01_f_blk"
                };
            };
        };
    }; */
    class Aux501_Weaps_Valken38Y: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Valken38Y";
        baseWeapon = "Aux501_Weaps_Valken38Y";
        picture = "\MRC\JLTS\weapons\DW32S\data\ui\DW32S_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DW32S\DW32S.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\DW32S\data\DW32S_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DW32S\anims\DW32S_handanim.rtm"};
        recoil = "3AS_recoil_DC15A";
        maxRecoilSway = 0;
        swayDecaySpeed = 0;
        inertia = 0.5;
        dexterity = 1.5;
        initSpeed = -1;
        maxZeroing = 2500;
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mwdp30", 
            "Aux501_Weapons_Mags_30mw10"
        };
        modes[] = {"Single","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single: Single
        {
            reloadTime = 0.2;		
            dispersion = 0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            dispersion = 0.00073;
            minRange = 25;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 0.1;
            aiRateOfFireDistance = 50;
        };
        class aiclose: aicqb
        {
            minRange = 50;
            minRangeProbab = 0.5;
            midRange = 150;
            midRangeProbab = 1;
            maxRange = 250;
            maxRangeProbably = 0.5;
            aiRateOfFireDistance = 150;
        };
        class aimedium: aicqb
        {
            minRange = 150;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 1;
            maxRange = 350;
            maxRangeProbab = 0.1;
            aiRateOfFireDistance = 250;
            requiredOpticType = 0;
        };
        class aifar: aicqb
        {
            minRange = 250;
            minRangeProbab = 0.5;
            midRange = 350;
            midRangeProbab = 1;
            maxRange = 600;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 350;
            requiredOpticType = 0;
        };
        class aiopticmode1: aicqb
        {
            minRange = 400;
            minRangeProbab = 0.5;
            midRange = 500;
            midRangeProbab = 1;
            maxRange = 700;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 500;
            minRangeProbab = 0.5;
            midRange = 700;
            midRangeProbab = 1;
            maxRange = 900;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        memoryPointCamera = "eye";
        modelOptics = "3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
        useModelOptics = 1;
        class OpticsModes
        {
            class Scope 
            {
                opticsID = 1;
                useModelOptics = 1;
                cameraDir = "";
                memoryPointCamera = "opticView";
                modelOptics = "\3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
                opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
                opticsZoomMin = "0.25/12";
                opticsZoomMax = "0.25/6";
                opticsZoomInit = "0.25/6";
                discreteinitIndex = 0;
                discretefov[] = {"0.25/6","0.25/12"};
                discreteDistanceInitIndex = 1;
                distanceZoomMin = 300;
                distanceZoomMax = 1200;
                thermalMode[] = {0,1};
                visionMode[] = {"Normal","NVG","TI"};
                opticsDisplayName = "Valken";
                opticsFlare = 1;
                opticsDisablePeripherialVision = 1;
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "Aux501_cows_DMS_TI",
                    "Aux501_cows_DMS_2_TI",
                    "Aux501_cows_DMS_3_TI",
                    "Aux501_cows_DMS_4_TI"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
                compatibleItems[] =
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";
                compatibleItems[] = 
                {
                    "acc_flashlight",
                    "acc_pointer_IR"
                }; 
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] = 
                {
                    "bipod_01_f_blk"
                };
            };
        };
        class GunParticles
        {
            class FirstEffect
            {
                directionName = "Konec hlavne";
                effectName = "RifleAssaultCloud";
                positionName = "Usti hlavne";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_20mw40;

    class Aux501_Weapons_Mags_30mw10: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 10Rnd 30MW Cell";
        displayNameShort = "10Rnd 30MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_valken38.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_30mw";
        initSpeed = 1500;
        descriptionShort = "Valken 38x High power magazine";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_blue;
    
    class Aux501_Weapons_Ammo_30mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
    };
};