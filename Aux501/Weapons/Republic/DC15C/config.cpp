class cfgPatches
{
    class Aux501_Patch_DC15C
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_DC15C",
            "Aux501_Weaps_DC15C_UGL"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;

class Mode_SemiAuto;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };
    class UGL_F;

    class Aux501_Weaps_DC15C: Aux501_rifle_base
    {
        scope = 2;
        displayName = "[501st] DC-15C";
        author = "501st Aux Team";
        baseWeapon = "Aux501_Weaps_DC15C";
        picture = "\Aux501\Weapons\Republic\DC15C\dc15c_ui.paa";
        model = "3AS\3AS_Weapons\DC15C\3AS_DC15C_f";
        handAnim[] = {"OFP2_ManSkeleton", "\3AS\3AS_Weapons\DC15C\Data\Anim\DC15C_handanim.rtm"};
        recoil = "3AS_recoil_dc15s";
        reloadAction = "3AS_GestureReloadDC15S";
        reloadTime = 0.1;
        dispersion = 0.00116;
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mw40"
        };
        class stun: Aux501_stun_muzzle{};
        modes[] = {"Single","FullAuto","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\DC15A\sounds\dc15a_fire",+3db,1,2200};
                soundBegin[] = {"begin1", 1};
            };
            reloadTime = 0.12;
            dispersion = 0.00116;
        };
        class FullAuto: FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\DC15A\sounds\dc15a_fire",+3db,1,2200};
                soundBegin[] = {"begin1", 1};
            };
            reloadTime = 0.12;
            dispersion = 0.00073;
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aiclose: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aimedium: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 100;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.5;
            maxRange = 300;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 300;
        };
        class aifar: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.5;
            maxRange = 500;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
        };
        class aiopticmode1: aicqb
        {
            minRange = 350;
            minRangeProbab = 0.5;
            midRange = 550;
            midRangeProbab = 1;
            maxRange = 650;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 550;
            minRangeProbab = 0.5;
            midRange = 650;
            midRangeProbab = 1;
            maxRange = 800;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] =
                {
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_HoloScope",
                    "Aux501_cows_HoloScope_2",
                    "Aux501_cows_HoloScope_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4",
                    "3AS_optic_acog_DC15C",
                    "Aux501_cows_reflex_optic"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
                compatibleItems[] = {"acc_flashlight", "acc_pointer_IR"};
            };
        };
    };
    class Aux501_Weaps_DC15C_UGL: Aux501_Weaps_DC15C
    {
        displayName = "[501st] DC-15C UGL";
        baseWeapon = "Aux501_Weaps_DC15C_UGL";
        picture = "\Aux501\Weapons\Republic\DC15C\dc15cugl_ui.paa";
        model = "3AS\3AS_Weapons\DC15C\3AS_DC15C_GL.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\A3\Weapons_F_Exp\Machineguns\LMG_03\Data\Anim\LIM.rtm"};
        muzzles[] =
        {
            "this",
            "Stun",
            "Aux501_15c_UGL_muzzle"
        };
        class Aux501_15c_UGL_muzzle: UGL_F
        {
            displayName = "[501st] Over-Under Grenade Launcher";
            descriptionShort = "Underbarrel GL Module for DC15C";
            useModelOptics = 0;
            useExternalOptic = 0;
            magazines[] =
            {
                "Aux501_Weapons_Mags_GL_HE1",
                "Aux501_Weapons_Mags_GL_smoke_white6",
                "Aux501_Weapons_Mags_GL_smoke_purple3",
                "Aux501_Weapons_Mags_GL_smoke_yellow3",
                "Aux501_Weapons_Mags_GL_smoke_red3",
                "Aux501_Weapons_Mags_GL_smoke_green3",
                "Aux501_Weapons_Mags_GL_smoke_blue3",
                "Aux501_Weapons_Mags_GL_smoke_orange3",
                "ACE_HuntIR_M203",
                "Aux501_Weapons_Mags_GL_flare_White3",
                "Aux501_Weapons_Mags_GL_flare_Green3",
                "Aux501_Weapons_Mags_GL_flare_Red3",
                "Aux501_Weapons_Mags_GL_flare_Yellow3",
                "Aux501_Weapons_Mags_GL_flare_Blue3",
                "Aux501_Weapons_Mags_GL_flare_Cyan3",
                "Aux501_Weapons_Mags_GL_flare_Purple3",
                "Aux501_Weapons_Mags_GL_flare_IR3"
            };
            magazineWell[] = {};
            cameraDir = "OP_look";
            discreteDistance[] = {50, 75, 100, 150, 200, 250, 300, 350, 400};
            discreteDistanceCameraPoint[] = {"OP_eye_50", "OP_eye_75", "OP_eye_100", "OP_eye_150", "OP_eye_200", "OP_eye_250", "OP_eye_300", "OP_eye_350", "OP_eye_400"};
            discreteDistanceInitIndex = 1;
            reloadAction = "GestureReloadMXUGL";
            reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons\Rifles\MX\Mx_UGL_reload", 1, 1, 10};
            class Single: Mode_SemiAuto
            {
                sounds[] = {"StandardSound"};
                class BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    closure1[] = {};
                    closure2[] = {};
                    soundClosure[] = {};
                };
                class StandardSound: BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    begin1[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin2[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin3[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
                };
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_10mw50;
    class Aux501_Weapons_Mags_GL_HE3;

    class Aux501_Weapons_Mags_20mw40: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 40Rnd 20MW Cell";
        displayNameShort = "40Rnd 20MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_15mw.paa";
        count = 40;
        ammo = "Aux501_Weapons_Ammo_20mw";
        initSpeed = 1050;
        descriptionShort = "DC15C magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };
    
    //DC15-UGL
    class Aux501_Weapons_Mags_GL_HE1: Aux501_Weapons_Mags_GL_HE3
    {
        displayName = "[501st] 1Rnd HE Grenade";
        displayNameShort = "1Rnd HE";
        count = 1;
        ammo = "Aux501_Weapons_Ammo_GL_light_HE";
        descriptionShort = "1Rnd HE Grenade";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_blue;

    class Aux501_Weapons_Ammo_GL_HE;

    class Aux501_Weapons_Ammo_20mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 17.5;
        typicalSpeed = 1050;
        caliber = 2.8;
        waterFriction = -0.009;
    };

    //DC15-UGL
    class Aux501_Weapons_Ammo_GL_light_HE: Aux501_Weapons_Ammo_GL_HE
    {
        hit = 30;
        indirectHit = 25;
        model = "\MRC\JLTS\weapons\Core\effects\laser_orange.p3d";
    };
};