class cfgPatches
{
    class Aux501_Patch_RAMR
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_weaps_RAMR"
        };
    };
};

class CowsSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_RAMR: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] RAMR";
        baseWeapon = "Aux501_Weaps_RAMR";
        picture = "\MRC\JLTS\weapons\EPL2\data\ui\EPL2_ui_ca.paa";
        model = "\MRC\JLTS\weapons\EPL2\EPL2.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\EPL2\anims\EPL2_handanim.rtm"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_50mw10"
        };
        modes[] = {"Single","AICQB","AIClose","AIMedium","AIFar"};
        class Single: Single
        {
            reloadTime = 0.24;		
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\EPL2\sounds\EPL2_fire",1,1,3000};
                soundBegin[] = {"begin1",1};
            };            
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            dispersion = 0.0021;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 50;
        };
        class aiclose: aicqb
        {
            dispersion = 0.00105;
            minRange = 50;
            minRangeProbab = 0.5;
            midRange = 150;
            midRangeProbab = 1;
            maxRange = 250;
            maxRangeProbably = 0.5;
            aiRateOfFire = 1.5;
            aiRateOfFireDistance = 100;
        };
        class aimedium: aicqb
        {
            dispersion = 0.000525;
            minRange = 250;
            minRangeProbab = 0.5;
            midRange = 350;
            midRangeProbab = 1;
            maxRange = 450;
            maxRangeProbab = 0.1;
            aiRateOfFire = 2.5;
            aiRateOfFireDistance = 250;
            requiredOpticType = 0;
        };
        class aifar: aicqb
        {
            dispersion = 0.00085;
            minRange = 350;
            minRangeProbab = 0.5;
            midRange = 500;
            midRangeProbab = 1;
            maxRange = 600;
            maxRangeProbab = 0.5;
            aiRateOfFire = 3.5;
            aiRateOfFireDistance = 450;
            requiredOpticType = 0;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3"
                };
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_40mw5;

    class Aux501_Weapons_Mags_50mw10: Aux501_Weapons_Mags_40mw5
    {
        displayName = "[501st] 10Rnd 50MW Cell";
        displayNameShort = "10Rnd 50MW";
        picture = "\MRC\JLTS\weapons\DC15A\data\ui\DC15A_mag_ui_ca.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_50mw";
        descriptionShort = "RAMR High Power magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_blue;
    
    class Aux501_Weapons_Ammo_50mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 300;
        explosive = 0.5;
        typicalSpeed = 1100;
        caliber = 5;
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Blue.p3d";
        ExplosionEffects = "3AS_ImpactPlasma";
        craterEffects = "";
    };
};