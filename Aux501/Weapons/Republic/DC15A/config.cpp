class cfgPatches
{
    class Aux501_Patch_DC15A
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DC15A",
            "Aux501_Weaps_DC15A_UGL"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;

class Mode_SemiAuto;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class arifle_MX_Base_F;
    class Aux501_rifle_base: arifle_MX_Base_F
    {
        class Single;
        class Burst;
        class FullAuto;
        class WeaponSlotsInfo;
    };
    class UGL_F;

    class Aux501_Weaps_DC15A: Aux501_rifle_base
    {
        scope = 2;
        displayName = "[501st] DC-15A";
        baseWeapon = "Aux501_Weaps_DC15A";
        picture = "\MRC\JLTS\weapons\DC15A\data\ui\DC15A_plastic_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_plastic.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DC15A\anims\DC15A_handanim.rtm"};
        reloadAction = "GestureReload_JLTS_DC15A";
        reloadTime = 0.1;
        recoil = "recoil_mx";
        magazines[]=
        {
            "Aux501_Weapons_Mags_20mwdp30", 
            "Aux501_Weapons_Mags_20mwup30"
        };
        modelOptics = "3AS\3AS_Weapons\Data\3AS_2D_Optic.p3d";
        class stun: Aux501_stun_muzzle{};
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Scope: Ironsights
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.125;
                opticsZoomMax = 0.125;
                opticsZoomInit = 0.125;
                memoryPointCamera = "opticView";
                visionMode[] = {"Normal","NVG"};
                opticsFlare = "true";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
                cameraDir = "";
            };
        };
        modes[] = {"Single","Burst","FullAuto","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single: Single
        {
            reloadTime=0.1;		
            dispersion=0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15A\sounds\dc15a_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15A\sounds\dc15a_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class Burst: Burst
        {
            reloadTime=0.1;
            dispersion=0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15A\sounds\dc15a_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15A\sounds\dc15a_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class FullAuto: FullAuto
        {
            reloadTime = 0.125;
            dispersion = 0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15A\sounds\dc15a_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\DC15A\sounds\dc15a_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aiclose: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aimedium: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 100;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.5;
            maxRange = 300;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 300;
        };
        class aifar: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.5;
            maxRange = 500;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
        };
        class aiopticmode1: aicqb
        {
            minRange = 350;
            minRangeProbab = 0.5;
            midRange = 550;
            midRangeProbab = 1;
            maxRange = 650;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 550;
            minRangeProbab = 0.5;
            midRange = 650;
            midRangeProbab = 1;
            maxRange = 800;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4",
                    "Aux501_cows_LEScope_DC15A"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy="\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName="$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                compatibleItems[] = {"acc_flashlight","acc_pointer_IR"};
                iconPicture="\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint="Center";
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot"; 
            };
        };
    };
    class Aux501_Weaps_DC15A_UGL: Aux501_Weaps_DC15A
    {
        displayName="[501st] DC-15A UGL";
        baseWeapon="Aux501_Weaps_DC15A_UGL";
        model = "3AS\3AS_Weapons\DC15A\3AS_DC15A_GL.p3d";
        handAnim[] = {"OFP2_ManSkeleton","3AS\3AS_Weapons\DC15A\Data\Anim\DC15A_handanim.rtm"};
        reloadAction = "3AS_GestureReloadDC15A";
        muzzles[]=
        {
            "this",
            "Stun",
            "Aux501_UGL_F"
        };
        class Aux501_UGL_F: UGL_F
        {
            displayName = "[501st] Over-Under Grenade Launcher";
            descriptionShort = "Underbarrel GL Module for DC15A";
            useModelOptics = 0;
            useExternalOptic = 0;
            cameraDir = "OP_look";
            discreteDistance[] = {75,100,150,200,250,300,350,400};
            discreteDistanceCameraPoint[] = {"OP_eye_75","OP_eye_100","OP_eye_150","OP_eye_200","OP_eye_250","OP_eye_300","OP_eye_350","OP_eye_400"};
            discreteDistanceInitIndex = 0;
            magazines[] = 
            {
                "Aux501_Weapons_Mags_GL_AP2",
                "Aux501_Weapons_Mags_GL_HE3",
                "Aux501_Weapons_Mags_GL_smoke_white6",
                "Aux501_Weapons_Mags_GL_smoke_purple3",
                "Aux501_Weapons_Mags_GL_smoke_yellow3",
                "Aux501_Weapons_Mags_GL_smoke_red3",
                "Aux501_Weapons_Mags_GL_smoke_green3",
                "Aux501_Weapons_Mags_GL_smoke_blue3",
                "Aux501_Weapons_Mags_GL_smoke_orange3",
                "ACE_HuntIR_M203",
                "Aux501_Weapons_Mags_GL_flare_White3",
                "Aux501_Weapons_Mags_GL_flare_Green3",
                "Aux501_Weapons_Mags_GL_flare_Red3",
                "Aux501_Weapons_Mags_GL_flare_Yellow3",
                "Aux501_Weapons_Mags_GL_flare_Blue3",
                "Aux501_Weapons_Mags_GL_flare_Cyan3",
                "Aux501_Weapons_Mags_GL_flare_Purple3",
                "Aux501_Weapons_Mags_GL_flare_IR3"
            };
            magazineWell[] = {};
            reloadAction = "GestureReloadMXUGL";
            reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons\Rifles\MX\Mx_UGL_reload",1,1,10};
            class Single: Mode_SemiAuto
            {
                sounds[] = {"StandardSound"};
                class BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    closure1[] = {};
                    closure2[] = {};
                    soundClosure[] = {};
                };
                class StandardSound: BaseSoundModeType
                {
                    weaponSoundEffect = "";
                    begin1[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin2[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    begin3[] = {"SWLW_clones\rifles\gl\sounds\gl",1,1,1800};
                    soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
                };
            };
        };
    };
};

class CfgMagazines
{
    class 1Rnd_HE_Grenade_shell;
    class Aux501_Weapons_Mags_20mw40;

    class Aux501_Weapons_Mags_20mwdp30: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 30Rnd 20MW DP Cell";
        displayNameShort = "30Rnd 20MW DP";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15a_dp.paa";
        count = 30;
        ammo = "Aux501_Weapons_Ammo_20mwdp";
        descriptionShort = "DC15A dense plasma magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };
    class Aux501_Weapons_Mags_20mwup30: Aux501_Weapons_Mags_20mwdp30
    {
        displayName = "[501st] 30Rnd 20MW UP Cell";
        displayNameShort = "30Rnd 20MW UP";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15a_up.paa";
        count = 30;
        ammo = "Aux501_Weapons_Ammo_20mwup";
        initSpeed = 900;
        descriptionShort = "DC15A unstable plasma magazine";
        model = "\MRC\JLTS\weapons\DC15A\DC15A_mag.p3d";
    };

    //UGL Magazines
    class Aux501_Weapons_Mags_GL_AP2: 1Rnd_HE_Grenade_shell
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "[501st] 2Rnd AP Grenade";
        displayNameShort = "2Rnd AP";
        model = "\SWLW_clones\machineguns\Z6\Z6_g_mag.p3d";
        picture = "\SWLW_clones\machineguns\Z6\data\ui\Z6_g_mag_ui.paa";
        ammo = "Aux501_Weapons_Ammo_GL_AP";
        initSpeed = 220;
        count = 2;
        nameSound = "";
        descriptionShort = "2Rnd AP Grenade";
        mass = 15;
    };
    class Aux501_Weapons_Mags_GL_HE3: Aux501_Weapons_Mags_GL_AP2
    {
        displayName = "[501st] 3Rnd HE Grenade";
        displayNameShort = "3Rnd HE";
        count = 3;
        initSpeed = 80;
        ammo = "Aux501_Weapons_Ammo_GL_HE";
        descriptionShort = "3Rnd HE DC-15A Grenade";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_20mw;
    
    class Aux501_Weapons_Ammo_20mwdp: Aux501_Weapons_Ammo_20mw
    {
        hit = 24;
    };
    class Aux501_Weapons_Ammo_20mwup: Aux501_Weapons_Ammo_20mw
    {
        hit = 20;
        typicalSpeed = 900;
        indirectHit = 3;
        indirectHitRange = 0.5;
        explosive = 0.4;
        caliber = 0.6;
        ExplosionEffects = "3AS_ImpactPlasma";
        craterEffects = "";
    };
};