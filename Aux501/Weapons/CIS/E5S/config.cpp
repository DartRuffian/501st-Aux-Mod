class cfgPatches
{
    class Aux501_Patch_E5S
    {
        url = "https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod";
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_E5S"
        };
    };
};

class CowsSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_E5S : Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] E5S";
        baseWeapon = "Aux501_Weaps_E5S";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_ui_ca.paa";
        model = "\MRC\JLTS\weapons\E5S\E5S.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E5S\anims\E5S_handanim.rtm"};
        recoil = "recoil_dmr_01";
        recoilProne = "recoil_single_prone_mx";
        magazines[] =
        {
            "Aux501_Weapons_Mags_E5S15"
        };
        modes[] = {"Single"};
        class Single : Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound : BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire",+3db,1,2200};
                soundBegin[] = {"begin1", 1};
            };
            reloadTime = 1.2;
            dispersion = 0.0003;
            minRange = 100;
            minRangeProbab = 0.2;
            midRange = 550;
            midRangeProbab = 0.7;
            maxRange = 800;
            maxRangeProbab = 0.05;
        };
        modelOptics = "\MRC\JLTS\weapons\E5S\E5S_reticle.p3d";
        class OpticsModes
        {
            class Snip
            {
                cameraDir = "";
                discreteDistance[] = {100};
                discreteDistanceInitIndex = 0;
                discretefov[] = {0.045,0.011};
                discreteInitIndex = 0;
                distanceZoomMax = 2400;
                distanceZoomMin = 300;
                memoryPointCamera = "opticView";
                modelOptics[] = {"\MRC\JLTS\weapons\E5S\E5S_reticle.p3d"};
                opticsDisablePeripherialVision = 1;
                opticsDisplayName = "WFOV";
                opticsFlare = 1;
                opticsID = 1;
                opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
                opticsZoomInit = 0.045;
                opticsZoomMax = 0.045;
                opticsZoomMin = 0.011;
                useModelOptics = 1;
                visionMode[] = {"Normal","NVG","TI"};
            };
            class Iron: Snip
            {
                opticsID = 2;
                useModelOptics = 0;
                opticsPPEffects[] = {"",""};
                opticsFlare = 0;
                opticsDisablePeripherialVision = 0;
                discreteDistance[] = {200};
                discreteDistanceInitIndex = 0;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.25;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
            };
        };
        class WeaponSlotsInfo : WeaponSlotsInfo
        {
            class CowsSlot : CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = {};
            };
        };
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;

    class Aux501_Weapons_Ammo_E5S_Blaster: Aux501_Weapons_Ammo_base_red
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
        model = "SWLW_main\Effects\laser_red.p3d";
        tracerscale = 1.5;
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_30mw10;

    class Aux501_Weapons_Mags_E5S15: Aux501_Weapons_Mags_30mw10
    {
        displayName = "[501st] E5S Blaster Cell";
        displayNameShort = "15Rnd 30MW";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_mag_ui_ca.paa";
        count = 15;
        ammo = "Aux501_Weapons_Ammo_E5S_Blaster";
        descriptionShort = "E5S High Power Magazine";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
    };
};     