class cfgPatches
{
    class Aux501_Patch_SBB3
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_SBB3"
        };
    };
};

class Mode_FullAuto;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
    };

    class Aux501_Weaps_SBB3: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName="[501st] SBB3";
        baseWeapon="Aux501_Weaps_SBB3";
        picture = "\MRC\JLTS\weapons\SBB3\data\ui\SBB3_ui_ca.paa";
        model = "\MRC\JLTS\weapons\SBB3\SBB3.p3d";
        hiddenSelections[] = {"camo1","illum"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\SBB3\data\SBB3_co.paa"};
        hiddenSelectionsMaterials[] = {"","\a3\characters_f_bootcamp\common\data\vrarmoremmisive.rvmat"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\SBB3\anims\SBB3_handanim.rtm"};
        recoil = "JLTS_recoil_SBB3";
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        magazines[]=
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25"
        };
        modes[] = {"FullAuto","aiclose","aimedium"};
        class FullAuto: Mode_FullAuto
        {
            reloadTime = 0.25;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\MRC\JLTS\weapons\SBB3\sounds\SBB3_fire.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
            dispersion = 0.001;
            minRange = 1;
            minRangeProbab = 0.1;
            midRange = 10;
            midRangeProbab = 0.8;
            maxRange = 50;
            maxRangeProbab = 0.15;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 20;

        };
        class aiclose: FullAuto
        {
            showToPlayer = 0;
            minRange = 50;
            minRangeProbab = 0.1;
            midRange = 130;
            midRangeProbab = 0.8;
            maxRange = 200;
            maxRangeProbab = 0.15;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 200;
        };
        class aimedium: aiclose
        {
            minRange = 150;
            minRangeProbab = 0.1;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.15;
            aiRateOfFire = 4;
            aiRateOfFireDistance = 300;
        };
    };
};

class CfgMagazines
{
    class 30Rnd_65x39_caseless_mag;

    class Aux501_Weapons_Mags_SBB3_shotgun25: 30Rnd_65x39_caseless_mag
    {
        displayName = "[501st] 25Rnd SBB3 Shotgun Cell";
        displayNameShort = "25Rnd SBB3 Cell";
        descriptionShort = "CIS Shotgun Cell";
        author = "501st Aux Team";
        modelSpecial = "";
        modelSpecialIsProxy = 0;
        picture = "\MRC\JLTS\weapons\Core\data\ui\stun_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\Core\stun_mag.p3d";
        count = 25;
        ammo = "Aux501_Weapons_Ammo_SBB3_Shotgun";
        tracersEvery = 1;
    };
};

class CfgAmmo
{
    class BulletBase;
    class B_9x21_Ball: BulletBase
    {
        class HitEffects;
    };

    class Aux501_Weapons_Ammo_SBB3_Shotgun: B_9x21_Ball
    {
        author = "501st Aux Team";
        model = "SWLW_main\Effects\laser_red.p3d";
        simulation = "shotSpread";
        fireSpreadAngle = 3;
        cartridge = "";
        lightcolor[] = {0.5,0.25,0.25};
        flaresize = 2;
        tracerscale = 0.5;
        effectflare = "FlareShell";
        tracerstarttime = 0.05;
        tracerendtime = 10;
        initTime = 0;
        nvgonly = 0;
        airlock = 1;
        irtarget = 1;
        effectfly = "JLTS_plasma_red";
        brightness = 1000;
        timetolive = 3;
        airfriction = 0;
        coefgravity = 0;
        deflecting = 0;
        bulletFly1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_flyby\plasma_flyby_1",2.2387211,1,100};
        bulletFly2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_flyby\plasma_flyby_2",2.2387211,1,100};
        bulletFly3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_flyby\plasma_flyby_3",2.2387211,1,100};
        bulletFly4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_flyby\plasma_flyby_4",2.2387211,1,100};
        bulletFly5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_flyby\plasma_flyby_5",2.2387211,1,100};
        bulletFly[] = {"bulletFly1",0.2,"bulletFly2",0.2,"bulletFly3",0.2,"bulletFly4",0.2,"bulletFly5",0.2};
        hitArmor[] = {"soundMetal1",0.1,"soundMetal2",0.1,"soundMetal3",0.1,"soundMetal4",0.1,"soundMetal5",0.1,"soundMetal6",0.1,"soundMetal7",0.1,"soundMetal8",0.1,"soundMetal9",0.1,"soundMetal10",0.1};
        hitArmorInt[] = {"soundMetal1",0.1,"soundMetal2",0.1,"soundMetal3",0.1,"soundMetal4",0.1,"soundMetal5",0.1,"soundMetal6",0.1,"soundMetal7",0.1,"soundMetal8",0.1,"soundMetal9",0.1,"soundMetal10",0.1};
        soundHitBody1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_1",2,1,75};
        soundHitBody2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_2",2,1,75};
        soundHitBody3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_3",2,1,75};
        soundHitBody4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_4",2,1,75};
        soundHitBody5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_5",2,1,75};
        soundHitBody6[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_6",2,1,75};
        soundHitBody7[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_7",2,1,75};
        soundHitBody8[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_8",2,1,75};
        soundHitBody9[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_9",2,1,75};
        soundHitBody10[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\body\body_10",2,1,75};
        hitMan[] = {"soundHitBody1",0.1,"soundHitBody2",0.1,"soundHitBody3",0.1,"soundHitBody4",0.1,"soundHitBody5",0.1,"soundHitBody6",0.1,"soundHitBody7",0.1,"soundHitBody8",0.1,"soundHitBody9",0.1,"soundHitBody10",0.1};
        hitBuilding[] = {"soundConcrete1",0.125,"soundConcrete2",0.125,"soundConcrete3",0.125,"soundConcrete4",0.125,"soundConcrete5",0.125,"soundConcrete6",0.125,"soundConcrete7",0.125,"soundConcrete8",0.125};
        soundDefault1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_1",2,1,75};
        soundDefault2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_2",2,1,75};
        soundDefault3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_3",2,1,75};
        soundDefault4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_4",2,1,75};
        soundDefault5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_5",2,1,75};
        soundDefault6[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_6",2,1,75};
        soundDefault7[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_7",2,1,75};
        soundDefault8[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\default\default_8",2,1,75};
        hitDefault[] = {"soundDefault1",0.125,"soundDefault2",0.125,"soundDefault3",0.125,"soundDefault4",0.125,"soundDefault5",0.125,"soundDefault6",0.125,"soundDefault7",0.125,"soundDefault8",0.125};
        soundHit1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\dirt\dirt_1",2,1,75};
        soundHit2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\dirt\dirt_2",2,1,75};
        soundHit3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\dirt\dirt_3",2,1,75};
        soundHit4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\dirt\dirt_4",2,1,75};
        soundHit5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\dirt\dirt_5",2,1,75};
        hitGround[] = {"soundHit1",0.2,"soundHit2",0.2,"soundHit3",0.2,"soundHit4",0.2,"soundHit5",0.2};
        hitGroundHard[] = {"soundHit1",0.2,"soundHit2",0.2,"soundHit3",0.2,"soundHit4",0.2,"soundHit5",0.2};
        hitGroundSoft[] = {"soundHit1",0.2,"soundHit2",0.2,"soundHit3",0.2,"soundHit4",0.2,"soundHit5",0.2};
        hitFoliage[] = {"soundHitBody1",0.1,"soundHitBody2",0.1,"soundHitBody3",0.1,"soundHitBody4",0.1,"soundHitBody5",0.1,"soundHitBody6",0.1,"soundHitBody7",0.1,"soundHitBody8",0.1,"soundHitBody9",0.1,"soundHitBody10",0.1};
        hitIron[] = {"soundMetal1",0.1,"soundMetal2",0.1,"soundMetal3",0.1,"soundMetal4",0.1,"soundMetal5",0.1,"soundMetal6",0.1,"soundMetal7",0.1,"soundMetal8",0.1,"soundMetal9",0.1,"soundMetal10",0.1};
        soundMetal1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_1",2,1,75};
        soundMetal2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_2",2,1,75};
        soundMetal3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_3",2,1,75};
        soundMetal4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_4",2,1,75};
        soundMetal5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_5",2,1,75};
        soundMetal6[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_6",2,1,75};
        soundMetal7[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_7",2,1,75};
        soundMetal8[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_8",2,1,75};
        soundMetal9[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_9",2,1,75};
        soundMetal10[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\metal\metal_10",2,1,75};
        hitMetal[] = {"soundMetal1",0.1,"soundMetal2",0.1,"soundMetal3",0.1,"soundMetal4",0.1,"soundMetal5",0.1,"soundMetal6",0.1,"soundMetal7",0.1,"soundMetal8",0.1,"soundMetal9",0.1,"soundMetal10",0.1};
        hitMetalInt[] = {"soundMetal1",0.1,"soundMetal2",0.1,"soundMetal3",0.1,"soundMetal4",0.1,"soundMetal5",0.1,"soundMetal6",0.1,"soundMetal7",0.1,"soundMetal8",0.1,"soundMetal9",0.1,"soundMetal10",0.1};
        hitMetalPlate[] = {"soundMetal1",0.1,"soundMetal2",0.1,"soundMetal3",0.1,"soundMetal4",0.1,"soundMetal5",0.1,"soundMetal6",0.1,"soundMetal7",0.1,"soundMetal8",0.1,"soundMetal9",0.1,"soundMetal10",0.1};
        hitPlastic[] = {"soundDefault1",0.125,"soundDefault2",0.125,"soundDefault3",0.125,"soundDefault4",0.125,"soundDefault5",0.125,"soundDefault6",0.125,"soundDefault7",0.125,"soundDefault8",0.125};
        hitRubber[] = {"soundDefault1",0.125,"soundDefault2",0.125,"soundDefault3",0.125,"soundDefault4",0.125,"soundDefault5",0.125,"soundDefault6",0.125,"soundDefault7",0.125,"soundDefault8",0.125};
        soundConcrete1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_1",2,1,75};
        soundConcrete2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_2",2,1,75};
        soundConcrete3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_3",2,1,75};
        soundConcrete4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_4",2,1,75};
        soundConcrete5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_5",2,1,75};
        soundConcrete6[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_6",2,1,75};
        soundConcrete7[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_7",2,1,75};
        soundConcrete8[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\stone\stone_8",2,1,75};
        hitConcrete[] = {"soundConcrete1",0.125,"soundConcrete2",0.125,"soundConcrete3",0.125,"soundConcrete4",0.125,"soundConcrete5",0.125,"soundConcrete6",0.125,"soundConcrete7",0.125,"soundConcrete8",0.125};
        hitTyre[] = {"soundDefault1",0.125,"soundDefault2",0.125,"soundDefault3",0.125,"soundDefault4",0.125,"soundDefault5",0.125,"soundDefault6",0.125,"soundDefault7",0.125,"soundDefault8",0.125};
        soundWater1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\water\water_1",2,1,75};
        soundWater2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\water\water_2",2,1,75};
        soundWater3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\water\water_3",2,1,75};
        hitWater[] = {"soundWater1",0.33,"soundWater2",0.33,"soundWater3",0.33};
        soundWood1[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_1",2,1,75};
        soundWood2[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_2",2,1,75};
        soundWood3[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_3",2,1,75};
        soundWood4[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_4",2,1,75};
        soundWood5[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_5",2,1,75};
        soundWood6[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_6",2,1,75};
        soundWood7[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_7",2,1,75};
        soundWood8[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_8",2,1,75};
        soundWood9[] = {"MRC\JLTS\weapons\Core\sounds\plasma_hit\wood\wood_9",2,1,75};
        hitWood[] = {"soundWood1",0.11,"soundWood2",0.11,"soundWood3",0.11,"soundWood4",0.11,"soundWood5",0.11,"soundWood6",0.11,"soundWood7",0.11,"soundWood8",0.11,"soundWood9",0.11};
        supersoniccracknear[] = {};
        supersoniccrackfar[] = {};
        soundSetSonicCrack[] = {};
        soundSetBulletFly[] = {"JLTS_plasma_bullet_flyby_soundSet"};
        class HitEffects: HitEffects
        {
            Hit_Foliage_green = "ImpactLeavesGreen";
            Hit_Foliage_Dead = "ImpactLeavesDead";
            Hit_Foliage_Green_big = "ImpactLeavesGreenBig";
            Hit_Foliage_Palm = "ImpactLeavesPalm";
            Hit_Foliage_Pine = "ImpactLeavesPine";
            hitFoliage = "ImpactLeaves";
            hitGlass = "JLTS_ImpactPlasma";
            hitGlassArmored = "JLTS_ImpactPlasma";
            hitWood = "JLTS_ImpactPlasma";
            hitMetal = "JLTS_ImpactPlasma";
            hitMetalPlate = "JLTS_ImpactPlasma";
            hitBuilding = "JLTS_ImpactPlasma";
            hitPlastic = "JLTS_ImpactPlasma";
            hitRubber = "JLTS_ImpactPlasma";
            hitTyre = "JLTS_ImpactPlasma";
            hitConcrete = "JLTS_ImpactPlasma";
            hitMan = "ImpactEffectsBlood";
            hitGroundSoft = "ImpactEffectsSmall";
            hitGroundRed = "ImpactEffectsRed";
            hitGroundHard = "ImpactEffectsHardGround";
            hitWater = "ImpactEffectsWater";
            hitVirtual = "ImpactMetal";
            default_mat = "ImpactMetal";
            hitHay = "ImpactHay";
        };
    };
};
