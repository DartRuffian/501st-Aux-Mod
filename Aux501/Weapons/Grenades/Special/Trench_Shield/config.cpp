class cfgPatches
{
    class Aux501_Patch_grenades_special_trench_shield
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F",
            "A3_Structures_F_Households_House_Small03"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Grenades_Trench_Shield"
        };
    };
};

class CfgVehicles
{
    class Land_House_Small_03_V1_ruins_F;

    class Aux501_Weapons_Grenades_Special_Object_Trench_Shield: Land_House_Small_03_V1_ruins_F
    {
        author = "501st Aux Team";
        scope = 2;
        scopeCurator = 2;
        displayName = "Trench Shield";
        mapSize = 21.1;
        class SimpleObject
        {
            eden = 0;
            animate[] = {};
            hide[] = {};
            verticalOffset = -0.023;
            verticalOffsetWorld = 0;
            init = "''";
        };
        aux501_fired_deployable_loopSound = "Aux501_Sound_Grenade_Shield_Loop";
        aux501_fired_deployable_loopDuration = 14;
        aux501_fired_deployable_endSound = "Aux501_Sound_Grenade_Shield_end";
        aux501_fired_deployable_endDuration = 1;
        aux501_fired_deployable_soundDistance = 300;
        editorPreview = "\A3\EditorPreviews_F\Data\CfgVehicles\Land_House_Small_03_V1_ruins_F.jpg";
        model = "\Aux501\Weapons\Grenades\Special\Trench_Shield\Trench_Shield.p3d";
        icon = "iconObject_1x1";
        //vehicleClass = "RD501_Vehicle_Class_statics";
        //editorCategory = "RD501_Editor_Category_statics";
        //editorSubcategory = "RD501_Editor_Category_static_msc";
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Grenades_Squad_Shield;
    
    class Aux501_Weapons_Mags_Grenades_Trench_Shield: Aux501_Weapons_Mags_Grenades_Squad_Shield
    {
        displayName = "[501st] Trench Shield";
        displayNameShort = "Trench Shield";
        model = "";
        ammo = "Aux501_Weapons_Ammo_Grenades_Trench_Shield";
        descriptionShort = "Trench Shield";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_Grenades_Squad_Shield;

    class Aux501_Weapons_Ammo_Grenades_Trench_Shield: Aux501_Weapons_Ammo_Grenades_Squad_Shield
    {
        Aux501_fired_deployable_object = "Aux501_Weapons_Grenades_Special_Object_Trench_Shield";
        Aux501_fired_deployable_timeToLive = 120;
        model = "";
    };
};