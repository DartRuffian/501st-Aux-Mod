class cfgPatches
{
    class Aux501_Patch_grenades_offensive
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_ThermalDetonator",
            "Aux501_Weapons_Mags_ThermalImploder",
            "Aux501_Weapons_Mags_ThermalImpacter",
            "Aux501_Weapons_Mags_Ctype",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_flashnade"
        };
    };
};

class CfgMagazines
{
    class HandGrenade;
    class ACE_M84;
    
    class Aux501_Weapons_Mags_Thermal_Detonator: HandGrenade
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "[501st] Thermal Detonator";
        displayNameShort = "Thermal Detonator";
        descriptionShort = "Clone Frag Grenade";
        type = 256;
        value = 1;
        count = 1;
        initSpeed = 18;
        mass = 8;
        nameSound = "handgrenade";
        maxLeadSpeed = 7;
        ammo = "Aux501_Weapons_Ammo_Grenades_Detonator";
        model = "\kobra\442_weapons\explosive\thermal_det.p3d";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_thermal_det_ui.paa";
    };
    class Aux501_Weapons_Mags_Thermal_Imploder: Aux501_Weapons_Mags_Thermal_Detonator
    {
        displayName = "[501st] Thermal Imploder";
        displayNameShort = "Thermal Imploder";
        descriptionShort = "Clone Grenade";
        picture = "\SWLW_clones\grenades\beltgrenade\data\ui\icon_beltGrenade.paa";
        model = "\ls_weapons\grenades\thermalDetN20\ls_grenade_thermalDet_n20";
        ammo = "Aux501_Weapons_Ammo_Grenades_Imploder";
    };
    class Aux501_Weapons_Mags_Thermal_Impacter: Aux501_Weapons_Mags_Thermal_Detonator
    {
        displayName = "[501st] Thermal Impacter";
        displayNameShort = "Thermal Impacter";
        descriptionShort = "Clone Impact Grenade";
        ammo = "Aux501_Weapons_Ammo_Grenades_Impacter";
        model = "\kobra\442_weapons\explosive\impact_grenade.p3d";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_thermal_impacter_ui.paa";
    };
    class Aux501_Weapons_Mags_Ctype: Aux501_Weapons_Mags_Thermal_Detonator
    {
        displayName = "[501st] Throwable C-Type";
        displayNameShort = "Throwable C-Type";
        descriptionShort = "Clone Satchel Charge";
        type = "2*  256";
        value = 5;
        mass = 80;
        initSpeed = 12;
        nameSoundWeapon = "satchelcharge";
        nameSound = "satchelcharge";
        sound[] = {"A3\sounds_f\dummysound",0.00031622776,1,10};
        ammo = "Aux501_Weapons_Ammo_Grenades_Ctype";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_dtype_ui.paa";
        uiPicture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_dtype_ui.paa";
        model = "ls_weapons\explosives\detPack\ls_explosives_detpack";
        //model = "\ls_weapons\explosives\demo\ls_explosives_demoPack";
    };
    class Aux501_Weapons_Mags_CISDetonator: Aux501_Weapons_Mags_Thermal_Detonator
    {
        displayName = "[501st] CIS Detonator";
        displayNameShort = "CIS Detonator";
        descriptionShort = "Droid Frag Grenade";
        ammo = "Aux501_Weapons_Ammo_Grenades_CISDetonator";
        model = "ls_weapons\grenades\thermalDet\ls_grenade_thermalDet";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_cis_nade_thermal_det_ui.paa";
    };
    class Aux501_Weapons_Mags_flashnade: ACE_M84
    {
        author = "501st Aux Team";
        displayName = "[501st] C-25 Flash Grenade";
        displayNameShort = "Flash Grenade";
        descriptionShort = "Clone Wars Flashbang";
        model = "\MRC\JLTS\weapons\Grenades\grenade_emp.p3d";
        ammo = "Aux501_Weapons_Ammo_Grenades_flash";
        picture = "\MRC\JLTS\weapons\grenades\data\ui\grenade_emp_ui_ca.paa";
    };
};

class CfgAmmo
{
    class grenade;
    class ACE_G_M84;

    class Aux501_Weapons_Ammo_Grenades_Detonator: grenade
    {
        hit = 18;
        indirectHit = 14;
        indirectHitRange = 8;
        ace_grenades_pullPinSound[] = {"\ls_sounds\weapons\grenade\pin.wss",3,1,10};
        model = "\kobra\442_weapons\explosive\thermal_det.p3d";
        explosionEffectsRadius = 1.5;
        suppressionRadiusHit = 24;
        typicalspeed = 18;
        visibleFire = 0.5;
        audibleFire = 0.05;
        visibleFireTime = 1;
        fuseDistance = 0;
        soundFly[] = {"\ls_sounds\weapons\grenade\thermalDet_classC_fuse.wss",20,1,200};
        SoundSetExplosion[] = {"Aux501_soundset_rep_thermal_detonator_exp","GrenadeHe_Tail_SoundSet","Explosion_Debris_SoundSet"};
        class CamShakeExplode
        {
            distance = 99.8178;
            duration = 1;
            frequency = 20;
            power = 6;
        };
        class NVGMarkers
        {
            class Blinking1
            {
                name = "blinkpos1";
                color[] = {0.01,0.01,0.01,1};
                ambient[] = {0.005,0.005,0.005,1};
                blinking = 1;
                brightness = 0.002;
                onlyInNvg = 1;
            };
            class Blinking2
            {
                color[] = {0.9,0.1,0.1};
                ambient[] = {0.1,0.1,0.1};
                name = "blinkpos2";
                blinking = 1;
                blinkingStartsOn = 1;
                blinkingPattern[] = {0.1,0.9};
                blinkingPatternGuarantee = 1;
                drawLightSize = 0.35;
                drawLightCenterSize = 0.05;
                brightness = 0.002;
                dayLight = 1;
                onlyInNvg = 0;
                intensity = 75;
                drawLight = 1;
                activeLight = 0;
                useFlare = 0;
            };
        };
    };
    class Aux501_Weapons_Ammo_Grenades_Imploder: grenade
    {
        ace_advanced_throwing_torqueDirection[] = {1,1,0};
        ace_advanced_throwing_torqueMagnitude = "(50 + random 100) * selectRandom [1, -1]";
        hit = 9;
        indirectHit= 40;
        indirectHitRange = 2;
        simulation = "shotGrenade";
        soundFly[] = {"\Aux501\Weapons\Grenades\data\sounds\imploder_flyby.wss",1.5,1,90};
        SoundSetExplosion[] = {"Aux501_soundset_thermal_imploder_exp"};
        ace_frag_enabled = 1;
        ace_frag_skip = 0;
        ace_frag_force = 1;
        ace_frag_classes[] = {"ace_frag_tiny_HD"};
        ace_frag_metal = 210;
        ace_frag_charge = 185;
        ace_frag_gurney_c = 2843;
        ace_frag_gurney_k = "3/5";
        model = "\SWLW_clones\grenades\beltgrenade\beltGrenade.p3d";
    };
    class Aux501_Weapons_Ammo_Grenades_Impacter: Aux501_Weapons_Ammo_Grenades_Detonator
    {
        hit = 50;
        indirectHit = 45;
        indirectHitRange = 2;
        fuseDistance = 0.5;
        explosionTime = 0;
        simulation = "shotShell";
        model = "\kobra\442_weapons\explosive\impact_grenade.p3d";
        SoundSetExplosion[] = {"GrenadeHe_Exp_SoundSet","GrenadeHe_Tail_SoundSet","Explosion_Debris_SoundSet"};
    };
    class Aux501_Weapons_Ammo_Grenades_Ctype: Aux501_Weapons_Ammo_Grenades_Detonator
    {
        hit=1500;
        indirectHit=1500;
        indirectHitRange=10;
        typicalspeed=40;
        explosionTime=10;
        timeToLive=20;
        model = "ls_weapons\explosives\detPack\ls_explosives_detpack";
        simulation = "shotGrenade";
        soundFly[] = {"3AS\3AS_Equipment\SFX\td.ogg",20,1,200};
        SoundSetExplosion[] = {"Aux501_soundset_ctype_exp"};
        ExplosionEffects = "MineNondirectionalExplosion";
        CraterEffects = "MineNondirectionalCrater";
    };
    class Aux501_Weapons_Ammo_Grenades_CISDetonator: Aux501_Weapons_Ammo_Grenades_Detonator
    {
        hit=10;
        indirectHit=8;
        indirectHitRange=6;
        cost=80;
        model = "\ls_weapons\grenades\thermalDet\ls_grenade_thermaldet";
        soundFly[] = {"\kobra\442_weapons\sounds\thermal_det\thermal_det.wss",1.5,1,90};
        SoundSetExplosion[] = {"Aux501_soundset_cis_thermal_detonator_exp","GrenadeHe_Tail_SoundSet","Explosion_Debris_SoundSet"};
    };
    class Aux501_Weapons_Ammo_Grenades_flash: ACE_G_M84
    {
        timeToLive = 4.5;
        simulation = "shotSmokeX";
        model = "\MRC\JLTS\weapons\Grenades\grenade_emp.p3d";
        SoundSetExplosion[] = {"Aux501_soundset_flash_stun_exp","GrenadeHe_Tail_SoundSet","Explosion_Debris_SoundSet"};
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_rep_thermaldetonator_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\rep_thermal_det_explode.wss",1}
        };
        volume = 1.0;
        range = 2200;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_rep_thermaldetonator_midExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\rep_thermal_det_explode.wss",1}
        };
        volume = 1.0;
        range = 2200;
        rangeCurve[] = {{0,1},{100,1},{500,0},{2200,0}};
    };
    class Aux501_SoundShader_cis_thermaldetonator_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\cis_thermal_det_explode.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_cis_thermaldetonator_midExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\cis_thermal_det_explode.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{100,1},{500,0},{2200,0}};
    };
    class Aux501_SoundShader_thermalimploder_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\thermal_imploder_explosion.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_ctype_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\throwable_ctype_explosion.wss",1}
        };
        volume = 2.0;
        range = 90;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_ctype_midExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\throwable_ctype_explosion.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{100,1},{500,0},{2200,0}};
    };
    class Aux501_SoundShader_flash_stun_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\thermal_stunner_explosion.wss",1}
        };
        volume = 2.0;
        range = 90;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_flash_stun_midExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\thermal_stunner_explosion.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{100,1},{500,0},{2200,0}};
    };
};

class CfgSoundSets
{
    class Aux501_soundset_rep_thermal_detonator_exp
    {
        soundShaders[] = {"Aux501_SoundShader_rep_thermaldetonator_closeExp","Aux501_SoundShader_rep_thermaldetonator_midExp"};
        volumeFactor = 1.0;
        volumeCurve = "InverseSquare2Curve";
        spatial = 1;
        doppler = 0;
        loop = 0;
        sound3DProcessingType = "ExplosionLight3DProcessingType";
        distanceFilter = "explosionDistanceFreqAttenuationFilter";
    };
    class Aux501_soundset_cis_thermal_detonator_exp: Aux501_soundset_rep_thermal_detonator_exp
    {
        soundShaders[] = {"Aux501_SoundShader_cis_thermaldetonator_closeExp","Aux501_SoundShader_cis_thermaldetonator_midExp"};
    };
    class Aux501_soundset_thermal_imploder_exp: Aux501_soundset_rep_thermal_detonator_exp
    {
        soundShaders[] = {"Aux501_SoundShader_thermalimploder_closeExp"};
    };
    class Aux501_soundset_ctype_exp: Aux501_soundset_rep_thermal_detonator_exp
    {
        soundShaders[] = {"Aux501_SoundShader_ctype_closeExp","Aux501_SoundShader_ctype_midExp"};
    };
    class Aux501_soundset_flash_stun_exp: Aux501_soundset_rep_thermal_detonator_exp
    {
        soundShaders[] = {"Aux501_SoundShader_flash_stun_closeExp","Aux501_SoundShader_flash_stun_midExp"};
    };
};