class cfgPatches
{
    class Aux501_Patch_grenades_smokes
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_Smoke_Purple",
            "Aux501_Weapons_Mags_Smoke_Yellow",
            "Aux501_Weapons_Mags_Smoke_Red",
            "Aux501_Weapons_Mags_Smoke_Green"
            "Aux501_Weapons_Mags_Smoke_Blue",
            "Aux501_Weapons_Mags_Smoke_Orange",
            "Aux501_Weapons_Mags_Smoke_MASH"
        };
    };
};

class CfgMagazines
{
    class SmokeShell;
    
    class Aux501_Weapons_Mags_Smoke_White: SmokeShell
    {
        scope = 2;
        author = "501st Aux Team";
        displayName = "[501st] Smoke Grenade (White)";
        displayNameShort = "White Smoke";
        descriptionShort = "Clone White Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_white";
        model = "\kobra\442_weapons\explosive\basic_smoke.p3d";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_white_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_Purple: Aux501_Weapons_Mags_Smoke_White
    {
        displayName = "[501st] Smoke Grenade (Purple)";
        displayNameShort = "Purple Smoke";
        descriptionShort = "Clone Purple Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_purple";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_purple_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_Yellow: Aux501_Weapons_Mags_Smoke_White
    {
        displayName = "[501st] Smoke Grenade (Yellow)";
        displayNameShort = "Yellow Smoke";
        descriptionShort = "Clone Yellow Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_yellow";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_yellow_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_Red: Aux501_Weapons_Mags_Smoke_White
    {
        displayName = "[501st] Smoke Grenade (Red)";
        displayNameShort = "Red Smoke";
        descriptionShort = "Clone Red Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_red";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_red_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_Green: Aux501_Weapons_Mags_Smoke_White
    {
        displayName = "[501st] Smoke Grenade (Green)";
        displayNameShort = "Green Smoke";
        descriptionShort = "Clone Green Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_green";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_green_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_Blue: Aux501_Weapons_Mags_Smoke_White
    {
        displayName = "[501st] Smoke Grenade (Blue)";
        displayNameShort = "Blue Smoke";
        descriptionShort = "Clone Blue Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_blue";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_blue_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_Orange: Aux501_Weapons_Mags_Smoke_White
    {
        displayName = "[501st] Smoke Grenade (Orange)";
        displayNameShort = "Orange Smoke";
        descriptionShort = "Clone Orange Smoke Grenade";
        ammo = "Aux501_Weapons_Ammo_smoke_orange";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_orange_ui.paa";
    };
    class Aux501_Weapons_Mags_Smoke_MASH: Aux501_Weapons_Mags_Smoke_Orange
    {
        displayName = "[501st] Smoke Grenade (MASH)";
        displayNameShort = "MASH Beacon";
        ammo = "Aux501_Weapons_Ammo_smoke_MASH";
        picture = "\Aux501\Weapons\Grenades\data\UI\Aux501_icon_mag_rep_nade_smoke_mash_ui.paa";
        rd501_fired_script_enabled = 1;
		rd501_fired_script = "_this call RD501_fnc_internal_CRML_launchMissileFromMashGrenade;";
    };
};

class CfgAmmo
{
    class SmokeShell;
    class SmokeShellOrange;

    class Aux501_Weapons_Ammo_smoke_white: SmokeShell
    {
        simulation = "shotSmokeX";
        smokeColor[] = {1,1,1,1};
        effectsSmoke = "Aux501_particle_effect_SmokeShellWhite";
        model = "kobra\442_weapons\explosive\basic_smoke.p3d";
        SmokeShellSoundHit1[] = {"\Aux501\Weapons\Grenades\data\sounds\smoke_explode.wss",+10db,1,500};
        SmokeShellSoundLoop1[] = {"\Aux501\Weapons\Grenades\data\sounds\smoke_loop.wss",+3db,1,150};
        grenadeFireSound[] = {"SmokeShellSoundHit1",1};
        grenadeBurningSound[] = {"SmokeShellSoundLoop1",1};
    };
    class Aux501_Weapons_Ammo_smoke_purple: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.4341,0.1388,0.41439998,1};
        effectsSmoke = "Aux501_particle_effect_SmokePurple";
    };
    class Aux501_Weapons_Ammo_smoke_yellow: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.9883,0.8606,0.0719,1};
        effectsSmoke = "Aux501_particle_effect_SmokeYellow";
    };
    class Aux501_Weapons_Ammo_smoke_red: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.8438,0.1383,0.1353,1};
        effectsSmoke = "Aux501_particle_effect_SmokeRed";
    };
    class Aux501_Weapons_Ammo_smoke_green: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.1383,0.8438,0.1353,1};
        effectsSmoke = "Aux501_particle_effect_SmokeGreen";
    };
    class Aux501_Weapons_Ammo_smoke_blue: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.1183,0.1867,1,1};
        effectsSmoke = "Aux501_particle_effect_SmokeBlue";
    };
    class Aux501_Weapons_Ammo_smoke_orange: Aux501_Weapons_Ammo_smoke_white
    {
        smokeColor[] = {0.9132,0.1763,0.007,0.8};
        effectsSmoke = "SmokeShellOrangeEffect";
    };
    class Aux501_Weapons_Ammo_smoke_MASH: SmokeShellOrange 
    {
        model = "\kobra\442_weapons\explosive\basic_smoke.p3d";
        rd501_fired_script_enabled = 1;
        rd501_fired_script = "_this call RD501_fnc_internal_CRML_launchMissileFromMashGrenade;";
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_smokenade_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\smoke_explode.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{50,0.75},{70,0}};
    };
    class Aux501_SoundShader_smokenade_midExp
    {
        samples[] = 
        {
            {"\Aux501\Weapons\Grenades\data\sounds\smoke_explode.wss",1}
        };
        volume = 2.0;
        range = 2200;
        rangeCurve[] = {{0,1},{100,1},{500,0},{2200,0}};
    };
};

class CfgSoundSets
{
    class Aux501_soundset_rep_thermal_detonator_exp;
    class Aux501_soundset_smokenade_exp: Aux501_soundset_rep_thermal_detonator_exp
    {
        soundShaders[] = {"Aux501_SoundShader_smokenade_closeExp","Aux501_SoundShader_smokenade_midExp"};
    };
};

class Aux501_particle_effect_SmokeShellWhite
{
    class SmokeShell
    {
        intensity = 20;
        interval = 10;
        position[] = {0,0,0};
        simulation = "particles";
        type = "Aux501_cloudlet_smokeshell_white";
    };
    class SmokeShell2: SmokeShell
    {
        type = "Aux501_cloudlet_smokeshell_white2";
    };
    class SmokeShell2UW: SmokeShell
    {
        type = "Aux501_cloudlet_SmokeShellWhite2UW";
    };
    class SmokeShellUW: SmokeShell
    {
        type = "Aux501_cloudlet_SmokeShellWhiteUW";
    };
};
class Aux501_particle_effect_SmokePurple: Aux501_particle_effect_SmokeShellWhite
{
    class SmokeShell: SmokeShell
    {
        type = "Aux501_cloudlet_smokeshell_purple";
    };
    class SmokeShell2: SmokeShell2
    {
        type = "Aux501_cloudlet_smokeshell_purple2";
    };
    class SmokeShell2UW: SmokeShell2UW
    {
        type = "Aux501_cloudlet_SmokeShellpurple2UW";
    };
    class SmokeShellUW: SmokeShellUW
    {
        type = "Aux501_cloudlet_SmokeShellpurpleUW";
    };
};
class Aux501_particle_effect_SmokeYellow: Aux501_particle_effect_SmokeShellWhite
{
    class SmokeShell: SmokeShell
    {
        type = "Aux501_cloudlet_smokeshell_yellow";
    };
    class SmokeShell2: SmokeShell2
    {
        type = "Aux501_cloudlet_smokeshell_yellow2";
    };
    class SmokeShell2UW: SmokeShell2UW
    {
        type = "Aux501_cloudlet_SmokeShellyellow2UW";
    };
    class SmokeShellUW: SmokeShellUW
    {
        type = "Aux501_cloudlet_SmokeShellyellowUW";
    };
};
class Aux501_particle_effect_SmokeRed: Aux501_particle_effect_SmokeShellWhite
{
    class SmokeShell: SmokeShell
    {
        type = "Aux501_cloudlet_smokeshell_red";
    };
    class SmokeShell2: SmokeShell2
    {
        type = "Aux501_cloudlet_smokeshell_red2";
    };
    class SmokeShell2UW: SmokeShell2UW
    {
        type = "Aux501_cloudlet_SmokeShellred2UW";
    };
    class SmokeShellUW: SmokeShellUW
    {
        type = "Aux501_cloudlet_SmokeShellredUW";
    };
};
class Aux501_particle_effect_SmokeGreen: Aux501_particle_effect_SmokeShellWhite
{
    class SmokeShell: SmokeShell
    {
        type = "Aux501_cloudlet_smokeshell_green";
    };
    class SmokeShell2: SmokeShell2
    {
        type = "Aux501_cloudlet_smokeshell_green2";
    };
    class SmokeShell2UW: SmokeShell2UW
    {
        type = "Aux501_cloudlet_SmokeShellgreen2UW";
    };
    class SmokeShellUW: SmokeShellUW
    {
        type = "Aux501_cloudlet_SmokeShellgreenUW";
    };
};
class Aux501_particle_effect_SmokeBlue: Aux501_particle_effect_SmokeShellWhite
{
    class SmokeShell: SmokeShell
    {
        type = "Aux501_cloudlet_smokeshell_blue";
    };
    class SmokeShell2: SmokeShell2
    {
        type = "Aux501_cloudlet_smokeshell_blue";
    };
    class SmokeShell2UW: SmokeShell2UW
    {
        type = "Aux501_cloudlet_SmokeShellblue2UW";
    };
    class SmokeShellUW: SmokeShellUW
    {
        type = "Aux501_cloudlet_SmokeShellblueUW";
    };
};
class Aux501_particle_effect_SmokeShellWhiteWater
{
    colorCoef[] = {0,0,0,1};
    interval = 0.25;
};
class Aux501_particle_effect_SmokeShellPurpleWater
{
    colorCoef[] = {0,0,0,1};
    interval = 0.25;
};
class Aux501_particle_effect_SmokeShellYellowWater
{
    colorCoef[] = {0,0,0,1};
    interval = 0.25;
};
class Aux501_particle_effect_SmokeShellRedWater
{
    colorCoef[] = {0,0,0,1};
    interval = 0.25;
};
class Aux501_particle_effect_SmokeShellGreenWater
{
    colorCoef[] = {0,0,0,1};
    interval = 0.25;
};
class Aux501_particle_effect_SmokeShellBlueWater
{
    colorCoef[] = {0,0,0,1};
    interval = 0.25;
};

class CfgCloudlets
{
    class SmokeShellWhite;
    class SmokeShellWhiteWater;
    class SmokeShellWhite2;
    class SmokeShellWhiteUW;
    class SmokeShellWhite2UW;

    class Aux501_cloudlet_smokeshell_white: SmokeShellWhite
    {
        colorCoef[] = {"colorR","colorG","colorB","colorA"};
        blockAIVisibility = 1;
        color[] = {{0.6,0.6,0.6,0.2},{0.6,0.6,0.6,0.05},{0.6,0.6,0.6,0}};
        moveVelocity[] = {0.2,0.5,0.1};
        size[] = {0.46,4.5,18};
        MoveVelocityVar[] = {0.7,0.4,0.7};
    };
    class Aux501_cloudlet_smokeshell_purple: Aux501_cloudlet_smokeshell_white
    {
        color[] = {{0.4341,0.1388,0.41439998,0.2},{0.4341,0.1388,0.41439998,0.05},{0.4341,0.1388,0.41439998,0}};
    };
    class Aux501_cloudlet_smokeshell_yellow: Aux501_cloudlet_smokeshell_white
    {
        color[] = {{0.9883,0.8606,0.0719,0.2},{0.9883,0.8606,0.0719,0.05},{0.9883,0.8606,0.0719,0}};
    };
    class Aux501_cloudlet_smokeshell_red: Aux501_cloudlet_smokeshell_white
    {
        color[] = {{0.8438,0.1383,0.1353,0.2},{0.8438,0.1383,0.1353,0.05},{0.8438,0.1383,0.1353,0}};
    };
    class Aux501_cloudlet_smokeshell_green: Aux501_cloudlet_smokeshell_white
    {
        color[] = {{0.2125,0.6258,0.48909998,0.2},{0.2125,0.6258,0.48909998,0.05},{0.2125,0.6258,0.48909998,0}};
    };
    class Aux501_cloudlet_smokeshell_blue: Aux501_cloudlet_smokeshell_white
    {
        color[] = {{0.1183,0.1867,1,0.2},{0.1183,0.1867,1,0.05},{0.1183,0.1867,1,0}};
    };
    class Aux501_cloudlet_smokeshell_white2: Aux501_cloudlet_smokeshell_white
    {
        particleFSNtieth = 16;
        particleFSIndex = 12;
        particleFSFrameCount = 4;
        particleFSLoop = 0;
        color[] = {{0.6,0.6,0.6,1},{0.6,0.6,0.6,0.5},{0.6,0.6,0.6,0}};
    };
    class Aux501_cloudlet_smokeshell_Purple2: Aux501_cloudlet_smokeshell_white2
    {
        color[] = {{0.4341,0.1388,0.41439998,0.2},{0.4341,0.1388,0.41439998,0.05},{0.4341,0.1388,0.41439998,0}};
    };
    class Aux501_cloudlet_smokeshell_Yellow2: Aux501_cloudlet_smokeshell_white2
    {
        color[] = {{0.9883,0.8606,0.0719,0.2},{0.9883,0.8606,0.0719,0.05},{0.9883,0.8606,0.0719,0}};
    };
    class Aux501_cloudlet_smokeshell_Red2: Aux501_cloudlet_smokeshell_white2
    {
        color[] = {{0.8438,0.1383,0.1353,0.2},{0.8438,0.1383,0.1353,0.05},{0.8438,0.1383,0.1353,0}};
    };
    class Aux501_cloudlet_smokeshell_Green2: Aux501_cloudlet_smokeshell_white2
    {
        color[] = {{0.2125,0.6258,0.48909998,0.2},{0.2125,0.6258,0.48909998,0.05},{0.2125,0.6258,0.48909998,0}};
    };
    class Aux501_cloudlet_smokeshell_Blue2: Aux501_cloudlet_smokeshell_white2
    {
        color[] = {{0.1183,0.1867,1,0.2},{0.1183,0.1867,1,0.05},{0.1183,0.1867,1,0}};
    };
    class Aux501_cloudlet_SmokeShellWhiteUW: SmokeShellWhiteUW
    {
        color[] = {{1,1,1,1}};
        randomDirectionIntensity = 0.2;
        MoveVelocityVar[] = {0.5,0.5,0.5};
    };
    class Aux501_cloudlet_SmokeShellPurpleUW: Aux501_cloudlet_SmokeShellWhiteUW
    {
        color[] = {{0.4341,0.1388,0.41439998,1}};
    };
    class Aux501_cloudlet_SmokeShellYellowUW: Aux501_cloudlet_SmokeShellWhiteUW
    {
        color[] = {{0.9883,0.8606,0.0719,1}};
    };
    class Aux501_cloudlet_SmokeShellRedUW: Aux501_cloudlet_SmokeShellWhiteUW
    {
        color[] = {{0.8438,0.1383,0.1353,1}};
    };
    class Aux501_cloudlet_SmokeShellGreenUW: Aux501_cloudlet_SmokeShellWhiteUW
    {
        color[] = {{0.2125,0.6258,0.48909998,1}};
    };
    class Aux501_cloudlet_SmokeShellBlueUW: Aux501_cloudlet_SmokeShellWhiteUW
    {
        color[] = {{0.1183,0.1867,1,1}};
    };
    class Aux501_cloudlet_SmokeShellWhite2UW: SmokeShellWhite2UW
    {
        postEffects = "Aux501_particle_effect_SmokeShellWhiteWater";
    };
    class Aux501_cloudlet_SmokeShellPurple2UW: Aux501_cloudlet_SmokeShellWhite2UW
    {
        postEffects = "Aux501_particle_effect_SmokeShellPurpleWater";
    };
    class Aux501_cloudlet_SmokeShellYellow2UW: Aux501_cloudlet_SmokeShellWhite2UW
    {
        postEffects = "Aux501_particle_effect_SmokeShellYellowWater";
    };
    class Aux501_cloudlet_SmokeShellRed2UW: Aux501_cloudlet_SmokeShellWhite2UW
    {
        postEffects = "Aux501_particle_effect_SmokeShellRedWater";
    };
    class Aux501_cloudlet_SmokeShellGreen2UW: Aux501_cloudlet_SmokeShellWhite2UW
    {
        postEffects = "Aux501_particle_effect_SmokeShellGreenWater";
    };
    class Aux501_cloudlet_SmokeShellBlue2UW: Aux501_cloudlet_SmokeShellWhite2UW
    {
        postEffects = "Aux501_particle_effect_SmokeShellBlueWater";
    };
};