class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Separatist_Scientist
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Civilians_Separatist_Scientist"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Civilians_Republic_Scientist;

    class Aux501_Units_Civilians_Separatist_Scientist: Aux501_Units_Civilians_Republic_Scientist
    {
        editorSubcategory = "Aux501_Editor_Subcategory_Confederacy";
        model = "\OPTRE_UNSC_Units\Army\officer.p3d";
        hiddenSelections[] = {"camo1","camo2","insignia","odst","bar_1","bar_2","awards","nametag"};
        hiddenSelectionsTextures[] = 
        {
            "OPTRE_UNSC_Units\Army\data\dress_uniform_gray_co.paa",
            "Aux501\Units\CIS\Human_Legion\Uniforms\data\textures\CIS_Human_Officer_Rank_CO.paa",
            "",
            "",
            "",
            "",
            "",
            ""
        };
        hiddenSelectionsMaterials[]={};
        uniformClass = "Aux501_Units_Civilians_Uniforms_Separatist_Scientist";
        headgearList[] = {};
        identityTypes[] = 
        {
            "LanguageGRE_F",
            "Head_EURO",
            "Head_Asian",
            "Head_Greek",
            "Head_m_mirialan",
            "Head_m_zelosian",
            "Head_m_Zabrak"
        };
        linkedItems[] =  
        {
            "ItemWatch"
        };
        respawnlinkedItems[] =  
        {
            "ItemWatch"
        };
        Items[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_credit_card",
            "JLTS_ids_gar_army"
        };
        RespawnItems[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_credit_card",
            "JLTS_ids_gar_army"
        };
    };
};