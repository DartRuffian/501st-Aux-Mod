class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{
    class UniformItem;

    class U_C_ConstructionCoverall_Blue_F;

    class Aux501_Units_Civilians_Uniforms_Republic_Engineer: U_C_ConstructionCoverall_Blue_F
    {
        author = "501st Aux Team";
        displayname = "[501st] REP Engineer Uniform";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"Aux501\Units\Civilians\Gear\Uniforms\data\textures\republic_engineer_uniform.paa"};
        hiddenSelectionsMaterials[]= {"Aux501\Units\Civilians\Gear\Uniforms\data\materials\republic_engineer_coverall.rvmat"};
        class ItemInfo: UniformItem
        {
            uniformModel = "-";
            uniformClass = "Aux501_Units_Civilians_Republic_Engineer";
            containerClass = "Supply40";
            mass = 50;
            modelSides[] = {6};
        };
    };

    class Aux501_Units_Civilians_Uniforms_Republic_Press: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[501st] REP HoloNet Reporter Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Republic_Press";
        };
    };
    
    class Aux501_Units_Civilians_Uniforms_Republic_Factory_Worker: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[501st] REP Factory Worker Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Republic_Factory_Worker";
        };
    };
    class Aux501_Units_Civilians_Uniforms_Republic_Scientist: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[501st] REP Scientist Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Republic_Scientist";
        };
    };
    class Aux501_Units_Civilians_Uniforms_Separatist_Scientist: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[501st] CIS Scientist Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Separatist_Scientist";
        };
    };
};