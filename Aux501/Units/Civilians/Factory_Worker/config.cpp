class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Republic_Factory_Worker
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Civilians_Republic_Engineer;

    class Aux501_Units_Civilians_Republic_Factory_Worker: Aux501_Units_Civilians_Republic_Engineer
    {
        scope = 2;
        scopeCurator = 2;
        displayname = "Factory Worker";
        model = "\A3\characters_F\common\coveralls";
        hiddenSelections[] = {"Camo","insignia"};
        hiddenSelectionsTextures[] = {"Aux501\Units\Civilians\Gear\Uniforms\data\textures\republic_factory_worker_uniform.paa"}; 
        icon = "iconMan";
        uniformClass = "Aux501_Units_Civilians_Uniforms_Republic_Factory_Worker";
        headgearList[] = {"H_Construction_earprot_white_F",1};
        linkedItems[] =  
        {
            "H_Construction_earprot_white_F",
            "G_RegulatorMask_F",
            "ItemWatch"
        };
        respawnlinkedItems[] =  
        {
            "H_Construction_earprot_white_F",
            "G_RegulatorMask_F",
            "ItemWatch"
        };
    };
};