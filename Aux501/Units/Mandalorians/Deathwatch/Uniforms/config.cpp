class cfgPatches
{
    class Aux501_Patch_Units_Mandalorian_Deathwatch_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Mandalorian_Undersuit_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Mandalorian_Undersuit_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[Mando] Bodyglove 01";
        picture = "ls_armor_greenfor\uniform\mandalorian\_ui\icon_uniform_undersuit.paa";
        model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Mandalorian_Deathwatch_Soldier_Unit";
            armor = 0;
            armorStructural = 0;
            explosionShielding = 0;
            impactDamageMultiplier = 0;
            modelSides[] = {6};
            uniformType = "Neopren";
            containerClass = "Supply100";
            mass = 40;
        };
    };
};