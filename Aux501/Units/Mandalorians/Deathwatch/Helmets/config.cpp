class cfgPatches
{
    class Aux501_Patch_Units_Mandalorian_Deathwatch_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier",
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_NCO",
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_Captain",
            "Aux501_Units_Mandalorian_Deathwatch_Helmet_NiteOwl"
        };
    };
};

class CfgWeapons
{
    class H_HelmetB;

    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class ItemInfo;
    };

    class Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        displayName = "[Mando] DW HELM 01 - Soldier";
        model = "\ls_armor_greenfor\helmet\mandalorian\traditional\ls_mandalorian_traditional_helmet.p3d";
        hiddenSelections[] = {"camo1","visor","neckTex"};
        hiddenSelectionsTextures[] = 
        {
            "\lsd_units_redfor\cis\deathwatch\textures\grunt_helmet.paa",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\visor_co.paa",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\neck_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\helmet.rvmat",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\visor.rvmat",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\neck.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            mass = 10;
            uniformModel = "\ls_armor_greenfor\helmet\mandalorian\traditional\ls_mandalorian_traditional_helmet.p3d";
            hiddenSelections[] = {"camo1","visor","neckTex"};
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 40;
                    passThrough = 0.5;
                };
                class Face
                {
                    hitPointName = "HitFace";
                    armor = 20;
                    passThrough = 0.3;
                };
            };
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_Helmet_NCO: Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier
    {
        displayName = "[Mando] DW HELM 02 - NCO";
        hiddenSelectionsTextures[] = 
        {
            "\lsd_units_redfor\cis\deathwatch\textures\sergeant_helmet.paa",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\visor_co.paa",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\neck_co.paa"
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_Helmet_Captain: Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier
    {
        displayName = "[Mando] DW HELM 03 - Captain";
        hiddenSelectionsTextures[] = 
        {
            "\lsd_units_redfor\cis\deathwatch\textures\leader_helmet.paa",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\visor_co.paa",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\neck_co.paa"
        };
    };
    class Aux501_Units_Mandalorian_Deathwatch_Helmet_NiteOwl: Aux501_Units_Mandalorian_Deathwatch_Helmet_Soldier
    {
        displayName = "[Mando] DW HELM 04 - Nite Owl";
        hiddenSelectionsTextures[] = 
        {
            "\lsd_units_redfor\cis\deathwatch\textures\nightowl_helmet.paa",
            "\ls_armor_greenfor\helmet\mandalorian\nightowl\data\visor_co.paa",
            "\ls_armor_greenfor\helmet\mandalorian\nightowl\data\neck_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\helmet.rvmat",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\visor.rvmat",
            "\ls_armor_greenfor\helmet\mandalorian\traditional\data\neck.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\ls_armor_greenfor\helmet\mandalorian\nightowl\ls_mandalorian_nightowl_helmet.p3d";
        };
    };
};