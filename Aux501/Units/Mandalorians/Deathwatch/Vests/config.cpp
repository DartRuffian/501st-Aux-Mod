class cfgPatches
{
    class Aux501_Patch_Units_Mandalorian_Deathwatch_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper",
            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_NCO",
            "Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Captain"
        };
    };
};

class cfgWeapons
{
    class Vest_NoCamo_Base;

    class V_PlateCarrier1_rgr: Vest_NoCamo_Base
    {
       class ItemInfo; 
    };

    class Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper: V_PlateCarrier1_rgr
    {	
        scope = 2;
        scopeArsenal = 2;
        displayName = "[Mando] DW Vest 01 - Grunt";
        picture = "\ls_armor_greenfor\vest\mandalorian\_ui\icon_vest_original.paa";
        model = "\ls_armor_greenfor\vest\mandalorian\original\ls_mandalorian_original_vest.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\lsd_units_redfor\cis\deathwatch\textures\grunt_vest.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\ls_armor_greenfor\vest\mandalorian\original\data\vest_original.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\ls_armor_greenfor\vest\mandalorian\original\ls_mandalorian_original_vest.p3d";
            modelsides[] = {6};
            hiddenSelections[] = {"camo1"};
            containerClass = "Supply100";
            class HitpointsProtectionInfo
            {
                class Chest
                {
                    HitpointName = "HitChest";
                    armor = 100;
                    PassThrough = 0.3;
                };
                class Diaphragm
                {
                    hitpointName = "HitDiaphragm";
                    armor = 100;
                    PassThrough = 0.3;
                };
                class Abdomen
                {
                    hitpointName	= "HitAbdomen";
                    armor = 100;
                    PassThrough = 0.3;
                };
                class Legs
                {
                    hitpointName = "HitLegs";
                    armor = 20;
                    passThrough = 0.3;
                };
                class Arms
                {
                    hitpointName = "HitArms";
                    armor = 50;
                    passThrough = 0.3;
                };
            };
        }; 
    };
    class Aux501_Units_Units_Mandalorian_Vests_Deathwatch_NCO: Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper
    {	
        displayName = "[Mando] DW Vest 02 - NCO";
        hiddenSelectionsTextures[] = {"\lsd_units_redfor\cis\deathwatch\textures\leader_vest.paa"};
    };
    class Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Captain: Aux501_Units_Units_Mandalorian_Vests_Deathwatch_Trooper
    {	
        displayName = "[Mando] DW Vest 03 - Captain";
        hiddenSelectionsTextures[] = {"\lsd_units_redfor\cis\deathwatch\textures\sergeant_vest.paa"};
    };
};