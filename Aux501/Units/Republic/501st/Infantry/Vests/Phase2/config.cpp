class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",
            "Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_Pad",
            "Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper",
            "Aux501_Units_Republic_501_Infantry_Vests_Lance_CP",
            "Aux501_Units_Republic_501_Infantry_Vests_Suspenders",

            "Aux501_Units_Republic_501_Infantry_Vests_CP",
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CP",

            "Aux501_Units_Republic_501_Infantry_Vests_CS",
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CS",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_2",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_3",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_4",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_2",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_3",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_4",

            "Aux501_Units_Republic_501_Infantry_Vests_2LT",
            "Aux501_Units_Republic_501_Infantry_Vests_2LT_2",
            "Aux501_Units_Republic_501_Infantry_Vests_2LT_3",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_4",

            "Aux501_Units_Republic_501_Infantry_Vests_1LT",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_2",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_3",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_4",

            "Aux501_Units_Republic_501_Infantry_Vests_Captain",
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_2",
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_3",
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_4"

            "Aux501_Units_Republic_501_Infantry_Vests_Major",
            "Aux501_Units_Republic_501_Infantry_Vests_Major_2",
            "Aux501_Units_Republic_501_Infantry_Vests_Major_3",
            "Aux501_Units_Republic_501_Infantry_Vests_Major_4"
        };
    };
};

class cfgWeapons
{
    class V_RebreatherB;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Base: V_RebreatherB
    {
        class ItemInfo;
    };
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        scope = 1;
        scopeArsenal = 1;
        displayname = "[501st] INF VEST 00 - Nanoweave";
        picture = "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\UI\CloneUndersuit.paa";
    };

    class Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 01 - Holster";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneVestHolster_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneVestHolster.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneVestHolster.p3d";
            hiddenSelections[] = {"camo1"};
        };
    };

    class Aux501_Units_Republic_501_Infantry_Vests_Holster_Pad: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 02 - Sr. CT";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_cfr_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_cfr_armor.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\heavy_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_clones\SWLB_clone_cfr_armor.p3d";
            hiddenSelections[] = {"camo1"};
        };
    };

    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 03 - Vet. CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
            hiddenSelections[] = {"camo2"};
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Suspenders: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 04 - Vet. CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_co.paa"};
        hiddenSelectionsMaterials[]= {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_cloth.rvmat"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
            hiddenSelections[] = {"camo1"};
        };
    };

    class Aux501_Units_Republic_501_Infantry_Vests_Lance_CP: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 05 - CLC";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest.paa"};
    };

    //CP
    class Aux501_Units_Republic_501_Infantry_Vests_CP: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 06 - CP";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"camo2"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            hiddenSelections[] = {"camo2"};
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_snr_CP: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        displayname = "[501st] INF VEST 07 - Sr. CP";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_snr_cp_vest.paa"};
    };

    //CS
    class Aux501_Units_Republic_501_Infantry_Vests_CS: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        displayname = "[501st] INF VEST 08 - CS";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa",
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\materials\inf_officer_pauldron.rvmat"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
            hiddenSelections[] = {"camo1","camo2"};
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_snr_CS: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        displayname = "[501st] INF VEST 09 - Sr. CS";
        hiddenSelectionsTextures[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_snr_cp_vest.paa"
        };
    };
    
    //Sergeant Majors

    //Platoon CS-M
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        displayname = "[501st] INF VEST 10 - CS-M";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_plt_csm_vest.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            hiddenSelections[] = {"camo1"};
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 11 - CS-M";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 12 - CS-M";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestLieutenant.p3d";
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestLieutenant.p3d";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 13 - CS-M";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer2.p3d";
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer2.p3d";
        };
    };

    //Company CS-M
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 14 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF VEST 15 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF VEST 16 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF VEST 17 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest.paa"};
    };

    //Battalion CS-M
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 18 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF VEST 19 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF VEST 20 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF VEST 21 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest.paa"};
    };

    //Officers

    //2nd LT
    class Aux501_Units_Republic_501_Infantry_Vests_2LT: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 22 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF VEST 23 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF VEST 24 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF VEST 25 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest.paa"};
    };

    //1st LT
    class Aux501_Units_Republic_501_Infantry_Vests_1LT: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayname = "[501st] INF VEST 26 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayname = "[501st] INF VEST 27 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        displayname = "[501st] INF VEST 28 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_4: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayname = "[501st] INF VEST 29 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest.paa"};
    };

    //Captain
    class Aux501_Units_Republic_501_Infantry_Vests_Captain: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayname = "[501st] INF VEST 30 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Captain_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayname = "[501st] INF VEST 31 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Captain_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        displayname = "[501st] INF VEST 32 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Captain_4: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayname = "[501st] INF VEST 33 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest.paa"};
    };

    //Major
    class Aux501_Units_Republic_501_Infantry_Vests_Major: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayname = "[501st] INF VEST 34 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Major_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayname = "[501st] INF VEST 35 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Major_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        displayname = "[501st] INF VEST 36 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest.paa"};
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Major_4: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayname = "[501st] INF VEST 37 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest.paa"};
    };
};