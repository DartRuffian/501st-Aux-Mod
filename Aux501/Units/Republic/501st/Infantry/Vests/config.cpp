class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Base"
        };
    };
};

class cfgWeapons
{
    class V_RebreatherB;
    class VestItem;

    class Aux501_Units_Republic_501_Infantry_Vests_Base: V_RebreatherB
    {
        scope = 1;
        scopeArsenal = 1;
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        hiddenSelectionsMaterials[] = {};
        model = "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\Empty.p3d";
        class ItemInfo: VestItem
        {
            uniformModel = "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\Empty.p3d";
            modelsides[] = {6};
            containerClass = "Supply100";
            mass = 20;
            vestType = "Rebreather";
            hiddenSelections[] = {};
            hiddenSelectionsMaterials[] = {};
            class HitpointsProtectionInfo
            {
                class Diaphragm
                {
                    hitpointName = "HitDiaphragm";
                    armor = 7500;
                    passThrough = 0.4;
                };
                class Chest
                {
                    hitpointName = "HitChest";
                    armor = 7000;
                    passThrough = 0.4;
                };
                class Abdomen
                {
                    hitpointName = "HitAbdomen";
                    armor = 5000;
                    passThrough = 0.4;
                };
                class Pelvis
                {
                    hitpointName = "HitPelvis";
                    armor = 6700;
                    passThrough = 0.4;
                };
                class Neck
                {
                    hitpointName = "HitNeck";
                    armor = 1000;
                    passThrough = 0.2;
                };
                class Arms
                {
                    hitpointName = "HitArms";
                    armor = 4000;
                    passThrough = 0.2;
                };
                class Body
                {
                    armor = 4000;
                    hitpointName = "HitBody";
                    passThrough = 0.4;
                };
            };
        };
    };
};