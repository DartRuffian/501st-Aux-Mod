class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Invisible
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Invisible",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Invisible_LR"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small;

    class Aux501_Units_Republic_501_Infantry_Backpacks_Invisible: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 1;
        scopeArsenal = 1;
        displayname = "[501st] INF Backpack 00";
        model = "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\Empty.p3d";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Invisible_LR: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small
    {
        displayname = "[501st] INF Backpack 00 - LR";
        model = "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\Empty.p3d";
    };
};