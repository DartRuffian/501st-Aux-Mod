class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Small
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "[501st] INF LR Small 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_LR_attachment_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneLRattachment.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_LR_attachment_co.paa"};
        tf_dialog = "JLTS_clone_lr_programmer_radio_dialog";
    };
};