class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Standard",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Assault",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Engineer",
            "Aux501_Units_Republic_501_Infantry_Backpacks_EOD",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_AT_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Engineer_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_EOD_straps"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 1;
        scopeArsenal = 1;
        displayName = "[501st] INF Backpack 01 - Standard";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_AT: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 02 - AT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Standard\data\textures\Aux501_backpack_staticweapons_rocket.paa"
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 03 - Grenadier";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Standard\data\textures\Aux501_backpack_staticweapons_grenade.paa"
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Engineer: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 04 - Engineer";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Standard\data\textures\Aux501_backpack_engineer.paa"
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_EOD: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 05 - EOD";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_eod_co.paa"
        };
    };

    //Medical Backpacks
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] MED Backpack 01";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_medic_co.paa"
        };
        maximumload = 1000;
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Medic
    {
        displayName = "[501st] MED Backpack 02";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
    };

    //Strappacks
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Strappack 01 - Standard";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_AT_straps: Aux501_Units_Republic_501_Infantry_Backpacks_AT
    {
        displayName = "[501st] INF Strappack 02 - AT";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier
    {
        displayName = "[501st] INF Strappack 03 - Grenadier";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Engineer_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Engineer
    {
        displayName = "[501st] INF Strappack 04 - Engineer";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_EOD_straps: Aux501_Units_Republic_501_Infantry_Backpacks_EOD
    {
        displayName = "[501st] INF Strappack 05 - EOD";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
    };
};