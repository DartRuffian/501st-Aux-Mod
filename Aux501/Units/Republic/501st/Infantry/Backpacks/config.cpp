class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_base"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class B_Kitbag_rgr;

    class Aux501_Units_Republic_501_Infantry_Backpacks_base: B_Kitbag_rgr
    {
        scope = 1;
        scopearesenal = 1;
        author = "501st Aux Team";
        displayName = "[501st]";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_backpack_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpack.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_co.paa"};
        maximumload = 700;
    };
};