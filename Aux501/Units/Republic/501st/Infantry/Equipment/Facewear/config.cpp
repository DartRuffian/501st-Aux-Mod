class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgGlasses
{
    class G_B_Diving;
    class G_Diving;
    class g_balaclava_ti_blk_f;
    class g_balaclava_blk;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display: G_B_Diving
    {
        scope = 2;
        scopeCurator = 2;
        scopeArsenal = 2;
        displayname = "[501st] FW Phase 2 Display 00";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_interior_ui_ca.paa";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\phase2.paa";
        ACE_OverlayCracked = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\phase2.paa";
        ace_overlayDirt = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\phase2.paa";
        modelOptics = "";
    };
    
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW Phase 2 Display 01 (Blue)";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\UI\no_blue.paa";
        ace_color[] = {0.0,0.0,-200.0};
		ace_tintAmount = 1;
		ace_resistance = 1;
		ace_protection = 0;
		ACE_Overlay = "";
		ace_overlayDirt = "";
		ace_dustPath = "";
		ACE_OverlayCracked = "";
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW Phase 2 Display 02 (Red)";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\UI\no_red.paa";
        ace_color[] = {-150.0,0.0,0.0};
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW Phase 2 Display 03 (Green)";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\UI\no_green.paa";
        ace_color[] = {0.0,-100.0,0.0};
    };

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUDless Display 00";
        ACE_Overlay = "";
        ACE_OverlayCracked = "";
        ace_overlayDirt = "";
        modelOptics = "";
    };

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUDless Display 01 (Blue)";
        ACE_Overlay = "";
        ACE_OverlayCracked = "";
        ace_overlayDirt = "";
        modelOptics = "";
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUDless Display 02 (Red)";
        ACE_Overlay = "";
        ACE_OverlayCracked = "";
        ace_overlayDirt = "";
        modelOptics = "";
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUDless Display 03 (Green)";
        ACE_Overlay = "";
        ACE_OverlayCracked = "";
        ace_overlayDirt = "";
        modelOptics = "";
    };
};