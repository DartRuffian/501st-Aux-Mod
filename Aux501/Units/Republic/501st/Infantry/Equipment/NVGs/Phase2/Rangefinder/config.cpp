class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Rangefinder
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 1;
        scopearesenal = 1;
        displayName = "[501st] INF Rangefinder 00 - Black ";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Rangefinder\data\textures\inf_rangefinder_black.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
            mass = 20;
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF Rangefinder 01 - White";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_range_co.paa"};
    };
};