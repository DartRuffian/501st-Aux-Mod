class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 1;
        scopearesenal = 1;
        displayName = "[501st] INF Officer Visor 00 - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_cc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_visor_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            mass = 20;
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor
    {
        displayName = "[501st] INF Officer Visor 01 - Black";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor
    {
        displayName = "[501st] INF Officer Visor 02 - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
    };
};