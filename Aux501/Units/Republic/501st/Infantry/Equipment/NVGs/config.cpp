class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_base"
        };
    };
};

class CfgWeapons
{
    class NVGoggles;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_base: NVGoggles
    {
        author = "501st Aux Team";
        scope = 1;
        scopearesenal = 1;
        picture = "";
        //RD501_isNV = 1; to be added later
        modelOptics = "";
        visionMode[] = {"Normal","NVG"};
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        thermalMode[] = {0};
        class ItemInfo
        {
            type = 616;
            uniformModel = "\MRC\JLTS\Core_mod\nvg_optic_dummy.p3d";
            modelOff = "\MRC\JLTS\Core_mod\nvg_optic_dummy.p3d";
            mass = 10;
            hiddenSelections[] = {};
        };
    };
};