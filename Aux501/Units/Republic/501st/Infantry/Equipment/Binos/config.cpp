class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Binos
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgWeapons
{
    class Laserdesignator;

    class Aux501_Units_Republic_Equipment_Electrobinos: Laserdesignator
    {
        author = "501st Aux Team";
        displayName = "[501st] Clone Electrobinoculars";
        descriptionShort = "Standard Grand Army Electrobinoculars";
        picture = "\SWLB_equipment\binoculars\data\ui\icon_SWLB_clone_commander_binocular_ca.paa";
        model = "\SWLB_equipment\binoculars\SWLB_clone_commander_binocular.p3d";
        modelOptics = "\MRC\JLTS\characters\DroidArmor\DroidBinocular_optics.p3d";
        opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Binos\data\textures\Standard_Electrobinos.paa"};
        visionMode[] = {"Normal","NVG","TI"};
        thermalMode[] = {0};		
    };

    class Aux501_Units_Republic_Equipment_Electrobinos_501st: Aux501_Units_Republic_Equipment_Electrobinos
    {
        displayName = "[501st] Clone Electrobinoculars (501st)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Binos\data\textures\Standard_Electrobinos_501st.paa"};
    };
    class Aux501_Units_Republic_Equipment_Electrobinos_Scout: Aux501_Units_Republic_Equipment_Electrobinos
    {
        displayName = "[501st] Clone Electrobinoculars (Scout)";
        descriptionShort = "Scout Electrobinoculars";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Binos\data\textures\Night_Electrobinos.paa"};
    };
};