class cfgPatches
{
    class Aux501_Patch_Units_Republic_212th
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_212th_Trooper_Unit",
            "Aux501_Units_Republic_212th_Corporal_Unit",
            "Aux501_Units_Republic_212th_Sergeant_Unit",
            "Aux501_Units_Republic_212th_CSM_Unit",
            "Aux501_Units_Republic_212th_2ndLT_Unit",
            "Aux501_Units_Republic_212th_1stLT_Unit",
            "Aux501_Units_Republic_212th_Captain_Unit"
        };
        weapons[] = {};
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Unit;
    class Aux501_Units_Republic_501st_CP_Unit;
    class Aux501_Units_Republic_501st_CS_Unit;
    class Aux501_Units_Republic_501st_Platoon_CSM_Unit;
    class Aux501_Units_Republic_501st_2LT_Unit;
    class Aux501_Units_Republic_501st_1LT_Unit;
    class Aux501_Units_Republic_501st_Captain_Unit;

    class Aux501_Units_Republic_212th_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
    class Aux501_Units_Republic_212th_Corporal_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CP",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CP",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
    class Aux501_Units_Republic_212th_Sergeant_Unit: Aux501_Units_Republic_501st_CS_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CS",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_full",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CS",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_full",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
    class Aux501_Units_Republic_212th_CSM_Unit: Aux501_Units_Republic_501st_Platoon_CSM_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
    class Aux501_Units_Republic_212th_2ndLT_Unit: Aux501_Units_Republic_501st_2LT_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_212th_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_212th_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
    class Aux501_Units_Republic_212th_1stLT_Unit: Aux501_Units_Republic_501st_1LT_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_212th_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_212th_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
    class Aux501_Units_Republic_212th_Captain_Unit: Aux501_Units_Republic_501st_Captain_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_212th";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_212thTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_212_Helmet",
            
            "Aux501_Units_Republic_212th_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_212_Helmet",

            "Aux501_Units_Republic_212th_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_212th_Uniform";
    };
};