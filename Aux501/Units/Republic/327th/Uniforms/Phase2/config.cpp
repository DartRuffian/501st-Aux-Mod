class cfgPatches
{
    class Aux501_Patch_Units_Republic_327_Uniforms_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_327th_Trooper_Uniform",
            "Aux501_Units_Republic_327th_NCO_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_327th_Trooper_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_327thTrooper_uniform_ca.paa";
        displayName = "[327th] P2 ARMR 01";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_327th_Trooper_Unit";
        };
    };
    class Aux501_Units_Republic_327th_NCO_Uniform: Aux501_Units_Republic_327th_Trooper_Uniform
    {
        displayName = "[327th] P2 ARMR 02 - NCO";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_327th_Sergeant_Unit";
        };
    };
};