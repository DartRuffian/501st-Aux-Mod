class cfgPatches
{
    class Aux501_Patch_Units_Republic_327_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_327_Helmet",
            "Aux501_Units_Republic_327_Helmet_NCO"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Base;

    class Aux501_Units_Republic_327_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;   
        displayName = "[327th] P2 HELM 01";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_327thTrooper_helmet_ca.paa";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\327th\Helmets\Phase2\data\327_ct_helmet.paa",
            "\Aux501\Units\Republic\327th\Helmets\Phase2\data\327_ct_helmet.paa"
        };
    };
    class Aux501_Units_Republic_327_Helmet_NCO: Aux501_Units_Republic_327_Helmet
    { 
        displayName = "[327th] P2 HELM 02 - NCO";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\327th\Helmets\Phase2\data\327_nco_helmet.paa",
            "\Aux501\Units\Republic\327th\Helmets\Phase2\data\327_nco_helmet.paa"
        };
    };
};