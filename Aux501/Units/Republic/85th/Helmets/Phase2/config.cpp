class cfgPatches
{
    class Aux501_Patch_Units_Republic_85_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_85_Helmet"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Base;

    class Aux501_Units_Republic_85_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;   
        displayName = "[85th] P2 HELM 01";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_21stTrooper_helmet_ca.paa";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\85th\Helmets\Phase2\data\textures\85_ct_helmet.paa",
            "\Aux501\Units\Republic\85th\Helmets\Phase2\data\textures\85_ct_helmet.paa"
        };
    };
};