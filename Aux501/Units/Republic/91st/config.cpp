class cfgPatches
{
    class Aux501_Patch_Units_Republic_91st
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_91st_Trooper_Unit",
            "Aux501_Units_Republic_91st_CP_Unit",
            "Aux501_Units_Republic_91st_CS_Unit",
            "Aux501_Units_Republic_91st_2Lt_Unit",
            "Aux501_Units_Republic_91st_1Lt_Unit",
            "Aux501_Units_Republic_91st_Captian_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Unit;
    class Aux501_Units_Republic_501st_CP_Unit;
    class Aux501_Units_Republic_501st_CS_Unit;
    class Aux501_Units_Republic_501st_2Lt_Unit;
    class Aux501_Units_Republic_501st_1LT_Unit;
    class Aux501_Units_Republic_501st_Captain_Unit;

    class Aux501_Units_Republic_91st_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        editorSubcategory = "Aux501_Editor_Subcategory_91st";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_upper.paa",
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_lower.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_91st_Trooper_Uniform";
    };

    //NCOs
    class Aux501_Units_Republic_91st_CP_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        editorSubcategory = "Aux501_Editor_Subcategory_91st";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_upper.paa",
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_lower.paa"
        };
        uniformClass = "Aux501_Units_Republic_91st_Trooper_Uniform";
        linkedItems[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_CP",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_down",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_CP",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_down",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
    };

    class Aux501_Units_Republic_91st_CS_Unit: Aux501_Units_Republic_501st_CS_Unit
    {
        editorSubcategory = "Aux501_Editor_Subcategory_91st";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_upper.paa",
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_lower.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_CS",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_CS",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium";
        uniformClass = "Aux501_Units_Republic_91st_Trooper_Uniform";
    };

    //Officers
    class Aux501_Units_Republic_91st_2Lt_Unit: Aux501_Units_Republic_501st_2Lt_Unit
    {
        editorSubcategory = "Aux501_Editor_Subcategory_91st";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_upper.paa",
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_lower.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_2Lt",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_full_down",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_2Lt",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_full_down",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_91st_Trooper_Uniform";
    };

    class Aux501_Units_Republic_91st_1Lt_Unit: Aux501_Units_Republic_501st_1LT_Unit
    {
        editorSubcategory = "Aux501_Editor_Subcategory_91st";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_upper.paa",
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_lower.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_2Lt",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_2Lt",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium";
        uniformClass = "Aux501_Units_Republic_91st_Trooper_Uniform";
    };

    class Aux501_Units_Republic_91st_Captian_Unit: Aux501_Units_Republic_501st_Captain_Unit
    {
        editorSubcategory = "Aux501_Editor_Subcategory_91st";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_upper.paa",
            "\Aux501\Units\Republic\91st\Uniforms\Phase2\data\textures\91_ct_armor_lower.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_2Lt",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_91_Infantry_Helmet_Trooper",
            
            "Aux501_Units_Republic_91_Vests_2Lt",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
           "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium";
        uniformClass = "Aux501_Units_Republic_91st_Trooper_Uniform";
    };
};