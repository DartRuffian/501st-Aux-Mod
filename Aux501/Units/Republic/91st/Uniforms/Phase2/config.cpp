class cfgPatches
{
    class Aux501_Patch_Units_Republic_91_Uniforms_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
           "Aux501_Units_Republic_91st_Trooper_Uniform" 
        };
    };
};

class CfgWeapons
{    
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class itemInfo;
    };

    class Aux501_Units_Republic_91st_Trooper_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_91stTrooper_uniform_ca.paa";
        displayName = "[91st] P2 ARMR 01";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_91st_Trooper_Unit";
        };
    };
};