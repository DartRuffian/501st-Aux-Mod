class cfgPatches
{
    class Aux501_Patch_Units_Republic_101_Infantry_Equipment_NVGs_Macro_Binos
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Macro_Binos"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_101_Equipment_NVGs_Macrobino"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino;

    class Aux501_Republic_101_Equipment_NVGs_Macrobino: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[101st] MacroBino 01";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_nvg_co.paa"};
    };
};