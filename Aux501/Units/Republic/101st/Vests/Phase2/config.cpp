class cfgPatches
{
    class Aux501_Patch_Units_Republic_101_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_101st_Officer_Vest"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_2LT;

    class Aux501_Units_Republic_101st_Officer_Vest: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[101st] Pauldron 01 - Officer";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_DCOfficer_vest_co.paa"};
    };
};