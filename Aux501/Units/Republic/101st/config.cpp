class cfgPatches
{
    class Aux501_Patch_Units_Republic_101st
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_101st_Trooper_Unit",
            "Aux501_Units_Republic_101st_Corporal_Unit",
            "Aux501_Units_Republic_101st_Sergeant_Unit",
            "Aux501_Units_Republic_101st_CSM_Unit",
            "Aux501_Units_Republic_101st_2ndLT_Unit",
            "Aux501_Units_Republic_101st_1stLT_Unit",
            "Aux501_Units_Republic_101st_Captain_Unit"
        };
        weapons[] = {};
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Unit;
    class Aux501_Units_Republic_501st_CP_Unit;
    class Aux501_Units_Republic_501st_CS_Unit;
    class Aux501_Units_Republic_501st_Platoon_CSM_Unit;
    class Aux501_Units_Republic_501st_2LT_Unit;
    class Aux501_Units_Republic_501st_1LT_Unit;
    class Aux501_Units_Republic_501st_Captain_Unit;

    class Aux501_Units_Republic_101st_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
    class Aux501_Units_Republic_101st_Corporal_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CP",

            "Aux501_Republic_101_Equipment_NVGs_Macrobino",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CP",

            "Aux501_Republic_101_Equipment_NVGs_Macrobino",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
    class Aux501_Units_Republic_101st_Sergeant_Unit: Aux501_Units_Republic_501st_CS_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CS",

            "Aux501_Republic_101_Equipment_NVGs_Macrobino",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CS",

            "Aux501_Republic_101_Equipment_NVGs_Macrobino",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
    class Aux501_Units_Republic_101st_CSM_Unit: Aux501_Units_Republic_501st_Platoon_CSM_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
    class Aux501_Units_Republic_101st_2ndLT_Unit: Aux501_Units_Republic_501st_2LT_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_101st_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_101st_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
    class Aux501_Units_Republic_101st_1stLT_Unit: Aux501_Units_Republic_501st_1LT_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_101st_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_101st_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
    class Aux501_Units_Republic_101st_Captain_Unit: Aux501_Units_Republic_501st_Captain_Unit
    {
        scope = 2;
        scopecurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_101st";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_armor2_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_101_Helmet",
            
            "Aux501_Units_Republic_101st_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_101_Helmet",

            "Aux501_Units_Republic_101st_Officer_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_101st_Uniform";
    };
};