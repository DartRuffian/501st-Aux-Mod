# Building the Aux Mod
This process is still WIP, We're working on setting up both HEMETT and CI to ease the build process, but for right now the reccomended method to build the mod is PBO Project.

## PBO Project
Some setup is required to build the mod using PBO Project, you'll need to install tools, extract required addons and learn how to use the included powershell scripts.

### Installing Required Programs
The TL;DR of programs required are: the beta version of Mikeros tools, (we have a custom version we're using while we wait for updates), Arma 3 Tools and the P drive.
#### Arma 3 Tools & P Drive
This is on steam, to install simply open steam, goto your library, select tools from the dropdown and then search for Arma 3 Tools, install and run it like any steam game.
To install the P Drive, open arma 3 tools and select "Project Drive management" and choose "mount the P Drive", you might also want to go into preferences > mount the project drive on startup. 

![image.png](./docs/_media/build/image.png) ![image-1.png](./docs/_media/build/image-1.png) ![image-2.png](./docs/_media/build/image-2.png) 

![image-4.png](./docs/_media/build/image-4.png) ![image-5.png](./docs/_media/build/image-5.png) ![image-6.png](./docs/_media/build/image-6.png)

### Mikero's Tools.
Mikero's Tools are a community made alternative to the offical Addon Builder, they offer a number of advantages with a few downsides, including actually telling you exactly whats wrong in your code when you try to build it. It does however require all files that are depended on to exist in the correct places, which annoyingly requires all mods we depend on to be fully extracted into your P drive in the correct places.
- Download and install the AIO Installer [here](https://mikero.bytex.digital/Downloads)
- Run it, install all tools. Unfortunately, 501st aux currently REQUIRES the beta versions of pbo project and DePbo to build (its cursed, idk why), these require the paid version of mikeros tools to work, follow these steps but dm either hobnob or mark for the 501st aux mikeros key. 
- Download these two beta versions of the tools needed to build aux. [pboProject](./tools/pboProject.3.16.8.33.Installer.exe)  [DePbo](./tools/DePbo.8.33.0.26.Installer.exe)

Now everything is installed, start pbo project (hit start, type PboProject, hit enter) Go into setup.
 - Change both the `-X Exclude From PBO` and `(-B Exclusions)` boxes to `thumbs.db,*.txt,*.h,*.dep,*.cpp,*.bak,*.png,*.log,*.pew,source,*.tga,*.bat` 
 - go into Warnings are Errors at the top, click the tickboxes until it matches the image 
 
 ![image-8.png](./docs/_media/build/image-8.png)


### Extracting Game data and Required Mods.
 - To extract base arma game data, make sure your P drive is mounted and then go into Arma 3 Tools, Project Drive Management and then Extract Game data, (It will warn you that extracting game data will wipe the P drive... 3 times, this is fine because its empty, we haven't put anything in there yet.)
 - The next part is going to require running some powershell scripts, as my scripts are not signed you'll need to allow unrestricted powershell script execution, if these words scare you don't worry its pretty simple. Hit start and open powershell, type `Set-ExecutionPolicy unrestricted` when asked, press A for "yes to all".
 - Now you can run powershell scripts, the first you need to run is MakeSymlinks, go into the tools folder, right click MakeSymlinks and hit run with powershell, it'll ask you for admin and then it'll create symbolic links from your mod folder into your p drive.
 - Now, to extract all required mods into the P drive, go into the tools folder and run Extract-All-Mods.ps1 using right click > run with powershell, this will run for a while and extract all the mods automatically from the steam workshop into the p drive, please make sure the P drive is mounted first. **Make Sure you are on an up to date branch that has the correct Make-All-Mods.ps1 script before running**
 
### the Build Command 
Now we're able to actually try building the mod. to tl;dr, in your VS Code Window, at the top press terminal > new terminal, in the window that pops up type:
 - cd tools
 - cd build
 - .\build.ps1 -confirm

this should build the mod, the script will only build outdated or missing pbo's, so you can spam it while testing stuff, just make sure arma isn't running while you do.

It also has two optional flags you can set that change the behaviour: 
 - `-Pause` Will make the script ask the user to press any key between each individual addon build, makes it easier to read error messages.
 - `-FullBuild -confirm` Will make the script rebuild the entire aux, instead of only missing / outdated PBO's

### Testing your Build
When testing in singleplayer use these [server settings](./docs/_media/build/server-settings.txt) to ensure that assets work as intended when on server. some settings such as medical can drastically change the behaviour of units.

To load these files open up your addon options and select import, paste the settings in the above text document and press ok

![import-settings.png](./docs/_media/build/import-settings.png)

![import-settings-paste.png](./docs/_media/build/import-settings-paste.png)
