param(
    [switch]$FullBuild = $False,
    [switch]$Pause = $False,
    [switch]$Confirm = $False
)

$fres = Get-WmiObject -query "select * from Win32_OptionalFeature where name = 'Client-ProjFS' and installstate = 1"

if ($null -eq $fres) {
    Write-Output "Enabling ProjFS"

    if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) 
    {
        $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
        Start-Process -Wait -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
    } else {
        Enable-WindowsOptionalFeature -Online -FeatureName Client-ProjFS -NoRestart
        Write-Output "Enabled ProjFS. Re-run build once done."
        exit 0
    }
}

$buildFolders = @("RD501_3DEN","RD501_Compositions","RD501_Droids","RD501_Droid_Dispenser","RD501_EMP","RD501_Helmets","RD501_Jumppack","RD501_Main","RD501_Particle_Effects","RD501_RDS","RD501_Units","RD501_Vehicles","RD501_Vehicle_Weapons","RD501_Weapons","RD501_Zeus","VenMK2")
foreach($folder in $buildFolders) {
    if(-Not $(Test-Path $(Join-Path "P:\" $folder))) {
        $symlinksPath = (Resolve-Path "$PSScriptRoot\..\shared\makeSymlinks.ps1").Path
        & $symlinksPath

        Write-Output "Creating symlinks for missing folder. Re-run build once done."
        Exit 0
    }
}

$noAPI = $False
if ($null -eq (Get-Process -Name "VirtualPDrive.API" -ErrorAction SilentlyContinue)) {
    Set-Item "Env:VPD_VIRTUAL_INSTANCE_ID" ""
    $noAPI = $True
}

if ($noAPI) {
    $path = "$PSScriptRoot\..\vpd-api\VirtualPDrive.API.exe"
    $apiBooterPath = (Resolve-Path "$PSScriptRoot\..\shared\api-boot.ps1").Path

    & $apiBooterPath $path
}

$booterParams = @{
    "Confirm"=$Confirm
}

$ErrorActionPreference = "Stop"
& .\build-booter.ps1 @booterParams

$buildParams = @{
    "FullBuild"=$FullBuild;
    "Pause"=$Pause
}

$buildPbosPath = (Resolve-Path "$PSScriptRoot\..\shared\build-pbos.ps1").Path
& $buildPbosPath @buildParams

if ($noAPI) {
    $destroyInstancePath = (Resolve-Path "$PSScriptRoot\..\shared\destroy-instance.ps1").Path
    & $destroyInstancePath "http://localhost:9127" -EnvVarName "VPD_VIRTUAL_INSTANCE_ID"

    Start-Sleep -Seconds 1

    Stop-Process -Name "VirtualPDrive.API"
}
