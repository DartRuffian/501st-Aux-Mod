$destroyInstancePath = (Resolve-Path "$PSScriptRoot\..\shared\destroy-instance.ps1").Path
& $destroyInstancePath "http://localhost:9127" -EnvVarName "VPD_VIRTUAL_INSTANCE_ID"

Start-Sleep -Seconds 1

Stop-Process -Name "VirtualPDrive.API"