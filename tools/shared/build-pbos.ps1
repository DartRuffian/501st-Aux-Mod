param(
    [switch]$FullBuild = $False,
    [switch]$Pause = $False
)

function a{Write-Host -BackgroundColor White -ForegroundColor DarkBlue -Separator "`n" $args[0]}
function b{Write-Host -BackgroundColor DarkBlue -ForegroundColor DarkBlue -Separator "`n" $args[0]}
function c{Write-Host -BackgroundColor DarkRed -ForegroundColor Blue -Separator "`n" $args[0]}

$blank = "|                                                              |"
b $blank
a $blank
a "|       501st Aux Mod Monke-Friendly Text Starts Here:         |" 
a $blank
b $blank

$pboProject = (Get-ItemProperty "HKCU:\Software\Mikero\pboProject\").exe
$output = $(Resolve-Path "$PSScriptRoot\..\..\@501st Community Auxiliary Mod").Path
$mods = @("RD501_3DEN","RD501_Compositions","RD501_Droids","RD501_Droid_Dispenser","RD501_EMP","RD501_Helmets","RD501_Jumppack","RD501_Main","RD501_Particle_Effects","RD501_RDS","RD501_Units","RD501_Vehicles","RD501_Vehicle_Weapons","RD501_Weapons","RD501_Zeus","VenMK2")
$modPath = "$PSScriptRoot\..\..\addons"
$newModPath = (Resolve-Path (Join-Path $PSScriptRoot "..\..\Aux501")).Path
$succeded = @()
$failed = @()
a "|           Building the Old Republic starts here              |"
if($FullBuild)
{
    Remove-Item -Recurse -Force -Confirm:$false "P:\temp" -ErrorAction SilentlyContinue
}
if($True)
{
    ForEach ($mod in $mods)
    {
        $srcPath = $(Join-Path $modPath $mod)
        $target = "$output\addons\$mod.pbo"
        $LastModified = (Get-ChildItem $srcPath -File -Recurse | Sort-Object LastWriteTime | Select-Object -Last 1).LastWriteTime
        $exists = Test-Path $target
        if($exists){ $LastBuilt = (Get-ChildItem $target).LastWriteTime }
        if($exists -And -Not $FullBuild -And $LastBuilt -gt $LastModified)
        {
            Write-Verbose "$mod Up to Date, skipping."
        }
        else
        {
            a "==Building $mod=="
            $args = @()
            if(-Not $Pause){ $args += "-P" }
            $args += "-M=$output"
            $args += "P:\$mod"
            & "$pboProject" @args
            Wait-Process pboProject
            if(Test-Path $target)
            {
                a "  Successfully Built $mod."
                $succeded += $mod
            }
            else 
            {
                c "  $mod Build Failed."
                $failed += $mod
            }
        }
    }
}
a "|           Building the New Republic starts here              |"
#build the new aux mod
ForEach ($mod in (Get-ChildItem (Join-Path $PSScriptRoot "..\..\Aux501")))
{
    $srcPath = $(Join-Path $newModPath $mod)
    $target = "$output\addons\$mod.pbo"
    $LastModified = (Get-ChildItem $srcPath -File -Recurse | Sort-Object LastWriteTime | Select-Object -Last 1).LastWriteTime
    $exists = Test-Path $target
    if($exists){ $LastBuilt = (Get-ChildItem $target).LastWriteTime }
    if($exists -And -Not $FullBuild -And $LastBuilt -gt $LastModified)
    {
        Write-Verbose "$mod Up to Date, skipping."
    }
    else
    {
        a "  ==Building $mod==  "
        $args = @()
        if(-Not $Pause){ $args += "-P" }
        $args += "-M=$output"
        $args += "P:\Aux501\$mod"
        $args += "+C" #Clean temp for specific related files
        $args += "+W" #warnings are errors
        $args += "+S" #stop on error
        #$args += "+K" #Sign PBO's #cba to sort out a single pri key that gets handed out. 
        $args += "+Z" #Compress
        #$args += "+O" #Obfuscate
        $args += "+R" #Restore settings
        & "$pboProject" @args
        Wait-Process pboProject
        if(Test-Path $target)
        {
            a "  Successfully Built New Aux501\$mod.  "
            $succeded += "New Aux501\$mod"
        }
        else 
        {
            c "  New Aux501\$mod Build Failed.  "
            $failed += "New Aux501\$mod"
        }
    }
}


if($succeded.Count -ne 0){ a "Succeded: ", $succeded}
if($failed.Count -ne 0){ c "Failed: ", $failed}
if($succeded.Count -eq 0 -and $failed.Count -eq 0){ a "Nothing to build."}
a $blank
b $blank
a $blank
a "|        501st Aux Mod Monke-Friendly Text Ends Here:          |" 
a $blank
b $blank