param(
    [string]$arma,
    [string]$local,
    [string]$output,
    [string]$route
)

$fres = Get-WmiObject -query "select * from Win32_OptionalFeature where name = 'Client-ProjFS' and installstate = 1"
if ($null -eq $fres) {
    Write-Error "ProjFS is not enabled. Enable this to use CI on this sytem."
    exit 1
} else {
    Write-Output "Found ProjFS install."
}

# we need to mount the P drive.
$workDrive = Join-Path (Get-ItemProperty "HKCU:\Software\Bohemia Interactive\Arma 3 Tools").path "WorkDrive\WorkDrive.exe"
Start-Process -FilePath $workDrive -ArgumentList "/Mount"
Write-Output "Mounting P Drive."

# A rough wait for the mount command because stupid command line.
Start-Sleep 3

& "$PSScriptRoot\ci-booter.ps1" $arma $local $output $route

$buildFolders = @("RD501_3DEN","RD501_Compositions","RD501_Droids","RD501_Droid_Dispenser","RD501_EMP","RD501_Helmets","RD501_Jumppack","RD501_Main","RD501_Particle_Effects","RD501_RDS","RD501_Units","RD501_Vehicles","RD501_Vehicle_Weapons","RD501_Weapons","RD501_Zeus","VenMK2")
foreach($folder in $buildFolders) {
    if(-Not $(Test-Path $(Join-Path "P:\" $folder))) {
        $addonPath = $(Join-Path $PSScriptRoot "..\..\addons")
        function SymLink
        {
            param([string]$source, [string]$dest)
            Write-Verbose("Creating SymLink: $source > $destination")
            New-Item -ItemType SymbolicLink -Path $dest -Target $source -Force
        }
        if(Test-Path 'P:') #if P Drive Mounted, create symlinks and build from there. 
        {
            foreach($addonFolder in Get-ChildItem -Directory $addonPath)
            {
                SymLink -source "$addonPath\$addonFolder" -dest "P:\$addonFolder"
            }
        }

        break
    }
}

Stop-Process -Name "WorkDrive"

$buildPbosPath = (Resolve-Path "$PSScriptRoot\..\shared\build-pbos.ps1").Path
& $buildPbosPath -FullBuild
