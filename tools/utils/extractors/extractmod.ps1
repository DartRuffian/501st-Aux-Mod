Write-Output "Starting..."

$path = Join-Path $(Get-Location) "addons"

Write-Output $path

$pbos = Get-ChildItem $path -Filter *.pbo
ForEach ($pbo in $pbos)
{
    & ((Get-ItemProperty HKCU:\Software\Mikero\ExtractPbo).path + "\bin\ExtractPbo.exe") "-P" $pbo.FullName "out" | Out-Null
}

Write-Output "Finished."