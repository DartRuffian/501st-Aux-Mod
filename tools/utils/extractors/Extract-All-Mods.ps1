$mods = @(
    "@3AS (Beta Test)",
    "@ace",
    "@CBA_A3",
    "@Legion Base - Stable",
    "@Just Like The Simulations - The Great War",
    "@Kobra Mod Pack - Main",
    "@Operation TREBUCHET",
    "@WebKnight Droids",
    "@DBA CIS",
    "@DBA Core",
    "@Black Legion Imperial Assets Mod",
    "@StarForge Armory",
    "@GARC Asset Pack"
)

Function Get-FolderName($initialDirectory) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    $OpenFolderDialog = New-Object System.Windows.Forms.FolderBrowserDialog
    $Topmost = New-Object System.Windows.Forms.Form
    $Topmost.TopMost = $True
    $Topmost.MinimizeBox = $True
    $OpenFolderDialog.ShowDialog($Topmost) | Out-Null
    return $OpenFolderDialog.SelectedPath
}

$arma = $(Join-Path $(Get-ItemProperty 'HKCU:\SOFTWARE\Valve\Steam').SteamPath "/steamapps/common/Arma 3")
if(-Not $(Test-Path $arma)) { $arma = Get-FolderName }
if(-Not $(Test-Path $(Join-Path $arma "arma3.exe"))) {
    Write-Output "Arma not found, exiting."
    Exit 0 
}

$stopwatch = [System.Diagnostics.Stopwatch]::StartNew()

# Mods that break when run with Extract PBO DOS
ForEach ($mod in $mods)
{
    $path = $(Join-Path $arma "!Workshop/$mod/addons")
    
    $pbos = Get-ChildItem $path -Filter *.pbo
    ForEach ($pbo in $pbos)
    {
        & ((Get-ItemProperty HKCU:\Software\Mikero\ExtractPbo).path + "\bin\ExtractPbo.exe") "-P" $pbo.FullName "P:\out" | Out-Null
    }
    
    robocopy /e "P:\out" "P:\" /r:5 /w:5
    Remove-Item -Recurse -Force -Confirm:$false "P:\out"
}

Expand-Archive -Force "$PSScriptRoot\dummy-mods.zip" "P:\"

$stopwatch.Stop()

Write-Output $stopwatch