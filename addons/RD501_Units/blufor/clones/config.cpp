#include "../../config_macros.hpp"

#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_clones
    {
        addonRootClass=RD501_patch_units;
        requiredAddons[]=
        {
            RD501_patch_units
        };
        requiredVersion=0.1;
        units[]=
        {
        
        };
        weapons[]=
        {
            
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;
    class UniformItem;
    class V_RebreatherB;
    class VestItem;


    //uniforms
    #include "_uniforms.hpp"
    #include "_headwear.hpp"

    class JLTS_clone_comlink;
    class rd501_JLTS_clone_comlink: JLTS_clone_comlink
    {
        tf_range=5000;
        displayName="[501st] Clone Comlink";
    };

    class JLTS_Clone_ARC_backpack;
    class rd501_JLTS_ARC_backpack_noLR: JLTS_Clone_ARC_backpack
    {
        f_hasLRradio = 0;
        displayName = "[501st] ARC Backpack no LR";
    };
    class lsd_gar_marine_backpack;	
    class rd501_LS_snow_backpack: lsd_gar_marine_backpack
    {	
        maximumload = 700;
        Displayname = "[501st] Snow Pack";
    };
    //chest
    
    class macro_new_vest_class(501_inf,acc_9): V_RebreatherB
    {	
        author = "RD501";
        scope = 2;
        side = 3;
        displayName = "[501st] INF ACC 10 (First Lt.)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\1st_lt_pauldron.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat",
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer_cloth.rvmat"
        };
        class ItemInfo: VestItem
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            modelsides[] = {6};
            hiddenSelections[] = {"Camo1"};
            macro_rebreather_armor_stuff
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat",
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer_cloth.rvmat"
            };
            class HitpointsProtectionInfo
            {
                class Diaphragm
                {
                    hitpointName = "HitDiaphragm";
                    armor = 7500;
                    passThrough = 0.4;
                };
                class Chest
                {
                    hitpointName = "HitChest";
                    armor = 7000;
                    passThrough = 0.4;
                };
                class Abdomen
                {
                    hitpointName = "HitAbdomen";
                    armor = 5000;
                    passThrough = 0.4;
                };
                class Pelvis
                {
                    hitpointName = "HitPelvis";
                    armor = 6700;
                    passThrough = 0.4;
                };
                class Neck
                {
                    hitpointName = "HitNeck";
                    armor = 1000;
                    passThrough = 0.2;
                };
                class Arms
                {
                    hitpointName = "HitArms";
                    armor = 4000;
                    passThrough = 0.2;
                };
                class Body
                {
                    armor = 4000;
                    hitpointName = "HitBody";
                    passThrough = 0.4;
                };
            };
        }; 
    };
    class macro_new_vest_class(501_inf,acc_9_v2): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 10 (First Lt. V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };	
    class macro_new_vest_class(501_inf,acc_8): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 09 (Second Lt.)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\2nd_lt_pauldron.paa};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_8_v2): macro_new_vest_class(501_inf,acc_8)
    {
        displayName = "[501st] INF ACC 09 (Second Lt. V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };	
    class macro_new_vest_class(501_inf,acc_12): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 06 (Sgt. Major)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\sgtmajor_pauldron.paa};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_12_v2): macro_new_vest_class(501_inf,acc_12)
    {
        displayName = "[501st] INF ACC 06 (Sgt. Major V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };	
    class macro_new_vest_class(501_inf,acc_6): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 07 (Sgt. Major [C])";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\comp_ncoic_pauldron.paa};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_6_v2): macro_new_vest_class(501_inf,acc_6)
    {
        displayName = "[501st] INF ACC 07 (Sgt. Major [C] V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };	
    class macro_new_vest_class(501_inf,acc_7): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 08 (Sgt. Major [B])";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\bn_ncoic_pauldron.paa};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_7_v2): macro_new_vest_class(501_inf,acc_7)
    {
        displayName = "[501st] INF ACC 08 (Sgt. Major [B] V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_inf,acc_10): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 11 (Captain)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\cpt_pauldron.paa};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_10_v2): macro_new_vest_class(501_inf,acc_10)
    {
        displayName = "[501st] INF ACC 11 (Captain V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_inf,acc_11): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 12 (Major)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\maj_pauldron.paa};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_11_v2): macro_new_vest_class(501_inf,acc_11)
    {
        displayName = "[501st] INF ACC 12 (Major V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_inf,acc_3): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 03 (Sr. Corporal)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\sr_nco_acc.paa};
        hiddenSelectionsMaterials[]=
        {
        
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo2"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_3_b): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 03 Medic (Sr. Corporal)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\inf_medic_nco.paa};
        hiddenSelectionsMaterials[]=
        {
        
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo2"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_3_c): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 03 RTO (Sr. Corporal)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\inf_rto_nco.paa};
        hiddenSelectionsMaterials[]=
        {
        
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo2"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_1): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 A (Vet. Trooper)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        hiddenSelectionsMaterials[]=
        {
        
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo2"};
            hiddenSelectionsMaterials[]=
            {
            
            };
        };
    };
    class RD501_501_inf_vest_acc_1_clc: macro_new_vest_class(501_inf,acc_1)
    {
        displayName = "[501st] INF ACC 01 A (CLC)";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\lance_cp_acc.paa};
    };
    class RD501_501_inf_vest_acc_1_med: RD501_501_inf_vest_acc_1_clc
    {
        displayName = "[501st] INF ACC 01 A (CM-L)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\lance_cp_med_acc.paa};
    };
    class RD501_501_inf_vest_acc_1_rto: RD501_501_inf_vest_acc_1_clc
    {
        displayName = "[501st] INF ACC 01 A (CI-L)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\lance_cp_rto_acc.paa};
    };
    class macro_new_vest_class(501_inf,acc_1_b): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 B (Vet. Trooper)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[]=
        {
            "camo1"
        };
        hiddenSelectionsTextures[]=
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_co.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_cloth.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
            containerClass="Supply100";
            hiddenSelectionsMaterials[]=
            {
                "\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_cloth.rvmat"
            };
        };
    };
    class JLTS_CloneVestAirborne;
    class macro_new_vest_class(501_inf,acc_1_c): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 C (Vet. Trooper)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]=
        {
            "camo1",
            "camo2"
        };
        hiddenSelectionsTextures[] = 
        {
            "", TEXTUREPATH\Republic\clones\Infantry\acc\trooper_heavy_acc.paa
        };
        hiddenSelectionsMaterials[]=
        {
        ""
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_1_c_medic): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 C (Medic)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]=
        {
            "camo1",
            "camo2"
        };
        hiddenSelectionsTextures[] = {"",TEXTUREPATH\Republic\clones\Infantry\acc\medic_heavy_acc.paa};
        hiddenSelectionsMaterials[]= {""};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_1_c_rto): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 C (RTO)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]=
        {
            "camo1",
            "camo2"
        };
        hiddenSelectionsTextures[] = {"",TEXTUREPATH\Republic\clones\Infantry\acc\rto_heavy_acc.paa};
        hiddenSelectionsMaterials[]=
        {
        ""
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_1_d): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 D (Holster w/Pad)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_cfr_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_cfr_armor.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\heavy_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_clones\SWLB_clone_cfr_armor.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1"};
        };
    };
    class macro_new_vest_class(501_inf,acc_1_e): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 E (Holster w/Pouch)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_airborne_armor_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_ARF_Vest.p3d";
        hiddenSelections[] = {"camo1","camo2","camo3"};
        hiddenSelectionsTextures[] = {"SWLB_clones\data\light_accessories_co.paa","SWLB_clones\data\heavy_accessories_co.paa","SWLB_clones\data\officer_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\light_accessories.rvmat","\SWLB_clones\data\heavy_accessories.rvmat","\SWLB_clones\data\officer_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_CEE\data\SWLB_CEE_ARF_Vest.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1","camo2","camo3"};
        };
    };
    class macro_new_vest_class(501_inf,acc_1_f): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 F (Suspender Pouches)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_light_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_specialist_armor.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\light_accessories_co.paa","\SWLB_clones\data\heavy_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\light_accessories.rvmat","\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_clones\SWLB_clone_specialist_armor.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1","camo2"};
        };
    };
    class macro_new_vest_class(501_inf,acc_1_g): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 G (Suspender 'Nades)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_light_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_grenadier_armor.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\light_accessories_co.paa","\SWLB_clones\data\heavy_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\light_accessories.rvmat","\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_clones\SWLB_clone_grenadier_armor.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1","camo2"};
        };
    };
    class macro_new_vest_class(501_inf,acc_1_h): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 01 H (Medical Bag)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_medic_armor.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\heavy_accessories_medic_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_clones\SWLB_clone_medic_armor.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1"};
        };
    };
    class macro_new_vest_class(501_inf,acc_2): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 02 A (Corporal)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        hiddenSelectionsMaterials[]=
        {
        
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            containerClass="Supply100";
            hiddenSelectionsMaterials[]=
            {
            
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_2_b): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 02 B (Chest Holster)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_light_armor_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Force_Recon.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"SWLB_clones\data\heavy_accessories_co.paa","SWLB_clones\data\light_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_CEE\data\SWLB_CEE_Force_Recon.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1","camo2"};
        };
    };
    class macro_new_vest_class(501_inf,acc_2_c): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 02 C (Leg Holster)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_recon_nco_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_recon_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","holster","pauldron"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\heavy_accessories_co.paa","","\SWLB_clones\data\heavy_accessories_co.paa","\SWLB_clones\data\heavy_accessories_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\light_accessories.rvmat","\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_clones\SWLB_clone_recon_armor.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1","camo2","holster","pauldron"};
        };
    };
    class macro_new_vest_class(501_inf,acc_2_d): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 02 D (Heavy Bag)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_recon_nco_armor_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Heavy_Vest.p3d";
        hiddenSelections[] = {"ammo","camo1","camo2","camo3","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "SWLB_clones\data\heavy_accessories_co.paa",
            "SWLB_clones\data\heavy_accessories_co.paa",
            "SWLB_clones\data\heavy_accessories_co.paa",
            "SWLB_clones\data\light_accessories_co.paa",
            "SWLB_clones\data\heavy_accessories_co.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "\SWLB_clones\data\light_accessories.rvmat","\SWLB_clones\data\heavy_accessories.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\SWLB_CEE\data\SWLB_CEE_Heavy_Vest.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"ammo","camo1","camo2","camo3","pauldron"};
        };
    };
    class macro_new_vest_class(501_inf,acc_4): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC 04 (Sergeant)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
        hiddenSelections[] = {"Camo1","camo2"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa", "MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
            containerClass="Supply100";
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_5): macro_new_vest_class(501_inf,acc_4)
    {
        displayName = "[501st] INF ACC 05 (Sr. Sgt)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
        hiddenSelections[] = {"Camo1","camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\1st_lt_pauldron.paa, TEXTUREPATH\Republic\clones\Infantry\acc\sr_nco_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            hiddenSelections[]= {"camo1","camo2"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
            containerClass="Supply100";
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_5_b): macro_new_vest_class(501_inf,acc_5)
    {
        displayName = "[501st] INF ACC 05 Medic (Sr. Sgt)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\bn_medic_pauldron.paa, TEXTUREPATH\Republic\clones\Infantry\acc\inf_medic_nco.paa};
    };
    class macro_new_vest_class(501_inf,acc_5_c): macro_new_vest_class(501_inf,acc_5)
    {
        displayName = "[501st] INF ACC 05 RTO (Sr. Sgt)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\bn_rto_pauldron.paa, TEXTUREPATH\Republic\clones\Infantry\acc\inf_rto_nco.paa};
    };
    class macro_new_vest_class(501_inf,acc_r1): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC RTO (NCOIC)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\bn_rto_pauldron.paa};
    };
    class macro_new_vest_class(501_inf,acc_r1_v2): macro_new_vest_class(501_inf,acc_r1)
    {
        displayName = "[501st] INF ACC RTO (NCOIC) V2";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };	
    class macro_new_vest_class(501_inf,acc_m1): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] INF ACC Medic (NCOIC)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\bn_medic_pauldron.paa};
    };
    class macro_new_vest_class(501_inf,acc_m1_v2): macro_new_vest_class(501_inf,acc_m1)
    {
        displayName = "[501st] INF ACC Medic (NCOIC) V2";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    //
    class macro_new_vest_class(501_ab,acc_1): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 01 (Cadet)";
        picture= "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model= "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CT_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_cpl_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_2): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 02 (Trooper)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model= "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CT_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_cpl_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat",
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_2_1): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 02 (Vt. Trooper)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_trooper_vt_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_2_2): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB RTO ACC 02 (Vt. Trooper)";
        scope = 0;
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_rto_vt_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat",
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_2_3): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB MED ACC 02 (Vt. Trooper)";
        scope = 0;
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_medic_vt_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_3): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 03 (Corporal)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_cpl_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat",
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_4): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 04 (Sergeant)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CS_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\heavy_accessories_co.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_5): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 05 (Sgt. Major)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CSM_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_5_v2): macro_new_vest_class(501_ab,acc_5)
    {
        displayName = "[501st] AB ACC 05 (Sgt. Major V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_ab,acc_6): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 07 (Second Lt.)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CC1_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_6_v2): macro_new_vest_class(501_ab,acc_6)
    {
        displayName = "[501st] AB ACC 07 (Second Lt. V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_ab,acc_7): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB ACC 08 (First Lt.)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CC2_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_7_v2): macro_new_vest_class(501_ab,acc_7)
    {
        displayName = "[501st] AB ACC 08 (First Lt. V2)";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_ab,acc_9): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB RTO ACC 01 (Corporal)";
        scope = 0;
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_rto_acc1.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat",
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_ab,acc_10): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AB MED ACC 01 (OLD)";
        scope = 0;
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\acc\ab_sgt_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\ab_medic_acc1.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };

    class macro_new_vest_class(501_ab,med_cadet): macro_new_vest_class(501_ab,acc_2)
    {
        displayName = "[501st] AB MED ACC (Cadet)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CT_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_MED_Cadet_vest.paa};
        class ItemInfo:ItemInfo {};
    };
    
    class macro_new_vest_class(501_ab,med_trooper): macro_new_vest_class(501_ab,acc_2)
    {
        displayName = "[501st] AB MED ACC (Medic)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CT_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_MED_vest.paa};
        class ItemInfo:ItemInfo {};
    };

    class macro_new_vest_class(501_ab,med_tech): macro_new_vest_class(501_ab,acc_2)
    {
        displayName = "[501st] AB MED ACC (Technician)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_MED_Tech_vest.paa};
        class ItemInfo:ItemInfo {};
    };

    class macro_new_vest_class(501_ab,med_cpl): macro_new_vest_class(501_ab,acc_3)
    {
        displayName = "[501st] AB MED ACC (Corporal)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_MED_CP_vest.paa};
        class ItemInfo:ItemInfo {};
    };
    class macro_new_vest_class(501_ab,rto_cadet): macro_new_vest_class(501_ab,acc_2)
    {
        displayName = "[501st] AB RTO ACC (Cadet)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CT_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_RTO_Cadet_vest.paa};
        class ItemInfo:ItemInfo {};
    };
    class macro_new_vest_class(501_ab,rto): macro_new_vest_class(501_ab,acc_2)
    {
        displayName = "[501st] AB RTO ACC (RTO)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CT_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_RTO_vest.paa};
        class ItemInfo:ItemInfo {};
    };
    class macro_new_vest_class(501_ab,rto_tech): macro_new_vest_class(501_ab,acc_2)
    {
        displayName = "[501st] AB RTO ACC (Technician)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_RTO_Tech_vest.paa};
        class ItemInfo:ItemInfo {};
    };
    class macro_new_vest_class(501_ab,rto_cpl): macro_new_vest_class(501_ab,acc_3)
    {
        displayName = "[501st] AB RTO ACC (Corporal)";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\ab\kamas\501_Ack_CP_acc.paa, TEXTUREPATH\Republic\clones\ab\acc\AB_RTO_CP_vest.paa};
        class ItemInfo:ItemInfo {};
    };
    //Aviation
    class macro_new_vest_class(501_avi,acc_1): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AVI ACC 01 (Flight Officer)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        hiddenSelectionsMaterials[]=
        {
        
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
            
            };
        };
    };
    class macro_new_vest_class(501_avi,acc_2): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AVI ACC 02 (Jr. Lieutenant)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa","MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1",
                "camo2"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_avi,acc_3): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AVI ACC 03 (Second Lieutenant)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_cx1_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_avi,acc_3_v2): macro_new_vest_class(501_avi,acc_3)
    {
        displayName = "[501st] AVI ACC 03 (Second Lieutenant) V2";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_avi,acc_4): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AVI ACC 04 (First Lieutenant)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_cx2_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_avi,acc_4_v2): macro_new_vest_class(501_avi,acc_4)
    {
        displayName = "[501st] AVI ACC 04 (First Lieutenant) V2";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_avi,acc_5): macro_new_vest_class(501_inf,acc_9)
    {
        displayName = "[501st] AVI ACC 05 (Captain)";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_cx_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_avi,acc_5_v2): macro_new_vest_class(501_avi,acc_5)
    {
        displayName = "[501st] AVI ACC 05 (Captain) V2";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
    };
    class macro_new_vest_class(501_avi,acc_6): macro_new_vest_class(501_inf,acc_9)
    {
        displayname = "[501st] AVI ACC 06 P3 (CX-C)";
        picture="";
        model = "3as\3as_Characters\clones\uniforms\Model\3AS_Pilot_Vest.p3d";
        hiddenSelections[] = {"Camo"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_p3_cxc.paa};
        class ItemInfo: ItemInfo
        {
            uniformmodel="3as\3as_Characters\clones\uniforms\Model\3AS_Pilot_Vest.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "Camo"
            };
        };
    };
    class macro_new_vest_class(501_avi,acc_7): macro_new_vest_class(501_avi,acc_6)
    {
        displayname = "[501st] AVI ACC 07 P3 (CX-X)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_p3_cxx.paa};
    };
    class macro_new_vest_class(501_avi,acc_8): macro_new_vest_class(501_avi,acc_6)
    {
        displayname = "[501st] AVI ACC 08 P3 (Sr. CX-X)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_p3_cxxx.paa};
    };
    class macro_new_vest_class(501_avi,acc_9): macro_new_vest_class(501_avi,acc_6)
    {
        displayname = "[501st] AVI ACC 09 P3 (Sr. CX-P)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_p3_cxp.paa};
    };
    class macro_new_vest_class(501_avi,acc_10): macro_new_vest_class(501_avi,acc_6)
    {
        displayname = "[501st] AVI ACC 10 P3 (Sr. CX-S)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_p3_cxs.paa};
    };
    class macro_new_vest_class(501_avi,acc_111): macro_new_vest_class(501_avi,acc_6)
    {
        displayname = "[501st] AVI ACC 11 P3 (Sr. CX)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\acc\pilot_p3_cx.paa};
    };
    // ARC
    class macro_new_vest_class(501_arc,acc_0): macro_new_vest_class(501_inf,acc_9)
    {
        displayname = "[501st] ARC ACC 00";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestARC_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestARCCadet.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_arc_co.paa"};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_arc.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel="\MRC\JLTS\characters\CloneArmor\CloneVestARCCadet.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_arc.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_arc,acc_1_A): macro_new_vest_class(501_inf,acc_9)
    {
        displayname = "[501st] ARC ACC 01 A";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestARC_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestARC.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_01_A.paa};
        hiddenSelectionsMaterials[]=
        {
            "\RD501_Units\textures\Republic\clones\mats\clone_vest_arc.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneVestARC.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_arc.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_arc,acc_1_B): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 01 B";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_01_B.paa};
    };
    class macro_new_vest_class(501_arc,acc_1_C): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 01 C";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_01_C.paa};
    };
    class macro_new_vest_class(501_arc,acc_1_D): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 01 D";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_01_D.paa};
    };
    class macro_new_vest_class(501_arc,acc_1_E): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 01 E";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_01_E.paa};
    };

    class macro_new_vest_class(501_arc,acc_2_A): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 02 A";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_02_A.paa};
    };
    class macro_new_vest_class(501_arc,acc_2_B): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 02 B";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_02_B.paa};
    };
    class macro_new_vest_class(501_arc,acc_2_C): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 02 C";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_02_C.paa};
    };
    class macro_new_vest_class(501_arc,acc_2_D): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 02 D";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_02_D.paa};
    };
    class macro_new_vest_class(501_arc,acc_2_E): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 02 E";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_02_E.paa};
    };
    class macro_new_vest_class(501_arc,acc_3_A): macro_new_vest_class(501_arc,acc_1_A)
    {

        displayname= "[501st] ARC ACC 03 A";
        hiddenSelectionsTextures[]= {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_03_A.paa};
    };
    class macro_new_vest_class(501_arc,acc_3_B): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 03 B";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_03_B.paa};
    };
    class macro_new_vest_class(501_arc,acc_3_C): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 03 C";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_03_C.paa};
    };
    class macro_new_vest_class(501_arc,acc_3_D): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 03 D";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_03_D.paa};
    };
    class macro_new_vest_class(501_arc,acc_3_E): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 03 E";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_03_E.paa};
    };
    class macro_new_vest_class(501_arc,acc_4_A): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 04 A";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_04_A.paa};
    };
    class macro_new_vest_class(501_arc,acc_4_B): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 04 B";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_04_B.paa};
    };
    class macro_new_vest_class(501_arc,acc_4_C): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 04 C";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_04_C.paa};
    };
    class macro_new_vest_class(501_arc,acc_4_D): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 04 D";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_04_D.paa};
    };
    class macro_new_vest_class(501_arc,acc_4_E): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 04 E";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_04_E.paa};
    };
    class macro_new_vest_class(501_arc,acc_5_A): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 05 A";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_05_A.paa};
    };
    class macro_new_vest_class(501_arc,acc_5_B): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 05 B";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_05_B.paa};
    };
    class macro_new_vest_class(501_arc,acc_5_C): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 05 C";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_05_C.paa};
    };
    class macro_new_vest_class(501_arc,acc_5_D): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 05 D";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_05_D.paa};
    };
    class macro_new_vest_class(501_arc,acc_5_E): macro_new_vest_class(501_arc,acc_1_A)
    {
        displayname= "[501st] ARC ACC 05 E";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\acc\arc_acc_05_E.paa};
    };
    class macro_new_vest_class(501_arc,acc_p1_01): macro_new_vest_class(501_inf,acc_9_v2)
    {
        displayname = "[501st] ARC ACC P1 01";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\acc\p1_arc_trooper_pauldron.paa};
    };
    class macro_new_vest_class(501_arc,acc_p1_02): macro_new_vest_class(501_inf,acc_9_v2)
    {
        displayname= "[501st] ARC ACC P1 02";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\acc\p1_arc_nco_pauldron.paa};
    };
    //
    class macro_new_vest_class(501_rg,acc_1): macro_new_vest_class(501_inf,acc_9)
    {
        displayname= "[Empire] Royal Guard ACC";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneVestKama.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\gcw\empire\rg\acc\rg_acc.paa};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneVestKama.p3d";
            containerClass="Supply100";
            hiddenSelections[]=
            {
                "camo1"
            };
            hiddenSelectionsMaterials[]=
            {
                "RD501_Units\textures\Republic\clones\mats\clone_vest_officer.rvmat"
            };
        };
    };
    class macro_new_vest_class(501_inf,acc_13): macro_new_vest_class(501_rg,acc_1)
    {
        displayname= "[501st] INF ACC 13 (Kama)";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\acc\2nd_lt_pauldron.paa};
    };
    //class macro_new_vest_class(501_inf,acc_snow_01): macro_new_vest_class(501_inf,acc_9)
    //{
        //displayName= "[501st] INF SNOW ACC 01 (Trooper)";
        //picture= "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        //model= "armor_unit\21\21st_Vest_V2.p3d";
        //hiddenSelections[] = {"camo1"};
        //hiddenSelectionsMaterials[] = {"armor_unit\21\vest\clothv2.rvmat"};
        //hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\acc\snow_trooper_vest.paa};
        //class ItemInfo: ItemInfo
        //{
            //uniformModel = "\armor_unit\21\21st_Vest_V2.p3d";
        //};
    //};
    //class macro_new_vest_class(501_inf,acc_snow_02): macro_new_vest_class(501_inf,acc_snow_01)
    //{
        //displayName= "[501st] INF SNOW ACC 02 (NCO)";
        //hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\acc\snow_nco_vest.paa};
    //};
    //
    class macro_new_vest_class(501_invis,vest): macro_new_vest_class(501_inf,acc_9)
    {
        displayname= "[501st] Nanoweave Under Armour";
        model="RD501_Units\nothing.p3d";
        hiddenSelections[] = {""};
        hiddenSelectionsTextures[] = {""};
        hiddenSelectionsMaterials[]={""};
        class ItemInfo: ItemInfo
        {
            uniformModel="RD501_Units\nothing.p3d";
            containerClass="Supply100";
            hiddenSelections[]={""};
            hiddenSelectionsMaterials[]={""};
        };
    };
    class macro_new_vest_class(501_holster,vest): macro_new_vest_class(501_inf,acc_9)
    {
        displayname= "[501st] Sidearm Holster";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneVestHolster_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneVestHolster.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneVestHolster.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1"};
        };
    };
    class macro_new_vest_class(501_razorblade_holster,vest): macro_new_vest_class(501_inf,acc_9)
    {
        displayname = "[501st] Razorblade Holster";
        picture="\RD501_Units\textures\republic\clones\avi\acc\razorblade_holster_pic.paa";
        model="\RD501_Units\models\razorblade_holster.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Units\textures\republic\clones\avi\acc\razorblade_holster_co.paa"};
        hiddenSelectionsMaterials[]= {"\RD501_Units\materials\razorblade_holster.rvmat"};
        class ItemInfo: ItemInfo
        {
            uniformModel="\RD501_Units\models\razorblade_holster.p3d";
            containerClass="Supply100";
            hiddenSelections[] = {"camo1"};
            hiddenSelectionsTextures[] = {"\RD501_Units\textures\republic\clones\avi\acc\razorblade_holster_co.paa"};
            hiddenSelectionsMaterials[]= {"\RD501_Units\materials\razorblade_holster.rvmat"};
        };
    };
};
class DefaultEventhandlers;
class CfgVehicles
{
    class B_Soldier_base_f;
    class B_Soldier_f: B_Soldier_base_F
    {
        class HitPoints;
    };

    class O_Soldier_base_F;
    ///Infantry///
    class macro_new_uniform_skin_class(501_inf,recruit): B_Soldier_f
    {
        author = "RD501";
        scope = 1;
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmor.p3d";
        nakedUniform = "U_BasicBody";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_uniform_ca.paa";
        hiddenSelections[] = {"Camo1","Camo2","insignia"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\recruit_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\recruit_armor_lower.paa};
        hiddenSelectionsMaterials[]=
        {
            "MRC\JLTS\characters\CloneArmor\data\clone_armor1_clean.rvmat",
            "MRC\JLTS\characters\CloneArmor\data\clone_armor2_clean.rvmat",
            "MRC\JLTS\Core_mod\data\insignias\insignia_CloneArmor.rvmat"
        };
        uniformClass= macro_new_uniform_class(501_inf,recruit);
        class HitPoints: HitPoints
        {
            class HitFace
            {
                armor=1;
                material=-1;
                name="face_hub";
                passThrough=0.80000001;
                radius=0.079999998;
                explosionShielding=0.1;
                minimalHit=0.0099999998;
            };
            class HitNeck: HitFace
            {
                armor=1;
                material=-1;
                name="neck";
                passThrough=0.80000001;
                radius=0.1;
                explosionShielding=0.5;
                minimalHit=0.0099999998;
            };
            class HitHead: HitNeck
            {
                armor=1;
                material=-1;
                name="head";
                passThrough=0.80000001;
                radius=0.2;
                explosionShielding=0.5;
                minimalHit=0.0099999998;
                depends="HitFace max HitNeck";
            };
            class HitPelvis: HitHead
            {
                armor=8;
                material=-1;
                name="pelvis";
                passThrough=0.80000001;
                radius=0.23999999;
                explosionShielding=3;
                visual="injury_body";
                minimalHit=0.0099999998;
                depends="";
            };
            class HitAbdomen: HitPelvis
            {
                armor=6;
                material=-1;
                name="spine1";
                passThrough=0.80000001;
                radius=0.16;
                explosionShielding=3;
                visual="injury_body";
                minimalHit=0.0099999998;
            };
            class HitDiaphragm: HitAbdomen
            {
                armor=6;
                material=-1;
                name="spine2";
                passThrough=0.33000001;
                radius=0.18000001;
                explosionShielding=6;
                visual="injury_body";
                minimalHit=0.0099999998;
            };
            class HitChest: HitDiaphragm
            {
                armor=8;
                material=-1;
                name="spine3";
                passThrough="0.33000001radius = 0.18";
                explosionShielding=6;
                visual="injury_body";
                minimalHit=0.0099999998;
            };
            class HitBody: HitChest
            {
                armor=1000;
                material=-1;
                name="body";
                passThrough=1;
                radius=0;
                explosionShielding=6;
                visual="injury_body";
                minimalHit=0.0099999998;
                depends="HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
            };
            class HitArms: HitBody
            {
                armor=6;
                material=-1;
                name="arms";
                passThrough=1;
                radius=0.1;
                explosionShielding=3;
                visual="injury_hands";
                minimalHit=0.0099999998;
                depends="0";
            };
            class HitHands: HitArms
            {
                armor=6;
                material=-1;
                name="hands";
                passThrough=1;
                radius=0.1;
                explosionShielding=1;
                visual="injury_hands";
                minimalHit=0.0099999998;
                depends="HitArms";
            };
            class HitLegs: HitHands
            {
                armor=6;
                material=-1;
                name="legs";
                passThrough=1;
                radius=0.14;
                explosionShielding=3;
                visual="injury_legs";
                minimalHit=0.0099999998;
                depends="0";
            };
            class Incapacitated: HitLegs
            {
                armor=1000;
                material=-1;
                name="body";
                passThrough=1;
                radius=0;
                explosionShielding=3;
                visual="";
                minimalHit=0;
                depends="(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
            };
            class HitLeftArm
            {
                armor=6;
                material=-1;
                name="hand_l";
                passThrough=1;
                radius=0.079999998;
                explosionShielding=3;
                visual="injury_hands";
                minimalHit=0.0099999998;
            };
            class HitRightArm: HitLeftArm
            {
                name="hand_r";
            };
            class HitLeftLeg
            {
                armor=6;
                material=-1;
                name="leg_l";
                passThrough=1;
                radius=0.1;
                explosionShielding=3;
                visual="injury_legs";
                minimalHit=0.0099999998;
            };
            class HitRightLeg: HitLeftLeg
            {
                name="leg_r";
            };
        };
        armor=2;
        armorStructural=4;
        explosionShielding=40.40000001;
        minTotalDamageThreshold=0.001;
        impactDamageMultiplier=0.5;
    };
    class macro_new_uniform_skin_class(501_inf,cadet): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\Cadet_Armor_Upper.paa,TEXTUREPATH\Republic\clones\Infantry\Cadet_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,cadet): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\Cadet_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\Cadet_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,cadet): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\Cadet_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\Cadet_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,trooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\trooper_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\trooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,trooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\trooper_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\trooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,trooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\trooper_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\trooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class RD501_501_inf_uniform_clc: macro_new_uniform_skin_class(501_inf,recruit)
    {
        author = "RD501";
        scope = 1;
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmor.p3d";
        nakedUniform = "U_BasicBody";
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\clc_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\clc_armor_lower.paa};
    };
    class RD501_501_inf_uniform_clc_medic: RD501_501_inf_uniform_clc
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\clc_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\clc_medic_armor_lower.paa};
    };
    class RD501_501_inf_uniform_clc_rto: RD501_501_inf_uniform_clc
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\clc_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\clc_rto_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,nco): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\nco_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,nco): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\nco_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,nco): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\nco_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,sr_cp): macro_new_uniform_skin_class(501_inf,recruit)
     {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,sr_cp): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_medic_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,sr_cp): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_rto_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cs_nco_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cs_nco_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_medic_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cs_nco_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cp_nco_rto_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,sr_cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\sr_cs_nco_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cs_nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,sr_cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\sr_cs_nco_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cs_nco_medic_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,sr_cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\sr_cs_nco_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\sr_cs_nco_rto_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,csm): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\csm_nco_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\csm_nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,csm_comp): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\comp_ncoic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\comp_ncoic_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,csm_bn): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\bn_ncoic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\bn_ncoic_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,csm): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\bn_ncoic_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\bn_ncoic_medic_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,csm): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\bn_ncoic_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\bn_ncoic_rto_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,2nd_lt): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\2nd_lt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\2nd_lt_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,1st_lt): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\1st_lt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\1st_lt_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,cpt): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cpt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\cpt_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,maj): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\maj_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\maj_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,2nd_lt_alt): macro_new_uniform_skin_class(501_inf,recruit)
    {
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmorMC.p3d";
        hiddenSelections[] = {"Camo1","Camo2","Camo3"};
        hiddenSelectionsMaterials[] = {"MRC\JLTS\characters\CloneArmor\data\clone_armor1_mc.rvmat", "MRC\JLTS\characters\CloneArmor\data\clone_armor2_clean.rvmat"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\2nd_lt_alt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\2nd_lt_armor_lower.paa, TEXTUREPATH\Republic\clones\Infantry\acc\2nd_lt_chest_rank.paa};
    };
    class macro_new_uniform_skin_class(501_inf,1st_lt_alt): macro_new_uniform_skin_class(501_inf,2nd_lt_alt)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\1st_lt_alt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\1st_lt_armor_lower.paa, TEXTUREPATH\Republic\clones\Infantry\acc\1st_lt_chest_rank.paa};
    };
    class macro_new_uniform_skin_class(501_inf,cpt_alt): macro_new_uniform_skin_class(501_inf,2nd_lt_alt)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cpt_alt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\cpt_armor_lower.paa, TEXTUREPATH\Republic\clones\Infantry\acc\cpt_chest_rank.paa};
    };
    class macro_new_uniform_skin_class(501_inf,maj_alt): macro_new_uniform_skin_class(501_inf,2nd_lt_alt)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\maj_alt_armor_upper.paa, TEXTUREPATH\Republic\clones\Infantry\maj_armor_lower.paa, TEXTUREPATH\Republic\clones\Infantry\acc\maj_chest_rank.paa};
    };
    class macro_new_uniform_skin_class(501_inf,phase1_nco): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\p1_nco_armor_upper.paa, TEXTUREPATH\Republic\clones\auxillary\p1_nco_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,phase1_sergeant): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_sgt_armor_upper.paa"};
    };
    class macro_new_uniform_skin_class(501_inf,phase1_commander): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_cmdr_armor_upper.paa"};
    };
    //class macro_new_uniform_skin_class(501_inf,snow): macro_new_uniform_skin_class(501_inf,recruit)
    //{
        //model = "\armor_unit\21\21.p3d";
        //hiddenSelections[] = {"Camo1","Camo2","CamoB"};
        //hiddenSelectionsMaterials[] = {"armor_unit\21\camo1.rvmat","armor_unit\21\camo2.rvmat","armor_unit\21\under.rvmat"};
        //hiddenSelectionsTextures[] = {"RD501_Units\textures\Republic\clones\auxillary\snow_trooper_armor_upper.paa","RD501_Units\textures\Republic\clones\auxillary\snow_trooper_armor_lower.paa","armor_unit\21\21_Undersuit_co.paa"};
    //};
    //class macro_new_uniform_skin_class(501_inf,snow_nco): macro_new_uniform_skin_class(501_inf,snow)
    //{
        //hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\snow_trooper_armor_upper.paa,TEXTUREPATH\Republic\clones\auxillary\snow_nco_armor_lower.paa,"armor_unit\21\21_Undersuit_co.paa"};
    //};
    //class macro_new_uniform_skin_class(501_inf,snow_medic): macro_new_uniform_skin_class(501_inf,snow)
    //{
        //hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\snow_medic_armor_upper.paa,TEXTUREPATH\Republic\clones\auxillary\snow_trooper_armor_lower.paa,"armor_unit\21\21_Undersuit_co.paa"};
    //};
    //class macro_new_uniform_skin_class(501_inf,snow_rto): macro_new_uniform_skin_class(501_inf,snow)
    //{
        //hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\auxillary\snow_rto_armor_upper.paa,TEXTUREPATH\Republic\clones\auxillary\snow_trooper_armor_lower.paa,"armor_unit\21\21_Undersuit_co.paa"};
    //};
    ///Airborne///
    class macro_new_uniform_skin_class(501_ab,cadet): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CRC\501_Ack_CRC_upper.paa,TEXTUREPATH\Republic\clones\AB\CRC\501_Ack_CRC_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_medic,cadet): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CRC\501_Ack_MED_CRC_upper.paa,TEXTUREPATH\Republic\clones\AB\CRC\501_Ack_CRC_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_rto,cadet): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CRC\501_Ack_RTO_CRC_upper.paa,TEXTUREPATH\Republic\clones\AB\CRC\501_Ack_CRC_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,trooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CT\501_Ack_CT_upper.paa,TEXTUREPATH\Republic\clones\AB\CT\501_Ack_CT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_medic,trooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CT\501_Ack_MED_CT_upper.paa,TEXTUREPATH\Republic\clones\AB\CT\501_Ack_MED_CT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_rto,trooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CT\501_Ack_RTO_CT_upper.paa,TEXTUREPATH\Republic\clones\AB\CT\501_Ack_RTO_CT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,strooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCT\501_Ack_SCT_upper.paa,TEXTUREPATH\Republic\clones\AB\SCT\501_Ack_SCT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_medic,strooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCT\501_Ack_MED_SCT_upper.paa,TEXTUREPATH\Republic\clones\AB\SCT\501_Ack_MED_SCT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_rto,strooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCT\501_Ack_RTO_SCT_upper.paa,TEXTUREPATH\Republic\clones\AB\SCT\501_Ack_RTO_SCT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,vtrooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\VCT\501_Ack_VCT_upper.paa,TEXTUREPATH\Republic\clones\AB\VCT\501_Ack_VCT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_medic,vtrooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\VCT\501_Ack_MED_VCT_upper.paa,TEXTUREPATH\Republic\clones\AB\VCT\501_Ack_MED_VCT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_rto,vtrooper): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\VCT\501_Ack_RTO_VCT_upper.paa,TEXTUREPATH\Republic\clones\AB\VCT\501_Ack_RTO_VCT_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,cpl): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CP\501_Ack_CP_upper.paa,TEXTUREPATH\Republic\clones\AB\CP\501_Ack_CP_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_medic,cpl): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CP\501_Ack_MED_CP_upper.paa,TEXTUREPATH\Republic\clones\AB\CP\501_Ack_MED_CP_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_rto,cpl): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CP\501_Ack_RTO_CP_upper.paa,TEXTUREPATH\Republic\clones\AB\CP\501_Ack_RTO_CP_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,scpl): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCP\501_Ack_SCP_upper.paa,TEXTUREPATH\Republic\clones\AB\SCP\501_Ack_SCP_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_medic,scpl): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCP\501_Ack_MED_SCP_upper.paa,TEXTUREPATH\Republic\clones\AB\SCP\501_Ack_MED_SCP_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab_rto,scpl): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCP\501_Ack_RTO_SCP_upper.paa,TEXTUREPATH\Republic\clones\AB\SCP\501_Ack_RTO_SCP_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,cs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CS\501_Ack_CS_upper.paa,TEXTUREPATH\Republic\clones\AB\CS\501_Ack_CS_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,scs): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\SCS\501_Ack_SCS_upper.paa,TEXTUREPATH\Republic\clones\AB\SCS\501_Ack_SCS_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,csm): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CSM\501_Ack_CSM_upper.paa,TEXTUREPATH\Republic\clones\AB\CSM\501_Ack_CSM_lower.paa};
    };
    class macro_new_uniform_skin_class(501_ab,cc1): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\AB\CC1\501_Ack_CC1_upper.paa,TEXTUREPATH\Republic\clones\AB\CC1\501_Ack_CC1_lower.paa};
    };
    class macro_new_uniform_skin_class(501_avi,KNI): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_KNI.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_KNI.paa};
    };
    class macro_new_uniform_skin_class(501_avi,MDSHP): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_MDSHP.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_MDSHP.paa};
    };
    class macro_new_uniform_skin_class(501_avi,ESN): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_ESN.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_LTjr_ESN.paa};
    };
    class macro_new_uniform_skin_class(501_avi,LTjr): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_LTjr.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_LTjr_ESN.paa};
    };
    class macro_new_uniform_skin_class(501_avi,LTsr): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_CXP.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_CXP.paa};
    };
    class macro_new_uniform_skin_class(501_avi,LTcmd): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_CXS.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_CXS.paa};
    };
    class macro_new_uniform_skin_class(501_avi,CPTjr): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_CPTjr.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_CPTjr.paa};
    };
    class macro_new_uniform_skin_class(501_avi,CPTsr): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\avi_armor_upper_CPTsr.paa,TEXTUREPATH\Republic\clones\avi\avi_armor_lower_CPTsr.paa};
    };
    class macro_new_uniform_skin_class(501_avi,Lightning): macro_new_uniform_skin_class(501_inf,2nd_lt_alt)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\AVI_lightning_armor_upper_MC_co.paa,TEXTUREPATH\Republic\clones\avi\AVI_lightning_armor_lower_MC_co.paa};
    };
    //
    class macro_new_uniform_skin_class(empire_rg,boi): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\gcw\empire\rg\rg_armour_upper.paa,TEXTUREPATH\gcw\empire\rg\rg_armour_lower.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_armour_upper_01.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,02): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_armour_upper_02.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_02.paa};
    };
    class macro_new_uniform_skin_class(501_arc,phase1_01): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_LT_armor_upper.paa"};
    
    };
    class macro_new_uniform_skin_class(501_arc,phase1_02): macro_new_uniform_skin_class(501_arc,phase1_01)
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_cpt_armor_upper.paa"};
    };
    class macro_new_uniform_skin_class(501_utc,01): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_blue_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,02): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_green_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,03): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_indigo_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,04): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_orange_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,05): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_pink_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,06): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_red_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,07): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_violet_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_utc,08): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\utc\utc_yellow_6_armor_upper.paa,TEXTUREPATH\Republic\clones\utc\utc_armor_lower.paa};
    };
    //Squad Specific Senior Trooper
    class macro_new_uniform_skin_class(501_inf,strooper_11): macro_new_uniform_skin_class(501_inf,recruit) 
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper_12): macro_new_uniform_skin_class(501_inf,recruit) 
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper_13): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper_21): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper_22): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};	
    };
    class macro_new_uniform_skin_class(501_inf,strooper_23): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper_31): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};	
    };
    class macro_new_uniform_skin_class(501_inf,strooper_32): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,strooper_33): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_11_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava1_strooper_medic_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_12_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava1_strooper_medic_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_13_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava1_strooper_medic_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_21_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava2_strooper_medic_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_22_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava2_strooper_medic_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_23_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava2_strooper_medic_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_31_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava3_strooper_medic_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_32_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava3_strooper_medic_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_33_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava3_strooper_medic_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_11_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc1_strooper_medic_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_12_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc1_strooper_medic_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_13_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc1_strooper_medic_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_21_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc2_strooper_medic_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_22_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc2_strooper_medic_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_23_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc2_strooper_medic_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_31_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc3_strooper_medic_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_32_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = { TEXTUREPATH\Republic\clones\Infantry\cyc3_strooper_medic_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,strooper_33_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc3_strooper_medic_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_11): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_12): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_13): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_21): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_22): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_23): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_31): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_32): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,strooper_33): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\strooper_rto_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\strooper_armor_lower.paa};
    };
    //Squad Specific Veteran Trooper
    class macro_new_uniform_skin_class(501_inf,vtrooper_11): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_12): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    }; 
    class macro_new_uniform_skin_class(501_inf,vtrooper_13): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_21): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_22): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_23): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_31): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_32): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf,vtrooper_33): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_11_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava1_vtrooper_medic_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_12_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava1_vtrooper_medic_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    }; 
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_13_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava1_vtrooper_medic_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_21_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava2_vtrooper_medic_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_22_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava2_vtrooper_medic_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_23_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava2_vtrooper_medic_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_31_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava3_vtrooper_medic_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_32_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava3_vtrooper_medic_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_33_ava): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\ava3_vtrooper_medic_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_11_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc1_vtrooper_medic_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_12_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc1_vtrooper_medic_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    }; 
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_13_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc1_vtrooper_medic_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_21_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc2_vtrooper_medic_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_22_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc2_vtrooper_medic_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_23_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc2_vtrooper_medic_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_31_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc3_vtrooper_medic_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_32_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc3_vtrooper_medic_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_medic,vtrooper_33_cyc): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\cyc3_vtrooper_medic_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_11): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_12): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_13): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_21): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_22): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_23): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_31): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_32): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_inf_rto,vtrooper_33): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\Infantry\vtrooper_rto_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    //Squad Specific ARC Trooper
    class macro_new_uniform_skin_class(501_arc,01_11): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_1_1_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_12): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_1_2_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_13): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_1_3_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_21): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_2_1_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_22): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_2_2_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_23): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_2_3_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_31): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_3_1_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_32): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_3_2_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };
    class macro_new_uniform_skin_class(501_arc,01_33): macro_new_uniform_skin_class(501_arc,01)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\arc\arc_3_3_armor_upper.paa,TEXTUREPATH\Republic\clones\arc\arc_armour_lower_01.paa};
    };

    //Warrent Officers
    class macro_new_uniform_skin_class(501_cw,01): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\warrent_officers\wo1_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_cw,02): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\warrent_officers\wo2_armor_upper.paa,TEXTUREPATH\Republic\clones\Infantry\vtrooper_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_cw,03): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\warrent_officers\wo3_armor_upper.paa,TEXTUREPATH\Republic\clones\warrent_officers\wo3_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_cw,04): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\warrent_officers\wo4_armor_upper.paa,TEXTUREPATH\Republic\clones\warrent_officers\wo3_armor_lower.paa};
    };
    class macro_new_uniform_skin_class(501_cw,05): macro_new_uniform_skin_class(501_inf,recruit)
    {
        hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\warrent_officers\wo5_armor_upper.paa,TEXTUREPATH\Republic\clones\warrent_officers\wo3_armor_lower.paa};
    };
};
class CfgUnitInsignia
{	
    class rd501_ava1
    {
        displayName = "Avalanche 1";
        author = "RD501";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava1.paa";
        material = "\MRC\JLTS\Core_mod\data\insignias\insignia_CloneArmor.rvmat";
    };
    class rd501_ava1_1: rd501_ava1
    {
        displayName = "Avalanche 1-1";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava1_1.paa";
    };
    class rd501_ava1_2: rd501_ava1
    {
        displayName = "Avalanche 1-2";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava1_2.paa";
    };
    class rd501_ava1_3: rd501_ava1
    {
        displayName = "Avalanche 1-3";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava1_3.paa";
    };
    class rd501_ava2: rd501_ava1
    {
        displayName = "Avalanche 2";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava2.paa";
    };
    class rd501_ava2_1: rd501_ava1
    {
        displayName = "Avalanche 2-1";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava2_1.paa";
    };
    class rd501_ava2_2: rd501_ava1
    {
        displayName = "Avalanche 2-2";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava2_2.paa";
    };
    class rd501_ava2_3: rd501_ava1
    {
        displayName = "Avalanche 2-3";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava2_3.paa";
    };
    class rd501_ava3: rd501_ava1
    {
        displayName = "Avalanche 3";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava3.paa";
    };
    class rd501_ava3_1: rd501_ava1
    {
        displayName = "Avalanche 3-1";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava3_1.paa";
    };
    class rd501_ava3_2: rd501_ava1
    {
        displayName = "Avalanche 3-2";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava3_2.paa";
    };
    class rd501_ava3_3: rd501_ava1
    {
        displayName = "Avalanche 3-3";
        texture = "RD501_Units\textures\Republic\clones\insignia\ava3_3.paa";
    };
    class rd501_krayt: rd501_ava1
    {
        displayName = "Krayt";
        texture = "\RD501_Units\textures\Republic\clones\insignia\krayt.paa";
    };
    class rd501_krayt1: rd501_ava1
    {
        displayName = "Krayt 1";
        texture = "\RD501_Units\textures\Republic\clones\insignia\krayt1.paa";
    };
    class rd501_krayt2: rd501_ava1
    {
        displayName = "Krayt 2";
        texture = "\RD501_Units\textures\Republic\clones\insignia\krayt2.paa";
    };

    class rd501_cyc_hq: rd501_ava1
    {
        displayName = "Cyclone HQ";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc_hq.paa";
    };
    class rd501_cyc1: rd501_ava1
    {
        displayName = "Cyclone 1";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc1.paa";
    };
    class rd501_cyc1_1: rd501_ava1
    {
        displayName = "Cyclone 1-1";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc1_1.paa";
    };
    class rd501_cyc1_2: rd501_ava1
    {
        displayName = "Cyclone 1-2";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc1_2.paa";
    };
    class rd501_cyc1_3: rd501_ava1
    {
        displayName = "Cyclone 1-3";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc1_3.paa";
    };
    class rd501_cyc2: rd501_ava1
    {
        displayName = "Cyclone 2";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc2.paa";
    };
    class rd501_cyc2_1: rd501_ava1
    {
        displayName = "Cyclone 2-1";
        texture = "\RD501_Units\textures\Republic\clones\insignia\cyc2_1.paa";
    };
    class rd501_cyc2_2: rd501_ava1
    {
        displayName = "Cyclone 2-2";
        texture = "\RD501_Units\textures\Republic\clones\insignia\cyc2_2.paa";
    };
    class rd501_cyc2_3: rd501_ava1
    {
        displayName = "Cyclone 2-3";
        texture = "\RD501_Units\textures\Republic\clones\insignia\cyc2_3.paa";
    };
    class rd501_cyc3: rd501_ava1
    {
        displayName = "Cyclone 3";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc3.paa";
    };
    class rd501_cyc3_1: rd501_ava1
    {
        displayName = "Cyclone 3-1";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc3_1.paa";
    };
    class rd501_cyc3_3: rd501_ava1
    {
        displayName = "Cyclone 3-3";
        texture = "RD501_Units\textures\Republic\clones\insignia\cyc3_3.paa";
    };
    class rd501_nexu: rd501_ava1
    {
        displayName = "Nexu";
        texture = "\RD501_Units\textures\Republic\clones\insignia\nexu.paa";
    };
    class rd501_nexu1: rd501_ava1
    {
        displayName = "Nexu 1";
        texture = "\RD501_Units\textures\Republic\clones\insignia\nexu1.paa";
    };
    class rd501_nexu2: rd501_ava1
    {
        displayName = "Nexu 2";
        texture = "\RD501_Units\textures\Republic\clones\insignia\nexu2.paa";
    };
    class rd501_ack1: rd501_ava1
    {
        displayName = "Acklay 1";
        texture = "RD501_Units\textures\Republic\clones\insignia\ack1.paa";
    };
    class rd501_ack1_1: rd501_ava1
    {
        displayName = "Acklay 1-1";
        texture = "\RD501_Units\textures\Republic\clones\insignia\ack1_1.paa";
    };
    class rd501_ack1_3: rd501_ava1
    {
        displayName = "Acklay 1-3";
        texture = "\RD501_Units\textures\Republic\clones\insignia\ack1_3.paa";
    };
    class rd501_ack_hq: rd501_ava1
    {
        displayName = "Acklay HQ";
        texture = "RD501_Units\textures\Republic\clones\insignia\ack_hq.paa";
    };
    class rd501_ack_arc: rd501_ava1
    {
        displayName = "Acklay Phoenix";
        texture = "RD501_Units\textures\Republic\clones\insignia\ack_arc.paa";
    };
    class rd501_ack_med: rd501_ava1
    {
        displayName = "Acklay Medic";
        texture = "RD501_Units\textures\Republic\clones\insignia\ack_med.paa";
    };
    class rd501_ack_rto: rd501_ava1
    {
        displayName = "Acklay RTO";
        texture = "RD501_Units\textures\Republic\clones\insignia\ack_rto.paa";
    };
    class rd501_warden: rd501_ava1
    {
        displayName = "Warden";
        texture = "RD501_Units\textures\Republic\clones\insignia\warden.paa";
    };
    class rd501_warden1_1: rd501_ava1
    {
        displayName = "Warden 1-1";
        texture = "RD501_Units\textures\Republic\clones\insignia\warden1_1.paa";
    };
    class rd501_warden1_2: rd501_ava1
    {
        displayName = "Warden 1-2";
        texture = "RD501_Units\textures\Republic\clones\insignia\warden1_2.paa";
    };
    class rd501_Razor: rd501_ava1
    {
        displayName = "Razor";
        texture = "RD501_Units\textures\Republic\clones\insignia\Razor.paa";
    };
    class rd501_knife: rd501_ava1
    {
        displayName = "Knife";
        texture = "RD501_Units\textures\Republic\clones\insignia\knife.paa";
    };
    class rd501_Noble: rd501_ava1
    {
        displayName = "Noble";
        texture = "RD501_Units\textures\Republic\clones\insignia\Noble.paa";
    };
    class rd501_Noble1: rd501_ava1
    {
        displayName = "Noble 1";
        texture = "RD501_Units\textures\Republic\clones\insignia\Noble1.paa";
    };
    class rd501_Noble4: rd501_ava1
    {
        displayName = "Noble 4";
        texture = "RD501_Units\textures\Republic\clones\insignia\Noble4.paa";
    };
    class rd501_hydra: rd501_ava1
    {
        displayName = "Hydra";
        texture = "RD501_Units\textures\Republic\clones\insignia\Hydra.paa";
    };
    class rd501_hydra4: rd501_ava1
    {
        displayName = "Hydra 4";
        texture = "RD501_Units\textures\Republic\clones\insignia\Hydra4.paa";
    };
    class rd501_lostplatoon_Frozen: rd501_ava1
    {
        displayName = "Lost Platoon - Frozen";
        texture = "RD501_Units\textures\Republic\clones\insignia\LostPlatoon_Frozen.paa";
    };
    class rd501_lostplatoon_Archangel: rd501_ava1
    {
        displayName = "Lost Platoon - Archangel";
        texture = "RD501_Units\textures\Republic\clones\insignia\LostPlatoon_Archangel.paa";
    };
    class rd501_lostplatoon_Phoenix: rd501_ava1
    {
        displayName = "Lost Platoon - Phoenix";
        texture = "RD501_Units\textures\Republic\clones\insignia\LostPlatoon_Phoenix.paa";
    };
    class rd501_lostplatoon_Redline: rd501_ava1
    {
        displayName = "Lost Platoon - Redline";
        texture = "RD501_Units\textures\Republic\clones\insignia\LostPlatoon_Redline.paa";
    };
    class rd501_lostplatoon_Scorched: rd501_ava1
    {
        displayName = "Lost Platoon - Scorched";
        texture = "RD501_Units\textures\Republic\clones\insignia\LostPlatoon_Scorched.paa";
    };
    class rd501_lostplatoon_Starfall: rd501_ava1
    {
        displayName = "Lost Platoon - Starfall";
        texture = "RD501_Units\textures\Republic\clones\insignia\LostPlatoon_Starfall.paa";
    };
    class rd501_Zeta1: rd501_ava1
    {
        displayName = "Zeta 1";
        texture = "\RD501_Units\textures\Republic\clones\insignia\Zeta1.paa";
    };
    class rd501_Zeta2: rd501_ava1
    {
        displayName = "Zeta 2";
        texture = "\RD501_Units\textures\Republic\clones\insignia\Zeta2.paa";
    };
};