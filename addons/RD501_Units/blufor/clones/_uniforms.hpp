class macro_new_uniform_class(501_inf,recruit): Uniform_Base
{
	author = "RD501";
	scope = 2;
	nakedUniform = "U_BasicBody";
	displayName = "[501st] INF ARMR 01 (Recruit)";
	picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_uniform_ca.paa";
	model="\MRC\JLTS\characters\CloneArmor\CloneArmor.p3d";
	class ItemInfo: UniformItem
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,recruit);
		armor = 100;
		armorStructural = 5;
		explosionShielding = 1.1;
		impactDamageMultiplier	= -100; // multiplier for falling damage, doesnt actualy work lol
		modelSides[] = {6};
		uniformType = "Neopren";
		containerClass = "Supply100";
		mass = 40;
	};
};
class macro_new_uniform_class(501_inf,cadet): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 02 (Cadet)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,cadet);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,cadet): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 02 (Cadet)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,cadet);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,cadet): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 02 (Cadet)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,cadet);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,trooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 03 (Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,trooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,trooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 03 (Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,trooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,trooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 03 (Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,trooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,clc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 A (CLC)";
	class ItemInfo: ItemInfo
	{
		uniformClass = "RD501_501_inf_uniform_clc";
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,clc_medic): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 A (CM-L)";
	class ItemInfo: ItemInfo
	{
		uniformClass = "RD501_501_inf_uniform_clc_medic";
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,clc_rto): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 A (CI-L)";
	class ItemInfo: ItemInfo
	{
		uniformClass = "RD501_501_inf_uniform_clc_rto";
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,nco): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 06 (Corporal)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,nco);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,nco): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 06 (Corporal)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,nco);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,nco): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 06 (Corporal)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,nco);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,sr_cp): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 07 (Sr. CP)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,sr_cp);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,sr_cp): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 07 (Sr. CP)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,sr_cp);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,sr_cp): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 07 (Sr. CP)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,sr_cp);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 08 (Sergeant)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 08 (Sergeant)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 08 (Sergeant)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,sr_cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 09 (Sr. CS)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,sr_cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,sr_cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 09 (Sr. CS)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,sr_cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,sr_cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 09 (Sr. CS)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,sr_cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,csm): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 10 (Sgt. Major)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,csm);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,csm): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 10 (Sgt. Major)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,csm);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,csm): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 10 (Sgt. Major)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,csm);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,csm_comp): macro_new_uniform_class(501_inf,recruit)
{
displayName = "[501st] INF ARMR 11 (Sgt. Major [C])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,csm_comp);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,csm_bn): macro_new_uniform_class(501_inf,recruit)
{
displayName = "[501st] INF ARMR 12 (Sgt. Major [B])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,csm_bn);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,2nd_lt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 13 (2nd Lt.)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,2nd_lt);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,2nd_lt_alt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 13 (2nd Lt.) Alt";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,2nd_lt_alt);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,1st_lt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 14 (1st Lt)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,1st_lt);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,1st_lt_alt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 14 (1st Lt.) Alt";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,1st_lt_alt);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,cpt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 15 (Captain)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,cpt);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,cpt_alt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 15 (Captain) Alt";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,cpt_alt);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,maj): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 16 (Major)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,maj);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,maj_alt): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 16 (Major) Alt";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,maj_alt);
		containerClass = "Supply100";
	};
};
//class macro_new_uniform_class(501_inf,snow): macro_new_uniform_class(501_inf,recruit)
//{
	//displayName = "[501st] INF ARMR SNOW 01 (Trooper)";
	//class ItemInfo: ItemInfo
	//{
		//uniformClass = macro_new_uniform_skin_class(501_inf,snow);
		//containerClass = "Supply100";
	//};
//};
//class macro_new_uniform_class(501_inf,snow_nco): macro_new_uniform_class(501_inf,recruit)
//{
	//displayName = "[501st] INF ARMR SNOW 02 (NCO)";
	//class ItemInfo: ItemInfo
	//{
		//uniformClass = macro_new_uniform_skin_class(501_inf,snow_nco);
		//containerClass = "Supply100";
	//};
//};
//class macro_new_uniform_class(501_inf,snow_medic): macro_new_uniform_class(501_inf,recruit)
//{
	//displayName = "[501st] INF ARMR SNOW 03 (Medic)";
	//class ItemInfo: ItemInfo
	//{
		//uniformClass = macro_new_uniform_skin_class(501_inf,snow_medic);
		//containerClass = "Supply100";
	//};
//};
//class macro_new_uniform_class(501_inf,snow_rto): macro_new_uniform_class(501_inf,recruit)
//{
	//displayName = "[501st] INF ARMR SNOW 04 (RTO)";
	//class ItemInfo: ItemInfo
	//{
		//uniformClass = macro_new_uniform_skin_class(501_inf,snow_rto);
		//containerClass = "Supply100";
	//};
//};
class macro_new_uniform_class(501_inf,phase1_nco): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR P1 01 (NCO)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,phase1_nco);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,phase1_sergeant): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR P1 02 (Sergeant)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,phase1_sergeant);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,phase1_commander): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR P1 03 (Commander)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,phase1_commander);
		containerClass = "Supply100";
	};
};
///
class macro_new_uniform_class(501_ab,cadet): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 01 (Cadet)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,cadet);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_medic,cadet): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB MED ARMR 01 (Cadet)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_medic,cadet);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_rto,cadet): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB RTO ARMR 01 (Cadet)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_rto,cadet);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,trooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 02 (Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,trooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_medic,trooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB MED ARMR 02 (Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_medic,trooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_rto,trooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB RTO ARMR 02 (Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_rto,trooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,strooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 03 (Senior Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,strooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_medic,strooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB MED ARMR 03 (Senior Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_medic,strooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_rto,strooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB RTO ARMR 03 (Senior Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_rto,strooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,vtrooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 04 (Veteran Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,vtrooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_medic,vtrooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB MED ARMR 04 (Veteran Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_medic,vtrooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_rto,vtrooper): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB RTO ARMR 04 (Veteran Trooper)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_rto,vtrooper);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,cpl): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 05 (Cpl)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,cpl);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_medic,cpl): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB MED ARMR 05 (Cpl)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_medic,cpl);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_rto,cpl): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB RTO ARMR 05 (Cpl)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_rto,cpl);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,scpl): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 06 (Sr. Cpl)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,scpl);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_medic,scpl): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB MED ARMR 06 (Sr. Cpl)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_medic,scpl);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab_rto,scpl): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB RTO ARMR 06 (Sr. Cpl)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab_rto,scpl);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,cs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 07 (CS)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,cs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,scs): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 08 (Sr. CS)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,scs);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,csm): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 09 (CS-M)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,csm);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_ab,cc1): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AB ARMR 10 (2nd Lt.)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_ab,cc1);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,KNI): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 01 (Knife)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,KNI);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,MDSHP): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 02 (Midshipman)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,MDSHP);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,ESN): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 03 (Ensign)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,ESN);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,LTjr): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 04 (Jr. Lt.)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,LTjr);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,LTsr): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 05 (Sr. Lt.)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,LTsr);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,LTcmd): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 06 (Lt. Cmdr.)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,LTcmd);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,CPTjr): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 07 (Cpt. Jr)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,CPTjr);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,CPTsr): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 08 (Cpt. Sr./Comm.)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,CPTsr);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_avi,Lightning): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] AVI ARMR 09 (Lightning)";
	hiddenSelections[] = {"Camo","Camo1"};
	hiddenSelectionsTextures[] = {TEXTUREPATH\Republic\clones\avi\AVI_lightning_armor_upper_MC_co.paa,TEXTUREPATH\Republic\clones\avi\AVI_lightning_armor_lower_MC_co.paa};
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_avi,Lightning);
		containerClass = "Supply100";
		hiddenSelections[]={"Camo","Camo1"};
	};
};
//
class macro_new_uniform_class(empire_rg,boi): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[Empire] Royal Guard Armour";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(empire_rg,boi);
		containerClass = "Supply100";
	};
};
//
class macro_new_uniform_class(501_arc,01): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,02): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 02";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,02);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,phase1_01): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR P1 01";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,phase1_01);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,phase1_02): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR P1 02";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,phase1_02);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,01): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Cyan)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,01);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,02): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Green)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,02);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,03): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Indigo)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,03);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,04): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Orange)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,04);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,05): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Pink)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,05);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,06): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Red)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,06);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,07): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Violet)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,07);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_utc,08): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] UTC (Yellow)";
	picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneArmor_ui_ca.paa";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_utc,08);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_11): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_11);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_12): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_12);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_13): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_13);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_21): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_21);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_22): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_22);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_23): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_23);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_31): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_31);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_32): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_32);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,strooper_33): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 04 (Sr. CT) [3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,strooper_33);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_11): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_11);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_12): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_12);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_13): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_13);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_21): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_21);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_22): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_22);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_23): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_23);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_31): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_31);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_32): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_32);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf,vtrooper_33): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF ARMR 05 (Vet. CT) [3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf,vtrooper_33);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_11_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_11_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_12_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_12_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_13_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_13_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_21_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_21_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_22_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_22_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_23_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_23_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_31_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_31_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_32_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_32_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_33_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [A3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_33_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_11_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_11_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_12_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_12_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_13_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_13_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_21_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_21_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_22_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_22_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_23_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_23_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_31_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_31_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_32_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_32_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,strooper_33_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 04 (Sr. CT) [C3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,strooper_33_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_11_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_11_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_12_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_12_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_13_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_13_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_21_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_21_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_22_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_22_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_23_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_23_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_31_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_31_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_32_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_32_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_33_ava): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [A3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_33_ava);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_11_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_11_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_12_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_12_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_13_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_13_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_21_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_21_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_22_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_22_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_23_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_23_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_31_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_31_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_32_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_32_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_medic,vtrooper_33_cyc): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF MED ARMR 05 (Vet. CT) [C3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_medic,vtrooper_33_cyc);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_11): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_11);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_12): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_12);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_13): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_13);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_21): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_21);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_22): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_22);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_23): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_23);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_31): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_31);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_32): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_32);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,strooper_33): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 04 (Sr. CT) [3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,strooper_33);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_11): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [1-1])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_11);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_12): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [1-2])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_12);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_13): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [1-3])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_13);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_21): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [2-1])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_21);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_22): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [2-2])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_22);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_23): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [2-3])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_23);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_31): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [3-1])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_31);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_32): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [3-2])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_32);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_inf_rto,vtrooper_33): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] INF RTO ARMR 05 (Vet. CT [3-3])";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_inf_rto,vtrooper_33);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_11): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [1-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_11);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_12): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [1-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_12);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_13): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [1-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_13);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_21): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [2-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_21);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_22): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [2-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_22);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_23): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [2-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_23);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_31): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [3-1]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_31);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_32): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [3-2]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_32);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_arc,01_33): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] ARC ARMR 01 [3-3]";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_arc,01_33);
		containerClass = "Supply100";
	};
};

class macro_new_uniform_class(501_cw,01): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] WRNT ARMR 01 (WO1)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_cw,01);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_cw,02): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] WRNT ARMR 02 (WO2)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_cw,02);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_cw,03): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] WRNT ARMR 03 (WO3)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_cw,03);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_cw,04): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] WRNT ARMR 04 (WO4)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_cw,04);
		containerClass = "Supply100";
	};
};
class macro_new_uniform_class(501_cw,05): macro_new_uniform_class(501_inf,recruit)
{
	displayName = "[501st] WRNT ARMR 05 (WO5)";
	class ItemInfo: ItemInfo
	{
		uniformClass = macro_new_uniform_skin_class(501_cw,05);
		containerClass = "Supply100";
	};
};