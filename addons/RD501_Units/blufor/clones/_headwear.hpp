class NVGoggles;
class JLTS_CloneNVG;
class JLTS_CloneNVG_spec;
class JLTS_CloneNVGRange: NVGoggles
{
    class ItemInfo;
};
class JLTS_CloneNVGCC;
class JLTS_CloneNVGMC;
class JLTS_CloneBinocular;
class JLTS_CloneBinocular_black;
class JLTS_NVG_droid_chip_1;
class lsd_gar_p1Rangefinder_nvg;
class lsd_gar_medicalScannerSPC_nvg;

class rd501_jlts_nvchipclean: JLTS_NVG_droid_chip_1
{
    displayname = "Operator Class Vision Enhancement Chip";
    RD501_isNV=1;
    modelOptics="";
    thermalMode[] = {0,1,2,3,4,5};
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_JLTS_CloneNVG: JLTS_CloneNVG
{
    displayname = "[GAR] Macro Visor (Toggle)";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_JLTS_CloneNVG_501st: JLTS_CloneNVG
{
    displayname = "[501st] Macro Visor (Toggle)";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\clone_visor.paa"
    };
};
class rd501_JLTS_CloneNVG_501st_blue_white: rd501_JLTS_CloneNVG_501st
{
    displayname = "[501st] Macro Visor - Blue (Toggle)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_blue_on_white.paa"
    };
};
class rd501_JLTS_CloneNVG_501st_red_white: rd501_JLTS_CloneNVG_501st
{
    displayname = "[501st] Macro Visor - Red (Toggle)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_red_on_white.paa"
    };
};
class rd501_JLTS_CloneNVG_501st_gray_white: rd501_JLTS_CloneNVG_501st
{
    displayname = "[501st] Macro Visor - Gray (Toggle)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_gray_on_white.paa"
    };
};
class rd501_JLTS_CloneNVG_501st_full_gray: rd501_JLTS_CloneNVG_501st
{
    displayname = "[501st] Macro Visor - Full Gray (Toggle)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_full_gray.paa"
    };
};
class rd501_JLTS_CloneNVG_501st_full_red: rd501_JLTS_CloneNVG_501st
{
    displayname = "[501st] Macro Visor - Full Red (Toggle)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_full_red.paa"
    };
};
class rd501_JLTS_CloneNVG_501st_full_black: rd501_JLTS_CloneNVG_501st
{
    displayname = "[501st] Macro Visor - Full Black (Toggle)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "\MRC\JLTS\characters\CloneArmor2\data\Clone_PurgeTrooper_nvg_co.paa"
    };
};
class rd501_JLTS_CloneNVG_spec: JLTS_CloneNVG_spec
{
    displayname = "[GAR] Macro Visor (Active)";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_JLTS_CloneNVG_spec_501st: JLTS_CloneNVG_spec
{
    displayname = "[501st] Macro Visor (Active)";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\clone_visor.paa"
    };
};
class rd501_ls_marksmenNVG: lsd_gar_medicalScannerSPC_nvg
	{
		scope = 2;
		displayName = "[501st] Adv. Marksmen Visor";
        RD501_isNV=1;
		picture = "\lsd_equipment_bluefor\nvg\gar\_ui\icon_medicScanner_nvg_ca.paa";
		modelOptics="";
	};
class rd501_JLTS_CloneNVG_spec_501st_blue_white: rd501_JLTS_CloneNVG_spec_501st
{
    displayname = "[501st] Macro Visor - Blue (Active)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
       "RD501_Units\textures\republic\clones\visors\clone_visor_blue_on_white.paa"
    };
};
class rd501_JLTS_CloneNVG_spec_501st_red_white: rd501_JLTS_CloneNVG_spec_501st
{
    displayname = "[501st] Macro Visor - Red (Active)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_red_on_white.paa"
    };
};
class rd501_JLTS_CloneNVG_spec_501st_gray_white: rd501_JLTS_CloneNVG_spec_501st
{
    displayname = "[501st] Macro Visor - Gray (Active)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_gray_on_white.paa"
    };
};
class rd501_JLTS_CloneNVG_spec_501st_full_gray: rd501_JLTS_CloneNVG_spec_501st
{
    displayname = "[501st] Macro Visor - Full Gray (Active)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_full_gray.paa"
    };
};
class rd501_JLTS_CloneNVG_spec_501st_full_black: rd501_JLTS_CloneNVG_spec_501st
{
    displayname = "[501st] Macro Visor - Full Black (Active)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "\MRC\JLTS\characters\CloneArmor2\data\Clone_PurgeTrooper_nvg_co.paa"
    };
};
class rd501_JLTS_CloneNVG_spec_501st_full_red: rd501_JLTS_CloneNVG_spec_501st
{
    displayname = "[501st] Macro Visor - Full Red (Active)";
    RD501_isNV=1;
    hiddenSelectionsTextures[]=
    {
        "RD501_Units\textures\republic\clones\visors\clone_visor_full_red.paa"
    };
};
class rd501_JLTS_CloneNVGRange: JLTS_CloneNVGRange
{
    displayname = "[501st] Rangefinder Antenna (Toggle)";
    RD501_isNV=1;
    modelOptics="";
    hiddenSelectionsTextures[] = {"RD501_Units\textures\Republic\clones\clone_rangefinder.paa"};
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_JLTS_ShockTrooperNVGRange: JLTS_CloneNVGRange
{
    displayname = "[501st] Shock Trooper Viewfinder Antenna (Toggle)";
    modelOptics="";
    hiddenSelectionsTextures[] = {"RD501_Units\textures\Republic\clones\clone_rangefinder.paa"};
	RD501_isNV=1;
    visionMode[]=
    {
        "Normal",
        "TI"
    };
};
class rd501_JLTS_CloneNVGRange_arc: JLTS_CloneNVGRange
{
    displayname = "[501st] ARC Rangefinder Antenna (Toggle)";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_lsd_CloneNVGRange_phase1: lsd_gar_p1Rangefinder_nvg
	{
		displayname = "[501st] Rangefinder Antenna (Phase 1)";
        RD501_isNV=1;
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
		modelOptics = "";
		visionMode[] = {"Normal","NVG","TI"};
	};
class rd501_JLTS_CloneNVGRange_down: JLTS_CloneNVGRange
{
    displayname = "[501st] Rangefinder Antenna (Active)";
    RD501_isNV=1;
    modelOptics="";
    hiddenSelectionsTextures[] = {"RD501_Units\textures\Republic\clones\clone_rangefinder.paa"};
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
    class ItemInfo: ItemInfo
    {
        type=616;
        uniformModel="\MRC\JLTS\characters\CloneArmor\CloneNVGRange_on.p3d";
        modelOff="\MRC\JLTS\characters\CloneArmor\CloneNVGRange_on.p3d";
        mass=20;
        hiddenSelections[]=
        {
            "camo1"
        };
    };
};
class rd501_JLTS_CloneNVGRange_down_arc: rd501_JLTS_CloneNVGRange_down
{
    displayname = "[501st] ARC Rangefinder Antenna (Active)";
    RD501_isNV=1;
    modelOptics="";
    hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_range_co.paa"};
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_JLTS_CloneNVGCC: JLTS_CloneNVGCC
{
    displayname = "[501st] Officer Visor";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class rd501_JLTS_CloneNVGMC: JLTS_CloneNVGMC
{
    displayname = "[501st] Commander Visor";
    RD501_isNV=1;
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
};
class macro_new_weapon(nvg,shock_trooper) : JLTS_NVG_droid_chip_1
{
	visionMode[] = {"Normal","NVG"};
	dlc = "rd501";
	author= "RD501";
	displayName = "NVG Chip(Clear)";
    RD501_isNV=1;
	macro_thermal_nvg_default
};

class macro_new_weapon(nvg,shock_trooper2) : JLTS_NVG_droid_chip_1
{
    visionMode[] = {"Normal"};
    dlc = "rd501";
    author= "RD501";
    RD501_isNV = 1;
    displayName = "Shock Trooper Chip";
};

class macro_new_weapon(nvg,shock_trooper_enhanced) : JLTS_NVG_droid_chip_1
{
    visionMode[] = {"Normal","TI"};
    thermalMode[] = {0,2};
    dlc = "rd501";
    author= "RD501";
    RD501_isNV = 1;
    displayName = "Shock Trooper Enhanced Chip";
};
class rd501_p1_CC_NVG: rd501_JLTS_CloneNVGCC
{
    displayname = "[501st] Officer Visor (Phase 1)";
    RD501_isNV=1;
    hiddenSelections[] = {"camo","camo1","camo2"};
	hiddenSelectionsTextures[] = 
    {
        "\RD501_Units\textures\Republic\clones\visors\clone_p1_officer_visor.paa",
        "3AS\3AS_Characters\Clones\Headgear\Attachments\data\accesories_co.paa"
    };
    hiddenSelectionsMaterials[] = 
    {
        "3AS\3AS_Characters\Clones\Headgear\Attachments\data\accesories.rvmat",
        "3AS\3AS_Characters\Clones\Headgear\Attachments\data\visor.rvmat"
    };
	model = "\3AS\3AS_Characters\Clones\Headgear\3as_P1_visor.p3d";
    modelOptics="";
    visionMode[]=
    {
        "Normal",
        "NVG",
        "TI"
    };
    class ItemInfo
		{
			type = 616;
			uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3as_P1_visor.p3d";;
			modelOff = "\3AS\3AS_Characters\Clones\Headgear\3as_P1_visor.p3d";;
			mass = 20;
			hiddenSelections[] = {"camo","camo1","camo2"};
		};
};