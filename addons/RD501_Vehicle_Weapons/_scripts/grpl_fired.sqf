params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];

if (!isserver) exitWith{};
diag_log text (["[RD501]", "[Guided Resupply]", "TRACE:", "Fired GRPL Event Dump:", 
	"Unit:", _unit, 
	"Weapon:", _weapon, 
	"Muzzle:", _muzzle,
	"Mode:", _mode,
	"Ammo:", _ammo,
	"Magazine:", _magazine,
	"Projectile:", _projectile,
	"Gunner:", _gunner] joinString " ");

private _pos = [0, 0, 0];
waitUntil {
	if (isNull _projectile) exitWith { true };
	_pos = getPos _projectile;
	false;
};

if (_magazine == "RD501_Guided_Resupply_Ammo_Resupply_Magazine") then {
	[_pos, 'RD501_Guided_Resupply_Ammo_Type_Var', 'RD501_resuppy_box_pod_ammo', 'RD501_resupply_smoke_ammo', 'SmokeShellYellow'] remoteExec ['RD501_fnc_GURE_spawnResupply', 2];
} else {
	if(_magazine == "RD501_Guided_Resupply_Medical_Resupply_Magazine") then {
		[_pos, 'RD501_Guided_Resupply_Medical_Type_Var', 'RD501_resuppy_box_pod_medical', 'RD501_resupply_smoke_medical', 'SmokeShellYellow'] remoteExec ['RD501_fnc_GURE_spawnResupply', 2];
	} else {
		diag_log text (["[RD501]", "[Guided Resupply]", "WARN:", "Unknown magazine type fired."] joinString " ");
	};
};