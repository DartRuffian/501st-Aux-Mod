
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_ball_turret
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"3AS_LAAT"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(turret,laat_ball_beam_l),
			macro_new_weapon(turret,laat_ball_beam_r)
		};
	};
};

class CfgWeapons
{
	class 3as_LAAT_autocannon_30mm;
	class ParticleBeamCannon_F: 3as_LAAT_autocannon_30mm
	{
		class HE;
	};
	class macro_new_weapon(turret,laat_ball_beam_l): ParticleBeamCannon_F
	{
		displayName = "BeamRider Mk-II";
		author = "RD501";

		magazines[] =
		{
			"RD501_Republic_Aircraft_Laser_Beam_Mag_500"
		};
		class HE: HE
		{
			displayName = "BeamRider Mk-II";
			magazines[] = {"RD501_Republic_Aircraft_Laser_Beam_Mag_500"};
			ballisticsComputer = "4+8";
		};
		stabilizedInAxes = 3;
		ballisticsComputer = "4+8";
		canLock = 2;
		magazineReloadTime=5;
	
	};
	class macro_new_weapon(turret,laat_ball_beam_r): macro_new_weapon(turret,laat_ball_beam_l)
	{

	};

	class LMG_RCWS;
	class Cannon_Portableun:LMG_RCWS
	{
		class manual;
	};

}; 

