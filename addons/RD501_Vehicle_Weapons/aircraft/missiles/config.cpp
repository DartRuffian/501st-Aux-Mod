
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_missiles
	{
		author = DANKAUTHORS;
		addonRootClass = MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"RD501_Republic_Aircraft_Missile_AA_IR",
			"RD501_Republic_Aircraft_Missile_AT_Wire",
			"RD501_Republic_Aircraft_Missile_AT_IR",
			"RD501_Republic_Aircraft_Missile_AP",
			"RD501_Republic_Aircraft_Missile_ARM_LR"
			

		};
	};
};

class CfgWeapons
{
	class missiles_Zephyr;
	class missiles_Jian;
	class Rocket_04_AP_Plane_CAS_01_F;
	class weapon_HARMLauncher;

	class RD501_Default
	{
		scope=0;
		value=2;
		picture="";
		uiPicture="";
		ammo="";
		cursor="";
		cursorAim="";
		cursorSize=1;
		showAimCursorInternal=1;
		cursorAimOn="";
		laser=0;
		hiddenSelections[]={};
		hiddenSelectionsTextures[]={};
		hiddenUnderwaterSelections[]={};
		shownUnderwaterSelections[]={};
		hiddenUnderwaterSelectionsTextures[]={};
		simulation="Weapon";
		type=65536;
		displayName="";
		nameSound="";
		count=0;
		multiplier=1;
		burst=1;
		magazineReloadTime=0;
		reloadTime=1;
		magazineReloadSwitchPhase=1;
		sound[]=
		{
			"",
			1,
			1
		};
		soundBegin[]=
		{
			"sound",
			1
		};
		soundBeginWater[]=
		{
			"sound",
			1
		};
		soundClosure[]=
		{
			"sound",
			1
		};
		soundEnd[]=
		{
			"sound",
			1
		};
		soundLoop[]=
		{
			"sound",
			1
		};
		soundContinuous=0;
		weaponSoundEffect="";
		soundBurst=1;
		drySound[]=
		{
			"",
			1,
			1
		};
		zeroingSound[]=
		{
			"",
			1,
			1
		};
		reloadSound[]=
		{
			"",
			1,
			1
		};
		changeFiremodeSound[]=
		{
			"",
			1,
			1
		};
		reloadMagazineSound[]=
		{
			"",
			1,
			1
		};
		emptySound[]=
		{
			"",
			1,
			1
		};
		soundBullet[]=
		{
			"emptySound",
			1
		};
		initSpeed=0;
		ballisticsComputer=0;
		irDistance=0;
		irDotIntensity=0.001;
		dispersion=0.0020000001;
		aiDispersionCoefX=1;
		aiDispersionCoefY=1;
		canLock=2;
		lockAcquire=1;
		enableAttack=1;
		ffMagnitude=0;
		ffFrequency=1;
		ffCount=1;
		recoil="empty";
		recoilProne="";
		maxRecoilSway=0.0080000004;
		swayDecaySpeed=2;
		model="";
		modelSpecial="";
		modelMagazine="";
		muzzlePos="usti hlavne";
		muzzleEnd="konec hlavne";
		irLaserPos="laser pos";
		irLaserEnd="laser dir";
		cartridgePos="nabojnicestart";
		cartridgeVel="nabojniceend";
		selectionFireAnim="zasleh";
		memoryPointCamera="eye";
		fireSpreadAngle=3;
		useModelOptics=1;
		opticsID=0;
		modelOptics="";
		opticsPPEffects[]={};
		opticsFlare=1;
		optics=1;
		forceOptics=0;
		useAsBinocular=0;
		opticsDisablePeripherialVision=0.67000002;
		opticsZoomMin=0.25;
		opticsZoomMax=1.25;
		opticsZoomInit=0.75;
		distanceZoomMin=400;
		distanceZoomMax=400;
		primary=10;
		showSwitchAction=0;
		showEmpty=1;
		autoFire=0;
		autoReload=1;
		showToPlayer=1;
		canShootInWater=0;
		aiRateOfFire=5;
		aiRateOfFireDistance=500;
		aiRateOfFireDispersion=0;
		fireLightDuration=0.050000001;
		fireLightIntensity=0.2;
		fireLightDiffuse[]={0.93699998,0.63099998,0.259};
		fireLightAmbient[]={0,0,0};
		class Eventhandlers
		{
		};
		backgroundReload=0;
		reloadAction="";
		muzzles[]=
		{
			"this"
		};
		magazines[]={};
		modes[]=
		{
			"this"
		};
		useAction=0;
		useActionTitle="";
		canDrop=1;
		weaponLockDelay=0;
		weaponLockSystem=0;
		cmImmunity=1;
		weight=0;
		minRange=1;
		minRangeProbab=0.30000001;
		midRange=150;
		midRangeProbab=0.57999998;
		maxRange=500;
		maxRangeProbab=0.039999999;
		handAnim[]={};
		lockingTargetSound[]=
		{
			"",
			0.00031622776,
			2
		};
		lockedTargetSound[]=
		{
			"",
			0.00031622776,
			6
		};
		detectRange=0;
		artilleryDispersion=1;
		artilleryCharge=1;
		fireAnims[]={};
		class Library
		{
			libTextDesc="";
		};
		descriptionShort="";
		class GunFire
		{
			cloudletDuration=0.2;
			cloudletAnimPeriod=1;
			cloudletSize=1;
			cloudletAlpha=1;
			cloudletGrowUp=0.2;
			cloudletFadeIn=0.0099999998;
			cloudletFadeOut=0.5;
			cloudletAccY=0;
			cloudletMinYSpeed=-100;
			cloudletMaxYSpeed=100;
			cloudletShape="cloudletFire";
			cloudletColor[]={1,1,1,0};
			cloudletDensityCoef=-1;
			interval=-0.0099999998;
			size=3;
			sourceSize=0.5;
			timeToLive=0;
			initT=4500;
			deltaT=-3000;
			class Table
			{
				class T0
				{
					maxT=0;
					color[]={0.81999999,0.94999999,0.93000001,0};
				};
				class T1
				{
					maxT=200;
					color[]={0.75,0.76999998,0.89999998,0};
				};
				class T2
				{
					maxT=400;
					color[]={0.56,0.62,0.67000002,0};
				};
				class T3
				{
					maxT=600;
					color[]={0.38999999,0.46000001,0.47,0};
				};
				class T4
				{
					maxT=800;
					color[]={0.23999999,0.31,0.31,0};
				};
				class T5
				{
					maxT=1000;
					color[]={0.23,0.31,0.28999999,0};
				};
				class T6
				{
					maxT=1500;
					color[]={0.20999999,0.28999999,0.27000001,0};
				};
				class T7
				{
					maxT=2000;
					color[]={0.19,0.23,0.20999999,0};
				};
				class T8
				{
					maxT=2300;
					color[]={0.22,0.19,0.1,0};
				};
				class T9
				{
					maxT=2500;
					color[]={0.34999999,0.2,0.02,0};
				};
				class T10
				{
					maxT=2600;
					color[]={0.62,0.28999999,0.029999999,0};
				};
				class T11
				{
					maxT=2650;
					color[]={0.58999997,0.34999999,0.050000001,0};
				};
				class T12
				{
					maxT=2700;
					color[]={0.75,0.37,0.029999999,0};
				};
				class T13
				{
					maxT=2750;
					color[]={0.88,0.34,0.029999999,0};
				};
				class T14
				{
					maxT=2800;
					color[]={0.91000003,0.5,0.17,0};
				};
				class T15
				{
					maxT=2850;
					color[]={1,0.60000002,0.2,0};
				};
				class T16
				{
					maxT=2900;
					color[]={1,0.70999998,0.30000001,0};
				};
				class T17
				{
					maxT=2950;
					color[]={0.98000002,0.82999998,0.41,0};
				};
				class T18
				{
					maxT=3000;
					color[]={0.98000002,0.91000003,0.54000002,0};
				};
				class T19
				{
					maxT=3100;
					color[]={0.98000002,0.99000001,0.60000002,0};
				};
				class T20
				{
					maxT=3300;
					color[]={0.95999998,0.99000001,0.72000003,0};
				};
				class T21
				{
					maxT=3600;
					color[]={1,0.98000002,0.91000003,0};
				};
				class T22
				{
					maxT=4200;
					color[]={1,1,1,0};
				};
			};
		};
		class GunClouds
		{
			cloudletGrowUp=0.050000001;
			cloudletFadeIn=0;
			cloudletFadeOut=0.1;
			cloudletDuration=0.050000001;
			cloudletAlpha=0.30000001;
			cloudletAccY=0;
			cloudletMinYSpeed=-100;
			cloudletMaxYSpeed=100;
			interval=-0.02;
			size=0.30000001;
			sourceSize=0.02;
			cloudletAnimPeriod=1;
			cloudletSize=1;
			cloudletShape="cloudletClouds";
			cloudletColor[]={1,1,1,0};
			timeToLive=0;
			initT=0;
			deltaT=0;
			class Table
			{
				class T0
				{
					maxT=0;
					color[]={1,1,1,0};
				};
			};
		};
		textureType="default";
		inertia=0.5;
		aimTransitionSpeed=1;
	};

	class RD501_LauncherCore: RD501_Default
	{
		type=4;
		dexterity=0.5;
		aiRateOfFire=0.5;
		aiRateOfFireDistance=300;
		count=1;
		inertia=1;
	};

	class RD501_MissileLauncher: RD501_LauncherCore
	{
		scope=1;
		type=65536;
		reloadTime=0.5;
		nameSound="MissileLauncher";
		cursor="EmptyCursor";
		cursorAim="missile";
		textureType="fullAuto";
	};

	class RD501_Missile_AGM_02_Plane_CAS_01_F: RD501_MissileLauncher
	{
		magazines[]=
		{
			"6Rnd_Missile_AGM_02_F",
			"PylonRack_1Rnd_Missile_AGM_02_F",
			"PylonRack_3Rnd_Missile_AGM_02_F"
		};
		holdsterAnimValue=3;
		reloadTime=0.1;
		magazineReloadTime=0.1;
		showAimCursorInternal=0;
		textureType="semi";
		weaponLockDelay=3;
		weaponLockSystem=2;
		cmImmunity=0.40000001;
		lockingTargetSound[]=
		{
			"\A3\Sounds_F\weapons\Rockets\locked_1",
			0.56234133,
			1
		};
		lockedTargetSound[]=
		{
			"\A3\Sounds_F\weapons\Rockets\locked_3",
			0.56234133,
			2.5
		};
		sounds[]=
		{
			"StandardSound"
		};
		class StandardSound
		{
			begin1[]=
			{
				"A3\Sounds_F_EPC\Weapons\missile_epc_1",
				1.7782794,
				1,
				3500
			};
			soundBegin[]=
			{
				"begin1",
				1
			};
		};
		modes[]=
		{
			"TopDown"
		};
		class Direct: RD501_MissileLauncher
		{
			textureType="semi";
			displayName="$STR_A3_Missile_AGM_02_Plane_CAS_01_F0";
			reloadTime=0.1;
			magazineReloadTime=0.1;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"A3\Sounds_F_EPC\Weapons\missile_epc_1",
					1.7782794,
					1,
					3500
				};
				soundBegin[]=
				{
					"begin1",
					1
				};
			};
			lockingTargetSound[]=
			{
				"\A3\Sounds_F\weapons\Rockets\locked_1",
				0.56234133,
				1
			};
			lockedTargetSound[]=
			{
				"\A3\Sounds_F\weapons\Rockets\locked_3",
				0.56234133,
				2.5
			};
			minRange=360;
			minRangeProbab=0.40000001;
			midRange=1500;
			midRangeProbab=1;
			maxRange=8000;
			maxRangeProbab=0.94999999;
		};
		class TopDown: Direct
		{
			textureType="topDown";
		};
		displayName="AGM-65 Maverick G";
	};

	class Missile_AGM_01_Plane_CAS_02_F: RD501_Missile_AGM_02_Plane_CAS_01_F
	{
		magazines[]=
		{
			"4Rnd_Missile_AGM_01_F",
			"PylonRack_1Rnd_Missile_AGM_01_F"
		};
		modes[]=
		{
			"Direct"
		};
		class Direct: Direct
		{
			displayName="$STR_A3_Missile_AGM_01_F0";
		};
		class TopDown: Direct
		{
			textureType="topDown";
		};
		displayName="Kh-25MTP";
	};

	//Torrent
	class RD501_Republic_Aircraft_Missile_AT_IR : Missile_AGM_01_Plane_CAS_02_F
	{
		displayName = "Torrent (AGM)";
		displayNameShort = "Torrent";
		canLock = 2;
		cmImmunity = 0.4;
		weaponLockSystem = "2 + 4";

		modes[]=
		{
			"Direct",
			"TopDown"
		};
		class Direct: Direct
		{
			displayName="$STR_A3_Missile_AGM_01_F0";
		};

		soundfly[] = {"\rd501_vehicle_weapons\_sounds\proton_torp.ogg",3,1,800};
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Missile_AT_IR_Mag_2",
			"RD501_Republic_Aircraft_Missile_AT_IR_Mag_3"
		};
	};

	//MRC
	class RD501_CIS_Static_Missile_AA_SR : RD501_MissileLauncher
	{
		weaponLockDelay=3;
		cmImmunity=0.60000002;
		weaponLockSystem="2 + 16";
		displayName = "Overlord";

		maxRange = 10000;
		maxRangeProbab = 1;
		midRange = 5000;
		midRangeProbab = 1;
		minRange = 100;
		minRangeProbab = 1;

		burst = 3;
		multiplier = 1;
		
		reloadTime=0.5;
		magazineReloadTime=450;
		magazines[] = 
		{
			"RD501_CIS_Static_Missile_AA_SR_Mag_21",
			"RD501_Republic_Static_Missile_AA_SR_Mag_21"
		};

		
		modes[] = {"TopDown"};
		class TopDown
		{
			textureType = "topdown";
			displayName = "Overlord";

			aiDispersionCoefY=0.5;
			aiDispersionCoefX=0.5;
			aiRateOfFire=9;
			aiRateOfFireDispersion=0;
			aiRateOfFireDistance=1000;

			maxRange = 10000;
			maxRangeProbab = 1;
			midRange = 5000;
			midRangeProbab = 1;
			minRange = 100;
			minRangeProbab = 1;
			
			reloadTime=0.5;
			magazineReloadTime=450;
			burst = 3;
			multiplier = 1;
		};

		holdsterAnimValue=1;
		textureType="semi";

		sounds[]=
		{
			"StandardSound"
		};
		class StandardSound
		{
			begin1[]=
			{
				"A3\Sounds_F\weapons\Rockets\missile_2",
				1.12202,
				1.3,
				1000
			};
			soundBegin[]=
			{
				"begin1",
				1
			};
			weaponSoundEffect="DefaultRifle";
		};
		soundFly[]=
		{
			"A3\Sounds_F\weapons\Rockets\rocket_fly_2",
			1,
			1.5,
			700
		};
		lockingTargetSound[]=
		{
			"\A3\Sounds_F\weapons\Rockets\locked_1",
			0.316228,
			1
		};
		lockedTargetSound[]=
		{
			"\A3\Sounds_F\weapons\Rockets\locked_3",
			0.316228,
			2.5
		};
		class GunParticles
		{
			class FirstEffect
			{
				effectName="MLRSFired";
				positionName="pos_missile_end";
				directionName="pos_missile";
			};
		};
	};

	//tyrant
	class RD501_CIS_Static_Missile_AA_LR : RD501_MissileLauncher
	{
		displayName = "LRAD Missile";
		weaponLockDelay=1.5;
		cmImmunity=0.69999999;
		weaponLockSystem=8;
		reloadTime=10;
		magazineReloadTime=600;
		magazines[]=
		{
			"RD501_CIS_Static_Missile_AA_LR_Mag_4",
			"RD501_Republic_Static_Missile_AA_LR_Mag_4"
		};
		aiRateOfFire=20;
		aiRateOfFireDispersion=-10;
		aiRateOfFireDistance=10000;
		maxRange=16000;
		maxRangeProbab=1;
		midRange=8000;
		midRangeProbab=1;
		minRange=50;
		minRangeProbab=1;		
		textureType="semi";

		
		modes[] = {"TopDown"};
		class TopDown
		{
			textureType = "topdown";
    		displayName = "Top-down";

			aiDispersionCoefY=0.5;
			aiDispersionCoefX=0.5;
			aiRateOfFire=20;
			aiRateOfFireDispersion=-10;
			aiRateOfFireDistance=10000;

			reloadTime=10;
			magazineReloadTime=600;

			maxRange=16000;
			maxRangeProbab=1;
			midRange=8000;
			midRangeProbab=1;
			minRange=50;
			minRangeProbab=1;	
		};

		sounds[]=
		{
			"StandardSound"
		};
		class StandardSound
		{
			begin1[]=
			{
				"A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\Titan",
				1.41254,
				1,
				1100
			};
			soundBegin[]=
			{
				"begin1",
				1
			};
			soundSetShot[]=
			{
				"Static_Launcher_Titan_ATAA_Shot_SoundSet",
				"Static_Launcher_Titan_ATAA_Tail_SoundSet"
			};
		};
		lockedTargetSound[]=
		{
			"A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\Locked_Titan",
			0.56234133,
			2.5
		};
		lockingTargetSound[]=
		{
			"A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\Locking_Titan",
			0.56234133,
			1
		};
		soundFly[]=
		{
			"A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\rocket_fly",
			1,
			1.1,
			700
		};
		class GunParticles
		{
			class FirstEffect
			{
				effectName="MLRSFired";
				positionName="pos_missile_backBlastFx_dir";
				directionName="pos_missile_backBlastFx";
			};
		};
	};

	//Zephyr
	class RD501_Republic_Aircraft_Missile_AA_IR : missiles_Zephyr
	{
		displayName = "Zephyr (AA)";
		displayNameShort = "Zephyr";
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag_12",
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag",
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag_2",
			"RD501_CIS_Aircraft_Missile_AA_IR_Mag_2",
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag_3"
		};
		soundFly[] = {""};
		lockedTargetSound[] = {""};
		lockingTargetSound[] = {""};
		magazineReloadTime=15;
		aiRateOfFire = 5;
		aiRateOfFireDispersion = 1;
		aiRateOfFireDistance = 1;		

		minRange = 500;
		minRangeProbab = 0.3;
		midRange = 1500;
		midRangeProbab = 1;
		maxRange = 3000;
		maxRangeProbab = 1;
	};

	//Flashfire
	class RD501_Republic_Aircraft_Missile_AT_Wire : missiles_Jian
	{
		displayName = "Flashfire (LGM)";
		displayNameShort = "Flashfire";
		soundfly[] = {"\rd501_vehicle_weapons\_sounds\proton_torp.ogg",3,1,800};
		magazines[] = {"RD501_Republic_Aircraft_Missile_AT_Wire_Mag"};
		weaponLockSystem=4;
	};

	//Hurricane
	class RD501_Republic_Aircraft_Missile_AP : Rocket_04_AP_Plane_CAS_01_F
	{
		displayName = "Hurricane (UGM)";
		displayNameShort = "Hurricane";
		soundfly[] = {""};
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Missile_AP_Mag_4",
			"RD501_Republic_Aircraft_Missile_AP_Mag_8",
			"RD501_Republic_Aircraft_Missile_AP_Mag_16"
		};
		modes[] =
		{
			"single",
			"ripple",
			"salvo",
		};
		class single
		{
			displayNameShort = "Single";
			textureType="semi";
			autoFire = 0;
			showToPlayer = 1;

			reloadtime = 0.2;
			dispersion = 0.0002;
			burst = 1;
			burstRangeMax = 0;
			multiplier = 1;

			aiRateOfFire = 1;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 200;		

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 500;
			midRangeProbab = 0.8;
			maxRange = 800;
			maxRangeProbab = 0.5;

			ffCount = 6;
			ffFrequency = 11;
			ffMagnitude = 0.5;

			recoil = "Empty";
		};

 		class ripple : single
		{
			displayNameShort = "Ripple";
			textureType="burst";
			burst = 2;
			autoFire=1;

			minRange = 25;
			minRangeProbab = 0.4;
			midRange = 250;
			midRangeProbab = 0.8;
			maxRange = 500;
			maxRangeProbab = 0.5;
		};
		class salvo : single
		{
			displayNameShort = "Salvo";
			textureType="fullAuto";
			autofire = 1;
			burst = 1;
		};

	};

	//HARM Invader
	class RD501_Republic_Aircraft_Missile_ARM_LR : RD501_MissileLauncher
	{
		displayName = "HARM";
		displayNameShort = "HARM";
		weaponLockDelay = 2;
		magazines[] = {"RD501_Republic_Aircraft_Missile_ARM_LR_Mag"};
		showEmpty=false;
		weaponLockSystem=0;
		cmImmunity=0.75;
		showAimCursorInternal=0;
		reloadTime=0.1;
		magazineReloadTime=0.1;
		aiRateOfFire=15;
		aiRateOfFireDispersion=-10;
		aiRateOfFireDistance=10000;
		minRange=1000;
		minRangeProbab=0.89999998;
		midRange=3000;
		midRangeProbab=1;
		maxRange=16000;
		maxRangeProbab=1;
		textureType="semi";
		sounds[]=
		{
			"StandardSound"
		};
		class StandardSound
		{
			begin1[]=
			{
				"A3\Sounds_F\weapons\Rockets\missile_2",
				1.12202,
				1.3,
				1000
			};
			soundBegin[]=
			{
				"begin1",
				1
			};
			weaponSoundEffect="DefaultRifle";
		};
		soundFly[]=
		{
			"A3\Sounds_F\weapons\Rockets\rocket_fly_1",
			1,
			1.5,
			700
		};
		lockedTargetSound[]=
		{
			"\A3\Sounds_F\weapons\Rockets\locked_3",
			0.56234133,
			2.5
		};
		lockingTargetSound[]=
		{
			"\A3\Sounds_F\weapons\Rockets\locked_1",
			0.56234133,
			1
		};
	};

	//HARM Intruder
	class RD501_Republic_Aircraft_Missile_ARM_SR : RD501_Republic_Aircraft_Missile_ARM_LR
	{
		displayName = "HARM";
		displayNameShort = "HARM";
		weaponLockDelay = 2;
		magazines[] = {"RD501_Republic_Aircraft_Missile_ARM_SR_Mag"};
		showEmpty=false;
/* 		removed for now due to flight path issues
		modes[] = 
		{
			"Direct",
			"TopDown"
		};
		class Direct : RD501_Republic_Aircraft_Missile_ARM_LR
		{
			textureType = "semi";
   			displayNameShort = "Direct";
			displayName= "Direct";
		};
		class TopDown : Direct
		{
			textureType = "topdown";
			displayName= "Top-down";
			displayNameShort = "Top-down";
		}; */
	};

	//amraam
	class missiles_ASRAAM;
	class RD501_Republic_Aircraft_Missile_AR_LR : missiles_ASRAAM
	{
		displayName = "Charhound";
		displayNameShort = "Charhound";
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Missile_AR_LR_Mag"
		};
		soundFly[] = {""};
		lockedTargetSound[] = {""};
		lockingTargetSound[] = {""};
	};

	//Mako
	class RD501_Republic_Aircraft_Missile_AR_XLR : missiles_ASRAAM
	{
		displayName = "Mako (AAM)";
		displayNameShort = "Mako (AAM)";
		magazines[] = 
		{
			"RD501_Republic_Aircraft_Missile_AR_XLR_Mag"
		};
		soundFly[] = {""};
		lockedTargetSound[] = {""};
		lockingTargetSound[] = {""};
	};
}; 

