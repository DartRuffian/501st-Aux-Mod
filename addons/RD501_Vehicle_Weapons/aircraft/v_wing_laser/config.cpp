
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class macro_patch_name(v_wing_laser)
	{
		author=MACRO_QUOTE(DANKAUTHORS);
		addonRootClass=MACRO_QUOTE(macro_patch_name(vehicle_weapons));
		requiredAddons[]=
		{
			MACRO_QUOTE(macro_patch_name(vehicle_weapons))
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			MACRO_QUOTE(macro_new_weapon(laser,v_wing))
		};
	};
};

class CfgWeapons
{
	class CannonCore;
	class Cannon_30mm_Plane_CAS_02_F: CannonCore
	{
		class LowROF;
	};
	class macro_new_weapon(laser,v_wing):Cannon_30mm_Plane_CAS_02_F
	{
		scope=2;
		displayName="Aircraft Laser Gun";
		modes[]=
		{
			"LowROF"
		};
		canLock=1;
		ballisticsComputer=1;
		weaponLockSystem=0;
		magazines[]=
		{
			macro_new_mag(generic_aircraft_gun_asg,3000)
		};
		magazineWell[]=
		{
			"RD501_generic_aircraft_laser_magwell"
		};
		class LowROF: LowROF
		{
			displayName="Energy Laser";
			multiplier=1;
			sounds[]=
			{
				"StandardSound"
			};
			class StandardSound
			{
				begin1[]=
				{
					"3AS\3AS_AAT\data\sounds\AAT_Cannon.wss",
					1,
					0.89999998,
					6000
				};
				begin2[]=
				{
					"3AS\3AS_AAT\data\sounds\AAT_Cannon.wss",
					1,
					0.89999998,
					6000
				};
				soundBegin[]=
				{
					"begin1",
					0.1,
					"begin2",
					0.5
				};
				class SoundTails
				{
					class TailForest
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_forest",
							1,
							1,
							2200
						};
						frequency=1;
						volume="(1-interior/1.4)*forest";
					};
					class TailHouses
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_houses",
							1,
							1,
							2200
						};
						frequency=1;
						volume="(1-interior/1.4)*houses";
					};
					class TailInterior
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_interior",
							1.99526,
							1,
							2200
						};
						frequency=1;
						volume="interior";
					};
					class TailMeadows
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_meadows",
							1,
							1,
							2200
						};
						frequency=1;
						volume="(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailTrees
					{
						sound[]=
						{
							"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_trees",
							1,
							1,
							2200
						};
						frequency=1;
						volume="(1-interior/1.4)*trees";
					};
				};
			};
			soundContinuous=0;
			flashSize=1;
			dispersion=0.002;
			autoFire="true";
			aiRateOfFire=0.001;
			reloadTime=0.02;
			aiRateOfFireDispersion=0;
			aiRateOfFireDistance=0;
			minRange=0;
			minRangeProbab=1;
			midRange=7500;
			midRangeProbab=1;
			maxRange=15000;
			maxRangeProbab=1;
			weaponLockDelay=0;
			weaponLockSystem=0;
			lockAcquire=1;
			FCSMaxLeadSpeed=1000;
			burst=2;
		};
	};
};

