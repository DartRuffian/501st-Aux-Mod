
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_speedboat_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			macro_new_weapon(speedboat,blaster)
		};
	};
};
class CfgWeapons
{
	class HMG_127;
	class MGun;
	class OPTRE_M41_LAAG;
    class macro_new_weapon(speedboat,blaster):OPTRE_M41_LAAG
	{
		magazines[] = 
        {
            macro_new_mag(republic_speedboat_m41_charge,1000)
        };
		class GunParticles
		{
			class effect1
			{
	
			};
			class effect2
			{
				
			};
			class effect3
			{
				
			};
		};
		selectionFireAnim = "zasleh";
		displayName = "Republic M41 Heavy Blaster";
		descriptionShort = "Heavy Blaster";
		aiDispersionCoefY = 1.25;
		aiDispersionCoefX = 1.25;
		showAimCursorInternal = 1;
		magazineReloadTime = 4;
		initFov = 0.75;
		minFov = 0.375;
		maxFov = 1.1;
		cursor = "EmptyCursor";
		cursorAim = "OPTRE_M41";
		modes[] = {"FullAuto","close","short","medium"};
		class FullAuto: MGun
		{
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin2[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin3[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin4[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin5[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin6[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin7[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin8[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				begin9[] = {"kobra\442_weapons\sounds\cannon\cannon14.wss",1,1,1500};
				soundBegin[] = {"begin1",0.2,"begin2",0.1,"begin3",0.1,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1};
				class SoundTails
				{
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_trees",1.0,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_forest",1.0,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_interior",1.9952624,1,1200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_meadows",1.0,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\Machineguns\Mk200\MK200_tail_houses",1.0,1,1200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
				};
			};
			reloadTime = 0.1;
			dispersion = 0.005;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 800;
			maxRangeProbab = 0.05;
		};
		class close: FullAuto
		{
			burst = 25;
			dispersion = 0.05;
			aiRateOfFire = 0.06;
			aiRateOfFireDistance = 50;
			minRange = 10;
			minRangeProbab = 0.05;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.04;
			showToPlayer = 0;
		};
		class short: close
		{
			burst = 18;
			dispersion = 0.025;
			aiRateOfFire = 0.1;
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 300;
			maxRangeProbab = 0.04;
		};
		class medium: close
		{
			burst = 10;
			dispersion = 0.005;
			aiRateOfFire = 0.16;
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.05;
			midRange = 300;
			midRangeProbab = 0.7;
			maxRange = 500;
			maxRangeProbab = 0.1;
		};
	};
};