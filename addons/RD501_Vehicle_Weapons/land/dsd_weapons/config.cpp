
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_dsd_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			macro_new_weapon(dsd,cannon)
		};
	};
};
class CfgWeapons
{
	class MGun;
	class LMG_RCWS: MGun
	{
		class manual: MGun{};
	};
	class macro_new_weapon(dsd,cannon): LMG_RCWS
	{
		displayName = "DSD Blaster Cannon";
		displayNameShort = "DSD Blaster";
		fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};	
		magazines[] = 
		{
			macro_new_mag(dsd_cannon_charge,20)
		};
		modes[] = {"manual","short","medium","far"};
		magazineReloadTime = 3;
		class manual
		{
			displayName = "DSD Blaster";
			textureType = "fullAuto";
			reloadTime = 1;
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_2",0.316228,1,20};
				closure2[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_3",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin2[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin3[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin4[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin5[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin6[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin7[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin8[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin9[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				begin10[] = {"swlw_rework\sounds\z-series\z6\Z6_chargedShot1.wss",0.95,1,2000};
				soundBegin[] = {"begin1",0.1,"begin2",0.1,"begin3",0.1,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
			};
			autoFire = 0;
			soundContinuous = 0;
			burst = 1;
			soundBurst = 0;
			multiplier = 1;
			dispersion = 0.0015;
			aiRateOfFire = 1;
			showToPlayer = 1;
			aiRateOfFireDistance = 10;
			minRange = 0;
			minRangeProbab = 0.01;
			midRange = 1;
			midRangeProbab = 0.01;
			maxRange = 2;
			maxRangeProbab = 0.01;
		};
		class close: manual
		{
			soundBurst = 0;
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 42;
			aiRateOfFire = 0.5;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 10;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 0;
			midRangeProbab = 0.75;
			maxRange = 300;
			maxRangeProbab = 0.2;
		};
		class short: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 36;
			aiRateOfFire = 1;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 150;
			minRange = 0;
			minRangeProbab = 0.75;
			midRange = 300;
			midRangeProbab = 0.75;
			maxRange = 600;
			maxRangeProbab = 0.2;
		};
		class medium: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 30;
			aiRateOfFire = 2;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 300;
			minRange = 300;
			minRangeProbab = 0.75;
			midRange = 600;
			midRangeProbab = 0.65;
			maxRange = 800;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 1;
			burstRangeMax = 12;
			aiRateOfFire = 4;
			aiRateOfFireDispersion = 4;
			aiRateOfFireDistance = 800;
			minRange = 800;
			minRangeProbab = 0.65;
			midRange = 1000;
			midRangeProbab = 0.3;
			maxRange = 1500;
			maxRangeProbab = 0.05;
		};
	};
};

