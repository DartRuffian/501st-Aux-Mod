#include "../../RD501_main/config_macros.hpp"
class CfgPatches
{
	class RD501_patch_vehicle_mags
	{
		author="RD501";
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			"3AS_VehicleWeapons",
			"A3_weapons_F_Jets"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
		};
	};
};

class CfgMagazines
{
	class VehicleMagazine;
	class SmokeLauncherMagMK2: VehicleMagazine
	{
		author="RD501";
		scope=2;
		ammo="SmokeLauncherAmmoMK2";
		count=2;
		nameSound="smokeshell";
		initSpeed=14;
	};

///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Aircraft Cannons and lasers////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
	//CM 
	class CA_Magazine;
	class RD501_CMFlare_Magazine_200 : CA_Magazine
	{
		author="RD501";
		count=200;
		ammo="RD501_CMflare_Chaff_Ammo";
		initSpeed=30;
	};


	//laser cannons
	class RD501_Republic_Aircraft_Laser_Cannon_Mag_100 : VehicleMagazine
	{
		author="RD501";
		scope=2;
		displayNameShort="Laser Cannons x100";

		nameSound="cannon";

		ammo= "RD501_Republic_Aircraft_Laser_Cannon_Ammo";
		count=100;
		initSpeed=2000;
		maxLeadSpeed=120;

		tracersEvery=1;
		muzzleImpulseFactor[]={0,0};
	};
	class RD501_CIS_Aircraft_Laser_Cannon_Mag_100 : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{
		ammo= "RD501_Republic_CIS_Laser_Cannon_Ammo";
	};

	//LE gun
	class RD501_Republic_Aircraft_Laser_Blaster_Mag_75 : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{
		displayNameShort="Laser Blaster x75";

		ammo= "RD501_Republic_Aircraft_Laser_Cannon_Ammo";
		count=75;
		initSpeed=1500;
		maxLeadSpeed=120;
	};

	class RD501_CIS_AA_Laser_Blaster_Mag_75 : RD501_Republic_Aircraft_Laser_Blaster_Mag_75 
	{
		ammo="RD501_CIS_AA_Laser_Blaster_Ammo";
	};

	//voltic
	class RD501_Republic_Aircraft_Laser_Repeater_Mag_1000 : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{
		displayName = "Voltic";
		displayNameShort = "Voltic";

		ammo = "RD501_Republic_Aircraft_Laser_Repeater_Ammo";
		count = 1000;
		initSpeed = 1050;
		maxLeadSpeed = 300;
	};

	class RD501_Republic_Aircraft_Laser_Repeater_Pod_Mag_500 : RD501_Republic_Aircraft_Laser_Repeater_Mag_1000
	{
		displayName = "Voltic Gunpod";
		displayNameShort = "Voltic Gunpod";

		ammo = "RD501_Republic_Aircraft_Laser_Repeater_Ammo";
		count = 500;
		initSpeed = 1050;
		maxLeadSpeed = 300;
		mass = 50;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Laser_Repeater_Pylon"
		};
		pylonWeapon= "RD501_Republic_Aircraft_Laser_Repeater_Pod";
	};

	//laser AA
	class RD501_Republic_Aircraft_Laser_AA_Mag_600 : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{

		displayNameShort="Laser AA";

		ammo= "RD501_Republic_Aircraft_Laser_AA_Ammo";
		count=600;
		initSpeed=1050;
		maxLeadSpeed=300;
	};

	class RD501_CIS_Aircraft_Laser_AA_Mag_600 : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{

		displayNameShort="Laser AA";

		ammo= "RD501_CIS_Aircraft_Laser_AA_Ammo";
		count=600;
		initSpeed=1050;
		maxLeadSpeed=300;
	};

	//Coaxium
	class RD501_Republic_Aircraft_Laser_Turbo_Mag_15 : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{
		displayName = "Coaxium Gunpod";
		displayNameShort = "Coaxium";
		
		ammo = "RD501_Republic_Aircraft_Laser_Turbo_Ammo";
		count = 15;
		initSpeed=1050;
		maxLeadSpeed=300;
		mass = 50;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Laser_Turbo_Pylon"
		};
		pylonWeapon= "RD501_Republic_Aircraft_Laser_Turbo";
	};

	//AA magazine 
	class macro_new_mag(ground_laser_aa,2000) : RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{
		displayNameShort = "AA Laser";
		
		ammo = MACRO_QUOTE(macro_new_ammo(ground_laser_aa));
		count=2000;
		initSpeed=1050;
		maxLeadSpeed=1200;
	};

	//CIS Mag
	class RD501_CIS_Laster_AA_Mag : macro_new_mag(ground_laser_aa,2000)
	{
		displayNameShort = "CIS Laser";
		
		ammo = MACRO_QUOTE(macro_new_ammo(aircraft_laser_CIS));
	};

	//LAAT Ball
	class RD501_Republic_Aircraft_Laser_Beam_Mag_500: RD501_Republic_Aircraft_Laser_Cannon_Mag_100
	{
		displayNameShort = "Ball Turret Mag";

		ammo = "RD501_Republic_Aircraft_Laser_Beam_Ammo";
		count = 500;
		initSpeed=1120;
		maxLeadSpeed=300;
	};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Missiles///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Zephyr
    class RD501_Republic_Aircraft_Missile_AA_IR_Mag_12 : VehicleMagazine
	{
		author="RD501";
		scope=2;
		displayName = "Zephyr AA 12x";
		displayNameShort = "Zephyr AA";

		ammo = "RD501_Republic_Aircraft_Missile_AA_IR_Ammo";
		count = 12;
		initSpeed=100;

		tracersEvery=1;
		muzzleImpulseFactor[]={0,0};
	};

/////////////////////////////////////////////////Pylons//////////////////////////////////////////////////////
	//Zephyr
	class RD501_Republic_Aircraft_Missile_AA_IR_Mag : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Zephyr AA 1x";
		displayNameShort = "Zephyr AA";
		count = 1;
		mass = 50;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AA_IR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AA_IR";
	};

	class RD501_Republic_Aircraft_Missile_AA_IR_Mag_2 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Zephyr AA 2x";
		displayNameShort = "Zephyr AA";
		count = 2;
		mass = 100;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AA_IR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AA_IR";
	};

	class RD501_CIS_Aircraft_Missile_AA_IR_Mag_2 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Zephyr AA 2x";
		displayNameShort = "Zephyr AA";
		ammo = "RD501_CIS_Aircraft_Missile_AA_IR_Ammo";
		count = 2;
		mass = 100;
	};

	class RD501_Republic_Aircraft_Missile_AA_IR_Mag_3 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Zephyr AA 3x";
		displayNameShort = "Zephyr AA";
		count = 3;
		mass = 150;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AA_IR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AA_IR";
	};
	
	//Flashfire
	class RD501_Republic_Aircraft_Missile_AT_Wire_Mag : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Flashfire 1x";
		displayNameShort = "Flashfire";
		mass = 25;

		ammo = "RD501_Republic_Aircraft_Missile_AT_Wire_Ammo";
		count = 1;
		initSpeed=200;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AT_Wire_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AT_Wire";
	};

	//Torrent
	class RD501_Republic_Aircraft_Missile_AT_IR_Mag_2 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Torrent 2x";
		displayNameShort = "Torrent";
		mass = 25;
		ammo = "RD501_Republic_Aircraft_Missile_AT_IR_Ammo";
		count = 2;
		initSpeed=200;
		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AT_IR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AT_IR";
	};

	class RD501_Republic_Aircraft_Missile_AT_IR_Mag_3 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Torrent 3x";
		displayNameShort = "Torrent";
		mass = 50;
		ammo = "RD501_Republic_Aircraft_Missile_AT_IR_Ammo";
		count = 3;
		initSpeed=200;
		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AT_IR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AT_IR";
	};

	//Hurricane
	class RD501_Republic_Aircraft_Missile_AP_Mag_4 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Hurricane 4x";
		displayNameShort = "Hurricane";
		mass = 25;

		ammo = "RD501_Republic_Aircraft_Missile_AP_Ammo";
		count = 4;
		initSpeed=200;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AP_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AP";
	};

		class RD501_Republic_Aircraft_Missile_AP_Mag_8 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Hurricane 8x";
		displayNameShort = "Hurricane";
		mass = 40;

		ammo = "RD501_Republic_Aircraft_Missile_AP_Ammo";
		count = 8;
		initSpeed=200;
	};

	class RD501_Republic_Aircraft_Missile_AP_Mag_16 : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Hurricane 16x";
		displayNameShort = "Hurricane";
		mass = 50;

		ammo = "RD501_Republic_Aircraft_Missile_AP_Ammo";
		count = 16;
		initSpeed=200;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AP_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AP";
	};

	//Big HARM Invader
	class RD501_Republic_Aircraft_Missile_ARM_LR_Mag : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Invader HARM";
		displayNameShort = "Invader HARM";
		mass = 50;

		ammo = "RD501_Republic_Aircraft_Missile_ARM_LR_Ammo";
		count = 1;
		initSpeed = 200;
		initSpeedY = 50;
		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_ARM_LR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_ARM_LR";
	};

	//Smol HARM Intruder
	class RD501_Republic_Aircraft_Missile_ARM_SR_Mag : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Intruder HARM";
		displayNameShort = "Intruder HARM";
		mass = 50;

		ammo = "RD501_Republic_Aircraft_Missile_ARM_SR_Ammo";
		count = 1;
		initSpeed = 200;
		initSpeedY = 50;
		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_ARM_SR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_ARM_SR";
	};

	class RD501_Republic_Aircraft_Missile_AR_LR_Mag : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Charhound (AAM)";
		displayNameShort = "Charhound (AAM)";
		mass = 50;

		ammo = "RD501_Republic_Aircraft_Missile_AR_LR_Ammo";
		count = 1;
		initSpeed=350;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AR_LR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AR_LR";
	};

	//Mako
	class RD501_Republic_Aircraft_Missile_AR_XLR_Mag : RD501_Republic_Aircraft_Missile_AA_IR_Mag_12
	{
		displayName = "Mako (AAM)";
		displayNameShort = "Mako (AAM)";
		mass = 50;

		ammo = "RD501_Republic_Aircraft_Missile_AR_XLR_Ammo";
		count = 1;
		initSpeed=350;

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Missile_AR_XLR_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Missile_AR_XLR";
	};

	//tyrant mag
	class RD501_CIS_Static_Missile_AA_LR_Mag_4 : VehicleMagazine
	{
		scope=2;
		displayName = "Tyrant 10x";
		displayNameShort = "Tyrant";
		ammo="RD501_CIS_Static_Missile_AA_LR_Ammo";
		count=4;
		initSpeed=45;
		maxLeadSpeed=1800;
		sound[]=
		{
			"A3\Sounds_F\weapons\Rockets\titan_2",
			1.25893,
			1,
			1000
		};
		soundFly[]=
		{
			"A3\Sounds_F\weapons\Rockets\rocket_fly_2",
			0.50118703,
			1.3,
			400
		};
		reloadSound[]=
		{
			"",
			0.00031622799,
			1,
			20
		};
		soundHit[]=
		{
			"",
			1.25893,
			1,
			1
		};
		nameSound="missiles";
	};

	class RD501_Republic_Static_Missile_AA_LR_Mag_4 : RD501_CIS_Static_Missile_AA_LR_Mag_4
	{
		ammo="RD501_Republic_Static_Missile_AA_LR_Ammo";
	};

	//MRC mag
	class RD501_CIS_Static_Missile_AA_SR_Mag_21 : VehicleMagazine
	{
		scope=2;
		displayName = "Overlord 21x";
		displayNameShort = "Overlord 21x";
		ammo="RD501_CIS_Static_Missile_AA_SR_Ammo";
		count=21;
		initSpeed=40;
		maxLeadSpeed=750;
		sound[]=
		{
			"A3\sounds_f\dummysound",
			1,
			1,
			1300
		};
		reloadSound[]=
		{
			"A3\sounds_f\dummysound",
			0.00031622799,
			1,
			20
		};
		nameSound="missiles";
	};

	class RD501_Republic_Static_Missile_AA_SR_Mag_21 : RD501_CIS_Static_Missile_AA_SR_Mag_21
	{
		ammo="RD501_Republic_Static_Missile_AA_SR_Ammo";
	};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Bombs//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class RD501_Republic_Aircraft_Bomb_Laser_Glide_Mag_4 : VehicleMagazine
	{
		author="RD501";
		scope=2;
		displayName = "Wraith 4x";
		displayNameShort = "Wraith";

		ammo = "RD501_Republic_Aircraft_Bomb_Laser_Glide_Ammo";
		count = 4;
		initSpeed=0;
		mass = 50;

		tracersEvery=0;
		muzzleImpulseFactor[]={0,0};

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Bomb_Laser_Glide_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Bomb_Laser_Glide";
	};

	class RD501_Republic_Aircraft_Bomb_Laser_Glide_Mag_12 : VehicleMagazine
	{
		author="RD501";
		scope=2;
		displayName = "Wraith 12x";
		displayNameShort = "Wraith";

		ammo = "RD501_Republic_Aircraft_Bomb_Laser_Glide_Ammo";
		count = 12;
		initSpeed=0;
		mass = 100;

		tracersEvery=0;
		muzzleImpulseFactor[]={0,0};

		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Bomb_Laser_Glide_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Bomb_Laser_Glide";
	};

	class RD501_Republic_Aircraft_Bomb_Dumb_Mag_20 : RD501_Republic_Aircraft_Bomb_Laser_Glide_Mag_4
	{
		displayName = "Slagger 20x";
		displayNameShort = "Slagger";

		ammo = "RD501_Republic_Aircraft_Bomb_Dumb_Ammo";
		count = 20;
		hardpoints[]=
		{
			"RD501_Republic_Aircraft_Bomb_Dumb_Pylon"
		};
		pylonWeapon = "RD501_Republic_Aircraft_Bomb_Dumb";
	};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Tank Cannons///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	class 100Rnd_TXLaser_Cannon_mag;
	class 3AS_24Rnd_GAT_AP;
	class 3as_24Rnd_AAT_AP;
	class 3as_15rnd_ATAP_AT_Mag;

	class macro_new_mag(aat_mbt,50): 3AS_24Rnd_GAT_AP
	{
		displayNameShort = "MBT 50rnd mag";
		ammo =MACRO_QUOTE(macro_new_ammo(aat_mbt));
		count = 50;
		tracersEvery = 1;
		initSpeed = 600;
		muzzleImpulseFactor = 0;
		maxLeadSpeed = 300;
	};

	class macro_new_mag(aat_mbt,10): 3as_24Rnd_AAT_AP
	{
		displayNameShort = "10Rnd King Laser Mag";
		ammo =MACRO_QUOTE(macro_new_ammo(aat_king));
		count = 10;
		tracersEvery = 1;
		//typicalSpeedSpeed = 800;
		typicalSpeedSpeed = 20;
		muzzleImpulseFactor = 0;
		maxLeadSpeed = 600;
	};
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////CIS Heavy Infantry///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	class 3AS_10Rnd_EM90_Mag;
	class macro_new_mag(dsd_cannon_charge,20): 3AS_10Rnd_EM90_Mag
	{
		displayname = "DSD Cannon Charge";
		ammo =MACRO_QUOTE(macro_new_ammo(cis_dsd_cannon_ammo));
		muzzleimpulsefactor[] = {0.05,0.05};
		displaynamemagazine = "DSD Cannon Charge";
		shortnamemagazine = "DSD Charge";
		displayNameMFDFormat = "DSD Charge";
		displayNameShort = "DSD Charge";
		count = 20;
		mass = 13;
		initSpeed = 300;
		tracersEvery = 1;
		lastRoundsTracer = 20;
	};
	
	class macro_new_mag(CIS_droideka_sniper_cell,10): 3AS_10Rnd_EM90_Mag
	{
		scope = 2;
		displayName = "Droideka Sniper Magazine";
		picture = "\3AS\3AS_Weapons\Data\Textures\Energy_Cell_Arsenal.paa";
		ammo=MACRO_QUOTE(macro_new_ammo(cis_droideka_sniper_ammo));
		count = 10;
		mass = 13;
		initSpeed = 250;
		tracersEvery = 1;
		lastRoundsTracer = 10;
	};
	class Droideka_mag;
	class macro_new_mag(droideka_dual_blaster_cell,60): Droideka_mag
	{
		scope = 2;
		displayName = "Droideka Dual Blasters Charge";
		picture = "";
		ammo = "Droideka_ammo";
		tracersEvery = 1;
		type = 16;
		count = 60;
		descriptionShort = "Magazine";
		mass = 10;
		initSpeed = 310;
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////CIS Walkers//////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class DBA_80mm_UBF5_x50_mag;
	class macro_new_mag(octo_cannon_charge,50): DBA_80mm_UBF5_x50_mag
	{
		displayname = "80mw Tri-Droid Charge";
		ammo=MACRO_QUOTE(macro_new_ammo(cis_octo_cannon_ammo));
		muzzleimpulsefactor[] = {0.05,0.05};
		displaynamemagazine = "80mw Tri-Droid Charge";
		shortnamemagazine = "Tri-Droid Charge";
		displayNameMFDFormat = "UBF-5";
		displayNameShort = "Tri-Droid Charge";
		count = 50;
		initSpeed = 220;
		tracersevery = 1;
	};
	class RD501_homing_spider_x10_mag: macro_new_mag(aat_mbt,10)
	{
		displayNameShort = "10Rnd OG 9 Laser Charge";
		ammo = "RD501_homing_spider_charge_ammo";
		count = 10;
		tracersEvery = 1;
		initSpeed = 220;
		//typicalSpeedSpeed = 800;
		typicalSpeedSpeed = 20;
		muzzleImpulseFactor = 0;
		maxLeadSpeed = 600;
	};
	
	class Aux501_Weapons_Mags_30mw10;
	class RD501_homing_spider_eweb_x250_mag: Aux501_Weapons_Mags_30mw10
	{
		displayname = "Heavy Repeating Blaster Cell";
		ammo = "RD501_homing_spider_eweb_charge_ammo";
		count = 250;
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////Boats////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class OPTRE_1000Rnd_127x99_M41;
	class macro_new_mag(republic_speedboat_m41_charge,1000): OPTRE_1000Rnd_127x99_M41 
	{
		displayName = "Republic M41 Heavy Blaster Charge";
		displayNameShort = "M41 Blaster Charge";
		ammo = "Aux501_Weapons_Ammo_30mw";
		initSpeed = 900;
		count = 1000;
		tracersEvery = 1;
		lastRoundsTracer = 1000;
		muzzleImpulseFactor[] = {0,0};
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////Statics//////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	class RD501_CIS_Repeater_Mag_24: RD501_CIS_AA_Laser_Blaster_Mag_75
	{
		displayname = "CIS Repeater Cell";
		displayNameShort="Repeater Cell x24";
		ammo="RD501_reapter_ammo";
		count = 24;
	};
	class RD501_CIS_Proton_Mag_he_12: macro_new_mag(aat_mbt,10)
	{
		ammo = "RD501_proton_cannon_he_ammo";
		count = 12;
	};
	class RD501_CIS_Proton_Mag_ap_12: macro_new_mag(aat_mbt,10)
	{
		ammo = "RD501_proton_cannon_ap_ammo";
		count = 12;
	};


	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////Speeders/////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class macro_new_mag(barc_ap,50): VehicleMagazine
	{
		scope = 2;
		ammo = MACRO_QUOTE(macro_new_ammo(barc_AP));
		initSpeed = 350;
		maxLeadSpeed = 25;
		nameSound = "";
		count = 50;
		displayName = "50Rnd AP Plasma Cartridge";
		descriptionShort = "PT Plasma Cartridge";
		muzzleImpulseFactor[] = {0.1,0.1};
	};
	class ls_50Rnd_speederHE_belt;
	class macro_new_mag(barc_he,50): ls_50Rnd_speederHE_belt
	{
		scope = 2;
		ammo = MACRO_QUOTE(macro_new_ammo(barc_HE));
		initSpeed = 350;
		maxLeadSpeed = 25;
		nameSound = "";
		count = 50;
		displayName = "50Rnd HE Plasma Cartridge";
		descriptionShort = "HE Plasma Cartridge";
		muzzleImpulseFactor[] = {0.1,0.1};
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////Resupply/////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class 2Rnd_GBU12_LGB;

	// Magazine names needs to be harded coded in order for custom scripts to
	// work properly.

	// Guided resupply pod - ammo resupply magazine.
    class RD501_Guided_Resupply_Ammo_Resupply_Magazine: 2Rnd_GBU12_LGB
    {
        author = MACRO_AUTHOR;
        ammo = MACRO_QUOTE(macro_new_ammo(guided_resupply_ammo_resupply));
        displayName = "Ammo Supply Pod";
        displayNameShort = "Ammo Supply Pod";
        descriptionShort = "Ammo Supply Pod";
    };

	// Guided resupply pod - medical resupply magazine.
    class RD501_Guided_Resupply_Medical_Resupply_Magazine: 2Rnd_GBU12_LGB
    {
        author = MACRO_AUTHOR;
        ammo = MACRO_QUOTE(macro_new_ammo(guided_resupply_medical_resupply));
        displayName = "MED Supply Pod";
        displayNameShort = "MED Supply Pod";
        descriptionShort = "Medical Supply Pod";
    };

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////Cruise Missiles//////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	class RD501_Cruise_Missile_Magazine_Base: VehicleMagazine
	{
		count = 18;
		scope = 2;
		nameSound = "missiles";
		maxLeadSpeed = 1.38889;
		initSpeed = 20;
	};

	class RD501_Mash_Missile_Magazine: RD501_Cruise_Missile_Magazine_Base
	{
		author = MACRO_AUTHOR;
		ammo = "RD501_Mash_Missile_Ammo";
		displayName = "Republic MASH Missile Magazine";
		displayNameShort = "MASH Missile";
		descriptionShort = "MASH Missile";
	};

	class RD501_Cruise_Missile_Magazine: RD501_Cruise_Missile_Magazine_Base
	{
		author = MACRO_AUTHOR;
		ammo = "RD501_Cruise_Missile_Ammo_Base";
		displayName = "Republic Cruise Missile Magazine";
		displayNameShort = "Cruise Missile";
		descriptionShort = "Cruise Missile";
	};

	class RD501_LAFM_Cruise_Missile_Magazine: RD501_Cruise_Missile_Magazine_Base
	{
		author = MACRO_AUTHOR;
		ammo = "RD501_LAFM_Cruise_Missile_Ammo";
		displayName = "Republic Low Altitude Fast Moving Cruise Missile";
		displayNameShort = "LAFM Cruise Missile";
		descriptionShort = "LAFM Cruise Missile";
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////Artillary///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	class 32Rnd_155mm_Mo_shells;
	class RD501_32Rnd_155mm_cis_he_mag: 32Rnd_155mm_Mo_shells
	{
		displayNameShort = "HE";
		scope = 2;
		displayName = "155 mm HE Shells";
		ammo = "RD501_CIS_155mm_HE_ammo";
		count = 32;
		nameSound = "heat";
		muzzleImpulseFactor[] = {0.5,0.5};
	};
};