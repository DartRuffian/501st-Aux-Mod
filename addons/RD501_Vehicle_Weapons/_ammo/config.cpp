#include "../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_vehicle_ammunition
    {
        author="RD501";
        addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
        requiredAddons[]={"3AS_VehicleWeapons", "A3_Weapons_F"};
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};

#include "../_common/sensor_templates.hpp"
class CfgAmmo
{

    class BulletBase;
    class SmokeLauncherAmmoMK2: BulletBase
    {
        muzzleEffect="BIS_fnc_effectFiredSmokeLauncher";
        weaponLockSystem="1 + 2 + 4";
        hit=1;
        indirectHit=0;
        indirectHitRange=0;
        timeToLive=10;
        thrustTime=10;
        airFriction=-0.1;
        simulation="shotCM";
        model="\A3\weapons_f\empty";
        maxControlRange=50;
        initTime=2;
        aiAmmoUsageFlags="4 + 8";
    };
///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Aircraft Cannons and lasers////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
    class B_35mm_AA;
    class Gatling_30mm_HE_Plane_CAS_01_F;
    class laserAmmo_F;

    //Cannon
    class RD501_Republic_Aircraft_Laser_Cannon_Ammo : B_35mm_AA
    {
        hit = 1000;
        indirectHit =800;
        indirectHitRange = 10;
        explosive = 0.5;

        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        cost = 5;

        craterEffects = "ImpactEffectsMedium";
        effectFly = "Aux501_particle_effect_heavybolt_fly_green";
        explosionSoundEffect = "DefaultExplosion";
        ExplosionEffects = "ExploAmmoExplosion";
        model="Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Green.p3d";
        tracerScale = 1;
        tracerStartTime=0;
        tracerEndTime=10;

        caliber = 1;
        ACE_caliber=1;
        timeToLive=9;	
    };

    class RD501_Republic_CIS_Laser_Cannon_Ammo: RD501_Republic_Aircraft_Laser_Cannon_Ammo
    {
        model="Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
    };

    //LE gun
    class RD501_Republic_Aircraft_Laser_Blaster_Ammo : RD501_Republic_Aircraft_Laser_Cannon_Ammo
    {
        hit = 200;
        indirectHit = 75;
        indirectHitRange = 6;
        explosive = 0.5;

        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        cost = 5;

        craterEffects = "ImpactEffectsSmall";
        effectFly = "Aux501_particle_effect_heavybolt_fly_green";
        explosionSoundEffect = "DefaultExplosion";
        ExplosionEffects = "RocketExplosion";
        model="Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Green.p3d";
        tracerScale = 1;
        tracerStartTime=0;
        tracerEndTime=10;

        caliber = 1;
        ACE_caliber=1;
        timeToLive=9;	
    };

    //cis aa ammo
    class RD501_CIS_AA_Laser_Blaster_Ammo : RD501_Republic_Aircraft_Laser_Blaster_Ammo
    {
        hit = 100;
        indirectHit = 75;
        indirectHitRange = 3;
        explosive = 0.5;
        cost = 5;
        aiAmmoUsageFlags = "64 + 128 + 512";
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
    };

    //Voltic
    class RD501_Republic_Aircraft_Laser_Repeater_Ammo : Gatling_30mm_HE_Plane_CAS_01_F
    {
        hit = 400;
        indirectHit = 150;
        indirectHitRange = 6;
        explosive = 0.2;

        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        cost = 5;

        airLock = 1;
        irLock = 1;
        laserLock = 1;

        model="Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Blue.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_blue";
        tracerScale=1;
        tracerStartTime=0;
        tracerEndTime=10;

        timeToLive = 9;
        caliber = 9;
        ACE_caliber=1;
    };

    //AA
    class RD501_Republic_Aircraft_Laser_AA_Ammo : B_35mm_AA
    {
        hit = 300;
        indirectHit = 100;
        indirectHitRange = 2;
        explosive = .4;

        aiAmmoUsageFlags = "128 + 256"; 
        allowAgainstInfantry = 0;
        cost = 30;
        
        airLock = 1;
        irLock = 1;

        CraterEffects = "";
        explosionEffects = "ExploAmmoLaserCannon";
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Green.p3d";
        effectFly = "Aux501_particle_effect_blasterbolt_fly_Green";
        tracerScale = 1;
        tracerStartTime = 0;
        tracerEndTime = 10;
        tracerColor[] = {"Red"};

        timetolive = 4;
        caliber = 1;
        ACE_caliber=1;		

    };

    class RD501_CIS_Aircraft_Laser_AA_Ammo : RD501_Republic_Aircraft_Laser_AA_Ammo
    {
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
    };

    //Coaxium
    class RD501_Republic_Aircraft_Laser_Turbo_Ammo : RD501_Republic_Aircraft_Laser_AA_Ammo
    {
        hit = 6000;
        indirectHit = 4000;
        indirectHitRange = 12;
        explosive = 1;

        aiAmmoUsageFlags = "64 + 128 + 512";
        allowAgainstInfantry = 1;
        cost = 10;

        airLock = 0;
        irLock = 1;
        laserLock = 1;

        CraterEffects = "ATRocketCrater";
        explosionEffects = "MortarExplosion";
        explosionSoundEffect = "DefaultExplosion";
        effectsFire = "CannonFire";
        effectFly = "Aux501_particle_effect_heavybolt_fly_blue";
        simulation = "shotShell";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Blue.p3d";

        tracerScale = 1;
        tracerStartTime=0;
        tracerEndTime=10;

        timetolive = 10;
        caliber = 6;
        ACE_caliber=1;

        soundHit1[] = {"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_01",2.51189,1,2000};
        soundHit2[] = {"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_02",2.51189,1,2000};
        soundHit3[] = {"A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_03",2.51189,1,2000};
        SoundSetExplosion[] = {"Shell155mm_Exp_SoundSet","Shell155mm_Tail_SoundSet","Explosion_Debris_SoundSet"};
        soundSetSonicCrack[] = {"bulletSonicCrack_SoundSet","bulletSonicCrackTail_SoundSet"};
        supersonicCrackFar[] = {"A3\Sounds_F\weapons\Explosion\supersonic_crack_50meters",0.223872,1,150};
        supersonicCrackNear[] = {"A3\Sounds_F\weapons\Explosion\supersonic_crack_close",0.316228,1,50};
    };

    //this is for testing dif color laser
    class macro_new_ammo(ground_laser_aa) : RD501_Republic_Aircraft_Laser_AA_Ammo
    {
        model="Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Purple.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_purple";
    };
    class macro_new_ammo(aircraft_laser_CIS) : RD501_Republic_Aircraft_Laser_AA_Ammo
    {
        hit = 100;
        aiAmmoUsageFlags = "256";
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";

    };

    //used for ballgun and laat copilot
    class RD501_Republic_Aircraft_Laser_Beam_Ammo : laserAmmo_F
    {
        hit = 600;
        indirectHit = 400;
        indirectHitRange = 1;
        explosive = 1;
        effectFly = "Aux501_particle_effect_blasterbolt_fly_green";
        model="Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Green.p3d";
        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        airLock=1;
        irLock = 1;
        laserLock = 1;

        caliber = 6;
        ACE_caliber=1;
    };

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Missiles///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    class RD501_Default
    {
        airFriction = -0.0005; //how much air slows the projectile down.
        airLock = 0; //can lock onto airtargets
        animated = 0; //
        artilleryCharge = 1; //coef to initSpeed for calc artillery range
        artilleryDispersion = 1;
        artilleryLock = 0; //can lock onto Artillery Target Object.
        audibleFire = 0; //how loud shot is for AI
        autoSeekTarget = 0; //self guided projectile for anti-tank artillery shells
        caliber = 1; //size of round used for pentration
        cartridge = ""; //cartridge .p3d model
        cmImmunity = 1; //missiles resistance to countermeasures
        cost = 0; //high cost unlikely for cheap targets.

        craterEffects = "ImpactEffectsMedium"; //what happens when round hits ground
        craterShape = "";
        craterWaterEffects = "ImpactEffectsWater";

        dangerRadiusBulletClose = -1; //distance in meters for AI to get alerted by impact
        dangerRadiusHit = -1; //see above

        deflecting = 0; //deflection angle
        deflectionSlowDown = 0.8;
        directionalExplosion = 0;

        effectFlare = "FlareShell";
        effectFly = "";
        effectsFire = "CannonFire";
        effectsMissile = "ExplosionEffects";
        effectsMissileInit = "";
        effectsSmoke = "SmokeShellWhite";

        class EventHandlers {};

        explosionAngle = 60;
        explosionDir = "explosionDir";
        explosionEffects = "ExplosionEffects";
        explosionEffectsDir = "explosionDir";
        explosionForceCoef = 1;
        explosionPos = "explosionPos";
        explosionSoundEffect = "";
        explosionTime = 0;
        explosionType = "explosive";
        explosive = 1;

        fuseDistance = 0;
        grenadeBurningSound = [];
        grenadeFireSound = [];
        hit = 0;
        hitArmor = ["soundHit",1];
        hitBuilding = ["soundHit",1];
        hitConcrete = ["soundHit",1];
        hitDefault = ["soundHit",1];
        class HitEffects {
            object = "ImpactConcrete";
            vehicle = "ImpactMetal";
        };
        hitFoliage = ["soundHit",1];
        hitGlass = ["soundHit",1];
        hitGlassArmored = ["soundHit",1];
        hitGroundHard = ["soundHit",1];
        hitGroundSoft = ["soundHit",1];
        hitIron = ["soundHit",1];
        hitMan = ["soundHit",1];
        hitMetal = ["soundHit",1];
        hitMetalplate = ["soundHit",1];
        hitOnWater = 0;
        hitPlastic = ["soundHit",1];
        hitRubber = ["soundHit",1];
        hitTyre = ["soundHit",1];
        hitWater = ["soundHit",1];
        hitWood = ["soundHit",1];
        icon = "";
        impactArmor = ["soundImpact",1];
        impactBuilding = ["soundImpact",1];
        impactConcrete = ["soundImpact",1];
        impactDefault = ["soundImpact",1];
        impactFoliage = ["soundImpact",1];
        impactGlass = ["soundImpact",1];
        impactGlassArmored = ["soundImpact",1];
        impactGroundHard = ["soundImpact",1];
        impactGroundSoft = ["soundImpact",1];
        impactIron = ["soundImpact",1];
        impactMan = ["soundImpact",1];
        impactMetal = ["soundImpact",1];
        impactMetalplate = ["soundImpact",1];
        impactPlastic = ["soundImpact",1];
        impactRubber = ["soundImpact",1];
        impactTyre = ["soundImpact",1];
        impactWater = ["soundImpact",1];
        impactWood = ["soundImpact",1];

        indirectHit = 0;
        indirectHitRange = 1;

        initTime = 0;

        irLock = 0;

        isCraterOriented = 0;
        laserLock = 0;
        lockSeekRadius = 100;
        lockType = 0;
        maneuvrability = 1;
        manualControl = 0;
        maverickweaponIndexOffset = 0;
        maxControlRange = 350;
        maxSpeed = 0;
        minDamageForCamShakeHit = 0.55;
        mineBoundingDist = 3;
        mineBoundingTime = 3;
        mineDiveSpeed = 1;
        mineFloating = -1;
        mineInconspicuousness = 10;
        mineJumpEffects = "";
        minePlaceDist = 0.5;
        mineTrigger = "RangeTrigger";
        minimumSafeZone = 0.1;
        minTimeToLive = 0;
        missileLockCone = 0;
        model = "";
        class NVGMarkers {};
        nvLock = 0;
        proxyShape = "";
        shadow = 0;
        shootDistraction = -1;
        sideAirFriction = 1;
        simulation = "";
        simulationStep = 0.05;
        soundActivation = [];
        soundDeactivation = [];
        soundEngine = ["",1,1];
        soundFakeFall = ["soundFall",1];
        soundFall = ["",1,1];
        soundFly = ["",1,1];
        soundHit = ["",1,1];
        soundImpact = ["",1,1];
        soundTrigger = [];
        submunitionAmmo = "";
        supersonicCrackFar = ["",1,1];
        supersonicCrackNear = ["",1,1];
        suppressionRadiusBulletClose = -1;
        suppressionRadiusHit = -1;
        thrust = 210;
        thrustTime = 1.5;
        timeToLive = 10;
        tracerColor = [0.7,0.7,0.5,0.04];
        tracerColorR = [0.7,0.7,0.5,0.04];
        trackLead = 1;
        trackOversteer = 1;
        typicalSpeed = 900;
        underwaterHitRangeCoef = 1;
        visibleFire = 0;
        visibleFireTime = 0;
        waterEffectOffset = 0.45;
        weaponLockSystem = 0;
        weaponType = "Default";
        whistleDist = 0;
        whistleOnFire = 0;
    };
    
    class RD501_MissileCore : RD501_Default
    {
        audibleFire = 32;
        cost = 10000;
        deflecting = 5;
        initTime = 0.15;
        maneuvrability = 3;
        manualControl = 1;
        maxControlRange = 250;
        missileLockCone = 50;
        simulation = "shotMissile";
        simulationStep = 0.05;
        soundEngine = ["",0.001,1];
        soundFly = ["",0.01,2];
        soundHit = ["",100,1];
        thrust = 350;
        thrustTime = 2.5;
        timeToLive = 20;
        visibleFire = 32;
        visibleFireTime = 20;
    };

    class RD501_MissileBase : RD501_MissileCore
    {
        manualControl=0;
        maneuvrability=20;
        maxSpeed=500;
        simulationStep=0.0099999998;
        airFriction=0.2;
        sideAirFriction=0.001;
        maxControlRange=4000;
        soundHit1[]=
        {
            "A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_01",
            2.5118864,
            1,
            2000
        };
        soundHit2[]=
        {
            "A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_02",
            2.5118864,
            1,
            2000
        };
        soundHit3[]=
        {
            "A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_03",
            2.5118864,
            1,
            2000
        };
        multiSoundHit[]=
        {
            "soundHit1",
            0.34,
            "soundHit2",
            0.33000001,
            "soundHit3",
            0.33000001
        };
        explosionSoundEffect="DefaultExplosion";
        soundFly[]=
        {
            "",
            1,
            1,
            400
        };
        soundEngine[]=
        {
            "",
            1,
            1,
            50
        };
        supersonicCrackNear[]=
        {
            "A3\Sounds_F\weapons\Explosion\supersonic_crack_close",
            0.39810717,
            1,
            20
        };
        supersonicCrackFar[]=
        {
            "A3\Sounds_F\weapons\Explosion\supersonic_crack_50meters",
            0.31622776,
            1,
            50
        };
        CraterEffects="ATMissileCrater";
        explosionEffects="ATMissileExplosion";
        muzzleEffect="BIS_fnc_effectFiredRocket";
        effectsMissile="missile4";
        deflecting=0;
        weaponLockSystem="2 + 16";
        cmImmunity=0.89999998;
        dangerRadiusHit=-1;
        suppressionRadiusHit=30;
        class HitEffects
        {
            hitWater="ImpactEffectsWaterRocket";
        };
        class Components;
        maverickWeaponIndexOffset=0;
        htMin=60;
        htMax=1800;
        afMax=200;
        mfMax=100;
        mFact=0;
        tBody=0;
    };
    
    class ammo_Missile_AntiRadiationBase: RD501_MissileBase
    {
        model="\A3\weapons_f\empty";
        proxyShape="\A3\weapons_f\empty";
        cost=1500;
        aiAmmoUsageFlags="128 + 512";
        hit=2100;
        indirectHit=85;
        indirectHitRange=8;
        warheadName="HE";
        airLock=0;
        missileLockCone=120;
        missileKeepLockedCone=120;
        missileLockMaxDistance=16000;
        missileLockMinDistance=1000;
        missileLockMaxSpeed=55;
        weaponLockSystem=16;
        cmImmunity=0.5;
        manualControl=0;
        maxControlRange=16000;
        autoSeekTarget=0;

        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class AntiRadiationSensorComponent: SensorTemplateAntiRadiation
                    {
                        class AirTarget
                        {
                            minRange=16000;
                            maxRange=16000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=16000;
                            maxRange=16000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        maxTrackableATL=100;
                        maxTrackableSpeed=60;
                        angleRangeHorizontal=60;
                        angleRangeVertical=180;
                    };
                };
            };
        };
        initTime=0;
        thrust=240;
        thrustTime=5;
        airFriction=0.05;
        sideAirFriction=0.1;
        maxSpeed=828;
        maneuvrability=27;
        simulationStep=0.002;
        fuseDistance=500;
        timeToLive=40;
        trackLead=1;
        trackOversteer=1;
        whistleDist=20;
        cameraViewAvailable=0;
        craterEffects="AAMissileCrater";
        effectsMissile="missile3";
        explosionEffects="AAMissileExplosion";
        effectsMissileInit="PylonBackEffects";
        muzzleEffect="BIS_fnc_effectFiredHeliRocket";
        class CamShakeExplode
        {
            power=22;
            duration=2;
            frequency=20;
            distance=163.905;
        };
        class CamShakeHit
        {
            power=110;
            duration=0.60000002;
            frequency=20;
            distance=1;
        };
        class CamShakeFire
        {
            power=2.9907;
            duration=1.8;
            frequency=20;
            distance=71.554199;
        };
        class CamShakePlayerFire
        {
            power=4;
            duration=0.1;
            frequency=20;
            distance=1;
        };
    };

    class RD501_Missile_SRAABase : RD501_MissileBase
    {
        model="\A3\weapons_f\empty";
        proxyShape="\A3\weapons_f\empty";
        hit=130;
        indirectHit=85;
        indirectHitRange=10;
        warheadName="HE";
        proximityExplosionDistance=20;
        fuseDistance=100;
        maneuvrability=42;
        airFriction=0.14;
        sideAirFriction=0.23;
        trackOversteer=1.6;
        trackLead=0.89999998;
        initTime=0;
        timeToLive=20;
        thrustTime=5;
        thrust=250;
        maxSpeed=700;
        simulationStep=0.002;
        airLock=2;
        lockType=0;
        cmimmunity=0.92000002;
        weaponLockSystem="2 + 16";
        missileLockCone=180;
        missileKeepLockedCone=180;
        missileLockMaxDistance=5000;
        missileLockMinDistance=250;
        missileLockMaxSpeed=600;
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=5000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=4000;
                            objectDistanceLimitCoef=1;
                            viewDistanceLimitCoef=1;
                        };
                        angleRangeHorizontal=180;
                        angleRangeVertical=180;
                        maxTrackableSpeed=600;
                        minTrackableATL=3;
                    };
                };
            };
        };
        cost=1000;
        whistleDist=20;
        aiAmmoUsageFlags=256;
        missileFireAnim="rocket_fire_hide";
        CraterEffects="AAMissileCrater";
        explosionEffects="AAMissileExplosion";
        effectsMissile="FX_Missile_AA";
        muzzleEffect="B01_fnc_effectFiredJetMissile";
    };
    
    class RD501_Missile_LRAABase : RD501_MissileBase
    {
        model="\A3\weapons_f\empty";
        proxyShape="\A3\weapons_f\empty";
        hit=400;
        indirectHit = 130;
         indirectHitRange = 65;
        warheadName="HE";
        proximityExplosionDistance=70;
        fuseDistance=100;
        maneuvrability=25;
        airFriction=0.090000004;
        sideAirFriction=0.18000001;
        trackOversteer=0.69999999;
        trackLead=1.1;
        timeToLive=55;
        initTime=0.1;
        thrustTime=20;
        thrust=450;
        maxSpeed=1600;
        simulationStep=0.001;
        activeSensorAlwaysOn=0;
        airLock=2;
        lockType=0;
        cmimmunity=0.94999999;
        weaponLockSystem="8 + 16";
        missileLockCone=70;
        missileKeepLockedCone=90;
        missileLockMaxDistance=16000;
        missileLockMinDistance=500;
        missileLockMaxSpeed=777.77802;
        autoSeekTarget=0;
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=16000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=16000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        groundNoiseDistanceCoef=-1;
                        maxGroundNoiseDistance=0;
                        minSpeedThreshold=116;
                        maxSpeedThreshold=160;
                        minTrackableATL=50;
                        maxTrackableATL=1e+010;
                        minTrackableSpeed=0;
                        maxTrackableSpeed=777.77802;
                        angleRangeHorizontal=120;
                        angleRangeVertical=120;
                    };
                    class DataLinkSensorComponent: ActiveRadarSensorComponent
                    {
                        componentType="DataLinkSensorComponent";
                    };
                };
            };
        };
        cost=500;
        aiAmmoUsageFlags=256;
        effectsFire="CannonFire";
        missileFireAnim="rocket_fire_hide";
        CraterEffects="AAMissileCrater";
        explosionEffects="AAMissileExplosion";
        effectsMissile="FX_Missile_SAM_LongRange";
        muzzleEffect="";
        lockedTargetSound[]=
        {
            "A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\Locked_Titan",
            0.56234097,
            2.5
        };
        lockingTargetSound[]=
        {
            "A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\Locking_Titan",
            0.56234097,
            1
        };
        reloadMagazineSound[]=
        {
            "A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\reload_Missile_Launcher",
            0.89125103,
            1,
            10
        };
        sounds[]=
        {
            "StandardSound"
        };
        soundFly[]=
        {
            "A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\rocket_fly",
            1,
            1.1,
            700
        };
        class StandardSound
        {
            begin1[]=
            {
                "A3\Sounds_F\arsenal\weapons_static\Missile_Launcher\Titan",
                1.41254,
                1,
                1100
            };
            soundBegin[]=
            {
                "begin1",
                1
            };
            soundSetShot[]=
            {
                "Static_Launcher_Titan_ATAA_Shot_SoundSet",
                "Static_Launcher_Titan_ATAA_Tail_SoundSet"
            };
        };
        soundHit1[]=
        {
            "A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_01",
            2.5118899,
            1,
            1900
        };
        soundHit2[]=
        {
            "A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_02",
            2.5118899,
            1,
            1900
        };
        soundHit3[]=
        {
            "A3\Sounds_F\arsenal\weapons\Launchers\Titan\Explosion_titan_missile_03",
            2.5118899,
            1,
            1900
        };
        SoundSetExplosion[]=
        {
            "RocketsHeavy_Exp_SoundSet",
            "RocketsHeavy_Tail_SoundSet",
            "Explosion_Debris_SoundSet"
        };
        multiSoundHit[]=
        {
            "soundHit1",
            0.34,
            "soundHit2",
            0.33000001,
            "soundHit3",
            0.33000001
        };
        supersonicCrackFar[]=
        {
            "A3\Sounds_F\weapons\Explosion\supersonic_crack_50meters",
            0.316228,
            1,
            50
        };
        supersonicCrackNear[]=
        {
            "A3\Sounds_F\weapons\Explosion\supersonic_crack_close",
            0.39810699,
            1,
            20
        };
    };

    class RD501_Missile_AGM_02_F: RD501_MissileBase
    {
        model="\A3\Weapons_F_EPC\Ammo\Missile_AGM_02_fly_F.p3d";
        proxyShape="\A3\Weapons_F_EPC\Ammo\Missile_AGM_02_F.p3d";
        maverickWeaponIndexOffset=2;
        cost=800;
        submunitionAmmo="ammo_Penetrator_AGM_02";
        submunitionDirectionType="SubmunitionModelDirection";
        submunitionInitSpeed=1000;
        submunitionParentSpeedCoef=0;
        submunitionInitialOffset[]={0,0,-0.2};
        triggerOnImpact=1;
        deleteParentWhenTriggered=0;
        hit=1100;
        indirectHit=85;
        indirectHitRange=8;
        warheadName="HE";
        cameraViewAvailable=1;
        airLock=0;
        missileLockCone=50;
        missileKeepLockedCone=30;
        missileLockMaxDistance=6000;
        missileLockMinDistance=350;
        missileLockMaxSpeed=55;
        weaponLockSystem="2 + 16";
        cmImmunity=0.5;
        flightProfiles[]=
        {
            "TopDown"
        };
        class TopDown
        {
            ascendHeight=400;
            descendDistance=1000;
            minDistance=400;
            ascendAngle=0.0099999998;
        };
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=8000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=8000;
                            objectDistanceLimitCoef=1;
                            viewDistanceLimitCoef=1;
                        };
                        maxTrackableSpeed=55;
                        angleRangeHorizontal=35;
                        angleRangeVertical=35;
                    };
                };
            };
        };
        aiAmmoUsageFlags="128 + 512";
        initTime=0;
        thrust=95;
        thrustTime=4;
        airFriction=0.050000001;
        sideAirFriction=0.15000001;
        maxSpeed=320;
        typicalSpeed=270;
        maneuvrability=27;
        simulationStep=0.0020000001;
        fuseDistance=350;
        timeToLive=40;
        trackLead=1;
        trackOversteer=4;
        craterEffects="AAMissileCrater";
        effectsMissile="missile3";
        explosionEffects="AAMissileExplosion";
        effectsMissileInit="PylonBackEffects";
        muzzleEffect="";
        soundFly[]=
        {
            "A3\Sounds_F\weapons\Rockets\rocket_fly_2",
            0.50118721,
            1,
            1700
        };
        whistleDist=20;
        class CamShakeExplode
        {
            power=22;
            duration=2;
            frequency=20;
            distance=163.905;
        };
        class CamShakeHit
        {
            power=110;
            duration=0.60000002;
            frequency=20;
            distance=1;
        };
        class CamShakeFire
        {
            power=2.9907;
            duration=1.8;
            frequency=20;
            distance=71.554199;
        };
        class CamShakePlayerFire
        {
            power=4;
            duration=0.1;
            frequency=20;
            distance=1;
        };
        SoundSetExplosion[]=
        {
            "RocketsLight_Exp_SoundSet",
            "RocketsLight_Tail_SoundSet",
            "Explosion_Debris_SoundSet"
        };
        ace_frag_enabled=1;
        ace_frag_classes[]=
        {
            "ace_frag_medium",
            "ace_frag_medium_HD"
        };
        ace_frag_metal=56250;
        ace_frag_charge=39000;
        ace_frag_gurney_c=2700;
        ace_frag_gurney_k="1/2";
        ace_rearm_dummy="ace_rearm_Missile_AGM_02_F";
    };

    class RD501_Missile_AGM_01_F: RD501_Missile_AGM_02_F
    {
        model="\A3\Weapons_F_EPC\Ammo\Missile_AGM_01_fly_F.p3d";
        proxyShape="\A3\Weapons_F_EPC\Ammo\Missile_AGM_01_F.p3d";
        maverickWeaponIndexOffset=2;
        thrustTime=6;
        submunitionAmmo="ammo_Penetrator_AGM_01";
        submunitionDirectionType="SubmunitionModelDirection";
        submunitionInitSpeed=1000;
        submunitionParentSpeedCoef=0;
        submunitionInitialOffset[]={0,0,-0.2};
        triggerOnImpact=1;
        deleteParentWhenTriggered=0;
        hit=1200;
        indirectHit=90;
        indirectHitRange=8;
        warheadName="HE";
        cameraViewAvailable=1;
        missileLockCone=20;
        missileKeepLockedCone=20;
        missileLockMaxDistance=6000;
        missileLockMinDistance=300;
        missileLockMaxSpeed=40;
        weaponLockSystem="2 + 16";
        cmImmunity=0.5;
        flightProfiles[]=
        {
            "Direct"
        };
        class Direct
        {
        };
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=8000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=8000;
                            objectDistanceLimitCoef=1;
                            viewDistanceLimitCoef=1;
                        };
                        maxTrackableSpeed=40;
                        angleRangeHorizontal=20;
                        angleRangeVertical=20;
                    };
                };
            };
        };
        ace_rearm_dummy="ace_rearm_Missile_AGM_01_F";
    };

    //Torrent
    class RD501_Republic_Aircraft_Missile_AT_IR_Ammo : RD501_Missile_AGM_01_F
    {
        hit = 2200;
        indirectHit = 620;
        indirectHitRange = 15;
        ACE_caliber=1;

        cost = 100;
        maxSpeed=320;
        thrustTime=60;
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Blue_White";
        muzzleEffect = "";
        tracerColor[] = {"blue"};
        brightness = 20000;
        lightColor[] = {0, 0, 1, 1};
        triggerTime = 0.1;

        weaponLockSystem = "1 + 2 + 4 + 16";
        timeToLive = 20;
        missileLockMinDistance = 1; 
        missileLockMaxDistance = 10000; 
        airLock = 0;
        flightProfiles[]=
        {
            "Direct",
            "TopDown"
        };
        class TopDown
        {
            ascendHeight=400;
            descendDistance=1000;
            minDistance=600;
            ascendAngle=60;
        };
        class Direct
        {
        };
        class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=6000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=6000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        maxTrackableSpeed=200;
                        angleRangeHorizontal=60;
                        angleRangeVertical=60;
                    };

                    class LaserSensorComponent: SensorTemplateLaser
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=8000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=8000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        maxTrackableSpeed=200;
                        angleRangeHorizontal=60;
                        angleRangeVertical=60;
                    };

                    class DataLinkSensorComponent : SensorTemplateDataLink
                    {
                        class AirTarget
                        {
                            minRange	= 1;
                            maxRange	= 5000;
                            objectDistanceLimitCoef	= -1;
                            viewDistanceLimitCoef	= -1;
                        };
                        class GroundTarget
                        {
                            minRange = 1;
                            maxRange = 5000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef 	= -1;
                        };
                        allowsMarking = 1;
                        typeRecognitionDistance = 16000;
                        angleRangeHorizontal 	= 360;
                        angleRangeVertical 		= 180;
                        groundNoiseDistanceCoef = 0.05;
                        maxGroundNoiseDistance 	= 40;
                        minSpeedThreshold 		= 5;
                        maxSpeedThreshold 		= 100;
                    };
                };
            };
        };
    
    };

    //LRAD
       class RD501_CIS_Static_Missile_AA_LR_Ammo : RD501_Missile_LRAABase
    {
        model="\A3\Weapons_F_Sams\Ammo\Missile_SAM_03_fly_F.p3d";
        proxyShape="\A3\Weapons_F_Sams\Ammo\Missile_SAM_03_fly_F.p3d";
        missileLockCone=120;
        missileKeepLockedCone=120;
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Red_Grey";
        thrustTime=55;
        thrust=350;
        maneuvrability=40;
        timeToLive=30;
        ACE_caliber=1;
        flightProfiles[] = {"TopDown"};

        class TopDown
        {
            ascendHeight = 200.0;
            descendDistance = 8000.0;
            minDistance = 6000.0;
            ascendAngle = 80.0;
        };

    };

    class RD501_Republic_Static_Missile_AA_LR_Ammo : RD501_CIS_Static_Missile_AA_LR_Ammo
    {
        effectsMissile = "RD501_FX_Missile_Red_White";
    };

       //MRC
       class RD501_CIS_Static_Missile_AA_SR_Ammo : RD501_Missile_SRAABase
    {
        model="\A3\Weapons_F_Jets\Ammo\Missile_SAM_01_fly_F.p3d";
        proxyShape="\A3\Weapons_F_Jets\Ammo\Missile_SAM_01_fly_F.p3d";
        airFriction=0.16;
        thrustTime=20;
        thrust=150;
        maxSpeed=800;
        missileLockMaxDistance=10000;
        cost = 100;
        hit = 60;
        indirectHit = 35;
        indirectHitRange = 40;
        timeToLive=30;
        proximityExplosionDistance=60;
        lockSeekRadius = 200;
        autoSeekTarget = 1;
        ACE_caliber=1;
        
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Red_Grey";

        flightProfiles[] = {"TopDown"};

        class TopDown
        {
            ascendHeight = 100.0;
            descendDistance = 3000.0;
            minDistance = 2000.0;
            ascendAngle = 80.0;
        };

        class Components: Components
        {
            class SensorsManagerComponent : SensorsManagerComponent
            {
                class Components : Components
                {
                    class IRSensorComponent : IRSensorComponent
                    {
                        class AirTarget
                        {
                            minRange=100;
                            maxRange=10000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=100;
                            maxRange=10000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        groundNoiseDistanceCoef=-1;
                        maxGroundNoiseDistance=0;
                        minSpeedThreshold=0;
                        maxSpeedThreshold=1000;
                        minTrackableATL=50;
                        maxTrackableATL=1e10;
                        minTrackableSpeed=0;
                        maxTrackableSpeed=1e10;
                        angleRangeHorizontal=180;
                        angleRangeVertical=180;
                    };
                };
            };
        };
    };

    class RD501_Republic_Static_Missile_AA_SR_Ammo : RD501_CIS_Static_Missile_AA_SR_Ammo
    {
        effectsMissile = "RD501_FX_Missile_Red_White";
    };
   
       //HARM Invader
    class RD501_Republic_Aircraft_Missile_ARM_LR_Ammo : ammo_Missile_AntiRadiationBase
    {
        model = "\A3\Weapons_F_Sams\Ammo\Missile_AR_01_F_fly.p3d";
        proxyShape = "\A3\Weapons_F_Sams\Ammo\Missile_AR_01_F.p3d";
        hit = 2100;
        indirectHit = 85;
        indirectHitRange = 8;
        maxSpeed=800;
        thrustTime=60;
        timeToLive=30;
        ACE_caliber=1;
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Orange_White";
         class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class AntiRadiationSensorComponent: SensorTemplateAntiRadiation
                    {
                        componentType="PassiveRadarSensorComponent";
                        class AirTarget {
                            maxRange = 8000;
                            minRange = 8000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 8000;
                            minRange = 8000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        aimDown = 45;
                        allowsMarking = 1;
                        angleRangeHorizontal = 90;
                        angleRangeVertical = 120;
                        color = [0,1,1,1];
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 0;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 20000;
                    };
                };
            };
        }; 
    };

    //smol HARM Intruder
    class RD501_Republic_Aircraft_Missile_ARM_SR_Ammo : RD501_Republic_Aircraft_Missile_ARM_LR_Ammo
    {
        hit = 2100;
        indirectHit = 85;
        indirectHitRange = 8;
        maxSpeed=800;
        thrustTime=60;
        maneuvrability = 38;
        timeToLive=30;
        ACE_caliber=1;
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Orange_White";
        /* removed for now due to flight path issues
        flightProfiles[]=
        {
            "Direct",
            "TopDown"
        };
        class Direct
        {
        };
        class TopDown
        {
            minDistance = 1000;
            ascendHeight = 300;
            ascendAngle = 45.0;
            descendDistance = 800;
        }; */
         class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class AntiRadiationSensorComponent: SensorTemplateAntiRadiation
                    {
                        componentType="PassiveRadarSensorComponent";
                        class AirTarget {
                            maxRange = 4000;
                            minRange = 4000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget {
                            maxRange = 4000;
                            minRange = 4000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        aimDown = 45;
                        allowsMarking = 1;
                        angleRangeHorizontal = 10;
                        angleRangeVertical = 120;
                        color = [0,1,1,1];
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 0;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 1000;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                        typeRecognitionDistance = 20000;
                    };
                };
            };
        }; 
    };

       //CM
       class RD501_CMflare_Chaff_Ammo : BulletBase
   {
        hit=1;
        indirectHit=0;
        indirectHitRange=0;
        timeToLive=15;
        thrustTime=4;
        airFriction=-0.0099999998;
        simulation="shotCM";
        effectsSmoke="CounterMeasureChaff";
        weaponLockSystem="2 + 8";
        model="\A3\weapons_f\empty";
        maxControlRange=-1;
        initTime=0;
        aiAmmoUsageFlags=8;
   };
   
    class MissileBase;
    class M_Air_AA : MissileBase
    {
        class Components;
    };
    class M_Jian_AT;
    class Rocket_04_AP_F;
    class ammo_Missile_LongRangeAABase;
    class ammo_Missile_mim145 : ammo_Missile_LongRangeAABase
    {
        class Components;
    };
    class ammo_Missile_HARM;
    class ammo_Missile_rim116;

    //Zephyr
    class RD501_Republic_Aircraft_Missile_AA_IR_Ammo : M_Air_AA
    {
        hit = 620;
        indirectHit = 100;
        indirectHitRange = 7;

        cmimmunity = 0.971;
        maneuvrability = 30;

        cost = 100;
        maxSpeed=1020;
        thrustTime=60;
        timeToLive=15;
        ACE_caliber=1;

        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Teal_White";
        muzzleEffect = "";
        tracerColor[] = {"blue"};

        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange=500;
                            maxRange=6000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=500;
                            maxRange=6000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        angleRangeHorizontal=90;
                        angleRangeVertical=90;
                    };
                };
            };
        }; 
    };

    class RD501_CIS_Aircraft_Missile_AA_IR_Ammo: RD501_Republic_Aircraft_Missile_AA_IR_Ammo
    {
        effectsMissile = "RD501_FX_Missile_Teal_Grey";
        hit = 250;
        indirectHit = 40;
    };

    //flashfire
    class RD501_Republic_Aircraft_Missile_AT_Wire_Ammo : M_Jian_AT
    {
        hit = 3000;
        indirectHit = 100;
        indirectHitRange = 5;

        cost = 200;
        maxSpeed=320;
        thrustTime=60;
        timeToLive=20;
        ACE_caliber=1;

        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Blue_White";
        muzzleEffect = "";
        soundfly[] = {"\rd501_vehicle_weapons\_sounds\proton_torp.ogg",3,1,800};
        tracerColor[] = {"blue"};
        brightness = 20000;
        lightColor[] = {0, 0, 1, 1};
        triggerTime = 0.1;

        explosionTime = -1; //explode on impact
        maxControlRange = 5000;

        cameraViewAvailable = 1; 
    };



    //Hurricane
    class RD501_Republic_Aircraft_Missile_AP_Ammo : Rocket_04_AP_F
    {
        hit = 1020;
        indirectHit = 620;
        indirectHitRange = 15;

        cost = 20;
        aiAmmoUsageFlags="64 + 128";
        maxSpeed=590;
        thrustTime=60;
        timeToLive=100;
        ACE_caliber=1;

        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Blue_White_Short";
        muzzleEffect = "";
        triggerTime = 0.1;

    };

    //Charhound
    class RD501_Republic_Aircraft_Missile_AR_LR_Ammo : M_Air_AA
    {
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Teal_White";
        muzzleEffect = "";
        tracerColor[] = {"blue"};
        brightness = 20000;
        lightColor[] = {0, 0, 1, 1};
        triggerTime = 0.1;

        cost = 200;
        maxSpeed=1020;
        thrustTime=60;
        timeToLive=10;
        ACE_caliber=1;

        //dmg
        hit = 2020;
        indirectHit = 620;
        indirectHitRange = 30;
        proximityExplosionDistance=20;

        //targeting
        airLock = 2;
        autoSeekTarget = 1;
        cmImmunity = .90; //very hard to break lock
        flightProfiles[] = {LOALDistance};
        lockSeekDistanceFromParent = 200; //distance from your aircraft until it searchs for target
        lockSeekRadius = 800; //max distance from initial target to find a target
        lockType = 0;
        maneuvrability = 32;
        missileKeepLockedCone = 30;
        missileLockMaxDistance = 6000;
        missileLockMinDistance = 100;
        missileLockMaxSpeed = 3000;
        trackLead = .6;
        trackOversteer = 1.2;
        weaponLockSystem = "8 + 16";
        aiAmmoUsageFlags = 256;
        weaponType = "missileAA";
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    delete IRSensorComponent;
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget
                        {
                            minRange=1000;
                            maxRange=6000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=1000;
                            maxRange=6000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        componentType = "ActiveRadarSensorComponent";

                        typeRecognitionDistance = 6000;
                        angleRangeHorizontal = 60;
                        angleRangeVertical = 60;
                        maxFogSeeThrough = -1;

                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 0;

                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 1000;

                        minTrackableSpeed = -1e10;
                        maxTrackableSpeed = 1e10;

                        minTrackableATL = -1e10;
                        maxTrackableATL = 1e10;

                        allowsMarking = 1;
                        aimDown = 0;
                        animDirection="";
                    };
                };
            };
        }; 
        

    };

    //Mako
    class RD501_Republic_Aircraft_Missile_AR_XLR_Ammo : M_Air_AA
    {
        effectsMissileInit = "RD501_laat_PylonBackEffectsFFAR";
        effectsMissile = "RD501_FX_Missile_Teal_White";
        muzzleEffect = "";
        triggerTime = 0.1;

        cost = 300;
        maxSpeed=1020;
        thrustTime=60;
        timeToLive=12;
        ACE_caliber=1;

        //dmg
        hit = 2020;
        indirectHit = 1020;
        indirectHitRange = 50;
        proximityExplosionDistance=40;

        //targeting
        airLock = 2;
        cmImmunity = 1; //very hard to break lock
        lockType = 0;
        maneuvrability = 28;
        missileKeepLockedCone = 30;
        missileLockMaxDistance = 12000;
        missileLockMinDistance = 2500;
        missileLockMaxSpeed = 3000;
        trackLead = .8;
        trackOversteer = 1.2;
        weaponLockSystem = "8 + 16";
        aiAmmoUsageFlags = 256;
        weaponType = "missileAA";
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    delete IRSensorComponent;
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget
                        {
                            minRange=2500;
                            maxRange=12000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=2500;
                            maxRange=12000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        componentType = "ActiveRadarSensorComponent";

                        typeRecognitionDistance = 12000;
                        angleRangeHorizontal = 60;
                        angleRangeVertical = 60;
                        maxFogSeeThrough = -1;

                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = 0;

                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 1000;

                        minTrackableSpeed = -1e10;
                        maxTrackableSpeed = 1e10;

                        minTrackableATL = -1e10;
                        maxTrackableATL = 1e10;

                        allowsMarking = 1;
                        aimDown = 0;
                        animDirection="";
                    };
                };
            };
        }; 
    };

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Bombs//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    class ammo_Bomb_SDB;
    class Bo_Mk82;

    //wraith
    class RD501_Republic_Aircraft_Bomb_Laser_Glide_Ammo : ammo_Bomb_SDB
    {
        hit = 1000;
        indirectHit = 500;
        indirectHitRange = 20;
        
        ExplosionEffects = "ProtonbombAmmoExplosion";
        muzzleEffect = "";
        tracerColor[] = {"blue"};
        brightness = 20000;
        lightColor[] = {0, 0, 1, 1};
        triggerTime = 0.1;

        class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class LaserSensorComponent: SensorTemplateLaser
                    {
                        class AirTarget
                        {
                            minRange=32000;
                            maxRange=32000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        class GroundTarget
                        {
                            minRange=32000;
                            maxRange=32000;
                            objectDistanceLimitCoef=-1;
                            viewDistanceLimitCoef=-1;
                        };
                        maxTrackableSpeed=100;
                        angleRangeHorizontal=180;
                        angleRangeVertical=180;
                    };
                };
            };
        };

    };

    //slagger
    class RD501_Republic_Aircraft_Bomb_Dumb_Ammo : Bo_Mk82
    {
        hit = 2000;
        indirectHit = 1000;
        indirectHitRange = 70;
        
        ExplosionEffects = "ProtonbombAmmoExplosion";
        muzzleEffect = "";
        tracerColor[] = {"blue"};
        brightness = 20000;
        lightColor[] = {0, 0, 1, 1};
        triggerTime = 0.1;

    };

///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Tank Cannons///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class 3AS_GAT_redPlasma_AT;
    class 3AS_ATT_redPlasma_AT;
    class 3AS_Sabre_AT;
    class Aux501_Weapons_Mags_CIS_eweb;

    class macro_new_ammo(aat_mbt): 3AS_GAT_redPlasma_AT//GAT_Laser
    {
        hit = 500;
        indirectHit = 120;
        indirectHitRange = 10;
        explosive = 0.2;
        cost = 30;
        caliber = 20;
        typicalSpeed = 70;
        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        CraterEffects = "ATMissileCrater";
        explosionEffects = "IEDMineSmallExplosion";
        explosionSoundEffect="DefaultExplosion";
        tracerScale = 1;
        tracerStartTime = 0;
        tracerEndTime = 10;
        brightness = 100000;
        tracerColor[] = {0,0,1,0};
        timeToLive=10;
        soundContinuous=0;
        soundHit1[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_01",
            1.7782794,
            1,
            1800
        };
        soundHit2[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_02",
            1.7782794,
            1,
            1800
        };
        soundHit3[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_03",
            1.7782794,
            1,
            1800
        };
        soundHit4[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_04",
            1.7782794,
            1,
            1800
        };
        multiSoundHit[]=
        {
            "soundHit1",
            0.25,
            "soundHit2",
            0.25,
            "soundHit3",
            0.25,
            "soundHit4",
            0.25
        };
    };
    class macro_new_ammo(aat_king): 3AS_ATT_redPlasma_AT//AAT_ADSD_Laser
    {
        hit = 500;
        indirectHit = 130;
        indirectHitRange = 10;
        visibleFire = 42;
        audibleFire = 42;
        //initSpeed=1000;
        explosive = 0.1;
        cost = 22;
        craterEffects = "ImpactEffectsMedium";
        explosionSoundEffect = "DefaultExplosion";
        ExplosionEffects = "ExploAmmoExplosion";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        tracerScale = 1.5;
        tracerStartTime = 0;
        tracerEndTime = 10;
        brightness = 100000;
        tracerColor[] = {1,0,0,0};
        airFriction = 0;
        muzzleEffect = "";
        caliber = 5;
        typicalSpeed = 70;
        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        timeToLive=10;
        soundHit1[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_01",
            1.7782794,
            1,
            1800
        };
        soundHit2[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_02",
            1.7782794,
            1,
            1800
        };
        soundHit3[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_03",
            1.7782794,
            1,
            1800
        };
        soundHit4[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_04",
            1.7782794,
            1,
            1800
        };
        multiSoundHit[]=
        {
            "soundHit1",
            0.25,
            "soundHit2",
            0.25,
            "soundHit3",
            0.25,
            "soundHit4",
            0.25
        };
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////CIS Heavy Infantry///////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class 3AS_EM90_RedPlasma;
    class macro_new_ammo(cis_dsd_cannon_ammo): 3AS_EM90_RedPlasma
    {
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        tracerscale = 1.5;
        tracerStartTime = 0;
        tracerEndTime = 30;
        hit = 25;
        indirectHit = 0;
        indirectHitRange = 0;
        cartridge = "";
        visibleFire = 8;
        audibleFire = 120;
        dangerRadiusBulletClose = 12;
        dangerRadiusHit = 16;
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 12;
        cost = 5;
        airLock = 1;
        caliber = 3.79;
        typicalSpeed = 250;
        airFriction = -0.00025;
        class CamShakeExplode
        {
            power = 0;
            duration = 0;
            frequency = 0;
            distance = 0;
        };
        class CamShakeHit
        {
            power = 0;
            duration = 0.4;
            frequency = 0;
            distance = 0;
        };
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////Statics//////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class macro_new_ammo(30mw_d);
    class macro_new_ammo(cis_eweb_ammo):macro_new_ammo(30mw_d)
    {
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        tracerscale = 1.5;
        tracerStartTime = 0.0;
        tracerEndTime = 30;
        airFriction = -0.00058679;
        hit=20;
        airLock=1;
        typicalSpeed=700;
    };
    class RD501_reapter_ammo: RD501_Republic_Aircraft_Laser_Blaster_Ammo
    {
        hit = 100;
        indirectHit = 75;
        indirectHitRange = 3;
        explosive = 0.5;
        cost = 5;
        fuseDistance = 0;
        coefGravity = 0;
        aiAmmoUsageFlags = "64 + 128 + 512";
        explosionSoundEffect = "DefaultExplosion";
        explosionEffects = "GrenadeExplosion";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
    };
    class RD501_proton_cannon_he_ammo : macro_new_ammo(aat_king)
    {
        model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_red";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        airFriction = -3.96e-05;
        tracerScale = 3;
        hit = 0;
        indirectHit = 130;
    };
    class RD501_proton_cannon_ap_ammo : macro_new_ammo(aat_king)
    {
        airFriction = -3.96e-05;
        tracerScale = 3;
        allowAgainstInfantry = 1;
        hit = 500;
        indirectHit = 130;
    };
    class macro_new_ammo(cis_droideka_sniper_ammo):3AS_EM90_RedPlasma
    {
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        tracerscale = 1.5;
        tracerStartTime = 0.0;
        tracerEndTime = 30;
        hit = 25;
        indirectHit = 0;
        indirectHitRange = 0;
        cartridge = "";
        visibleFire = 8;
        audibleFire = 120;
        dangerRadiusBulletClose = 12;
        dangerRadiusHit = 16;
        suppressionRadiusBulletClose = 8;
        suppressionRadiusHit = 12;
        cost = 5;
        airLock = 1;
        caliber=2.4;
        typicalSpeed = 600;
        airFriction = -0.00025;
        class CamShakeExplode
        {
            power = 0;
            duration = 0;
            frequency = 0;
            distance = 0;
        };
        class CamShakeHit
        {
            power = 0;
            duration = 0.4;
            frequency = 0;
            distance = 0;
        };
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////CIS Walkers///////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class DBA_80mm_UBF5_HEF;
    class macro_new_ammo(cis_octo_cannon_ammo):DBA_80mm_UBF5_HEF
    {
        ace_frag_enabled = 1;
        ace_frag_metal = 5900;
        ace_frag_charge = 480;
        ace_frag_gurney_c = 2830;
        ace_frag_gurney_k = "1/2";
        ace_frag_classes[] = {"ACE_frag_medium"};
        ace_frag_skip = 0;
        ace_frag_force = 1;
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        hit = 150;
        indirectHit = 60;
        indirectHitRange = 4.5;
        displayname = "80mw Tri-Droid Ammo";
        warheadName = "UBF-5 HE-F";
        explosive = 1;
        airLock = 0;
        canLock = 2;
        aiAmmoUsageFlags = "64 + 128";
        cost = 15;
        model = "3AS\3as_static\OG9\lazor.p3d";
        tracerScale = 3;
        tracerStartTime = 0.0001;
        tracerEndTime = 20;
        brightness = 100000;
        muzzleEffect = "";
        caliber = 1.1;
        typicalSpeed = 340;
        proximityExplosionDistance = 30;
        fuseDistance = 0;
        submunitionAmmo = "";
        submunitionDirectionType = "SubmunitionModelDirection";
        submunitionInitSpeed = 910;
        submunitionParentSpeedCoef = 0;
        submunitionInitialOffset[] = {0,0,-0.2};
        allowAgainstInfantry = 1;
        timeToLive = 20;
        coefGravity = 0;
        airFriction = -0.0005;
        deflecting = 0;
        waterFriction = 0;
        class CamShakeExplode
        {
            power = 0;
            duration = 1.75;
            frequency = 0;
            distance = 75;
        };
        class CamShakeHit
        {
            power = 0;
            duration = 2;
            frequency = 0;
            distance = 25;
        };
        class CamShakeFire
        {
            power = 0;
            duration = 1.5;
            frequency = 0;
            distance = 100;
        };
        class CamShakePlayerFire
        {
            power = 0;
            duration = 0.05;
            frequency = 0;
            distance = 0.5;
        };
    };
    class macro_new_ammo(homing_spider_charge):macro_new_ammo(aat_king)
    {
        hit = 650;
        indirectHit = 130;
        indirectHitRange = 10;
        visibleFire = 42;
        audibleFire = 42;
        //initSpeed=1000;
        initSpeed=  20;
        explosive = 0.1;
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        cost = 22;
        craterEffects = "ImpactEffectsMedium";
        explosionSoundEffect = "DefaultExplosion";
        ExplosionEffects = "ExploAmmoExplosion";
        tracerScale = 3;
        tracerStartTime = 0;
        tracerEndTime = 10;
        brightness = 100000;
        airFriction = -0.0005;
        muzzleEffect = "";
        caliber = 5;
        fuseDistance = 0;
        coefGravity = 0;
        typicalSpeed = 140;
        aiAmmoUsageFlags = "64 + 128 + 256 + 512";
        allowAgainstInfantry = 1;
        model = "3AS\3as_static\OG9\lazor.p3d";
        timeToLive=10;
        soundHit1[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_01",
            1.7782794,
            1,
            1800
        };
        soundHit2[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_02",
            1.7782794,
            1,
            1800
        };
        soundHit3[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_03",
            1.7782794,
            1,
            1800
        };
        soundHit4[]=
        {
            "A3\Sounds_F\arsenal\explosives\shells\Tank_shell_explosion_04",
            1.7782794,
            1,
            1800
        };
        multiSoundHit[]=
        {
            "soundHit1",
            0.25,
            "soundHit2",
            0.25,
            "soundHit3",
            0.25,
            "soundHit4",
            0.25
        };

    };
    class RD501_homing_spider_eweb_charge_ammo: Aux501_Weapons_Mags_CIS_eweb
    {
        model = "3AS\3as_static\OG9\lazor.p3d";
        tracerscale = 1;
        tracerStartTime = 0.0001;
        tracerEndTime = 30;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////Speeder//////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class Aux501_Weapons_Ammo_GL_AP;
    class macro_new_ammo(barc_AP): Aux501_Weapons_Ammo_GL_AP
    {
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Blue.p3d";
        effectfly = "Aux501_particle_effect_blasterbolt_fly_blue";
    };
    class ls_speeder_HE;
    class macro_new_ammo(barc_HE): ls_speeder_HE
    {
        model = "Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Medium_Blue.p3d";
        effectfly = "Aux501_particle_effect_blasterbolt_fly_blue";
    };
    

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////Resupply/////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class Bo_GBU12_LGB;

    class macro_new_ammo(guided_resupply_base): Bo_GBU12_LGB
    {
        // Disable all damage - but keep effects.
        hit = 0;
        indirectHit = 0;
        explosive = 0;
        
        // Always whistle!
        whistleOnFire = 1;
        // Don't use?
        aiAmmoUsageFlags = 0;
    };

    class macro_new_ammo(guided_resupply_ammo_resupply): macro_new_ammo(guided_resupply_base) { };

    class macro_new_ammo(guided_resupply_medical_resupply): macro_new_ammo(guided_resupply_base) { }; 

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////CRUISE MISSILE//////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    class ammo_Missile_CruiseBase;

    class RD501_Cruise_Missile_Ammo_Base: ammo_Missile_CruiseBase
    {
        // Should this missile fire even if the launcher
        // is not yet reloaded.
        RD501_CRML_alwaysFire = 0;

        flightProfiles[] = {"Cruise"};

        class Cruise
        {
            preferredFlightAltitude = 3500.0;
            lockDistanceToTarget = 75.0;
        };

        maxSpeed = 1200;
        thrust = 300;
        manualControl = 1;
        maxControlRange = 999999;

        thrustTime = 145;
        timeToLive = 140;

        hit = 6000;
        indirectHit = 4000;
        indirectHitRange = 12;
        explosive = 1;

        // AI stuffs
        aiAmmoUsageFlags = "0";
        allowAgainstInfantry = 0;
        minRange = 999999;
        maxRange = 0;
        cost = 10;

        soundSetExplosion[] = {"BombsHeavy_Tail_SoundSet","Explosion_Debris_SoundSet","RD501_CruiseMissile_Explosion_SoundSet"};

        cameraViewAvailable = 1;
        model = "\A3\Weapons_F_Destroyer\Ammo\Missile_Cruise_01_Fly_F";
        proxyShape = "\A3\Weapons_F_Destroyer\Ammo\Missile_Cruise_01_Fly_F";
    };

    // No-Damage Cruise missile 
    class RD501_No_Damage_Cruise_Missile_Ammo: RD501_Cruise_Missile_Ammo_Base
    {
        // Disable all damage - but keep effects.
        hit = 0;
        indirectHit = 0;
        explosive = 1;
        // Don't use?
        aiAmmoUsageFlags = 0;

        soundSetExplosion[] = {"Explosion_Debris_SoundSet","RD501_CruiseMissile_Explosion_SoundSet"};
    };

    // Mash-Model no damage missile
    class RD501_Mash_Missile_Ammo: RD501_No_Damage_Cruise_Missile_Ammo
    {
        model = "\A3\Weapons_F_Destroyer\Ammo\Missile_Cruise_01_Fly_F";
        RD501_CRML_alwaysFire = 1;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////Artillary////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    class Sh_155mm_AMOS;
    class RD501_CIS_155mm_HE_ammo: Sh_155mm_AMOS
    {
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        tracerScale = 1;
        tracerStartTime = 0;
        whistleDist = 120;
    };
};

class CfgSoundSets
{
    class RD501_CruiseMissile_Explosion_SoundSet
    {
        soundShaders[]=
        {
            RD501_CruiseMissile_Explosion_SoundShader
        };
        volumeFactor=1;
        volumeCurve="InverseSquare2Curve";
        spatial=1;
        doppler=0;
        loop=0;
        sound3DProcessingType="ExplosionLight3DProcessingType";
        distanceFilter="explosionDistanceFreqAttenuationFilter";
    };
};

class CfgSoundShaders
{
    class RD501_CruiseMissile_Explosion_SoundShader
    {
        samples[] = 
        {
            {"RD501_Vehicle_Weapons\_sounds\coax_test_1.wav",1}
        };
        volume = 1;
        range = 1750;
        rangeCurve[] =
        {
            {0,1},
            {0.5,0.7},
            {1,0}
        };
    };
};