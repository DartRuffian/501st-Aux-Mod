params ["_object", "_value"];

if (_value) then {
	if (isNil "RD501_3DEN_ZM_ZeusMapMarkerList") then {
		RD501_3DEN_ZM_ZeusMapMarkerList = [];
		publicVariable "RD501_3DEN_ZM_ZeusMapMarkerList";
	};

	RD501_3DEN_ZM_ZeusMapMarkerList pushBackUnique _object;
};
