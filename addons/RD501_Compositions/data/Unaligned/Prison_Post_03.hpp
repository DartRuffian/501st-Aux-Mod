class Object0	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-3.33887,0.0861626,0}; dir = -358.521;};
class Object1	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-9.97192,0.256504,0}; dir = -358.521;};
class Object2	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-16.6035,0.42778,0}; dir = -358.521;};
class Object3	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-23.2366,0.599098,0}; dir = -358.521;};
class Object4	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-29.8691,0.769424,0}; dir = -358.521;};
class Object5	{side = 8; vehicle = "3AS_Wall_Laser_Corner"; rank = ""; position[] = {3.2937,-0.0846519,0}; dir = -358.521;};
class Object6	{side = 8; vehicle = "3AS_Wall_Laser_Corner"; rank = ""; position[] = {-37.0261,-6.0719,0}; dir = -88.5205;};
class Object7	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {9.9646,-13.8759,0}; dir = -268.521;};
class Object8	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {10.1348,-7.24256,0}; dir = -268.521;};
class Object9	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-37.0254,-6.02745,0}; dir = -268.521;};
class Object10	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-37.1958,-12.6608,0}; dir = -268.521;};
class Object11	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-24.4736,-49.114,0}; dir = -178.52;};
class Object12	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-17.8408,-49.2844,0}; dir = -178.52;};
class Object13	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-11.209,-49.4556,0}; dir = -178.52;};
class Object14	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-4.57617,-49.627,0}; dir = -178.52;};
class Object15	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {2.0564,-49.7973,0}; dir = -178.52;};
class Object16	{side = 8; vehicle = "3AS_Wall_Laser_Corner"; rank = ""; position[] = {9.21313,-42.956,0}; dir = -268.521;};
class Object17	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {9.21265,-43.0004,0}; dir = -88.5205;};
class Object18	{side = 8; vehicle = "3AS_Wall_Laser_Corner"; rank = ""; position[] = {-31.1064,-48.9432,0}; dir = -178.52;};
class Object19	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-37.9475,-41.7853,0}; dir = -88.5205;};
class Object20	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-7.94092,-45.0649,3.8147e-006}; dir = -358.521;};
class Object21	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-14.5728,-44.8936,3.8147e-006}; dir = -358.521;};
class Object22	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-21.2056,-44.7228,3.8147e-006}; dir = -358.521;};
class Object23	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-27.8389,-44.552,3.8147e-006}; dir = -358.521;};
class Object24	{side = 8; vehicle = "land_3AS_Imperial_Tower"; rank = ""; position[] = {-30.3972,-41.6099,-4.76837e-007}; dir = -268.521;};
class Object25	{side = 8; vehicle = "land_3AS_Imperial_Tower"; rank = ""; position[] = {1.43555,-42.4556,-4.76837e-007}; dir = -88.5205;};
class Object26	{side = 8; vehicle = "land_3AS_Imperial_Tower"; rank = ""; position[] = {-29.4822,-6.18172,-4.76837e-007}; dir = -268.521;};
class Object27	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-20.106,-3.57238,4.76837e-006}; dir = -178.52;};
class Object28	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-13.4741,-3.74366,4.76837e-006}; dir = -178.52;};
class Object29	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-7.15991,-3.90625,4.76837e-006}; dir = -178.52;};
class Object30	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {-0.848877,-4.06877,4.76837e-006}; dir = -178.52;};
class Object31	{side = 8; vehicle = "land_3AS_Wall_Laser_Corner"; rank = ""; position[] = {-1.21045,-4.05943,0}; dir = -358.521;};
class Object32	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {5.29541,-23.8856,0}; dir = -88.5205;};
class Object33	{side = 8; vehicle = "land_3AS_Wall_Laser_v2"; rank = ""; position[] = {5.45776,-17.574,0}; dir = -88.5205;};
class Object34	{side = 8; vehicle = "3AS_Wall_Laser_Corner"; rank = ""; position[] = {-1.2749,-17.8179,0}; dir = -178.52;};
class Object35	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {5.35791,-17.9887,0}; dir = -178.52;};
class Object36	{side = 8; vehicle = "3AS_Wall_Laser_Door"; rank = ""; position[] = {-8.11548,-10.6571,0}; dir = -88.5205;};
class Object37	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-8.34302,-13.9058,0}; dir = -178.52;};
class Object38	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-11.6594,-13.6653,0}; dir = -88.5205;};
class Object39	{side = 8; vehicle = "3AS_Wall_Laser_Door"; rank = ""; position[] = {-14.6589,-13.7437,0}; dir = -178.52;};
class Object40	{side = 8; vehicle = "3AS_Wall_Laser"; rank = ""; position[] = {-15.3667,-10.4605,0}; dir = -88.5205;};
class Object41	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {-22.4709,-8.18727,0}; dir = -268.521;};
class Object42	{side = 8; vehicle = "land_3as_Bunker_Metal"; rank = ""; position[] = {-9.29028,-28.1384,1.54273}; dir = -88.5205;};
class Object43	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {-42.9219,-13.817,9.53674e-007}; dir = -268.521;};
class Object44	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {-43.0291,-18.1975,9.53674e-007}; dir = -268.521;};
class Object45	{side = 8; vehicle = "3AS_Short_Wall_Long"; rank = ""; position[] = {-42.8516,-16.0727,9.53674e-007}; dir = -358.521;};
class Object46	{side = 8; vehicle = "3AS_Short_Wall"; rank = ""; position[] = {-41.6008,-18.2696,9.53674e-007}; dir = -268.521;};
class Object47	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {-40.1868,-18.2294,9.53674e-007}; dir = -178.52;};
class Object48	{side = 8; vehicle = "3AS_Short_Wall"; rank = ""; position[] = {-41.4937,-13.913,9.53674e-007}; dir = -268.521;};
class Object49	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {-40.0068,-13.8913,9.53674e-007}; dir = -268.521;};
class Object50	{side = 8; vehicle = "3AS_Short_Wall_Bunker"; rank = ""; position[] = {-42.2424,-36.9246,0}; dir = -88.5205;};
class Object51	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {14.7761,-41.7052,9.53674e-007}; dir = -88.5204;};
class Object52	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {14.8833,-37.3246,9.53674e-007}; dir = -88.5204;};
class Object53	{side = 8; vehicle = "3AS_Short_Wall_Long"; rank = ""; position[] = {14.7061,-39.4489,9.53674e-007}; dir = -178.52;};
class Object54	{side = 8; vehicle = "3AS_Short_Wall"; rank = ""; position[] = {13.4551,-37.2526,9.53674e-007}; dir = -88.5204;};
class Object55	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {12.041,-37.2927,9.53674e-007}; dir = -358.52;};
class Object56	{side = 8; vehicle = "3AS_Short_Wall"; rank = ""; position[] = {13.3479,-41.6091,9.53674e-007}; dir = -88.5204;};
class Object57	{side = 8; vehicle = "3AS_H_barrier_small_1"; rank = ""; position[] = {11.8611,-41.6308,9.53674e-007}; dir = -88.5204;};
class Object58	{side = 8; vehicle = "3AS_Short_Wall_Bunker"; rank = ""; position[] = {14.0967,-18.597,0}; dir = -268.521;};
class Object59	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {-22.6191,-13.9114,0}; dir = -88.5204;};
class Object60	{side = 8; vehicle = "3AS_Short_Wall_Bunker"; rank = ""; position[] = {-17.3691,-7.48938,0}; dir = -178.52;};
class Object61	{side = 8; vehicle = "3AS_H_barrier_small_5"; rank = ""; position[] = {-19.999,-26.2563,0}; dir = -268.521;};
class Object62	{side = 8; vehicle = "3AS_H_barrier_small_5"; rank = ""; position[] = {-16.2466,-25.3545,0}; dir = -358.521;};
class Object63	{side = 8; vehicle = "3AS_Short_Wall_Long"; rank = ""; position[] = {-18.7959,-32.1218,0}; dir = -268.521;};
class Object64	{side = 8; vehicle = "3AS_Short_Wall_Long"; rank = ""; position[] = {-20.1221,-30.7287,0}; dir = -178.52;};
class Object65	{side = 8; vehicle = "3as_FOB_Light_Tall_Prop"; rank = ""; position[] = {-9.97583,-30.3942,-4.76837e-007}; dir = -268.521;};
