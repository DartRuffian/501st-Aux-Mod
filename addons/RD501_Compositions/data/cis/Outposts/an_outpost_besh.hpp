class Object0	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {1.66014,-2.65955,0.0371823}; dir = -89.3286;};
class Object1	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {-3.12189,-27.964,0.0371823}; dir = 0.671432;};
class Object2	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {-33.2979,-23.045,0.0371823}; dir = 90.6715;};
class Object3	{side = 8; vehicle = "land_3as_cis_wall_corner_v2"; rank = ""; position[] = {-28.5022,2.21692,0.0371823}; dir = 180.671;};
class Object4	{side = 8; vehicle = "land_3as_cis_wall_door_v2"; rank = ""; position[] = {-33.2031,-12.6218,0.0371823}; dir = 90.6715;};
class Object5	{side = 8; vehicle = "land_3as_cis_wall_tower_v2"; rank = ""; position[] = {2.09186,-13.1169,4.76837e-007}; dir = -89.3285;};
class Object6	{side = 8; vehicle = "land_3as_cis_wall_med_v2"; rank = ""; position[] = {-12.5724,2.03577,0.0374527}; dir = 180.671;};
class Object7	{side = 8; vehicle = "land_3as_cis_wall_med_v2"; rank = ""; position[] = {-12.8922,-27.8778,0.0374527}; dir = 0.671425;};
class Object8	{side = 8; vehicle = "land_3as_cis_wall_short_v2"; rank = ""; position[] = {1.62993,-7.6123,0.0374527}; dir = -89.3286;};
class Object9	{side = 8; vehicle = "land_3as_cis_wall_short_v2"; rank = ""; position[] = {1.5196,-18.5686,0.0374527}; dir = -89.3286;};
class Object10	{side = 8; vehicle = "land_3as_cis_wall_tower_v2"; rank = ""; position[] = {-22.884,2.69812,4.76837e-007}; dir = 180.671;};
class Object11	{side = 8; vehicle = "land_3as_cis_wall_tower_v2"; rank = ""; position[] = {-23.2122,-28.2693,4.76837e-007}; dir = 0.671471;};
class Object12	{side = 8; vehicle = "3as_Metal_Trench_Short_Single"; rank = ""; position[] = {-15.1179,2.10046,0}; dir = 90.6714;};
class Object13	{side = 8; vehicle = "3as_Metal_Trench_Short_Single"; rank = ""; position[] = {-8.1086,2.02271,0}; dir = 90.6714;};
class Object14	{side = 8; vehicle = "3as_Metal_Trench_Short_Single"; rank = ""; position[] = {1.54895,-5.31372,0}; dir = 180.671;};
class Object15	{side = 8; vehicle = "3as_Metal_Trench_Short_Single"; rank = ""; position[] = {1.3709,-20.8817,0}; dir = 180.671;};
class Object16	{side = 8; vehicle = "3as_Metal_Trench_Short_Single"; rank = ""; position[] = {-15.4202,-27.733,0}; dir = -89.3286;};
class Object17	{side = 8; vehicle = "3as_Metal_Trench_Short_Single"; rank = ""; position[] = {-8.41242,-27.8196,0}; dir = -89.3286;};
class Object18	{side = 8; vehicle = "land_3as_Bunker_Metal"; rank = ""; position[] = {-21.3483,-12.6146,1.41123}; dir = 180.671;};
class Object19	{side = 8; vehicle = "3as_small_crate_stack_2_prop"; rank = ""; position[] = {-17.6327,-9.29858,0}; dir = 90.6715;};
class Object20	{side = 8; vehicle = "3as_small_crate_stack_1_prop"; rank = ""; position[] = {-18.1446,-17.3899,0}; dir = -89.3286;};