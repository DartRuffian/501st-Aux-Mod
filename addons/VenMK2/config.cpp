class CfgPatches
{
	class RD501_patch_VenatorMK2
	{
		requiredVersion=1;
		version=1;
		requiredAddons[]=
		{
			"RD501_patch_main"
		};
		units[]=
		{
			"Venator_MK2",
			"Venator_guns",
			"Venator_open",
			"Venator_MK2_Main",
			"Venator_Main_doors_open",
			"Venator_doors_closed"
		};
		weapons[]={};
	};
};
class CfgVehicles
{
	class Strategic;
	class Venator_MK2: Strategic
	{
		scope=2;
		model="\VenMK2\venMK2.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Venator MK2";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\VenMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Venatormk2_guns: Strategic
	{
		scope=2;
		model="\VenMK2\ven_guns.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Venator guns";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Venatormk2_open: Strategic
	{
		scope=2;
		model="\VenMK2\venMK2_open.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Venator MK2 open";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Venator_MK2_Main: Strategic
	{
		scope=2;
		model="\VenMK2\venMK2_Main.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Venator MK2 Main";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Venator_Main_doors_open: Strategic
	{
		scope=2;
		model="\VenMK2\ven_Main_doors_open.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Venator Main doors open";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Venator_Main_doors_closed: Strategic
	{
		scope=2;
		model="\VenMK2\ven_Main_doors_closed.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Venator Main doors closed";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class ventral_hatch: Strategic
	{
		scope=2;
		model="\VenMK2\ventral_hatch.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Ventral Hatch";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Ven_side_doors: Strategic
	{
		scope=2;
		model="\VenMK2\Ven_side_doors.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Side doors";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
	class Ven_low: Strategic
	{
		scope=2;
		model="\VenMK2\Ven_low.p3d";
		placement="vertical";
		mapSize=3;
		displayName="Low Poly Venator";
		vehicleClass="RD501_Vehicle_Class_static_turrets";
		icon="\venMK2\data\venator.paa";
		cost=0;
		armor=8000;
		scopeCurator=2;
		editorCategory="RD501_Editor_Category_statics";
		editorSubcategory="RD501_Editor_Category_static_ships";
	};
};