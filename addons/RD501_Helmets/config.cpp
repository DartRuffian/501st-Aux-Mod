
#include "../RD501_main/config_macros.hpp"
#include "config_macros.cpp"
#define TEXTUREAB \RD501_Helmets\_textures\airborne
class CfgPatches
{
    class RD501_patch_helmets
    {
        author=DANKAUTHORS;
        requiredAddons[]=
        {
            macro_lvl1_req,
            "RD501_patch_main"
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]=
        {
            macro_new_helmet(infantry,jlts_recruit)
        };
    };
};

class CfgWeapons
{
    class HeadgearItem;
    class H_HelmetB;
    class SWLB_P2_SpecOps_Helmet;
    class 21st_clone_P2_helmet;
    class 3as_P1_Base;
    class 21st_clone_P2_ENG_helmet;
    class JLTS_CloneHelmetP2;
    class JLTS_CloneHelmetARC;
    class Aux501_Units_Republic_501_Warden_Helmet;
    
    class macro_new_helmet(empire_rg,boi): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    

        displayName = "[Empire] Royal Guard Helm 01";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetSCC.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"RD501_Units\textures\gcw\empire\RG\rg_helmet.paa"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetSC.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    //jlts
    class macro_new_helmet(arc,base_jlts): JLTS_CloneHelmetARC
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] ARC HELM (Base)";
        picture="\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetARC_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"RD501_Helmets\_textures\ARC\ARC.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\Clone_helmet_ARC"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
            hiddenSelections[] = {"Camo1"};
            modelSides[] = {6};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(arc,sgt_jlts): JLTS_CloneHelmetARC
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] ARC HELM (Sergeant)";
        picture="\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetARC_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"RD501_Helmets\_textures\ARC\ARC_CS.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\Clone_helmet_ARC"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
            hiddenSelections[] = {"Camo1"};
            modelSides[] = {6};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(arc,alya): macro_new_helmet(arc,base_jlts)
    {
        displayName = "[501st] ARC HELM ('Alya')";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Alya.paa,macro_custom_helmet_textures\ARC\Alya.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Alya.rvmat};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
            hiddenSelections[] = {"Camo1","Camo2"};
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    
    class macro_new_helmet(arc,panther): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Panther')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Panther.paa,macro_custom_helmet_textures\ARC\Panther.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Panther.rvmat};
    };
    class macro_new_helmet(arc,merlin): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Merlin')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Merlin.paa,macro_custom_helmet_textures\ARC\Merlin.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Merlin.rvmat};
    };
    class macro_new_helmet(arc,halo): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Halo')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Halo.paa,macro_custom_helmet_textures\ARC\Halo.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Halo.rvmat};
    };
    class macro_new_helmet(arc,sabre): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Sabre')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Sabre.paa,macro_custom_helmet_textures\ARC\Sabre.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Sabre.rvmat};
    };
    class macro_new_helmet(arc,signus): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Signus')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Signus.paa,macro_custom_helmet_textures\ARC\Signus.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Signus.rvmat};
    };
    class macro_new_helmet(arc,mogar): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Mogar')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Mogar.paa,macro_custom_helmet_textures\ARC\Mogar.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Mogar.rvmat};
    };
    class macro_new_helmet(arc,zareth): macro_new_helmet(arc,alya)
    {
        displayName = "[501st] ARC HELM ('Zareth')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\ARC\Zareth.paa,macro_custom_helmet_textures\ARC\Zareth.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\Clone_helmet_ARC.rvmat", macro_custom_helmet_textures\ARC\Zareth.rvmat};
    };
    
    //BARC
    class macro_new_helmet(barc,base_jlts): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF MED HELM 06 A (CM-C)";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetBARC_ui_ca.paa";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cm_c_barc_helmet.paa"};
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
        hiddenSelectionsMaterials[]= {"MRC\JLTS\characters\CloneArmor2\data\Clone_helmet_BARC.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(barc,CM): macro_new_helmet(barc,base_jlts)
    {
        displayName = "[501st] INF MED HELM 06 B (CM)";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cm_barc_helmet.paa"};
    };
    class macro_new_helmet(barc,osiris): macro_new_helmet(barc,base_jlts)
    {
        scope = 2;
        displayName = "[501st] INF MED HELM 06 ('Osiris')";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\medic\Osiris.paa,macro_custom_helmet_textures\medic\Osiris.paa};
        hiddenSelectionsMaterials[] = {"MRC\JLTS\characters\CloneArmor2\data\Clone_helmet_BARC.rvmat", macro_custom_helmet_textures\medic\Osiris.rvmat};
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
        subItems[] = {};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
            hiddenSelections[] = {"Camo1","Camo2"};
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };

    //Infantry
    class macro_new_helmet(infantry,jlts_recruit): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF HELM 01 (Base)";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\recruit_helmet.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(infantry,jlts_cadet): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF HELM 02 (Cadet)";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cadet_helmet.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(infantry,jlts_trooper): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   

        displayName = "[501st] INF HELM 03 (Trooper)";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\trooper_helmet.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(warrant,jlts_warrant): macro_new_helmet(infantry,jlts_trooper)
    {
        displayName = "[501st] WRNT HELM 01";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\warrant_helmet.paa"};
    };
    class macro_new_helmet(Sauce,jlts_Sauce): macro_new_helmet(infantry,jlts_trooper)
    {
        displayName = "[501st] INF HELM ('Source')";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Infantry\Sauce.paa"};
    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// Custom Helmets with RVMATs/VisorGlow ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class macro_new_helmet(infantry,jub): JLTS_CloneHelmetP2
    {
        scope = 2;
        displayName = "[501st] INF HELM ('Jub')";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Jub.paa,macro_custom_helmet_textures\infantry\Jub.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Jub.rvmat};
        model = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        subItems[] = {};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            hiddenSelections[] = {"Camo1","Camo2"};
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(infantry,drammon): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Drammon')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Drammon.paa,macro_custom_helmet_textures\infantry\Drammon.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Drammon.rvmat};
    };
    class macro_new_helmet(infantry,Kestrel): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Kestrel')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Kestrel.paa,macro_custom_helmet_textures\infantry\Kestrel.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Kestrel.rvmat};
    };
    class macro_new_helmet(infantry,swanny): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Swanny')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Swanny.paa,macro_custom_helmet_textures\infantry\Swanny.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Swanny.rvmat};
    };
    class macro_new_helmet(infantry,Anselm): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Anselm')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Anselm.paa,macro_custom_helmet_textures\infantry\Anselm.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Anselm.rvmat};
    };
    class macro_new_helmet(infantry,triage): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Triage')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Triage.paa,macro_custom_helmet_textures\infantry\Triage.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Triage.rvmat};
    };
    class macro_new_helmet(infantry,gallagher): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Gallagher')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Gallagher.paa,macro_custom_helmet_textures\infantry\Gallagher.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Gallagher.rvmat};
    };
    class macro_new_helmet(infantry,stitches): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Stitches')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Stitches.paa,macro_custom_helmet_textures\infantry\Stitches.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Stitches.rvmat};
    };
    class macro_new_helmet(infantry,chimera): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Chimera')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Chimera.paa,macro_custom_helmet_textures\infantry\Chimera.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Chimera.rvmat};
    };
    class macro_new_helmet(infantry,checkers): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Checkers')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Checkers.paa,macro_custom_helmet_textures\infantry\Checkers.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Checkers.rvmat};
    };
    class macro_new_helmet(infantry,rythian): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Rythian')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Rythian.paa,macro_custom_helmet_textures\infantry\Rythian.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Rythian.rvmat};
    };
    class macro_new_helmet(infantry,crinkcase): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Crinkcase')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Crinkcase.paa,macro_custom_helmet_textures\infantry\Crinkcase.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Crinkcase.rvmat};
    };
    class macro_new_helmet(infantry,garviel): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Garviel')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Garviel.paa,macro_custom_helmet_textures\infantry\Garviel.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Garviel.rvmat};
    };
    class macro_new_helmet(infantry,nico): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Nico')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Nico.paa,macro_custom_helmet_textures\infantry\Nico.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Nico.rvmat};
    };
    class macro_new_helmet(infantry,chaser): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Chaser')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Chaser.paa,macro_custom_helmet_textures\infantry\Chaser.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Chaser.rvmat};
    };
    class macro_new_helmet(infantry,anta): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Anta')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Anta.paa,macro_custom_helmet_textures\infantry\Anta.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Anta.rvmat};
    };
    class macro_new_helmet(infantry,hobnob): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Hobnob')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Hobnob.paa,macro_custom_helmet_textures\infantry\Hobnob.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Hobnob.rvmat};
    };
    class macro_new_helmet(infantry,napkin): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Napkin')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Napkin.paa,macro_custom_helmet_textures\infantry\Napkin.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Napkin.rvmat};
    };
    class macro_new_helmet(infantry,bridger): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Bridger')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Bridger.paa,macro_custom_helmet_textures\infantry\Bridger.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Bridger.rvmat};
    };
    class macro_new_helmet(infantry,juggernaut): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Juggernaut')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Juggernaut.paa,macro_custom_helmet_textures\infantry\Juggernaut.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Juggernaut.rvmat};
    };
    class macro_new_helmet(infantry,craz): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Craz')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Craz.paa,macro_custom_helmet_textures\infantry\Craz.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Craz.rvmat};
    };
    class macro_new_helmet(infantry,hungarian): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Hungarian')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Hungarian.paa,macro_custom_helmet_textures\infantry\Hungarian.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Hungarian.rvmat};
    };
    class macro_new_helmet(infantry,daan): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Daan')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Daan.paa,macro_custom_helmet_textures\infantry\Daan.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Daan.rvmat};
    };
    class macro_new_helmet(infantry,Bones): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Bones')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Bones.paa,macro_custom_helmet_textures\infantry\Bones.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Bones.rvmat};
    };
    class macro_new_helmet(infantry,stomerone): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Stomerone')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Stomerone.paa,macro_custom_helmet_textures\infantry\Stomerone.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Stomerone.rvmat};
    };
    class macro_new_helmet(infantry,narrator): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Narrator')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Narrator.paa,macro_custom_helmet_textures\infantry\Narrator.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Narrator.rvmat};
    };
    class macro_new_helmet(infantry,bradley): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Bradley')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Bradley.paa,macro_custom_helmet_textures\infantry\Bradley.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Bradley.rvmat};
    };
    class macro_new_helmet(infantry,jatt): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Jatt')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Jatt.paa,macro_custom_helmet_textures\infantry\Jatt.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Jatt.rvmat};
    };
    class macro_new_helmet(infantry,worgan): macro_new_helmet(infantry,jub)
    {
        displayName = "[501st] INF HELM ('Worgan')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\infantry\Worgan.paa,macro_custom_helmet_textures\infantry\Worgan.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_p2.rvmat", macro_custom_helmet_textures\infantry\Worgan.rvmat};
    };

    class macro_new_helmet(infantry,jlts_sgt): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   

        displayName = "[501st] INF HELM 04 (Sgt)";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\sgt_helmet.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(infantry,jlts_cm_c): macro_new_helmet(infantry,jlts_trooper)
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   

        displayName = "[501st] INF MED HELM 03 (CM-C)";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cm_c_trooper_helmet.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(infantry,jlts_cm): macro_new_helmet(infantry,jlts_cm_c)
    {
        displayName = "[501st] INF MED HELM 03 (CM)";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\inf\cm_trooper_helmet.paa"};
    }; 
    class macro_new_helmet(infantry,jlts_odin): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        RD501_isNV = 1;
        displayName = "[501st] INF HELM ('Odin')";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Infantry\Odin.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_p2.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    //Airborne
    class JLTS_CloneHelmetAB;
    class macro_new_helmet(airborne,jlts_base): JLTS_CloneHelmetAB
    {
        author = "501st Aux Team";
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB HELM 01 (Cadet)";
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_Helmet_AB_co.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_ab.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 10;
            uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            hiddenSelections[] = {"camo1"};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 60;
                    passThrough = 0.5;
                };
            };
        };
    };		
    class macro_new_helmet(airborne,jlts_trooper): JLTS_CloneHelmetAB
    {
        author = "501st Aux Team";
        scope = 2;
        scopeArsenal = 2;
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] AB HELM 02 (Trooper)";
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\ab\ab_helmet_trooper.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_ab.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 10;
            uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            hiddenSelections[] = {"camo1"};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 60;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(airborne,jlts_vtrooper): JLTS_CloneHelmetAB
    {
        author = "501st Aux Team";
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB HELM 03 (Vet. Trooper)";
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        model="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;    
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\jlts\ab\ab_helmet_vtrooper.paa"};
        hiddenSelectionsMaterials[]= {"RD501_Helmets\_materials\clone_helmet_ab.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 10;
            uniformmodel="\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            hiddenSelections[] = {"camo1"};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 60;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(airborne,Medic_1): macro_new_helmet(airborne,jlts_vtrooper)
    {
        displayName = "[501st] AB HELM 02 A (Medic)";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Airborne\Medic2.paa"};
    };
    class macro_new_helmet(airborne,RTO_1): macro_new_helmet(airborne,jlts_vtrooper)
    {
        displayName = "[501st] AB HELM 02 B (RTO)";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Airborne\RTO2.paa"};
    };
    class macro_new_helmet(airborne,Medic_2): macro_new_helmet(airborne,jlts_vtrooper)
    {
        displayName = "[501st] AB HELM 03 A (Medic)";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Airborne\Medic1.paa"};
    };
    class macro_new_helmet(airborne,RTO_2): macro_new_helmet(airborne,jlts_vtrooper)
    {
        displayName = "[501st] AB HELM 03 B (RTO)";
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Airborne\RTO.paa"};
    };

    class macro_new_helmet(airborne,bandit): macro_new_helmet(airborne,jlts_vtrooper)
    {
        displayName = "[501st] AB HELM ('Bandit')";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Bandit.paa, macro_custom_helmet_textures\Airborne\Bandit.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Bandit.rvmat};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
            hiddenSelections[] = {"Camo1","Camo2"};
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    class macro_new_helmet(airborne,akira): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Akira')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Akira.paa,macro_custom_helmet_textures\Airborne\Akira.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Akira.rvmat};
    };
    class macro_new_helmet(airborne,spire): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Spire')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Spire.paa,macro_custom_helmet_textures\Airborne\Spire.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Spire.rvmat};
    };
    class macro_new_helmet(airborne,power): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Power')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Power.paa,macro_custom_helmet_textures\Airborne\Power.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Power.rvmat};
    };
    class macro_new_helmet(airborne,death): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Death')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Death.paa,macro_custom_helmet_textures\Airborne\Death.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Death.rvmat};
    };
    class macro_new_helmet(airborne,eights): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Eights')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Eights.paa,macro_custom_helmet_textures\Airborne\Eights.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Eights.rvmat};
    };
    class macro_new_helmet(airborne,anubis): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Anubis')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Anubis.paa,macro_custom_helmet_textures\Airborne\Anubis.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Anubis.rvmat};
    };
    //class macro_new_helmet(airborne,signus): macro_new_helmet(airborne,bandit)
    //{
        //displayName = "[501st] AB HELM ('Signus')";
        //hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Signus.paa,macro_custom_helmet_textures\Airborne\Signus.paa};
        //hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Signus.rvmat};
    //};
    class macro_new_helmet(airborne,killer): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Killer')";;
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Killer.paa,macro_custom_helmet_textures\Airborne\Killer.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Killer.rvmat};
    };
    class macro_new_helmet(airborne,uni): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Uni')";;
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Uni.paa,macro_custom_helmet_textures\Airborne\Uni.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Uni.rvmat};
    };
    class macro_new_helmet(airborne,patriot): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Patriot')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Patriot.paa,macro_custom_helmet_textures\Airborne\Patriot.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Patriot.rvmat};
    };
    class macro_new_helmet(airborne,blood): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Blood')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Blood.paa,macro_custom_helmet_textures\Airborne\Blood.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Blood.rvmat};
    };
    class macro_new_helmet(airborne,scramz): macro_new_helmet(airborne,bandit)
    {
        displayName = "[501st] AB HELM ('Scramz')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\Airborne\Scramz.paa,macro_custom_helmet_textures\Airborne\Scramz.paa};
        hiddenSelectionsMaterials[] = {"RD501_Helmets\_materials\clone_helmet_ab.rvmat", macro_custom_helmet_textures\Airborne\Scramz.rvmat};
    };

    //Warden
    class Aux501_Customs_Warden_Helmet_Wirtimus: Aux501_Units_Republic_501_Warden_Helmet
    {
        displayName = "[501st] WRDN HELM ('Wirtimus')";
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\warden\Wirtimus.paa",
            "\RD501_Helmets\_textures\aviation\warden\Visor\Wirtimus.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\Aux501\Units\Republic\501st\Warden\Helmets\Phase2\data\materials\Warden_Helmet.rvmat",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\materials\Visor_Glow.rvmat"
        };
    };
    
    //Legion
    class macro_new_helmet(infantry,ls_base): SWLB_P2_SpecOps_Helmet
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF RTO HELM 01 (Alt)";
        model = "SWLB_CEE\data\SWLB_P2_SpecOps_Helmet.p3d";
        picture="\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LS\rto_alt_helmet.paa"};
        hiddenSelectionsMaterials[]= {"swlb_cee\data\SWLB_P2_SpecOps.rvmat"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "SWLB_CEE\data\SWLB_P2_SpecOps_Helmet.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };
    
    //Last Force Project

    //class macro_new_helmet(infantry,lf_base): 21st_clone_P2_helmet
    //{
        //scope = 2;
        //scopeArsenal = 2;
        //author = "501st Aux Team";
        //weaponPoolAvailable = 1;       
        //ace_hearing_protection = 0.85; 		
        //ace_hearing_lowerVolume = 0;   
        //subItems[] = {"G_B_Diving","ItemcTabHCam"};
        //displayName = "[501st] INF SNOW HELM 01 (Trooper)";
        //picture="\RD501_Helmets\_textures\ui\snow_helm_01_ui.paa";
        //hiddenSelections[] = {"Helmet"};
        //hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LF\snow_trooper_helmet.paa"};
        //hiddenSelectionsMaterials[]= {"armor_unit\21\helmet\helm.rvmat"};
        //model = "\armor_unit\21\HelmetP2_21.p3d";
        //class ItemInfo: HeadgearItem
        //{
            //mass = 30;
            //uniformModel = "\armor_unit\21\HelmetP2_21.p3d";
            //picture = "armor_unit\21\ui\21_Armor.paa";
            //modelSides[] = {6};
            //hiddenSelections[] = {"Helmet"};
            //material = -1;
            //explosionShielding = 2.2;
            //minimalHit = 0.01;
            //passThrough = 0.01;
            //class HitpointsProtectionInfo
            //{
                //class Head
                //{
                    //hitpointName = "HitHead";
                    //armor = 50;
                    //passThrough = 0.6;
                //};
            //};
        //};
    //};

    //class macro_new_helmet(infantry,snow_nco) : macro_new_helmet(infantry,lf_base)
    //{
        //displayName = "[501st] INF SNOW HELM 02 (NCO)";
        //picture="\RD501_Helmets\_textures\ui\snow_helm_02_ui.paa";
        //hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LF\snow_nco_helmet.paa"};
    //};

    //class macro_new_helmet(warden,snow) : 21st_clone_P2_ENG_helmet
    //{
        //scope = 2;
        //scopeArsenal = 2;
        //author = "501st Aux Team";
        //weaponPoolAvailable = 1;       
        //ace_hearing_protection = 0.85; 		
        //ace_hearing_lowerVolume = 0;   
        //subItems[] = {"G_B_Diving","ItemcTabHCam"};
        //displayName = "[501st] WRDN SNOW HELM 01";
        //picture="\RD501_Helmets\_textures\ui\snow_helm_03_ui.paa";
        //hiddenSelections[] = {"Helmet"};
        //hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LF\warden_snow_helmet.paa"};
        //hiddenSelectionsMaterials[]= {"armor_unit\21\helmet\helm_ENG.rvmat"};
        //model = "\armor_unit\21\21_HelmetP2_ENG.p3d";
        //class ItemInfo: HeadgearItem
        //{
            //mass = 30;
            //uniformModel = "\armor_unit\21\21_HelmetP2_ENG.p3d";
            //picture = "armor_unit\21\ui\21_Armor.paa";
            //modelSides[] = {6};
            //hiddenSelections[] = {"Helmet"};
            //material = -1;
            //explosionShielding = 2.2;
            //minimalHit = 0.01;
            //passThrough = 0.01;
            //class HitpointsProtectionInfo
            //{
                //class Head
                //{
                    //hitpointName = "HitHead";
                    //armor = 50;
                    //passThrough = 0.6;
                //};
            //};
        //};
    //};
    
    //Kobra
    class k_cadet_Helmet;
    class macro_new_helmet(infantry,UTC): k_cadet_Helmet
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF UTC HELM";
        model = "kobra\442_equipment\helmets\model\clone\k_cadet_helmet.p3d";
        picture="\RD501_Helmets\_textures\ui\utc_cadet_ui.paa";
        hiddenSelections[] = {"helmet"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\Kobra\utc_cadet_helmet_co.paa"};
        hiddenSelectionsMaterials[]= 
        {
            "\kobra\442_equipment\helmets\data\cadet_helmet\helmet.rvmat",
            "\kobra\442_equipment\helmets\data\cadet_helmet\visor.rvmat"
        };
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "kobra\442_equipment\helmets\model\clone\k_cadet_helmet.p3d";
            hiddenSelections[] = {"helmet"};
            modelSides[] = {3,1};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    //3AS
    class macro_new_helmet(infantry,p1_trooper): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF P1 HELM 01 (Trooper)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_helmet_p1_ca.paa";
        model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Unmarked_CO.paa"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
            hiddenSelections[] = {"camo"};
            modelSides[] = {3,1};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(infantry,p1_trooper_nco): macro_new_helmet(infantry,p1_trooper)
    {
        scope = 2;
        scopeArsenal = 2;
        author = "RD501";
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] INF P1 HELM 02 (NCO)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_helmet_p1_ca.paa";
        model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\3AS\p1_nco_helmet.paa"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
            hiddenSelections[] = {"camo"};
            modelSides[] = {3,1};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(infantry,p1_trooper_sergeant): macro_new_helmet(infantry,p1_trooper)
    {
        displayName = "[501st] INF P1 HELM 03 (Sergeant)";
        hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Sergeant_CO.paa"};
    };
    class macro_new_helmet(infantry,p1_trooper_commander): macro_new_helmet(infantry,p1_trooper)
    {
        displayName = "[501st] INF P1 HELM 04 (Commander)";
        hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Commander_CO.paa"};
    };
    class macro_new_helmet(arc,p1_trooper): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] ARC P1 HELM 01 (Trooper)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_helmet_p1_ca.paa";
        model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Lieutenant_CO.paa"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
            hiddenSelections[] = {"camo"};
            hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Lieutenant_CO.paa"};
            modelSides[] = {3,1};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(arc,p1_nco): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";;
        weaponPoolAvailable = 1;       
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;   
        subItems[] = {"G_B_Diving","ItemcTabHCam"};
        displayName = "[501st] ARC P1 HELM 02 (NCO)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_helmet_p1_ca.paa";
        model = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Captain_CO.paa"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformModel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Standard_P1.p3d";
            hiddenSelections[] = {"camo"};
            hiddenSelectionsTextures[] = {"3AS\3AS_Characters\Clones\Headgear\Textures\Phase1\Phase1_Captain_CO.paa"};
            modelSides[] = {3,1};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    
    //PILOT HELMETS//
    /*class macro_new_helmet(pilot,base): H_HelmetB
    {
        scope = 2;
        scopeArsenal = 2;
        author = "RD501";
        weaponPoolAvailable = 1;
        ace_hearing_protection = 0.85;
        ace_hearing_lowerVolume = 0;    
        displayName = "[501st] AVI HELM (Base)";
        model = "\RD501_Helmets\AB\AB_helmet_p1.p3d";
        hiddenSelections[] = {
            "camo1",
            "mat"
        };
    
        hiddenSelectionsTextures[]=
        {
            "RD501_Helmets\_textures\aviation\clonePilotHelmet_co.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "",
            "RD501_Helmets\AB\data\pilot_jlts.rvmat"
        };*/


    /*	class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\RD501_Helmets\AB\AB_helmet_p1.p3d";//"SWOP_clones\helmet\CloneHelmetPilot.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {
                "camo1",
                "mat"
            };
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };

        subItems[] = {"G_B_Diving","ItemcTabHCam"};
    };*/
    class macro_new_helmet(3as_pilot,base): H_HelmetB
    {
        author = "501st Aux Team";
        scope=2;
        weaponPoolAvailable=1;
        displayName="[501st] AVI HELM 01 (Base)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
        hiddenSelections[] = {"Camo","Camo2","Camo3","Camo4","Camo5"};
        hiddenSelectionsTextures[]=
        {
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase2_Pilot_Razor_co.paa",
            "",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase2_Pilot_Standard_co.paa",
            ""
        };
        class ItemInfo: HeadgearItem
        {
            mass=40;
            uniformModel="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
            hiddenSelections[] = {"Camo","Camo2","Camo3","Camo4","Camo5"};
            modelSides[]={3,1};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName="HitHead";
                    armor=6;
                    passThrough=0.5;
                };
            };
        };
    };
    class macro_new_helmet(3as_pilot,base_2): H_HelmetB
    {
        author = "501st Aux Team";
        scope=2;
        weaponPoolAvailable=1;
        displayName="[501st] AVI HELM 02 (Base 2)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
        hiddenSelections[] = {"Camo","Camo2","Camo3","Camo4","Camo5"};
        hiddenSelectionsTextures[] = 
        {
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase2_Pilot_Razor_co.paa",
            "",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "3as\3as_characters\clones\headgear\textures\pilotp2\Phase2_Pilot_Panel_501st_co.paa"
        };
        class ItemInfo: HeadgearItem
        {
            mass=40;
            uniformModel="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
            hiddenSelections[] = {"Camo","Camo2","Camo3","Camo4","Camo5"};
            modelSides[]={3,1};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    
    class macro_new_helmet(3as_pilot,Orange): H_HelmetB
    {
        author = "501st Aux Team";
        scope=2;
        weaponPoolAvailable=1;
        displayName="[501st] AVI HELM ('Orange')";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
        hiddenSelections[] = {"Camo","Camo1","Camo2","Camo3","Camo4","Camo5"};
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\Orange.paa",
            "\RD501_Helmets\_textures\aviation\Orange.paa",
            "",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "\RD501_Helmets\_textures\aviation\box\Orange.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "","\RD501_Helmets\_textures\aviation\Orange.rvmat","","","",""
        };
        class ItemInfo: HeadgearItem
        {
            mass=40;
            uniformModel="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
            hiddenSelections[] = {"Camo","Camo1","Camo2","Camo3","Camo4","Camo5"};
            modelSides[]={3,1};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(3as_pilot,Sazi): macro_new_helmet(3as_pilot,Orange)
    {
        displayName = "[501st] AVI HELM ('Sazi')";
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\Sazi.paa",
            "\RD501_Helmets\_textures\aviation\Sazi.paa",
            "",
            "\3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "\RD501_Helmets\_textures\aviation\Box\Sazi.paa",
            
        };
        hiddenSelectionsMaterials[] = 
        {
            "","\RD501_Helmets\_textures\aviation\Sazi.rvmat","","","",""
        };
    };
    class macro_new_helmet(3as_pilot,Ethan): macro_new_helmet(3as_pilot,Orange)
    {
        displayName = "[501st] AVI HELM ('Ethan')";
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\Ethan.paa",
            "\RD501_Helmets\_textures\aviation\Ethan.paa",
            "",
            "\3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "\RD501_Helmets\_textures\aviation\Box\Ethan.paa",
            
        };
        hiddenSelectionsMaterials[] = 
        {
            "","\RD501_Helmets\_textures\aviation\Ethan.rvmat","","","",""
        };
    };
    class macro_new_helmet(3as_pilot,Diablo): macro_new_helmet(3as_pilot,Orange)
    {
        displayName = "[501st] AVI HELM ('Diablo')";
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\Diablo.paa",
            "\RD501_Helmets\_textures\aviation\Diablo.paa",
            "",
            "\3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "\RD501_Helmets\_textures\aviation\Box\Diablo.paa",
            
        };
    };
    class macro_new_helmet(3as_pilot,Lightning): H_HelmetB
    {
        author = "501st Aux Team";
        scope=2;
        weaponPoolAvailable=1;
        displayName="[501st] AVI HELM (Lightning)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
        hiddenSelections[] = {"Camo","Camo1","Camo2","Camo3","Camo4","Camo5"};
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\Lightning.paa",
            "\RD501_Helmets\_textures\aviation\Lightning.paa",
            "",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "\RD501_Helmets\_textures\aviation\box\Lightning.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "","\RD501_Helmets\_textures\aviation\Lightning.rvmat","","","",""
        };
        class ItemInfo: HeadgearItem
        {
            mass=40;
            uniformModel="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
            hiddenSelections[] = {"Camo","Camo1","Camo2","Camo3","Camo4","Camo5"};
            modelSides[]={3,1};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    class macro_new_helmet(3as_pilot,Knife): macro_new_helmet(3as_pilot,Lightning)
    {
        displayName="[501st] AVI HELM (Knife)";
        hiddenSelectionsTextures[] = 
        {
            "\RD501_Helmets\_textures\aviation\Knife.paa",
            "\RD501_Helmets\_textures\aviation\Knife.paa",
            "",
            "\3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa",
            "",
            "\3as\3as_characters\clones\headgear\textures\pilotp2\Phase2_Pilot_Panel_501st_co.paa"
        };
    };
    class macro_new_helmet(3as_pilot,base_p3): H_HelmetB
    {
        scope=2;
        displayName="[501st] AVI HELM 03 (Base)";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P3.p3d";
        hiddenSelections[]=
        {
            "Camo",
            "Camo1"
        };
        hiddenSelectionsTextures[]=
        {
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP3\Phase3_Pilot_Helmet_501st_co.paa",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP3\Phase3_Pilot_Tubes_501st_co.paa"
        };
        class ItemInfo: HeadgearItem
        {
            mass=40;
            uniformModel="3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P3.p3d";
            hiddenSelections[]=
            {
                "Camo",
                "Camo1"
            };
        };
    };
    //Lightning Aviation Helmet
    
    //class macro_new_helmet(lightning_pilot,linkorn): macro_new_helmet(3as_pilot,base_p3)
    //{
        //author = "501st Aux Team";
        //scope = 2;
        //displayName = "[501st] AVI HELM LIMA (Lightning)";
        //hiddenSelections[] = {"camo","emiss"};
        //hiddenSelectionsTextures[]=
        //{
            //"RD501_Helmets\_textures\aviation\AVI_lightning_helm.paa",
            //""
        //};
        //model = "\3d\data\echo_h.p3d";
        //class ItemInfo: HeadgearItem
        //{
            //mass=40;
            //uniformModel = "\3d\data\echo_h.p3d";
            //hiddenSelections[] = {"camo","emiss"};
            //class HitpointsProtectionInfo
            //{
                //class Head
                //{
                    //hitpointName="HitHead";
                    //armor=6;
                    //passThrough=0.5;
                //};
            //};
        //};
    //};
    /*class macro_new_helmet(pilot,scuba): macro_new_helmet(pilot,base)
    {
        scope = 2;
        displayName = "[501st] AVI HELM ('Scuba')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\aviation\pilots\scuba_helm.paa};
        subItems[] = {"G_B_Diving"};
        hiddenSelectionsMaterials[]=
        {
            "",
            "\RD501_Helmets\AB\data\white_glow.rvmat"
            //"\501st_Helmets\AB\data\red_glow.rvmat"//"swop_clones\data\helmpilot.rvmat"//"\501st_Helmets\AB\data\white_glow.rvmat"
        };
    };
    class macro_new_helmet(pilot,zatama): macro_new_helmet(pilot,base)
    {
        scope = 2;
        displayName = "[501st] AVI HELM ('Zatama')";
        hiddenSelectionsTextures[] = {macro_custom_helmet_textures\aviation\pilots\DragonGemHelm.paa};
        subItems[] = {"G_B_Diving"};
        hiddenSelectionsMaterials[]=
        {
            "",
            "\RD501_Helmets\AB\data\red_glow.rvmat"//"swop_clones\data\helmpilot.rvmat"//  \501st_Helmets\AB\data\white_glow.rvmat
        };
    };*/
    class macro_new_helmet(pilot,candidate): H_HelmetB
    {
        author = "501st Aux Team";
        displayName = "[501st] AVI HELM P1 ('Candidate')";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_helmet_p1_ca.paa";
        model = "SWLB_CEE\data\SWLB_P1_Pilot_Helmet.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = {"RD501_Helmets\_textures\LS\p1_pilot_helmet.paa","swlb_cee\data\swlb_p1_pilot_lifesupport_co.paa"};
        hiddenSelectionsMaterials[] = {"swlb_cee\data\swlb_p1_pilot_helmet.rvmat","swlb_cee\data\swlb_p1_pilot_lifesupport.rvmat"};
        scope = 2;
        scopeArsenal = 2;
        weaponPoolAvailable = 1;
        ace_hearing_protection = 0.85;
        ace_hearing_lowerVolume = 0;
        class ItemInfo: HeadgearItem
        {
            mass = 10;
            uniformModel = "SWLB_CEE\data\SWLB_P1_Pilot_Helmet.p3d";
            allowedSlots[] = {801,901,701,605};
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1","Camo2"};
            hiddenSelectionsTextures[] = {"RD501_Helmets\_textures\LS\p1_pilot_helmet.paa","swlb_cee\data\swlb_p1_pilot_lifesupport_co.paa"};
            hiddenSelectionsMaterials[] = {"swlb_cee\data\swlb_p1_pilot_helmet.rvmat","swlb_cee\data\swlb_p1_pilot_lifesupport.rvmat"};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.5;
                };
            };
        };
    };
    //WARDEN HELMETS//

    class ls_gar_engineer_helmet;
    class macro_new_helmet(ls_warden,501st): ls_gar_engineer_helmet
    {
        author = "501st Aux Team";
        scopeArsenal = 2;
        DisplayName="[501st] WRDN LS HELM 01";
        hiddenSelections[] = {"camo1","illum","visor"};
        hiddenSelectionsTextures[] = 
        {
            "RD501_Helmets\_textures\LS_Warden\WardenBase.paa",
            "ls_armor_bluefor\helmet\gar\engineer\data\light_co.paa",
            "ls_armor_bluefor\helmet\gar\engineer\data\visor_co.paa"
        };
        subItems[] = {"G_B_Diving"};
    };

    class macro_new_helmet(ls_warden2,501st): macro_new_helmet(ls_warden,501st)
    {
        DisplayName="[501st] WRDN LS HELM 02";
        hiddenSelectionsTextures[] = 
        {
            "RD501_Helmets\_textures\LS_Warden\Warden2.paa",
            "ls_armor_bluefor\helmet\gar\engineer\data\light_co.paa",
            "ls_armor_bluefor\helmet\gar\engineer\data\visor_co.paa"
        };
    };

    //class Aux501_LFP_RTO_Helmet: H_HelmetB
    //{
        //author = "501st Aux Team";
        //displayName = "[501st] INF RTO HELM 01";
        //picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
        //model = "\Clone_Armor_Unit\Heavy_P2_Helmet.p3d";
        //hiddenSelections[] = {"camo1","camo2"};
        //hiddenSelectionsTextures[] = {"\RD501_Helmets\_textures\LF\rto_helmet.paa","\RD501_Helmets\_textures\LF\rto_helmet.paa"};
        //hiddenSelectionsMaterials[] = {"\Clone_Armor_Unit\rvmat\Clone_Helmet_Heavy_P2.rvmat","\Clone_Armor_Unit\rvmat\Clone_Helmet_Heavy_P2.rvmat"};
        //subItems[] = {};
        //ace_hearing_lowerVolume = 0.6;
        //ace_hearing_protection = 0.85;
        //class ItemInfo: HeadgearItem
        //{
            //mass = 10;
            //uniformModel = "\Clone_Armor_Unit\Heavy_P2_Helmet.p3d";
            //hiddenSelections[] = {"camo1","camo2"};
            //allowedSlots[] = {801,901,701,605};
            //modelSides[] = {6};
            //class HitpointsProtectionInfo
            //{
                //class Head
                //{
                    //hitpointName = "HitHead";
                    //armor = 50;
                    //passThrough = 0.5;
                //};
            //};
        //};
    //};

    class macro_new_helmet(infantry,lum_base);
    class macro_new_helmet(infantry,3AS_base);
    NEW_501_Inf_Helm_JLTS(Pilot)

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////CUSTOM///HELMETS////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //AIRBORNE//
    //NEW_501_AB_HELM_JLTS(SpecialEventAB)
    //NEW_501_AB_HELM_JLTS(Mister)
    //NEW_501_AB_HELM_JLTS(Jaeger)
    //NEW_501_AB_HELM_JLTS(Zoozle)
    //NEW_501_AB_HELM_JLTS(Brandon)
    NEW_501_AB_HELM_JLTS(Scarab) 
    //NEW_501_AB_HELM_JLTS(Jericho)
    //NEW_501_AB_HELM_JLTS(Ritter)
    //NEW_501_AB_HELM_JLTS(Duck)
    NEW_501_AB_HELM_JLTS(Chroma)
    //NEW_501_AB_HELM_JLTS(Snorri)
    //NEW_501_AB_HELM_JLTS(Grinch)
    //NEW_501_AB_HELM_JLTS(Sparrow)
    NEW_501_AB_HELM_JLTS(Omen)
    //NEW_501_AB_HELM_JLTS(Nuclear)
    NEW_501_AB_HELM_JLTS(Aedan)
    //NEW_501_AB_HELM_JLTS(Waze)
    NEW_501_AB_HELM_JLTS(Spilberge)
    //NEW_501_AB_HELM_JLTS(Purge)
    //NEW_501_AB_HELM_JLTS(Galaxy)
    //NEW_501_AB_HELM_JLTS(Anubis)
    //NEW_501_AB_HELM_JLTS(Lissy)
    NEW_501_AB_HELM_JLTS(Sponge)
    //NEW_501_AB_HELM_JLTS(Saprez)
    //NEW_501_AB_HELM_JLTS(Exile)
    NEW_501_AB_HELM_JLTS(Valhalla)
    NEW_501_AB_HELM_JLTS(Stoic)
    NEW_501_AB_HELM_JLTS(Sable)
    NEW_501_AB_HELM_JLTS(Giga)
    NEW_501_AB_HELM_JLTS(Mitsu)
    NEW_501_AB_HELM_JLTS(Holliday)
    //NEW_501_AB_HELM_JLTS(Patriot)
    NEW_501_AB_HELM_JLTS(Vegas)
    NEW_501_AB_HELM_JLTS(Sidetrack)
    NEW_501_AB_HELM_JLTS(Soap)
    NEW_501_AB_HELM_JLTS(Storm)

    //ARC//
    //NEW_501_ARC_Helm_JLTS(Rayne)
    //NEW_501_ARC_Helm_JLTS(Archaic)
    NEW_501_ARC_Helm_JLTS(Duke)
    NEW_501_ARC_Helm_JLTS(Jink)
    //NEW_501_ARC_Helm_JLTS(Brennan)
    //NEW_501_ARC_Helm_JLTS(Bronson)
    //NEW_501_ARC_Helm_JLTS(Darkstar)
    //NEW_501_ARC_Helm_JLTS(Korvus)
    //NEW_501_ARC_Helm_JLTS(Virtox)
    //NEW_501_ARC_Helm_JLTS(Train)
    NEW_501_ARC_Helm_JLTS(Nightingale)
    NEW_501_ARC_Helm_JLTS(Odd)
    NEW_501_ARC_Helm_JLTS(Blade)
    //NEW_501_ARC_Helm_JLTS(Leon)
    //NEW_501_ARC_Helm_JLTS(Aedan)
    NEW_501_ARC_Helm_JLTS(Tal)
    NEW_501_ARC_Helm_JLTS(Guts)
    NEW_501_ARC_Helm_JLTS(Scruff)
    //NEW_501_ARC_Helm_JLTS(Dalton)
    //NEW_501_ARC_Helm_JLTS(Aeon)
    NEW_501_ARC_Helm_JLTS(Sandman)
    //NEW_501_ARC_Helm_JLTS(Valhalla)
    NEW_501_ARC_Helm_JLTS(Parkes)
    NEW_501_ARC_Helm_JLTS(Brad)
    NEW_501_ARC_Helm_JLTS(Bemount)
    NEW_501_ARC_Helm_JLTS(Cito)
    NEW_501_ARC_Helm_JLTS(Weapon)
    NEW_501_ARC_Helm_JLTS(Athley)
    NEW_501_ARC_Helm_JLTS(Smoke)
    NEW_501_ARC_Helm_JLTS(Rinzler)
    NEW_501_ARC_Helm_JLTS(Starpup)
    NEW_501_ARC_Helm_JLTS(Doug)
    //NEW_501_ARC_Helm_JLTS(Krios)
    NEW_501_ARC_Helm_JLTS(Deytow)
    NEW_501_ARC_Helm_JLTS(Mane)

    //AVIATION //
    NEW_3AS_Pilot_HELM(Xavier)
    //NEW_3AS_Pilot_HELM(Kushiban)
    //NEW_3AS_Pilot_HELM(Shnuffles)
    NEW_3AS_Pilot_HELM(Neb)
    NEW_3AS_Pilot_HELM(Casskun)
    //NEW_3AS_Pilot_HELM(Duncan)
    NEW_3AS_Pilot_HELM(Dylan)
    //NEW_3AS_Pilot_HELM(Ethan)
    //NEW_3AS_Pilot_HELM(Floff)
    NEW_3AS_Pilot_HELM(Shock)
    //NEW_3AS_Pilot_HELM(Jackson)
    NEW_3AS_Pilot_HELM(Jaisus)
    //NEW_3AS_Pilot_HELM(Keryl)
    //NEW_3AS_Pilot_HELM(Hobnob)
    //NEW_3AS_Pilot_HELM(Ginger)
    NEW_3AS_Pilot_HELM(Bastil)
    NEW_3AS_Pilot_HELM(Mulkot)
    NEW_3AS_Pilot_HELM(Phantom)
    //NEW_3AS_Pilot_HELM(Kenny)
    //NEW_3AS_Pilot_HELM(Diablo)
    NEW_3AS_Pilot_HELM(Bacon)
    //NEW_3AS_Pilot_HELM(Winter)
    NEW_3AS_Pilot_HELM(Saltopus)
    NEW_3AS_Pilot_HELM(Boneish)
    NEW_3AS_Pilot_HELM(Dagger)
    NEW_3AS_Pilot_HELM(Lutom)
    NEW_3AS_Pilot_HELM(Exile)

    //INFANTRY//
    //NEW_501_Inf_Helm_JLTS(SpecialEvent)
    NEW_501_Inf_Helm_JLTS(Hosed)
    NEW_501_Inf_Helm_JLTS(Roster)
    NEW_501_Inf_Helm_JLTS(Scanlon)
    NEW_501_Inf_Helm_JLTS(Simon)
    //NEW_501_Inf_Helm_JLTS(Snippy)
    //NEW_501_Inf_Helm_JLTS(Spark)
    NEW_501_Inf_Helm_JLTS(Target)
    NEW_501_Inf_Helm_JLTS(Tupiks)
    NEW_501_Inf_Helm_JLTS(Klinger)
    NEW_501_Inf_Helm_JLTS(Vengeance)
    NEW_501_Inf_Helm_JLTS(Dobby)
    //NEW_501_Inf_Helm_JLTS(Hyper)
    NEW_501_Inf_Helm_JLTS(Ranque)
    NEW_501_Inf_Helm_JLTS(Tuch)
    NEW_501_Inf_Helm_JLTS(Zeros)
    NEW_501_Inf_Helm_JLTS(Pollyon)
    //NEW_501_Inf_Helm_JLTS(Del)
    NEW_501_Inf_Helm_JLTS(Raktharg)
    NEW_501_Inf_Helm_JLTS(Buggs)
    //NEW_501_Inf_Helm_JLTS(Reck)
    //NEW_501_Inf_Helm_JLTS(Cursed)
    NEW_501_Inf_Helm_JLTS(Alistair)
    NEW_501_Inf_Helm_JLTS(Dimitri)
    NEW_501_Inf_Helm_JLTS(Money)
    NEW_501_Inf_Helm_JLTS(Tee)
    NEW_501_Inf_Helm_JLTS(Tideend)
    //NEW_501_Inf_Helm_JLTS(Muzzer)
    NEW_501_Inf_Helm_JLTS(Biscuit)
    NEW_501_Inf_Helm_JLTS(Goldarp)
    //NEW_501_Inf_Helm_JLTS(Husky)
    NEW_501_Inf_Helm_JLTS(Jay)
    //NEW_501_Inf_Helm_JLTS(Drifter)
    //NEW_501_Inf_Helm_JLTS(Weiss)
    //NEW_501_Inf_Helm_JLTS(Sam)
    //NEW_501_Inf_Helm_JLTS(Korvus)
    NEW_501_Inf_Helm_JLTS(Ripjaw)
    NEW_501_Inf_Helm_JLTS(Super)
    NEW_501_Inf_Helm_JLTS(Leon)
    //NEW_501_Inf_Helm_JLTS(Delta)
    //NEW_501_Inf_Helm_JLTS(Richard)
    //NEW_501_Inf_Helm_JLTS(Zulu)
    NEW_501_Inf_Helm_JLTS(Chan)
    //NEW_501_Inf_Helm_JLTS(Guide)
    NEW_501_Inf_Helm_JLTS(Ford)
    //NEW_501_Inf_Helm_JLTS(Courtney)
    //NEW_501_Inf_Helm_JLTS(Archon)
    //NEW_501_Inf_Helm_JLTS(Crisis)
    //NEW_501_Inf_Helm_JLTS(Deus)
    //NEW_501_Inf_Helm_JLTS(Osprey)
    NEW_501_Inf_Helm_JLTS(Clock)
    //NEW_501_Inf_Helm_JLTS(Habas)
    NEW_501_Inf_Helm_JLTS(Koda)
    //NEW_501_Inf_Helm_JLTS(Nuwisha)
    NEW_501_Inf_Helm_JLTS(Fluorite)
    NEW_501_Inf_Helm_JLTS(Slyder)
    //NEW_501_Inf_Helm_JLTS(Sticks)
    NEW_501_Inf_Helm_JLTS(Twine)
    NEW_501_Inf_Helm_JLTS(Bishop)
    NEW_501_Inf_Helm_JLTS(Shape)
    //NEW_501_Inf_Helm_JLTS(Spectre)
    NEW_501_Inf_Helm_JLTS(Andromeda)
    NEW_501_Inf_Helm_JLTS(Oktapius)
    //NEW_501_Inf_Helm_JLTS(Goddest)
    NEW_501_Inf_Helm_JLTS(Dusty)
    NEW_501_Inf_Helm_JLTS(Bjorn)
    NEW_501_Inf_Helm_JLTS(Hoodoo)
    //NEW_501_Inf_Helm_JLTS(Doug)
    //NEW_501_Inf_Helm_JLTS(Dragon)
    NEW_501_Inf_Helm_JLTS(Fixit)
    NEW_501_Inf_Helm_JLTS(Legia)
    NEW_501_Inf_Helm_JLTS(Sour)
    //NEW_501_Inf_Helm_JLTS(Adhock)
    NEW_501_Inf_Helm_JLTS(Rebellion)
    NEW_501_Inf_Helm_JLTS(Dadecoy)
    //NEW_501_Inf_Helm_JLTS(Compo)
    //NEW_501_Inf_Helm_JLTS(Hackett)
    //NEW_501_Inf_Helm_JLTS(Phoenix)
    //NEW_501_Inf_Helm_JLTS(Storm)
    //NEW_501_Inf_Helm_JLTS(Sigma)
    NEW_501_Inf_Helm_JLTS(Walsh)
    NEW_501_Inf_Helm_JLTS(Stoanes)
    NEW_501_Inf_Helm_JLTS(Cruisie)
    NEW_501_Inf_Helm_JLTS(Crebar)
    NEW_501_Inf_Helm_JLTS(Broad)
    //NEW_501_Inf_Helm_JLTS(Bacon)
    //NEW_501_Inf_Helm_JLTS(Aedan)
    NEW_501_Inf_Helm_JLTS(Midnight)
    NEW_501_Inf_Helm_JLTS(Versa)
    NEW_501_Inf_Helm_JLTS(Dakota)
    NEW_501_Inf_Helm_JLTS(Silver)
    NEW_501_Inf_Helm_JLTS(Sunshine)
    //NEW_501_Inf_Helm_JLTS(Fenrir)
    NEW_501_Inf_Helm_JLTS(Kyrie)
    //NEW_501_Inf_Helm_JLTS(Power)
    NEW_501_Inf_Helm_JLTS(Vulpes)
    //NEW_501_Inf_Helm_JLTS(Cutter)
    NEW_501_Inf_Helm_JLTS(Schames)
    NEW_501_Inf_Helm_JLTS(Bruce)
    //NEW_501_Inf_Helm_JLTS(Flooded)
    //NEW_501_Inf_Helm_JLTS(Genesis)
    NEW_501_Inf_Helm_JLTS(Cherokee)
    //NEW_501_Inf_Helm_JLTS(Bit)
    //NEW_501_Inf_Helm_JLTS(Uncle)
    NEW_501_Inf_Helm_JLTS(Shredded)
    NEW_501_Inf_Helm_JLTS(Popeye)
    NEW_501_Inf_Helm_JLTS(Sledge)
    NEW_501_Inf_Helm_JLTS(Mirror)
    NEW_501_Inf_Helm_JLTS(Waylander)
    NEW_501_Inf_Helm_JLTS(Young)
    NEW_501_Inf_Helm_JLTS(Smiley)
    NEW_501_Inf_Helm_JLTS(Knight)
    NEW_501_Inf_Helm_JLTS(Deceiving)
    //NEW_501_Inf_Helm_JLTS(Anubis)
    //NEW_501_Inf_Helm_JLTS(Weapon)
    NEW_501_Inf_Helm_JLTS(Sparticus)
    NEW_501_Inf_Helm_JLTS(Neta)
    NEW_501_Inf_Helm_JLTS(Spud)
    //NEW_501_Inf_Helm_JLTS(Araxis)
    NEW_501_Inf_Helm_JLTS(Clover)
    //NEW_501_Inf_Helm_JLTS(Salvo)
    NEW_501_Inf_Helm_JLTS(Hegener)
    //NEW_501_Inf_Helm_JLTS(Steps)
    NEW_501_Inf_Helm_JLTS(Diverge)
    //NEW_501_Inf_Helm_JLTS(Ajax)
    NEW_501_Inf_Helm_JLTS(Great)
    //NEW_501_Inf_Helm_JLTS(Scrungo)
    NEW_501_Inf_Helm_JLTS(Buffalo)
    NEW_501_Inf_Helm_JLTS(Jimmy)
    NEW_501_Inf_Helm_JLTS(Iron)
    //NEW_501_Inf_Helm_JLTS(Brotha)
    NEW_501_Inf_Helm_JLTS(September)
    NEW_501_Inf_Helm_JLTS(Asher)
    NEW_501_Inf_Helm_JLTS(Rupert)
    //NEW_501_Inf_Helm_JLTS(Juggernaut)
    //NEW_501_Inf_Helm_JLTS(Deytow)
    //NEW_501_Inf_Helm_JLTS(Sierra)
    NEW_501_Inf_Helm_JLTS(Kitti)
    NEW_501_Inf_Helm_JLTS(Defender)
    NEW_501_Inf_Helm_JLTS(Peterson)
    NEW_501_Inf_Helm_JLTS(Maelstrom)
    NEW_501_Inf_Helm_JLTS(Black)
    NEW_501_Inf_Helm_JLTS(Snoopy)
    NEW_501_Inf_Helm_JLTS(Taken)
    //NEW_501_Inf_Helm_JLTS(Radar)
    NEW_501_Inf_Helm_JLTS(Charl)
    NEW_501_Inf_Helm_JLTS(Len)
    NEW_501_Inf_Helm_JLTS(Purdy)
    //NEW_501_Inf_Helm_JLTS(Salvatore)
    NEW_501_Inf_Helm_JLTS(Wag)
    NEW_501_Inf_Helm_JLTS(Effort)
    NEW_501_Inf_Helm_JLTS(Rage)
    //NEW_501_Inf_Helm_JLTS(Cito)
    NEW_501_Inf_Helm_JLTS(Volley)
    NEW_501_Inf_Helm_JLTS(Tatum)
    NEW_501_Inf_Helm_JLTS(AJ)
    NEW_501_Inf_Helm_JLTS(Maverick)
    NEW_501_Inf_Helm_JLTS(Tim)
    NEW_501_Inf_Helm_JLTS(Anheiser)
    NEW_501_Inf_Helm_JLTS(Burrito)
    NEW_501_Inf_Helm_JLTS(Kahn)
    NEW_501_Inf_Helm_JLTS(Dredge)
    NEW_501_Inf_Helm_JLTS(Greene)
    NEW_501_Inf_Helm_JLTS(Soyvolon)
    NEW_501_Inf_Helm_JLTS(Crush)
    NEW_501_Inf_Helm_JLTS(Yokai)
    NEW_501_Inf_Helm_JLTS(Singed)
    //NEW_501_Inf_Helm_JLTS(Stew)
    NEW_501_Inf_Helm_JLTS(Beldin)
    NEW_501_Inf_Helm_JLTS(Shadow)
    NEW_501_Inf_Helm_JLTS(Blaze)
    NEW_501_Inf_Helm_JLTS(Mustang)
    NEW_501_Inf_Helm_JLTS(Eikor)
    NEW_501_Inf_Helm_JLTS(Taka)
    NEW_501_Inf_Helm_JLTS(Shatter)
    NEW_501_Inf_Helm_JLTS(Mills)
    //NEW_501_Inf_Helm_JLTS(Scard)
    NEW_501_Inf_Helm_JLTS(Wizard)
    NEW_501_Inf_Helm_JLTS(Buddy)
    //NEW_501_Inf_Helm_JLTS(Morris)
    NEW_501_Inf_Helm_JLTS(Twisted)
    NEW_501_Inf_Helm_JLTS(Heat)
    NEW_501_Inf_Helm_JLTS(Jamenson)
    //NEW_501_Inf_Helm_JLTS(Ferrum)
    NEW_501_Inf_Helm_JLTS(Sig)
    NEW_501_Inf_Helm_JLTS(Casual)
    NEW_501_Inf_Helm_JLTS(Conqueror)
    NEW_501_Inf_Helm_JLTS(Fly)
    NEW_501_Inf_Helm_JLTS(Loki)
    NEW_501_Inf_Helm_JLTS(Bigness)
    NEW_501_Inf_Helm_JLTS(Shark)
    NEW_501_Inf_Helm_JLTS(Achilles)
    NEW_501_Inf_Helm_JLTS(Bambo)
    NEW_501_Inf_Helm_JLTS(Base)
    NEW_501_Inf_Helm_JLTS(June)
    NEW_501_Inf_Helm_JLTS(Ahmanni)
    NEW_501_Inf_Helm_JLTS(Train)
    NEW_501_Inf_Helm_JLTS(Revenant)
    NEW_501_Inf_Helm_JLTS(Roe)
    NEW_501_Inf_Helm_JLTS(Krios)
    NEW_501_Inf_Helm_JLTS(Ichigo)
    NEW_501_Inf_Helm_JLTS(Warson)
    NEW_501_Inf_Helm_JLTS(Donut)
    NEW_501_Inf_Helm_JLTS(Rias)
    NEW_501_Inf_Helm_JLTS(Boozy)
    NEW_501_Inf_Helm_JLTS(Ashley)
    NEW_501_Inf_Helm_JLTS(Guardian)
    NEW_501_Inf_Helm_JLTS(Widyen)
    NEW_501_Inf_Helm_JLTS(Kenny)
    NEW_501_Inf_Helm_JLTS(Liberty)
    NEW_501_Inf_Helm_JLTS(Squeaks)

    //Medic//
    NEW_501_BARC_Helm_JLTS(Trip)
    NEW_501_BARC_Helm_JLTS(Spark)
    //NEW_501_BARC_Helm_JLTS(Greene)
    //NEW_501_BARC_Helm_JLTS(Law)
    NEW_501_BARC_Helm_JLTS(Jester)
    NEW_501_BARC_Helm_JLTS(Bark)
    //NEW_501_BARC_Helm_JLTS(Moist)
    //NEW_501_BARC_Helm_JLTS(Bigness)
    NEW_501_BARC_Helm_JLTS(Cold)

    //RTO//
    NEW_501_RTO_Helm_LS(Mark)
    NEW_501_RTO_Helm_LS(Dunpar)
    NEW_501_RTO_Helm_LS(Peter)
    NEW_501_RTO_Helm_LS(Rushmore)

    //WARDEN//
    //NEW_WARDEN_HELM(Flog,Flog,warden\Flog.paa)
    //NEW_WARDEN_HELM(Dova,Dova,warden\Dova.paa)
    //LS_WARDEN_HELM(Hobnob)

    //AUX501_WARDEN_HELM(Example)
};