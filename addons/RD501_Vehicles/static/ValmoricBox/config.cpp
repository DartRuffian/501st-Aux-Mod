#include "../../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_ValmoricBox
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
        };
        weapons[]=
        {
        };
    };
};


class CfgVehicles
{
    class Misc_thing;
    class RD501_ValmoricBox: Misc_thing
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "Valmoric's Arsenal Box";
        editorPreview = "";
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
        icon = "iconObject_5x2";
        model = "RD501_Vehicles\static\ValmoricBox\ValmoricBox.p3d";
        vehicleClass = "Misc";
    };
};