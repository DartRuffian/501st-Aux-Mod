#include "../../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_fx7_droid
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
        };
        weapons[]=
        {
        };
    };
};

class CfgVehicles
{
    class House_F;
    class RD501_FX7Droid: House_F
    {
        displayName = "FX7 Medical Droid";
        editorPreview = "";
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
        icon = "iconObject_5x2";
        model = "RD501_Vehicles\static\FX7Droid\FX7Droid.p3d";
        scope = 2;
        scopeCurator = 2;
        vehicleClass = "Misc";
    };
};