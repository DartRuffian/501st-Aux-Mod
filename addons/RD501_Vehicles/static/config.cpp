#include "../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_staticturret
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(stat,reweb),
            macro_new_vehicle(stat,ragl40),
            macro_new_vehicle(stat,Striker),
            macro_new_vehicle(stat,Railgun)
        };
        weapons[]=
        {
            macro_new_weapon(stat,eweb),
            macro_new_weapon(stat,ragl40),
            macro_new_weapon(stat,aap4),
            macro_new_weapon(stat,mar1)
        };
    };
};

class CfgVehicles
{
    class All;
    class Strategic;
    class Land;
    class LandVehicle;
    class StaticWeapon: LandVehicle
    {
        class Turrets;
        class ACE_Actions;
    };
    class StaticAAWeapon: StaticWeapon
    {
        class Turrets;
        class ACE_Actions;
    };
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
    };

    class HMG_02_base_F: StaticMGWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
    };
    class OPTRE_LAU65D_pod: StaticAAWeapon  
    {
        class Turrets: Turrets
        {   
            class MainTurret;
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
    };
        
    class RD501_stat_reweb_base: StaticMGWeapon
    {
        scope = 1;
        author = "501st Aux Team";
        displayName = "EWHB-12 'Boomer'";
        class Armory
        {
            description = "Mark I Medium Repeating Blaster Cannon";
        };
        editorSubcategory = "EdSubcat_Turrets";
        model = "ls_vehicles_turrets\mrbc\ls_turret_mrbc.p3d";
        picture = "ls_vehicles_turrets\mrbc\data\ui\ls_MRBC_mk1_editor_CA.paa";
        UiPicture = "ls_vehicles_turrets\mrbc\data\ui\ls_MRBC_mk1_editor_CA.paa";
        cost = 150000;
        getInAction = "";
        getOutAction = "";
        armor = 30;
        explosionShielding = 10;
        class Damage
        {
            tex[] = {};
            mat[] = 
            {
                "A3\Static_F_Gamma\data\StaticTurret_01.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_destruct.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_03.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_03_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_03_destruct.rvmat"
            };
        };
        minTotalDamageThreshold = 0.5;
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                selectionFireAnim = "zasleh";
                optics = 1;
                discreteDistance[] = {100,200,300,400,600,800,1000,1200,1500,1600,1700,1800,1900,2000};
                discreteDistanceInitIndex = 2;
                turetInfoType = "RscOptics_crows";
                gunnerOpticsModel = "\a3\weapons_f_gamma\reticle\HMG_01_Optics_Gunner_F";
                minElev = -15;
                maxElev = 25;
                minTurn = -180;
                maxTurn = 180;
                weapons[] = {"Aux501_Weaps_EWHB12"};
                magazines[] = 
                {
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG"
                };
                gunnerAction = "ls_mrbc_Gunner";
                memoryPointGunnerOptics = "gunnerview";
                gunnerLeftHandAnimName = "gun";
                gunnerRightHandAnimName = "gun";
                gunnerLeftLegAnimName = "";
                gunnerRightLegAnimName = "";
                gunnerInAction = "";
                gunnerGetOutAction = "";
                GunnerName = "Gunner";
                gunnerForceOptics = 0;
                disableSountAttenuation = 1;
                ejectDeadGunner = 1;
                memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";
                class ViewOptics
                {
                    initAngleX = 0;
                    minAngleX = -30;
                    maxAngleX = 30;
                    initAngleY = 0;
                    minAngleY = -100;
                    maxAngleY = 100;
                    initFov = 0.5;
                    minFov = 0.25;
                    maxFov = 1.25;
                    visionMode[] = {"Normal","NVG"};
                    thermalMode[] = {0,1};
                };
            };
        };
        class AnimationSources
        {
            class Revolving
            {
                source = "revolving";
                weapon = "Aux501_Weaps_EWHB12";
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "ace interact";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_EWHB12";
            magazineLocation = "_target selectionPosition 'konec hlavne'";
            disassembleWeapon = "Aux501_Weaps_EWHB12_carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 500;
        };
        soundGetOut[] = {"A3\sounds_f\dummysound",0.0009999999,1,5};
        soundGetIn[] = {"A3\sounds_f\dummysound",0.00031622773,1,5};
    };

    class RD501_stat_reweb: RD501_stat_reweb_base
    {
        editorPreview = "\3as\3as_static\images\3AS_HeavyRepeater_Unarmoured.jpg";
        _generalMacro = "B_HMG_01_F";
        scope = 2;
        side = 1;
        faction = "RD501_republic_Faction";
        crew = "JLTS_Clone_P2_DC15S_501";
        editorSubcategory = "RD501_Editor_Category_turrets";
        vehicleClass = "RD501_Vehicle_Class_turrets";
    };
    class RD501_stat_Striker: OPTRE_LAU65D_pod
    {
        armor = 150;
        scope = 2;
        author = "501st Aux Mod";
        displayName = "AAP4 'Striker'";
        faction = "RD501_republic_Faction";
        editorSubcategory = "RD501_Editor_Category_turrets";
        vehicleClass = "RD501_Vehicle_Class_turrets";
        model = "\OPTRE_Weapons\static\LAU65D\LAU65D_pod.p3d";
        ace_cargo_size = 1;
        crew = "JLTS_Clone_P2_DC15S_501";
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                animationSourceBody = "Turret_rot";
                animationSourceCamElev = "camElev";
                animationSourceGun = "camElev";
                memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";
                body = "Turret_rot";
                discreteDistanceCameraPoint[] = {"eye"};
                stabilizedInAxes = "StabilizedInAxesNone";
                gunnerAction = "Gunner_OPTRE_Lau";
                maxTurn = 360;
                weapons[] = {"Aux501_Weaps_AAP4"};
                magazines[] = 
                {
                    "Aux501_Weapons_Mags_AAP4",
                    "Aux501_Weapons_Mags_AAP4",
                    "Aux501_Weapons_Mags_AAP4",
                    "Aux501_Weapons_Mags_AAP4"
                };
                turretInfoType = "ACE_RscOptics_javelin";
                gunnerOpticsColor[] = {0,0,0,1};
                gunnerOpticsEffect[] = {};
                gunnerOpticsModel = "\z\ace\addons\javelin\data\reticle_titan.p3d";
                gunnerOpticsShowCursor = 0;
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "osaveze";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_AAP4";
            magazineLocation = "_target selectionPosition 'konec rakety'";
            disassembleWeapon = "Aux501_Weaps_AAP4_carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 6;
        };
    };

    class RD501_stat_Railgun: StaticMGWeapon
    {
        scope = 2;
        scopeCurator = 2;
        author = "501st Aux Mod";
        displayName = "MAR1 'Driver'";
        model = "\OPTRE_Weapons\static\StaticTurret\Static_Gauss.p3d";
        picture = "\OPTRE_Weapons\static\LAU65D\data\UI\LAU65D_ca.paa";
        icon = "\OPTRE_Weapons\static\LAU65D\data\UI\map_LAU65D_ca.paa";
        armor = 150;
        side = 1;
        ace_cargo_size = 1;
        faction = "RD501_republic_Faction";
        editorSubcategory = "RD501_Editor_Category_turrets";
        vehicleClass = "RD501_Vehicle_Class_turrets";
        mapSize = 4;
        nameSound = "veh_static_MG_s";
        radarType = 2;
        crew = "JLTS_Clone_P2_DC15S_501";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"OPTRE_Vehicles\Warthog\data\warthog_addons_co.paa"};
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                selectionFireAnim = "zasleh";
                body = "mainturret";
                gun = "maingun";
                animationsourcebody = "mainturret";
                animationSourceGun = "maingun";
                gunAxis = "Osa Hlavne";
                gunBeg = "Usti hlavne";
                gunEnd = "konec hlavne";
                minElev = -15;
                maxElev = 45;
                minTurn = -360;
                maxTurn = 360;
                initTurn = 0;
                turretAxis = "OsaVeze";
                maxHorizontalRotSpeed = 1.75;
                maxVerticalRotSpeed = 1.5;
                gunnerAction = "OPTRE_Static_Gaus_Gunner";
                gunnerInAction = "";
                gunnerGetInAction = "GetInMRAP_01";
                gunnerGetOutAction = "GetOutMRAP_01";
                gunnerName = "Gunner";
                ejectDeadGunner = 1;
                hideWeaponsGunner = 0;
                stabilizedInAxes = 3;
                soundServo[] = {"",0.01,1};
                outGunnerMayFire = 1;
                inGunnerMayFire = 1;
                commanding = 1;
                primaryGunner = 1;
                memoryPointsGetInGunner = "Pos Gunner";
                memoryPointsGetInGunnerDir = "Pos Gunner dir";
                gunnerLeftHandAnimName = "trigger";
                gunnerRightHandAnimName = "trigger";
                memoryPointGun = "machinegun";
                weapons[] = {"Aux501_Weaps_MAR1"};
                magazines[] = 
                {
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1"
                };
                memoryPointGunnerOptics = "gunnerview";
                memoryPointGunneroutOptics = "gunneroutview";
                gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_MBT_01_w_F.p3d";
                gunnerOpticsShowCursor = 1;
                turretInfoType = "RscWeaponZeroing";
                castGunnerShadow = 1;
                startEngine = 0;
                enableManualFire = 0;
                gunnerForceOptics = 1;
                class Viewoptics
                {
                    initAngleX = 0;
                    initAngleY = 0;
                    initFov = 0.75;
                    maxAngleX = 30;
                    maxAngleY = 100;
                    maxFov = 1.1;
                    maxMoveX = 0;
                    maxMoveY = 0;
                    maxMoveZ = 0;
                    minAngleX = -30;
                    minAngleY = -100;
                    minFov = 0.0125;
                    minMoveX = 0;
                    minMoveY = 0;
                    minMoveZ = 0;
                    opticsZoomInit = 0.75;
                    opticsZoomMax = 0.75;
                    opticsZoomMin = 0.25;
                    thermalMode[] = {5,6};
                    visionMode[] = {"Normal","NVG","Ti"};
                };
                class ViewGunner
                {
                    initAngleX = 0;
                    minAngleX = -45;
                    maxAngleX = 75;
                    initAngleY = 0;
                    minAngleY = -120;
                    maxAngleY = 120;
                    initFov = 0.4;
                    minFov = 0.4;
                    maxFov = 0.4;
                };
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "osaveze";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_MAR1";
            magazineLocation = "_target selectionPosition 'machinegun'";
            disassembleWeapon = "Aux501_Weaps_MAR1_carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 10;
        };
    };
};
class CfgMagazines
{
    class 40Rnd_20mm_G_belt;
    class macro_new_mag(ragl40he,40): 40Rnd_20mm_G_belt
    {
        scope = 1;
        scopeArsenal = 1;
        type = 256;
        picture = "\RD501_Vehicles\static\data\rd501_icon_mag_staticgl.paa";
        muzzleImpulseFactor[] = {0,0};
        displayName = "[501st] Sabre GL";
        model = "\A3\Structures_F_EPB\Items\Military\Ammobox_rounds_F.p3d";
        count = 40;
        ammo = "Aux501_Weapons_Ammo_GL_HE";
        weaponpoolavailable = 1;
        mass = 95;	
    };
};