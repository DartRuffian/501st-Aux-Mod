class RD501_Texture_Selector 
{
	// -------------------------------------------
	// ------------------ Hulls ------------------
	// -------------------------------------------

	class RD501_Hull_Ackbar 
	{
		// The title of this option.
		title="Ackbar";
		// The submenu this option will show up in.
		submenu="Hulls";
		// For a LAAT: {"Hull.paa", "Wings.paa", "Weapons.paa", "WeaponDetails.paa", "Interior.paa"};
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_ackbar.paa", "", "", "", ""};
		// This texture set will be applied if the regex matches. Leave blank to skip this feature.
		autoApplyNameRegex="";
		// This texture is limited to names that match this regex. Leave blank to make it avalible to all.
		restrictNameRegex="";
	};

	class RD501_Hull_Bird 
	{
		title="Bird";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_bird.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Pilot_Black 
	{
		title="Pilot Black";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_bpilot.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_CBomb 
	{
		title="Clone Bomb";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_cbomb.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Crosshair 
	{
		title="Crosshair";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_Crosshair.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Death 
	{
		title="Death";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_Death.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Hammer 
	{
		title="Hammer";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_Hammer.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Marks 
	{
		title="Marks";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_Marks.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_NBomb 
	{
		title="Nexu Bomber";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nbomb.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Scythe 
	{
		title="Scythe";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_Scythe.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Stache 
	{
		title="Stache";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_stache.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Tantaun 
	{
		title="Tantaun";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_tantaun.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Teeth 
	{
		title="Teeth";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_teeth.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Twilek 
	{
		title="Twilek";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_twilek.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Wampa 
	{
		title="Wampa";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_wampa.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Wookie 
	{
		title="Wookie";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_Wookie.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Pilot_White 
	{
		title="Pilot White";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_wpilot.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Splinter 
	{
		title="Splinter Hull";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_splinter.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};
	
	class RD501_Hull_AvExp 
	{
		title="Avalanche Express Hull";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_AvExp.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Hull_Winner 
	{
		title="Winner Hull";
		submenu="Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_winner_hull.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	// -------------------------------------------
	// ---------------- NCO Hulls ----------------
	// -------------------------------------------

	class RD501_Hull_NCO_Ackbar 
	{
		title="NCO Ackbar";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Ackbar.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	/*class RD501_Hull_NCO_Battlebird 
	{
		title="Battlebird";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Battlebird.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_bird 
	{
		title="NCO Bird";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Bird.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};*/

	class RD501_Hull_NCO_Bpilot 
	{
		title="NCO Pilot Black";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Bpilot.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Clone_Bomb 
	{
		title="NCO Clone Bomb";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_CBomb.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Crosshair 
	{
		title="NCO Crosshair";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Crosshair.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Death 
	{
		title="NCO Death";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Death.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Hammer 
	{
		title="NCO Hammer";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Hammer.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Marks 
	{
		title="NCO Marks";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Marks.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Nexu_Bomb 
	{
		title="NCO Nexu Bomb";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_NBomb.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Scythe 
	{
		title="NCO Scythe";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Scythe.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Stache 
	{
		title="NCO Stache";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Stache.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Tally 
	{
		title="NCO Tally";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Tally.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Tantaun 
	{
		title="NCO Tantaun";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Tantaun.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Teeth 
	{
		title="NCO Teeth";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Teeth.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Twilek 
	{
		title="NCO Twilek";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Twilek.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Wampa 
	{
		title="NCO Wampa";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Wampa.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Wookie 
	{
		title="NCO Wookie";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_Wookie.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_Pilot_White 
	{
		title="NCO Pilot White";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_wpilot.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	class RD501_Hull_NCO_AvExp 
	{
		title="NCO Avalanche Express";
		submenu="NCO Hulls";
		decals[]={"RD501_Vehicles\textures\LAAT\Hulls\LAATi_hull_nco_AvExp.paa", "", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="^CX-*[ PS].*";
	};

	// -------------------------------------------
	// ------------------ Wings ------------------
	// -------------------------------------------

	class RD501_Wings_Blank 
	{
		title="Blank Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_blank.paa", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Wings_Base501 
	{
		title="501st Base Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_base.paa", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Wings_CX 
	{
		title="CX Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_cx.paa", "", "", ""};
		autoApplyNameRegex="^CX .*";
		restrictNameRegex="^CX .*";
	};

	class RD501_Wings_CXC 
	{
		title="CX-C Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_cxc.paa", "", "", ""};
		autoApplyNameRegex="^CX-C.*";
		restrictNameRegex="^CX-C.*";
	};

	class RD501_Wings_CXP 
	{
		title="CX-P Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_cxp.paa", "", "", ""};
		autoApplyNameRegex="^CX-P.*";
		restrictNameRegex="^CX-P.*";
	};

	class RD501_Wings_CXS 
	{
		title="CX-S Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_cxs.paa", "", "", ""};
		autoApplyNameRegex="^CX-S.*";
		restrictNameRegex="^CX-S.*";
	};

	class RD501_Wings_CXX 
	{
		title="CX-X Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_cxx.paa", "", "", ""};
		autoApplyNameRegex="^CX-X.*";
		restrictNameRegex="^CX-X.*";
	};

	class RD501_Wings_Winner 
	{
		title="Winner Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_winner_wings.paa", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};

	class RD501_Wings_Splinter 
	{
		title="Splinter Wings";
		submenu="Wings";
		decals[]={"", "RD501_Vehicles\textures\LAAT\Wings\LAATi_wings_splinter.paa", "", "", ""};
		autoApplyNameRegex="";
		restrictNameRegex="";
	};
};