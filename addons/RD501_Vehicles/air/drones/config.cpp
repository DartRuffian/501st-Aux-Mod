//Get this addons macro

//get the macro for the air subaddon

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon drones
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_drone_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_drones
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{			
			"A3_data_F",
			"A3_anims_F",
			"A3_weapons_F",
			"A3_characters_F",
			"A3_Air_F_Exp",
			"RD501_patch_vehicle_weapons",
			"RD501_patch_vehicle_mags"
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(drone,cis_Hover_Droid),
			macro_new_vehicle(drone,razor_medical),
			macro_new_vehicle(drone,razor_ammo),
			macro_new_vehicle(drone,razor_turret)
		};
		weapons[]={};
	};
};


#include "../../common/sensor_templates.hpp"
class CfgVehicles
{
	class assembleInfo;
	class JLTS_UAV_prowler_gar;
	class B_UAV_06_F;

	class 3as_CIS_ScavDroid;
	class macro_new_vehicle(drone,cis_Hover_Droid): 3as_CIS_ScavDroid
	{
		scope=2;
		side=0;
		forceInGarage = 1;
		displayName = "CIS Hover Droid MK.II";
		armor = 0.1;
		cost = 20000;
		altFullForce = 1000;
		altNoForce = 1100;
		LODTurnedIn = -1;
		faction = macro_faction(CIS);
		editorSubcategory="EdSubcat_Drones";
		vehicleClass="Autonomous";

	};

	#define ARR_2(a,b) a,b
	class macro_new_vehicle(drone,razor_medical): B_UAV_06_F
	{
		displayName = "Prime Medical Drone";
		faction = MACRO_QUOTE(macro_faction(republic));
		scope=2;
		forceInGarage = 1;
		altFullForce = 1000;
		altNoForce = 1100;
		threat[] = {.05, .05, .05};
		cost= 1000;
		hiddenSelectionsTextures[]=
		{
			"\RD501_Vehicles\air\drones\data\b_uav_06_medical_co.paa"
		};
		typicalCargo[]=
		{
			""
		};
		class TransportItems
		{
			#include "../../common/common_items_medical_drone.hpp"				
		};
        
	};
	class macro_new_vehicle(drone,razor_ammo): B_UAV_06_F
	{
		displayName = "Prime Ammo Drone";
		faction = MACRO_QUOTE(macro_faction(republic));
		scope=2;
		forceInGarage = 1;
		altFullForce = 1000;
		altNoForce = 1100;
		threat[] = {.05, .05, .05};
		cost= 1000;
		hiddenSelectionsTextures[]=
		{
			"\RD501_Vehicles\air\drones\data\b_uav_06_ammo_co.paa"
		};
		typicalCargo[]=
		{
			""
		};
		class TransportMagazines
		{
			#include "../../common/common_ammo_mag_drone.hpp"
		};
		class TransportItems
		{
			#include "../../common/common_items_pod.hpp"
		};
		
	};
	class macro_new_vehicle(drone,razor_turret): B_UAV_06_F
	{
		displayName = "Prime Turret Drone";
		faction = MACRO_QUOTE(macro_faction(republic));
		scope=2;
		forceInGarage = 1;
		altFullForce = 1000;
		altNoForce = 1100;
		threat[] = {.05, .05, .05};
		cost= 1000;
		hiddenSelectionsTextures[]=
		{
			"\RD501_Vehicles\air\drones\data\b_uav_06_ammo_co.paa"
		};
		typicalCargo[]=
		{
			""
		};
		class TransportItems{};
		class TransportMagazines
		{
			//eweb ammo
			class _xx_Aux501_Weapons_Mags_EWHB12_MG_csw
			{
				magazine = "Aux501_Weapons_Mags_EWHB12_MG_csw";
				count = 10;
			};
			//boomer ammo
			class _xx_Aux501_Weapons_Mags_EWHB12_GL_csw
			{
				magazine = "Aux501_Weapons_Mags_EWHB12_GL_csw";
				count = 10;
			};
			//striker ammo
			class _xx_Aux501_Weapons_Mags_AAP4_csw
			{
				magazine = "Aux501_Weapons_Mags_AAP4_csw";
				count = 10;
			};
			//driver ammo
			class _xx_Aux501_Weapons_Mags_mar1_csw
			{
				magazine = "Aux501_Weapons_Mags_mar1_csw";
				count = 10;
			};
			//Mortar ammo
			class _xx_Aux501_Weapons_Mags_81mm_mortar_HE_csw
			{
				magazine = "Aux501_Weapons_Mags_81mm_mortar_HE_csw";
				count = 10;
			};
			class _xx_Aux501_Weapons_Mags_81mm_mortar_smoke_csw
			{
				magazine = "Aux501_Weapons_Mags_81mm_mortar_smoke_csw";
				count = 10;
			};
			class _xx_Aux501_Weapons_Mags_81mm_mortar_illum_csw
			{
				magazine = "Aux501_Weapons_Mags_81mm_mortar_illum_csw";
				count = 10;
			};
			class _xx_Aux501_Weapons_Mags_81mm_mortar_guided_HE_csw
			{
				magazine = "Aux501_Weapons_Mags_81mm_mortar_guided_HE_csw";
				count = 10;
			};
			class _xx_Aux501_Weapons_Mags_81mm_mortar_laserguided_HE_csw
			{
				magazine = "Aux501_Weapons_Mags_81mm_mortar_laserguided_HE_csw";
				count = 10;
			};
		};
		class TransportWeapons
        {
            class _xx_Aux501_Weaps_EWHB12_carry
            {
                weapon = "Aux501_Weaps_EWHB12_carry";
                count = 1;
            };
			class _xx_Aux501_Weaps_AAP4_carry
            {
                weapon = "Aux501_Weaps_AAP4_carry";
                count = 1;
            };
			class _xx_Aux501_Weaps_MAR1_carry
            {
                weapon = "Aux501_Weaps_MAR1_carry";
                count = 1;
            };
			class _xx_Aux501_Weaps_81mm_plasma_Mortar_Carry
            {
                weapon = "Aux501_Weaps_81mm_plasma_Mortar_Carry";
                count = 1;
            };
		};
	};
};