//Get this addons macro

//get the macro for the air subaddon

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon HMP
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_hmp_class(name) vehicle_classname##_##name

class CBA_Extended_EventHandlers_base;

class CfgPatches
{
	class RD501_patch_HMP
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(HMP,gunship),
			macro_new_vehicle(HMP,transport)
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"
class Eventhandlers;
class CfgVehicles
{
	class 442_test_hmp_base;
	class 442_hmp: 442_test_hmp_base
	{
		class components;
	};

	class 442_hmp_transport;
	class macro_new_vehicle(HMP,gunship):442_hmp
	{
		scope=2;
		scopeCurator=2;
		forceInGarage=1;
		displayName = "H.M.P Gunship Mk.I";
		armor = 650;
		irTarget = 1;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 1;
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		altFullForce = 16000;
		altNoForce = 19000;

		class Eventhandlers: Eventhandlers
		{
			init="(_this#0) confirmSensorTarget [west,true]";
		};

		crew=MACRO_QUOTE(macro_new_unit_class(opfor,B1_pilot));

		#include "common.hpp"

		weapons[] = {
			"RD501_Aircraft_Laser_Cannon",
			"RD501_CMFlareLauncher"
		};
		magazines[] = {
			"RD501_CIS_Aircraft_Laser_Cannon_Mag_100",
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200"

		};
		class components: components
		{
			class TransportPylonsComponent
			{
				class Pylons
				{
					delete PylonLeft1;
					delete PylonRight1;
				};

			};
		};

	};

	class macro_new_vehicle(HMP,transport): 442_hmp_transport//swop_HMP_droidgunship_transport
	{

		displayName = "H.M.P Transport Mk.I";
		forceInGarage = 1;
		maxSpeed=220;
		armor = 650;
		armorStructural = 2;
		armorLights = 1;
		liftForceCoef = 1;
		bodyFrictionCoef = 0.6777;
		scope=2;
		scopeCurator=2;
		altFullForce = 16000;
		altNoForce = 19000;
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		weapons[] = {
			"RD501_Aircraft_Laser_Cannon",
			"CMFlareLauncher"
		};
		magazines[] = {
			"RD501_CIS_Aircraft_Laser_Cannon_Mag_100",
			"300Rnd_CMFlare_Chaff_Magazine"

		};

		class Eventhandlers: Eventhandlers
		{
			init="(_this#0) confirmSensorTarget [west,true]";
		};

		RD501_magclamp_small_1[] = {-0.3,0.0,-2.5};

		crew=MACRO_QUOTE(macro_new_unit_class(opfor,B1_pilot));

		#include "common.hpp"

	};
};