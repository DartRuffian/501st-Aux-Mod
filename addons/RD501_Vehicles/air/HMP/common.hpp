faction = MACRO_QUOTE(macro_faction(CIS));
editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(CIS_heli));
vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(CIS));
airBrakeFrictionCoef = 80.4;
