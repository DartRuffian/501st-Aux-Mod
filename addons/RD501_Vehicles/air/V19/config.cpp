#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_V19
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(v19,MKI)
		};
		weapons[]=
		{
			
		};
	};
};
#include "../../common/sensor_templates.hpp"
class CfgVehicles
{
	class Plane_Fighter_03_base_F;
	class 3as_V19_base : Plane_Fighter_03_base_F
	{
		class Components;
	};

	class macro_new_vehicle(v19,MKI):3as_V19_base
	{
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		side=1;
		scope=2;
		scopeCurator=2;
		displayName = "Republic V19 Torrent";
		forceInGarage = 1;
		//dmg stuff
		armor=200;
		armorStructural = 1;
		vtol = 4;
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_vtol));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		ballisticsComputer = 1+8; //this should go in weaponss not vehicle?
		//visualTarget = 1; 
		//visualTargetSize = 1;
		reportOwnPosition = true;
		receiveRemoteTargets = true;
		reportRemoteTargets = true;
		radarTargetSize = 0.9;
		radarTarget = 1;
		laserScanner = 1;
		irTarget = 1;
		irTargetSize = 1;
		countermeasureActivationRadius = 2000;
		incomingMissileDetectionSystem = 16;
		threat[] = {.1, .4, .9};
		cost= 9000;
		altFullForce = 16000;
		altNoForce = 19000;
		
		RD501_magclamp_large_offset[] = {0.0, 0.0, -6.5};
		RD501_magclamp_small_forbidden = 1;

		//flight model
		acceleration=500;
		maxSpeed=1500;
		stallSpeed=5;
		draconicTorqueYCoef[]={1.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		draconicForceYCoef=6;
		envelope[] = {0.1,0.2,0.3,1.5,1.72,2.69,3.87,5.27,6.89,8.72,9.7,9.6,9.2,8.5,8.2,8};
		thrustCoef[] = {3.0,3.0,3.2,3.6,4,5,6,7,9,4,3.4,3,2.2,2,2,2};
		elevatorControlsSensitivityCoef = 2.5;
        aileronControlsSensitivityCoef = 2.3;
        rudderControlsSensitivityCoef = 4;

		class EventHandlers{};

		//#include "../../common/universal_mfd.hpp"
		/*class ViewPilot: ViewPilot
		{
			initAngleX = 0;
		};*/
		weapons[]=
		{
			"RD501_CMFlareLauncher",
			"RD501_Republic_Aircraft_Missile_AA_IR",
			"RD501_Republic_Aircraft_Laser_AA",
		};
		magazines[]=
		{
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"RD501_Republic_Aircraft_Missile_AA_IR_Mag_12",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
			"RD501_Republic_Aircraft_Laser_AA_Mag_600",
		};
		class ACE_SelfActions
		{
			class ACE_Passengers
			{
				condition = "alive _target";
				displayName = "Passengers";
				insertChildren = "_this call ace_interaction_fnc_addPassengersActions";
				statement = "";
			};
			#include "../../common/universal_hud_color_changer.hpp"
		};
		class pilotCamera
		{
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName="WFOV";
					initAngleX=0;
					minAngleX=-10;
					maxAngleX=90;
					initAngleY=0;
					minAngleY=-90;
					maxAngleY=90;
					initFov=0.425;//"(30 / 120)";
					minFov=0.425;//"(30 / 120)";
					maxFov=0.425;//"(30 / 120)";
					directionStabilized=1;
					thermalMode[] = {0,1,2,3,4,5};
					visionMode[]=
					{
						"Normal",
						"NVG",
						"Ti"
					};
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_wide_F.p3d";
					opticsPPEffects[]=
					{
						"OpticsCHAbera2",
						"OpticsBlur2"
					};
				};
				class zoomx4: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.425/4)";//"(3.75 / 120)";
					minFov="(0.425/4)";//"(3.75 / 120)";
					maxFov="(0.425/4)";//"(3.75 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX8: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/8)";//"(.375 / 120)";
					minFov="(0.42/8)";//"(.375 / 120)";
					maxFov="(0.42/8)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX20: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/20)";//"(.375 / 120)";
					minFov="(0.42/20)";//"(.375 / 120)";
					maxFov="(0.42/20)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX50: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/50)";//"(.375 / 120)";
					minFov="(0.42/50)";//"(.375 / 120)";
					maxFov="(0.42/50)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX70: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/70)";//"(.375 / 120)";
					minFov="(0.42/70)";//"(.375 / 120)";
					maxFov="(0.42/70)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				showMiniMapInOptics=1;
				showUAVViewInOptics=0;
				showSlingLoadManagerInOptics=1;
			};
			minTurn=-180;
			maxTurn=180;

			initTurn=0;
			minElev=-10;
			maxElev=90;
			initElev=-10;
			maxXRotSpeed=0.30000001;
			maxYRotSpeed=0.30000001;
			pilotOpticsShowCursor=1;
			controllable=1;
		};
		class Sounds
		{
			class EngineLowOut
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\Plane_Fighter_03_low_ext","db8",0.5,1200};
				frequency = "1.0 min (rpm + 0.5)";
				volume = "camPos*2*(rpm factor[0.95, 0])*(rpm factor[0, 0.95])";
			};
			class EngineHighOut
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\Plane_Fighter_03_engi_ext","db8",0.6,1400};
				frequency = "1";
				volume = "camPos*4*(rpm factor[0.5, 1.1])*(rpm factor[1.1, 0.5])";
			};
			class ForsageOut
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\Plane_Fighter_03-fors_ext","db5",0.5,1700};
				frequency = "1";
				volume = "engineOn*camPos*(thrust factor[0.6, 1.0])";
				cone[] = {3.14,3.92,2.0,0.5};
			};
			class WindNoiseOut
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\noise","db-5",0.5,150};
				frequency = "(0.1+(1.2*(speed factor[1, 150])))";
				volume = "camPos*(speed factor[1, 150])";
			};
			class EngineLowIn
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\Plane_Fighter_03_low_int","db0",0.5};
				frequency = "1.0 min (rpm + 0.5)";
				volume = "(1-camPos)*((rpm factor[0.7, 0.1])*(rpm factor[0.1, 0.7]))";
			};
			class EngineHighIn
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\Plane_Fighter_03_engi_int","db0",0.6};
				frequency = "1";
				volume = "(1-camPos)*(rpm factor[0.85, 1.0])";
			};
			class ForsageIn
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\Plane_Fighter_03-fors_int","db0",0.5};
				frequency = "1";
				volume = "(1-camPos)*(engineOn*(thrust factor[0.6, 1.0]))";
			};
			class WindNoiseIn
			{
				sound[] = {"A3\Sounds_F\air\Plane_Fighter_03\noise","db-6",0.5};
				frequency = "(0.1+(1.2*(speed factor[1, 150])))";
				volume = "(1-camPos)*(speed factor[1, 150])";
			};
		};
		class Components : Components
		{

			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000};
						resource = "RscCustomInfoSensors";
					};
				};
			};

			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=0;
							maxRange=3000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=3000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						componentType = "IRSensorComponent";

						typeRecognitionDistance = 2500;
						angleRangeHorizontal = 40;
						angleRangeVertical = 40;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,0,0,1};
					};

					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
					{
						class AirTarget
						{
							minRange=0;
							maxRange=4000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=4000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "ActiveRadarSensorComponent";

						typeRecognitionDistance = 3500;
						angleRangeHorizontal = 160;
						angleRangeVertical = 10;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={0,1,1,1};
					};

					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 1000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";

						typeRecognitionDistance = 750;
						angleRangeHorizontal = 90;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						color[]={1,1,0.5,0.80000001};
					};

					class LaserSensorComponent: SensorTemplateLaser
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="LaserSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};
				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};	
				};
			
			};

		};


	};
};
	