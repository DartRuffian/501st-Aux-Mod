//Get this addons macro

//get the macro for the air subaddon

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon Y_Wing
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_y_wing_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_Y_Wing
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			"RD501_patch_vehicles",
			"3as_arc_170"
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_YWing"
		};
		weapons[]=
		{
			
		};
	};
};

#include "../../common/sensor_templates.hpp"
class CfgVehicles
{
	class thingX;
	class Motorcycle;
	class Air;
	class Plane: Air
	{
		class NewTurret;
		class ViewPilot;
		class HitPoints
		{
			class HitHull;
		};
	};
	class Plane_Base_F: Plane
	{
		class AnimationSources;
		class Components;
	};
	class Plane_Fighter_03_base_F: Plane_Base_F
	{
		class Turrets
		{
			class MainTurret;
		};
	};
	class Plane_Fighter_03_dynamicLoadout_base_F: Plane_Fighter_03_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: Mainturret{};
		};
		class Components: Components
		{
			class TransportPylonsComponent
			{
				class Pylons;
				class Presets;
			};
		};
		class MarkerLights;
	};
	class 3as_arc_170_base : Plane_Fighter_03_dynamicLoadout_base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: Mainturret{};
		};
		class Components: Components
		{
			class TransportPylonsComponent
			{
				class Pylons;
				class Presets;
			};
		};
		class MarkerLights;
	};
	class 3as_arc_170_blue : 3as_arc_170_base
	{
		class Turrets: Turrets
		{
			class MainTurret: Mainturret{};
		};
		class Components: Components
		{
			class TransportPylonsComponent
			{
				class Pylons;
				class Presets;
			};
		};
		class MarkerLights;
	};
	class 3as_arc_170_yellow : 3as_arc_170_blue
	{
		class Turrets: Turrets
		{
			class MainTurret: Mainturret{};
		};
		class Components: Components
		{
			class TransportPylonsComponent
			{
				class Pylons;
				class Presets;
			};
		};
		class MarkerLights;
	};
	class RD501_YWing : 3as_arc_170_yellow
	{
		
		soundSetSonicBoom[] = {"Plane_Fighter_SonicBoom_SoundSet"};
		scope = 2;
		scopeCurator = 2;
		irTarget = 1;
		irTargetSize = 1;
		radarTarget = 1;
		radarTargetSize = 1.1;
		displayName = "Y-Wing Mk.X";

		//flight model
		altFullForce = 18000;
		altNoForce = 21000;
		draconicTorqueYCoef[]={0,1.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		draconicForceYCoef=6;
		maxSpeed=800;
		thrustCoef[] = {2.0,2.0,2.2,2.4,2.7,2.7,3,3.2,3.5,3.2,3.1,2.5,2.3,2.0,1.7,1.5,1.5};
		envelope[] = {0.1,0.2,0.3,1.5,2,2.69,3.87,5.27,6.89,8.72,9.7,9.6,9.2,8.5,8.2,8};
		aileronCoef[] = {.5,.6,.7,.7,.6,.5,0.4};
		rudderCoef[] = 	{0.75,0.975,1.1,1.1,0.9,.6,.45};
		elevatorCoef[] = {0.35,0.4,0.45,.5,.55,.4,.3,.325,.31,.3,.26,.275,.25};

		elevatorSensitivity = 1.1;
		elevatorControlsSensitivityCoef = 1.5;
		aileronSensitivity = 2.2;
		aileronControlsSensitivityCoef = 3;
		rudderInfluence = 0.9;
		rudderControlsSensitivityCoef = 2;


		weapons[] = 
		{
			"RD501_CMFlareLauncher",
			"SmokeLauncherMK2",
			"RD501_Republic_Aircraft_Laser_Repeater"
		};
		magazines[] = 
		{
			"RD501_CMFlare_Magazine_200",
			"RD501_CMFlare_Magazine_200",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"SmokeLauncherMagMK2",
			"RD501_Republic_Aircraft_Laser_Repeater_Mag_1000",
			"RD501_Republic_Aircraft_Laser_Repeater_Mag_1000"
		};
		receiveRemoteTargets = true;
		reportRemoteTargets = true;
		reportOwnPosition = true;
		threat[] = {.4, .8, .9};
		cost= 15000;
		faction = MACRO_QUOTE(macro_faction(republic));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat_air(Republic_vtol));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type_air(Republic));
		RD501_magclamp_small_forbidden=1;
		RD501_magclamp_large_offset[]={0.0,0.0,-3.0};
		class pilotCamera
		{
			class OpticsIn
			{
				class Wide
				{
					opticsDisplayName="WFOV";
					initAngleX=0;
					minAngleX=-10;
					maxAngleX=90;
					initAngleY=0;
					minAngleY=-90;
					maxAngleY=90;
					initFov=0.425;//"(30 / 120)";
					minFov=0.425;//"(30 / 120)";
					maxFov=0.425;//"(30 / 120)";
					thermalMode[] = {0,1,2,3,4,5};
					visionMode[]=
					{
						"Normal",
						"NVG",
						"Ti"
					};
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_wide_F.p3d";
					opticsPPEffects[]=
					{
						"OpticsCHAbera2",
						"OpticsBlur2"
					};
				};

				class zoomx4: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.425/4)";//"(3.75 / 120)";
					minFov="(0.425/4)";//"(3.75 / 120)";
					maxFov="(0.425/4)";//"(3.75 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				class zoomX8: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/8)";//"(.375 / 120)";
					minFov="(0.42/8)";//"(.375 / 120)";
					maxFov="(0.42/8)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX20: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/20)";//"(.375 / 120)";
					minFov="(0.42/20)";//"(.375 / 120)";
					maxFov="(0.42/20)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};
				class zoomX50: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/50)";//"(.375 / 120)";
					minFov="(0.42/50)";//"(.375 / 120)";
					maxFov="(0.42/50)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				class zoomX70: Wide
				{
					opticsDisplayName="NFOV";
					initFov="(0.42/70)";//"(.375 / 120)";
					minFov="(0.42/70)";//"(.375 / 120)";
					maxFov="(0.42/70)";//"(.375 / 120)";
					gunnerOpticsModel="\A3\Drones_F\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_narrow_F.p3d";
				};

				showMiniMapInOptics=1;
				showUAVViewInOptics=0;
				showSlingLoadManagerInOptics=1;
			};

			minTurn=-180;
			maxTurn=180;
			initTurn=0;

			minElev=-10;
			maxElev=90;
			initElev=-10;

			maxXRotSpeed=0.30000001;
			maxYRotSpeed=0.30000001;
			pilotOpticsShowCursor=1;
			controllable=1;
		};

		class ViewPilot: ViewPilot
		{
			initAngleX = 0;
		};
		class Components : Components
		{
			class VehicleSystemsDisplayManagerComponentLeft: VehicleSystemsTemplateLeftPilot
			{
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,2000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			class VehicleSystemsDisplayManagerComponentRight: VehicleSystemsTemplateRightPilot
			{
				defaultDisplay = "SensorDisplay";
				class Components: components
				{
					class EmptyDisplay
					{
						componentType = "EmptyDisplayComponent";
					};
					class MinimapDisplay
					{
						componentType = "MinimapDisplayComponent";
						resource = "RscCustomInfoAirborneMiniMap";
					};
					class CrewDisplay
					{
						componentType = "CrewDisplayComponent";
						resource = "RscCustomInfoCrew";
					};
					class UAVDisplay
					{
						componentType = "UAVFeedDisplayComponent";
					};
					class VehiclePrimaryGunnerDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
					class VehicleMissileDisplay
					{
						componentType = "TransportFeedDisplayComponent";
						source = "Missile";
					};
					class SensorDisplay
					{
						componentType = "SensorsDisplayComponent";
						range[] = {4000,8000,16000,2000};
						resource = "RscCustomInfoSensors";
					};
				};
			};
			
			class SensorsManagerComponent
			{
				class Components
				{
					class IRSensorComponent: SensorTemplateIR
					{
						class AirTarget
						{
							minRange=0;
							maxRange=5000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=5000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=1;
						};
						componentType = "IRSensorComponent";

						typeRecognitionDistance = 3000;
						angleRangeHorizontal = 90;
						angleRangeVertical = 30;
						maxFogSeeThrough = 0.85;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 10;
						animDirection="";
						color[]={1,0,0,1};
					};
					
					class ActiveRadarSensorComponent: SensorTemplateActiveRadar
					{
						class AirTarget
						{
							minRange=0;
							maxRange=12000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						class GroundTarget
						{
							minRange=0;
							maxRange=12000;
							objectDistanceLimitCoef=-1;
							viewDistanceLimitCoef=-1;
						};
						componentType = "ActiveRadarSensorComponent";

						typeRecognitionDistance = 7500;
						angleRangeHorizontal = 60;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 60;
						animDirection="";
						color[]={0,1,1,1};
					};

					class VisualRadarSensorComponent: SensorTemplateVisual
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 1000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 1000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType = "VisualSensorComponent";

						typeRecognitionDistance = 750;
						angleRangeHorizontal = 180;
						angleRangeVertical = 90;
						maxFogSeeThrough = -1;

						groundNoiseDistanceCoef = -1;
						maxGroundNoiseDistance = 0;

						minSpeedThreshold = 0;
						maxSpeedThreshold = 1000;

						minTrackableSpeed = -1e10;
						maxTrackableSpeed = 1e10;

						minTrackableATL = -1e10;
						maxTrackableATL = 1e10;

						allowsMarking = 1;
						aimDown = 0;
						animDirection="";
						color[]={1,1,0.5,0.80000001};
					};

					class LaserSensorComponent: SensorTemplateLaser
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="LaserSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};

					class NVSensorComponent: SensorTemplateNV
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="NVSensorComponent";
						angleRangeHorizontal=360;
						angleRangeVertical=180;
						aimDown=0;
						maxFogSeeThrough = 0.85;
					};
				
					class DataLinkSensorComponent: SensorTemplateDataLink
					{
						class AirTarget
						{
							minRange	= 0;
							maxRange	= 16000;
							objectDistanceLimitCoef	= -1;
							viewDistanceLimitCoef	= -1;
						};
						class GroundTarget
						{
							minRange = 0;
							maxRange = 16000;
							objectDistanceLimitCoef = -1;
							viewDistanceLimitCoef 	= -1;
						};
						componentType="DataLinkSensorComponent";
						allowsMarking=1;
						typeRecognitionDistance=0;
						color[]={1,1,1,0};
					};

				};
			};
			class TransportPylonsComponent
			{
				uiPicture = "\A3\Air_F_Gamma\Plane_Fighter_03\Data\UI\Plane_A143_3DEN_CA.paa";

				class Pylons
				{
					class pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Bomb_Laser_Glide_Pylon"
						};
						attachment = "empty";
						priority=8;
						maxweight=75;
						UIposition[]={0.1,0.45};
					};
					class pylons2: pylons1
					{
						priority=6;
						UIposition[]={0.15,0.35};
					};
					class pylons3: pylons1
					{
						hardpoints[]=
						{
							"RD501_Republic_Aircraft_Bomb_Laser_Glide_Pylon",
							"RD501_Republic_Aircraft_Bomb_Dumb_Pylon"
						};
						priority=4;
						UIposition[]={0.2,0.25};
					};
					class pylons4: pylons3
					{
						priority=2;
						UIposition[]={0.25,0.15};
					};
					class pylons5: pylons3
					{
						priority=1;
						UIposition[]={0.6,0.15};
						mirroredMissilePos=4;
					};
					class pylons6: pylons3
					{
						priority=3;
						UIposition[]={0.65,0.25};
						mirroredMissilePos=3;
					};
					class pylons7: pylons1
					{
						priority=5;
						UIposition[]={0.7,0.35};
						mirroredMissilePos=2;
					};
					class pylons8: pylons1
					{
						priority=7;
						UIposition[]={0.75,0.45};
						mirroredMissilePos=1;
					};
				};

				class Presets
				{
					class Empty
					{
						displayName = "empty";
						attachment[]={};
					};
				};
			};
		};
	};
};
