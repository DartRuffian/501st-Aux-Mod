#include "../../RD501_main/config_macros.hpp"
class CfgPatches
{
	class RD501_patch_v_wing
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);
		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
		};
		weapons[]={};
	};
class RD501_patch_laat_variants
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);
		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
		};
		weapons[]={};
	};
class RD501_patch_artillery
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);
		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
		};
		weapons[]={};
	};
class RD501_patch_ATTE
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);
		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
		};
		weapons[]={};
	};
};