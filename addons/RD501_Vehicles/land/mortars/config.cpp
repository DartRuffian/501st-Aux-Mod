//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon mortar
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_mortar_class(name) vehicle_classname##_##name

class CfgPatches
{
    class RD501_patch_mortar
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            RD501_patch_vehicles
        };
        requiredVersion=0.1;
        units[]=
        {
            macro_new_vehicle(mortar,republic),
        };
        weapons[]=
        {
            macro_new_weapon(stat,mortar_carry)
        };
    };
};

//remember to do the flares later on when I do effects.
#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;
class CfgVehicles
{
    class StaticMortar;
    class Mortar_01_base_F: StaticMortar
    {
        class Turrets;
        class ACE_Actions;
    };
    class B_Mortar_01_F: Mortar_01_base_F
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
    };
    class Aux501_stat_81mm_plasma_mortar: B_Mortar_01_F
    {   
        scope = 2;
        scopeCurator = 2;
        displayname = "81mm Plasma Mortar";
        faction = MACRO_QUOTE(macro_faction(republic));
        icon = "3AS\3as_static\Mortar\Data\ui\Mortar_top_ca.paa";
        model = "3AS\3as_static\Mortar\model\republicmortar.p3d";
        hiddenSelections[] = {"Camo_1","Camo_2"};
		hiddenSelectionsTextures[] = {"\3as\3as_static\Mortar\data\base.001_co.paa","\3as\3as_static\Mortar\data\tube.001_co.paa"};
		hiddenSelectionsMaterials[] = {"\3as\3as_static\Mortar\data\base.rvmat","\3as\3as_static\Mortar\data\tube.rvmat"};
        crew = "JLTS_Clone_P2_DC15S_501";
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                turretInfoType = "RscWeaponRangeArtillery";
                gunnerforceoptics = 1;
                initelev = 0;
                maxelev = 40;
				minelev = -15;
                maxTurn = 360;
                minTurn = -360;
                weapons[] = {"Aux501_Weaps_81mm_plasma_Mortar"};
                magazines[] =
                {
                    "Aux501_Weapons_Mags_81mm_mortar_HE",
                    "Aux501_Weapons_Mags_81mm_mortar_HE",
                    "Aux501_Weapons_Mags_81mm_mortar_HE",
                    "Aux501_Weapons_Mags_81mm_mortar_smoke",
                    "Aux501_Weapons_Mags_81mm_mortar_smoke",
                    "Aux501_Weapons_Mags_81mm_mortar_illum",
                    "Aux501_Weapons_Mags_81mm_mortar_guided_HE",
                    "Aux501_Weapons_Mags_81mm_mortar_laserguided_HE"
                };
                class Viewoptics
                {
                    initAngleX = 0;
                    initAngleY = 0;
                    initFov = 0.75;
                    maxAngleX = 30;
                    maxAngleY = 100;
                    maxFov = 1.1;
                    maxMoveX = 0;
                    maxMoveY = 0;
                    maxMoveZ = 0;
                    minAngleX = -30;
                    minAngleY = -100;
                    minFov = 0.0125;
                    minMoveX = 0;
                    minMoveY = 0;
                    minMoveZ = 0;
                    opticsZoomInit = 0.75;
                    opticsZoomMax = 0.75;
                    opticsZoomMin = 0.25;
                    thermalMode[] = {6};
                    turretInfoType = "RscWeaponRangeArtillery";
                    visionMode[] = {"Normal","NVG","Ti"};
                };
            };
        };
        class Damage
		{
			tex[] = {};
			mat[] = 
            {
                "3as\3as_static\mortar\data\base.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat",
                "3as\3as_static\mortar\data\tube.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_destruct.rvmat"
            };
		};
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "zamerny";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_81mm_plasma_Mortar";
            magazineLocation = "_target selectionPosition 'usti hlavne'";
            disassembleWeapon = "Aux501_Weaps_81mm_plasma_Mortar_Carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 8;
        };
    };
};