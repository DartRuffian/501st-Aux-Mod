//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon MTT
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define macro_new_mtt_class(name) vehicle_classname##_##name


class CfgPatches
{
	class RD501_patch_vehicle_addon
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(cis,mtt)
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;
class CfgVehicles
{
	class 3as_MTT;

	class macro_new_vehicle(cis,mtt) : 3as_MTT
	{
		displayName = "CIS MTT";
		crew=MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		scope = 2;
		side=0;
		scopeCurator=2;

		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));

		author = "RD501";
		forceInGarage=1;
	};
	


};