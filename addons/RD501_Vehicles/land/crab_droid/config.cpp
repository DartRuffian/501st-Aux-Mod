#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class rd501_patch_name_crab_droid
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(crab_droid,MkII),
			macro_new_vehicle(crab_droid,Movable_MkII)
		};
		weapons[]=
		{
			
		};
	};
};

#include "../../common/sensor_templates.hpp"

class DefaultEventhandlers;
class CfgVehicles
{
	class LandVehicle;
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret;
		};
	};
	class StaticMGWeapon: StaticWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};
	class 101st_Crab_Droid : StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{};
		};
	};
	class macro_new_vehicle(crab_droid,MkII): 101st_Crab_Droid
	{
		displayName = "Crab Droid";
		scope = 2;
		forceInGarage = 1;
		armor = 65;
		armorStructural = 1.0;
		model = "DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_CRAB\Crab.p3d";
		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_CRAB\data\Body_co.paa","DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_CRAB\data\Legs_co.paa"};
		explosionShielding	= 0.1;
		icon = "iconStaticMG";
		minTotalDamageThreshold	= 0.01;
		impactDamageMultiplier = 0.1;
		class EventHandlers: DefaultEventhandlers {}; 
		ace_dragging_canCarry = 0;
		ace_dragging_canDrag = 0;
		faction = macro_faction(CIS);
		editorSubcategory = macro_editor_cat(heavy_armored_infantry);
		vehicleClass = macro_editor_vehicle_type(heavy_armored_infantry);
		gunnerCanSee = "1+2+4+16";
		commanderCanSee = "1+2+4+16";
		driverCanSee = "1+2+4+16";
		radarTarget = 1;
		radarTargetSize = 1.2;
		visualTarget = 1;
		visualTargetSize = 1.2;
		irTargetSize = 0;
		reportRemoteTargets = 1;
		receiveRemoteTargets = 1;
        uavCameraGunnerPos = "PiP_pos";
		uavCameraGunnerDir = "PiP_dir";
        irTarget = 1;
		irScanRangeMin = 50;
		irScanRangeMax = 10500;
		irScanToEyeFactor = 4;
		irScanGround = 1;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[]=
				{
					"GMG_20mm"
				};
				magazines[]=
				{
					"40Rnd_20mm_g_belt",
					"40Rnd_20mm_g_belt",
					"40Rnd_20mm_g_belt",
					"40Rnd_20mm_g_belt",
					"40Rnd_20mm_g_belt"
				};
				class HitPoints
				{
					class HitGun
					{
						armor = 1;
						material = -1;
						name = "gun";
						visual = "autonomous_unhide";
						passThrough = 0;
						radius = 0.2;
					};
					class HitTurret: HitGun
					{
						armor = 1;
						name = "turret";
					};
				};
			};
		};
		destrType = "DestructBuilding";
		explosionEffect="FuelExplosion";
		class DestructionEffects
		{
			class Dust
			{
				intensity = 0.1;
				interval = 1;
				lifeTime = 0.01;
				position = "destructionEffect2";
				simulation = "particles";
				type = "HousePartDust";
			};
			class Light1
			{
				enabled = "distToWater";
				intensity = 0.1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "light";
				type = "ObjectDestructionLightSmall";
			};
			class Fire1
			{
				intensity = 0.15;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "ObjectDestructionFire1Small";
			};
			class Refract1
			{
				intensity = 1;
				interval = 1;
				lifeTime = 3;
				position = "destructionEffect";
				simulation = "particles";
				type = "SmallFireFRefract";
			};
			class Sound
			{
				intensity = 1;
				interval = 1;
				lifeTime = 1;
				position = "destructionEffect";
				simulation = "sound";
				type = "Fire";
			};
			class sparks1
			{
				intensity = 0.5;
				interval = 1;
				lifeTime = 0;
				position = "destructionEffect2";
				simulation = "particles";
				type = "ObjectDestructionSparks";
			};
			class Smoke1
			{
				simulation="particles";
				type="BarelDestructionSmoke";
				position[]={0,0,0};
				intensity=0.2;
				interval=1;
				lifeTime=1;
			};
			class HouseDestr
			{
				intensity=1;
				interval=1;
				lifeTime=5;
				position="";
				simulation="destroy";
				type="DelayedDestruction";
			};
		};
	};
	class macro_new_vehicle(crab_droid,Movable_MkII): macro_new_vehicle(crab_droid,MkII)
	{
		displayName = "Crab Droid (Mobile)";
		forceInGarage = 0;
	};
};