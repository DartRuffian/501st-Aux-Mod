#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_rtt
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(gar,rtt)
		};
		weapons[]={};
	};
};

class CfgVehicles
{
    class 3as_RTT_Base;
    class macro_new_vehicle(gar,rtt): 3as_RTT_Base {
            side = 1;
		    scope = 2;
		    scopeCurator = 2;
		    displayName = "RTT";
		    forceInGarage = 1;
            faction = MACRO_QUOTE(macro_faction(republic));
            editorSubcategory = MACRO_QUOTE(macro_editor_cat(APC));
            vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(APC));

            RD501_magclamp_large_offset[] = {0.0, 0.0, -4.0};
            RD501_magclamp_small_offset[] = {0.0, 0.0, -4.0};

            hiddenSelections[] = {"camo1"};
            hiddenSelectionsTextures[] = {"3as\3as_rtt\data\rtt\exterior_CO.paa"};
    };
};