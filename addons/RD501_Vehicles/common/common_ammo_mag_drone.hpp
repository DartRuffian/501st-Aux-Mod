
//dc-17a
class _transport_20mw30p
{
	magazine = "Aux501_Weapons_Mags_20mw30";
	count = 7;
};
//dc-17
class transport_20mwdp20
{
	magazine = "Aux501_Weapons_Mags_20mwdp20";
	count = 7;
};
//dc-15s
class _transport_10mw50
{
	magazine = "Aux501_Weapons_Mags_10mw50";
	count = 30;
};
//dc-17m
class _transport_10mw71
{
	magazine = "Aux501_Weapons_Mags_10mw71";
	count = 30;
};
//dc-15C family
class _transport_20mw40
{
	magazine = "Aux501_Weapons_Mags_20mw40";
	count = 30;
};
//z-6
class _transport_10mw400
{
	magazine = "Aux501_Weapons_Mags_10mw400";
	count = 7;
};
//dc-15a
class _transport_20mwup30
{
	magazine = "Aux501_Weapons_Mags_20mwup30";
	count = 20;
};	
//dc-15a
class _transport_20mwdp30
{
	magazine = "Aux501_Weapons_Mags_20mwdp30";
	count = 20;
};
//dc-17l	
class _transport_20mw240
{
	magazine = "Aux501_Weapons_Mags_20mw240";
	count = 20;
};	
//Valken
class _transport_30mw10
{
	magazine = "Aux501_Weapons_Mags_30mw10";
	count = 7;
};
//dc-15sa razorblade
class _transport_30mw7
{
	magazine = "Aux501_Weapons_Mags_30mw7";
	count = 7;
};
//dc-15x
class _transport_40mw5
{
	magazine = "Aux501_Weapons_Mags_40mw5";
	count = 10;
};

//RAMER
class _transport_50mw10
{
	magazine = "Aux501_Weapons_Mags_50mw10";
	count = 10;
};
//stun rounds
class _transport_stun5
{
	magazine = "Aux501_Weapons_Mags_stun5";
	count = 20;
};
//RPS6 AT
class _transport_launcher_RPS6_AT_mags
{
	magazine = "Aux501_Weapons_Mags_rps6_at";
	count = 20;
};
//RPS6 AA
class _transport_launcher_RPS6_AA_mags
{
	magazine = "Aux501_Weapons_Mags_rps6_aa";
	count = 20;
};
//RPS4 heat
class _transport_launcher_RPS4_HEAT_mags
{
	magazine = "Aux501_Weapons_Mags_rps4_heat";
	count = 20;
};
//RPS4 HE
class _transport_launcher_RPS4_HE_mags
{
	magazine = "Aux501_Weapons_Mags_rps4_he";
	count = 20;
};
//PLX AT
class _transport_launcher_PLX_AT_mags
{
	magazine = "Aux501_Weapons_Mags_plx1_at";
	count = 10;
};
//PLX AP
class _transport_launcher_PLX_AP_mags
{
	magazine =  "Aux501_Weapons_Mags_plx1_ap";
	count = 10;
};
//3 Rnd HE DC-15A Grenades
class _transport_DC15a_HE
{
	magazine = "Aux501_Weapons_Mags_GL_HE3";
	count = 7;
};
//1 Rnd HE DC-15C Grenades
class _transport_DC15c_HE
{
	magazine = "Aux501_Weapons_Mags_GL_HE1";
	count = 7;
};
//2 Rnd AP DC-15A Grenades
class _transport_DC15a_AP
{
	magazine = "Aux501_Weapons_Mags_GL_AP2";
	count = 7;
};
//huntIR
class _transport_HuntIR_M203
{
	magazine = "ACE_HuntIR_M203";
	count = 7;
};
//RGL rounds HE
class _transport_RGL_HE
{
	magazine = "Aux501_Weapons_Mags_RGL_HE6";
	count = 10;
};
//RGL rounds AP
class _transport_RGL_AP
{
	magazine = "Aux501_Weapons_Mags_RGL_AP6";
	count = 10;
};
//Shotgun scatter
class _transport_Drexl_SP
{
	magazine = "Aux501_Weapons_Mags_shotgun_scatter20";
	count = 20;
};
//Shotgun HE
class _transport_Drexl_HE
{
	magazine = "Aux501_Weapons_Mags_shotgun_he10";
	count = 10;
};
//Shotgun Slug
class _transport_Drexl_Slug
{
	magazine = "Aux501_Weapons_Mags_shotgun_slug24";
	count = 10;
};
//eweb ammo
class _transport_StaticEweb
{
	magazine = "Aux501_Weapons_Mags_EWHB12_MG_csw";
	count = 10;
};
//striker ammo
class _transport_StaticStriker
{
	magazine = "Aux501_Weapons_Mags_AAP4_csw";
	count = 10;
};
//boomer ammo
class _transport_StaticBoomer
{
	magazine = "Aux501_Weapons_Mags_EWHB12_GL_csw";
	count = 10;
};
//driver ammo
class _transport_StaticDriver
{
	magazine = "Aux501_Weapons_Mags_mar1_csw";
	count = 10;
};
//C12 explosive charge
class _transport_Explosives_void3
{
	magazine = "Aux501_Weapons_Mags_Explosives_void3";
	count = 2;
};