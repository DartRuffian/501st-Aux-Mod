# Medical Notification Message

## Breaking Changes

This introduces some standardisation around variable names which means unfortunately your old medical message will be lost after migrating to this. If people have issues you can run:

```sqf
private _settingsHash = profileNamespace getVariable ["cba_settings_hash", nil]; 
_settingInfo = [_settingsHash, toLower "RD501_MedNotif_Message"] call CBA_fnc_hashGet;
systemChat format["%1", _settingInfo select 0];
diag_log format["%1", _settingInfo select 0];
copyToClipboard format["%1", _settingInfo select 0];
```

Copy the above into the debug console in VA. Then click execute local at the bottom of the console. You should now have the message on your clipboard.
