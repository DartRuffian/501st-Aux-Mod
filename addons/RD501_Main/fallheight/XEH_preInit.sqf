#include "functions\function_macros.hpp"

LOG("PreInit Begin");
private _addonConfig = configFile >> "CfgPatches" >> QUOTE(ADDON);
private _version = getArray(_addonConfig >> "version");
LOG(format["Version: %1", _verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

LOG("CBASettings Begin");
private _settingsCategory = ["RD501", "Fallheight"];
[QGVAR(enabled), "CHECKBOX", ["Enable", "Enables or disables custom fall height."], _settingsCategory, false, 1, {
	[player] call FUNC(setFreefallHeight);
}] call CBA_fnc_addSetting;
[QGVAR(height), "SLIDER", ["Minimum Fall Height", "Shortest distance in metres before you begin the HALO fall animation."], _settingsCategory, [10, 8000, 10, 0], 1, {
	[player] call FUNC(setFreefallHeight);
}, false] call CBA_fnc_addSetting;
LOG("CBASettings Complete");

LOG("PreInit Complete");