/*
 * Author: M3ales
 *
 * Arguments:
 * Unit
 * Unit to update minFreefallHeight on.
 *
 * Return Value:
 * Nothing
 *
 * Example:
 * [player] call rd501_fallheight_setFreefallHeight
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_unit"];

if(_unit isEqualTo objNull) exitWith {
	LOG_ERROR("Unit Unspecified");
};

if(!local _unit) exitWith {
	LOGF_1("'%1' is not local, skipping.");
};

if!(alive _unit) exitWith {
	LOG_ERROR("Triggered on dead unit");
};

if!(GVAR(enabled)) exitWith {
	_unit setUnitFreefallHeight -1;
	LOG("Module disabled, set to default");
};

if!(_unit in allPlayers) exitWith {
	LOG("Not a player, skipping.");
};

private _targetHeight = GVAR(height);
if(isNil "_targetHeight") then { _targetHeight = -1 };

(getUnitFreefallInfo _unit) params ["_isFalling", "_isInFreefallPose", "_minFreefallHeight"];

if(_targetHeight isEqualTo _minFreefallHeight) exitWith {};

LOGF_2("Setting %1 freefall to %2",_unit,_targetHeight);

_unit setUnitFreefallHeight _targetHeight;
