/*
 * Author: M3ales & Hobnob
 *
 * Arguments:
 *
 * Example:
 * [] call rd501_awacs_fnc_toggleAWACS
 *
 * Public: No
 */

#include "function_macros.hpp"

GVAR(running) = !GVAR(running);
if(GVAR(running)) exitWith {
    [] call FUNC(BRAA);
    [] call FUNC(AWACS);
};