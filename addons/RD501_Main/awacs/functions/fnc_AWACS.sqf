/*
 * Author: Hobnob
 * Adds AWACS Style sensor display on the map.
 *
 *
 * Arguments:
 *
 * Example:
 * [] call rd501_awacs_fnc_AWACS
 *
 * Public: No
 */
#include "function_macros.hpp"

GVAR(AWACS_Index) = 0;
createMarkerLocal[QGVAR(ownship), vehicle player];
QGVAR(ownship) setMarkerShapeLocal "ICON";
QGVAR(ownship) setMarkerTypeLocal "mil_start_noShadow";
QGVAR(ownship) setMarkerColorLocal "ColorGreen";
QGVAR(ownship) setMarkerDirLocal ((getDir (vehicle player)) % 360);

private _fnc_tick = {
	params ["_args", "_handle"];

	//clean up last run's markers.
	for "_loop" from 0 to GVAR(AWACS_Index) do { LOGF_1("Deleting: %1", _loop); deleteMarkerLocal format ["RD501_AWACS_T_%1", _loop ]; };
	
	LOGF_1("Index Reseting: %1", GVAR(AWACS_Index));
	GVAR(AWACS_Index) = 0;
	LOGF_1("Index Reset: %1", GVAR(AWACS_Index));

	private _fnc_exit = 
	{
		params ["_handle"];
		[_handle] call CBA_fnc_removePerFrameHandler;
		deleteMarkerLocal QGVAR(ownship);
		removeMissionEventHandler ["MapSingleClick", GVAR(BRAA_EHID)];
		GVAR(Running) = false;
	};

	//sanity checks
	if(!GVAR(Running)) exitwith { LOG("Exiting because Running = False"); [_handle] call _fnc_exit; }; //check for user turning off AWACS system. 
	if(!alive player) exitwith { LOG("Exiting because Player is Kill."); [_handle] call _fnc_exit; }; //makes sure player is alive
	private _vehicle = vehicle player;
	if(_vehicle == player) exitWith { LOG("Exiting because Vehicle is Player"); [_handle] call _fnc_exit; }; // Not in a vehicle
	private _validTypes = GVAR(validTypes);
	if (_validTypes findIf { _vehicle isKindOf (_x) } == -1) exitWith { LOG("Exiting because Vehicle is not Valid"); [_handle] call _fnc_exit; }; // This vehicle is not awacs capable

	QGVAR(ownship) setMarkerPosLocal _vehicle;
	QGVAR(ownship) setMarkerDirLocal ((getDir (_vehicle)) % 360);
	
	private _rT = nil;
	_rT = getSensorTargets _vehicle;
	{
		_x params ["_sensorTarget", "_sensorType", "_relationship", "_sensor"];
		private _name = format ["RD501_AWACS_T_%1", GVAR(AWACS_Index) ];
		GVAR(AWACS_Index) = GVAR(AWACS_Index) + 1;
		createMarkerLocal[_name, _sensorTarget];
		_name setMarkerShapeLocal "ICON";
		_name setMarkerTypeLocal "mil_start_noShadow";
		private _displayName = getText (configFile >> "CfgVehicles" >> (typeOf (_sensorTarget)) >> "displayName");
		private _altitude = floor ((getPosASL _sensorTarget)#2 / 1000); //gets Z (altitude) from object and converts to KM.
		if ((speed (_sensorTarget)) < 1325) then
		{
			_name setMarkerTextLocal format ["%1: A%2, %3KM/H", _displayName, _altitude, floor speed (_sensorTarget)];
		} else {
			_mach = [(speed (_sensorTarget))/1325 , 2] call BIS_fnc_cutDecimals;
			_name setMarkerTextLocal format ["%1: A%2, Mach %3", _displayName, _altitude, _mach];
		};
		switch (_relationship) do 
		{
			case "friendly": { _name setMarkerColorLocal "ColorWest" };
			case "unknown" : { _name setMarkerColorLocal "ColorYellow"};
			case "enemy"   : { _name setMarkerColorLocal "ColorEast" };
		};
		_name setMarkerDirLocal ((getDir (_sensorTarget)) % 360);
	} forEach _rT;
};

private _pfhID = [_fnc_tick, GVAR(UpdateRate)] call CBA_fnc_addPerFrameHandler;
