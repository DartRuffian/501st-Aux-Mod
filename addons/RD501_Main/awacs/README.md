# AWACS

Displays ownship sensor information on the map, and provides A simple distance and bearing measuring tool to perform AWACS tasks.

## Add AWACS to Vehicles

Any object in CfgVehicles can be registered as eligable for AWACS, by including: 

```cpp
class CfgVehicles {
    class rd501_someVehicle {
        class rd501_awacs {};
    }
}
```

This will automatically add the ace self action to the driver seat while in the map.

If you don't have access or don't want to customise the behaviour of a given class you can specify it directly in this module's config.

```cpp

class CfgPatches {
    class rd501_awacs {
        externalTypes[] = { "classNameA", "classNameB" };
    }
}