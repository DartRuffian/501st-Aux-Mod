#include "config_macros.hpp"

//stretcher
macro_prep_xeh(stretcher\deploy_stretcher.sqf,deploy_stretcher)
macro_prep_xeh(stretcher\put_stretcher_in_backpack.sqf,put_stretcher_in_backpack)
macro_prep_xeh(stretcher\drop_stretcher.sqf,drop_stretcher)
macro_prep_xeh(stretcher\carry_stretcher.sqf,carry_stretcher)

//Zatamas aircraft turrets
macro_prep_xeh(attach_turrets\apply_turrets.sqf,apply_aircraft_turrets)
macro_prep_xeh(attach_turrets\remove_attached_objects.sqf,remove_attached_objects)

//Zatamas aircraft drones
macro_prep_xeh(attach_drones\apply_drones.sqf,apply_aircraft_drone)

//set mass 
macro_prep_xeh(mass_changer\set_mass.sqf,set_mass)

//droidekka move
macro_prep_xeh(movable_scripts\droideka_driver.sqf,droidekka_move)

//dwarf move
macro_prep_xeh(movable_scripts\dwarf_spider_droid_driver.sqf,dwarf_move)

//og10 move
macro_prep_xeh(movable_scripts\homing_spider_droid_driver.sqf,og10_move)

//add healing option
macro_prep_xeh(add_healing_action.sqf,add_heal_action)

//add dog
macro_prep_xeh(add_doggo.sqf,add_doggo)

//ATTE UAV init
macro_prep_xeh(atte\atte.sqf,atte_uav_init)
macro_prep_xeh(atte\create_itt.sqf,create_itt)
macro_prep_xeh(atte\create_turrets.sqf,create_atte_turrets)

//adds the killed and MPkilled EH to remove attached objects on death.
macro_prep_xeh(remove_attached_on_death\add_remove_attached_EH.sqf,add_remove_attached_EH)
macro_prep_xeh(remove_attached_on_death\remove_attached.sqf,remove_attached)

//warden tow
macro_prep_xeh(init\tow.sqf,warden_tow)

//init
macro_prep_xeh(init\force_b2_walk.sqf,force_b2_walk)
macro_prep_xeh(init\specops.sqf,b1_specop_apply_skill)
macro_prep_xeh(init\b2_armor.sqf,b2_apply_armor)
macro_prep_xeh(init\jam_sandwich.sqf,apply_jammer)
macro_prep_xeh(init\aat.sqf,aatInit)

//nightvision
macro_prep_xeh(nightvision.sqf,nightvision)

//magclamp
macro_prep_xeh(magclamp\magclamp.sqf,magclamp)
macro_prep_xeh(magclamp\fnc_startRefuel.sqf,mc_startRefuel)
macro_prep_xeh(magclamp\fnc_canRefuel.sqf,mc_canRefuel)
macro_prep_xeh(magclamp\fnc_canStopRefuel.sqf,mc_canStopRefuel)

//HUD color
macro_prep_xeh(hud_color_change.sqf,change_hud_color)

//showe dmg
macro_prep_xeh(show_damage_report.sqf,add_show_dmg_report)

//Yeet them out
call compile preprocessFileLineNumbers 'macro_mod_script_path\add_radio_freq_shower.sqf';

//R-2 Recon Drone
macro_prep_xeh(infantry_uav\fnc_getRefuelMagazine.sqf,getRefuelMagazine)
macro_prep_xeh(infantry_uav\fnc_refuelUAVDrone.sqf,refuelUAVDrone)
macro_prep_xeh(infantry_uav\fnc_canRefuel.sqf,canRefuel)

macro_prep_xeh(heavy_weapon\fnc_heavyWeaponHandlePlayerWeaponChanged.sqf,heavyWeaponHandlePlayerWeaponChanged)
macro_prep_xeh(reload_on_select\fnc_reloadWeaponOnFirstSelected.sqf,reloadWeaponOnFirstSelected)
macro_prep_xeh(reload_on_select\fnc_swapToEmptyWeapon.sqf,swapToEmptyWeapon)
macro_prep_xeh(reload_on_select\fnc_onWeaponFiredSwapToEmpty.sqf,onWeaponFiredSwapToEmpty)

// Flip backpack
macro_prep_xeh(flip_vehicle\fnc_flipVehicle.sqf,flipVehicle)
macro_prep_xeh(flip_vehicle\fnc_canFlipVehicle.sqf,canFlipVehicle)

// Reload Externally
macro_prep_xeh(external_reload\fnc_canReloadExternal.sqf,canReloadExternal)
macro_prep_xeh(external_reload\fnc_reloadExternal.sqf,reloadExternal)
macro_prep_xeh(external_reload\fnc_onReloadExternalHandler.sqf,onReloadExternalHandler)

// Heal Nearby
macro_prep_xeh(heal_nearby\fnc_healAllNearby.sqf,healAllNearby)

// stun
macro_prep_xeh(stun\stun.sqf,stun)

// Medical CCP
macro_prep_xeh(medical_ccp\fnc_stitchAllWounds.sqf,stitchAllWounds)
macro_prep_xeh(medical_ccp\fnc_bandageAllWounds.sqf,bandageAllWounds)
macro_prep_xeh(medical_ccp\fnc_checkInsideCCP.sqf,checkInsideCCP)
macro_prep_xeh(medical_ccp\fnc_stitchAllWoundsNearbyCCP.sqf,stitchAllWoundsNearbyCCP)
macro_prep_xeh(medical_ccp\fnc_bandageAllNearbyCCP.sqf,bandageAllNearbyCCP)
macro_prep_xeh(medical_ccp\fnc_valueProgressBar.sqf,valueProgressBar)
macro_prep_xeh(medical_ccp\fnc_incrementBandageProgress.sqf,incrementBandageProgress)
macro_prep_xeh(medical_ccp\fnc_incrementStitchProgress.sqf,incrementStitchProgress)
macro_prep_xeh(medical_ccp\fnc_deployCCPLocal.sqf,deployCCPLocal)
macro_prep_xeh(medical_ccp\fnc_isDoctor.sqf,isDoctor)
macro_prep_xeh(medical_ccp\fnc_canBandageNearbyCCP.sqf,canBandageNearbyCCP)
macro_prep_xeh(medical_ccp\fnc_canStitchNearbyCCP.sqf,canStitchNearbyCCP)

// Jammer
macro_prep_xeh(jammer\fnc_jammerControlActions.sqf,jammerControlActions)
macro_prep_xeh(jammer\fnc_jammersAdd.sqf,jammersAdd)
macro_prep_xeh(jammer\fnc_jammersAddServer.sqf,jammersAddServer)
macro_prep_xeh(jammer\fnc_jammersClear.sqf,jammersClear)
macro_prep_xeh(jammer\fnc_jammersClientPFH.sqf,jammersClientPFH)
macro_prep_xeh(jammer\fnc_jammersGetLocal.sqf,jammersGetLocal)
macro_prep_xeh(jammer\fnc_jammersRemove.sqf,jammersRemove)
macro_prep_xeh(jammer\fnc_jammersRemoveServer.sqf,jammersRemoveServer)
macro_prep_xeh(jammer\fnc_jammersServerPFH.sqf,jammersServerPFH)
macro_prep_xeh(jammer\fnc_jammersUpdateLocal.sqf,jammersUpdateLocal)
macro_prep_xeh(jammer\fnc_jammersUpdateServer.sqf,jammersUpdateServer)

// Droideka Shield
macro_prep_xeh(init\deka_shield_init.sqf,deka_shield_init)

// JLTS Shield
macro_prep_xeh(jlts_shield\fnc_jlts_shield_aiToggle.sqf,jlts_shield_aiToggle)

// Volatile
macro_prep_xeh(volatile\fnc_volatile_create.sqf,volatile_create)
macro_prep_xeh(volatile\fnc_volatile_destroy.sqf,volatile_destroy)
macro_prep_xeh(volatile\fnc_volatile_handleDamage.sqf,volatile_handleDamage)

// Map Markers
macro_prep_xeh(map_markers\fnc_placeDotMarkerAtSelf.sqf,placeDotMarkerAtSelf)

// Shield
macro_prep_xeh(shield\fnc_shield_checkState.sqf,shield_checkState)
macro_prep_xeh(shield\fnc_shield_getTextureSet.sqf,shield_getTextureSet)
macro_prep_xeh(shield\fnc_shield_init.sqf,shield_init)
macro_prep_xeh(shield\fnc_shield_onDestroy.sqf,shield_onDestroy)
macro_prep_xeh(shield\fnc_shield_onHit.sqf,shield_onHit)
macro_prep_xeh(shield\fnc_shield_hitHandler.sqf,shield_hitHandler)
macro_prep_xeh(shield\fnc_shield_onLowHealth.sqf,shield_onLowHealth)
macro_prep_xeh(shield\fnc_shield_onNormalHealth.sqf,shield_onNormalHealth)
macro_prep_xeh(shield\fnc_shield_regenPerFrameHandler.sqf,shield_regenPerFrameHandler)

// Vehicle EMP
macro_prep_xeh(emp_vehicle\fnc_emp_hitVehicle.sqf,emp_hitVehicle)
macro_prep_xeh(emp_vehicle\fnc_emp_hitHandler.sqf,emp_hitHandler)
macro_prep_xeh(emp_vehicle\fnc_emp_enableVehicle.sqf,emp_enableVehicle)
macro_prep_xeh(emp_vehicle\fnc_emp_disableVehicle.sqf,emp_disableVehicle)

// Zeus Login
[
	"RD501_ZL_Enabled",
    "CHECKBOX",
    ["Enabled", "If disabled you will not get the option to log into zeus with ace self interact."],
    ["RD501", "Zeus Login"],
	true
] call CBA_fnc_addSetting;

diag_log "RD501 PREP Complete";