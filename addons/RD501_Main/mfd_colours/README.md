# Custom HUD Colours

## Config

Requires that MFD class makes use of MFD input variables:

- user0 = Red (0-1)
- user1 = Green (0-1)
- user2 = Blue (0-1)
- user3 = Alpha (0-1)

`rd501_mfd_colours_enabled=1`

> Will add the change hud colour root action to a given vehicle.
