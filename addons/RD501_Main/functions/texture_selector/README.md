# Info

Functions that handle the texture switcher addon.

# Control IDs

Group ID: `001`
Full Prefix: `501001`

| Code  | Control Name             |
| :---: | ------------------------ |
|  001  | Group Selector           |
|  002  | Texture Selector         |
|  003  | Apply Button             |
|  004  | Save To Defaults Button  |
|  005  | Defaults Selector        |
|  006  | Delete Default Button    |
|  007  | Move Default Up Button   |
|  008  | Move Default Down Button |
|  009  | Close Button             |