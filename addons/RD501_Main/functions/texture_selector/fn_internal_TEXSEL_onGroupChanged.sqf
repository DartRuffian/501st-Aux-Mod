private _diag = findDisplay 501001;
private _groupSelector = _diag displayCtrl 001;
private _textureSelector = _diag displayCtrl 002;

private _textures = localNamespace getVariable ["RD501_TEXSEL_EligibleTextureConfigurations", createHashMap];

[["Pulled texture data", _textures] joinString " ", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

private _newGroup = _groupSelector lbText (lbCurSel _groupSelector);

private _data = _textures getOrDefault [_newGroup, []];

[["Changed selected data group to", _newGroup, "with data", _data] joinString " ", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

lbClear _textureSelector;
{
	_textureSelector lbAdd (_x select 0);
} forEach _data;
