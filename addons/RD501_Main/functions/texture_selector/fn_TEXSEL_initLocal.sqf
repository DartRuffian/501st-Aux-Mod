// Add the ACE interactions

["ace_interact_menu_newControllableObject", {
	params ["_type"];

	if (not (_type isKindOf "AllVehicles") || _type isKindOf "Man") exitWith {};

	_action = ["TextureSelector", "Texture Selector", "", {
		// Statement
		params ["_target", "_player", "_params"];
		[_target] call RD501_fnc_TEXSEL_openTextureChangeMenu;
	}, {
		// Condition
		[player, vehicle player] call RD501_fnc_internal_TEXSEL_canChangeTextures;
	}] call ace_interact_menu_fnc_createAction;

	[_type, 1, ["ACE_SelfActions"], _action] call ace_interact_menu_fnc_addActionToClass;
}] call CBA_fnc_addEventHandler;

["vehicle", {
	_this call RD501_fnc_internal_TEXSEL_onVehicle;
}, true] call CBA_fnc_addPlayerEventHandler;
