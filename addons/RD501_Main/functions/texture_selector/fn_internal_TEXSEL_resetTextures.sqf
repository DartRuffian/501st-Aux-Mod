private _vic = vehicle player;
private _textures = getArray (configFile >> "CfgVehicles" >> (typeOf _vic) >> "hiddenSelectionsTextures");

if (not (isNil '_textures')) then {
	{
		_vic setObjectTextureGlobal [_forEachIndex, _x];
	} forEach _textures;
};
