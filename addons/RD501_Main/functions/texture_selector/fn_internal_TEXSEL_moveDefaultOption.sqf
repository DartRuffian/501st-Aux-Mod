#include "..\loglevel.hh"

params ["_up"];

private _diag = findDisplay 501001;
private _defaultsSelector = _diag displayCtrl 005;

private _defaults = profileNamespace getVariable "RD501_TEXSEL_DefaultTextureConfigurations";
if (isNil '_defaults') then {
	_defaults = createHashMap;
	profileNamespace setVariable ["RD501_TEXSEL_DefaultTextureConfigurations", _defaults];
};

private _name = _defaultsSelector lbText (lbCurSel _defaultsSelector);
private _class = typeOf (vehicle player);

if (_class in _defaults) then {
	private _group = _defaults get _class;
	private _dataSetIndex = _group findIf { (_x select 0) isEqualTo _name };

	if (_dataSetIndex >= 0) then {
		if (_up && _dataSetIndex == 0) exitWith {
			["Can't move an element further up than 0.", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;
		};

		if ((not _up) && _dataSetIndex == ((count _group) - 1)) exitWith {
			["Can't move an element further down than the size of the dataset.", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;
		};

		private _tmp = _group select _dataSetIndex;
		private _newIndex = _dataSetIndex;
		if (_up) then {
			_newIndex = _newIndex - 1;
		} else {
			_newIndex = _newIndex + 1;
		};

		[["Moving element from", _dataSetIndex, "to", _newIndex] joinString " ", LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

		_group set [_dataSetIndex, _group select (_newIndex)];
		_group set [_newIndex, _tmp];

		_defaults set [_class, _group];

		lbClear _defaultsSelector;

		{
			_defaultsSelector lbAdd (_x select 0);
		} forEach _group;
	};
};

