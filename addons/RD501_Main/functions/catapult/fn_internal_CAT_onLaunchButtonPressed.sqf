#include "..\loglevel.hh"

// Launch Controller Button Code
_index = lbSelection (displayCtrl 1515);
private _catapults = localNamespace getVariable ["RD501_CAT_catapultList", []];
{
	_cat = _catapults select _x;
	private _pad = _cat select 0;
	private _vic = [_pad] call RD501_fnc_internal_CAT_detectVic;

	if (!(isNil "_vic")) then {
		["RD501_event_internal_CAT_launch", [
			// Pad
			_pad,
			// Direction
			parseNumber ctrlText 1512, 
			// Speed
			parseNumber ctrlText 1501, 
			// Launch Speed
			parseNumber ctrlText 1502, 
			// Launch Rotation
			parseNumber ctrlText 1503, 
			// Launch Height
			parseNumber ctrlText 1511, 
			// Max Distance
			parseNumber ctrlText 1504, 
			// Hover Cycles
			parseNumber ctrlText 1513, 
			// Sleep Timer
			parseNumber ctrlText 1514,
			// Vic to launch
			_vic
		], _vic] call CBA_fnc_targetEvent;
	} else {
		[["No vic found to launch from catapult:", _pad] joinString " ", LOG_WARN, "CATAPULT"] call RD501_fnc_logMessage;
	};
} forEach _index;
closeDialog 1;