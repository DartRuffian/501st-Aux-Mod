The Handle Player Fired system can be enabled in any `CfgAmmo` class with the following line:
```
rd501_fired_script_enabled = 1;
```

When enabled, it will compile and run the script provided in the following config line:
```
rd501_fired_script = "_this call RD501_fnc_functionNameHere;";
```

The parameters passed are as follows:
```sqf
params ["_unit", "_weapon", "_muzzle", "_node", "_ammo", "_magazine", "_projectile"];
```