params ["_player", "_didJIP"];

diag_log text (["[RD501]", "[UTIL]", "TRACE:", "Registered new util interactions for:", _this] joinString " ");

_insertChildren = {
	_children = [
		[["Disable Lights", "Disable Lights", "", {
			profileNamespace setVariable ["RD501_UTIL_LightsEnabled", false];
		}, {
			_res = profileNamespace getVariable ["RD501_UTIL_LightsEnabled", true];

			_res
		}] call ace_interact_menu_fnc_createAction, [], _target],

		[["Enable Lights", "Enable Lights", "", {
			profileNamespace setVariable ["RD501_UTIL_LightsEnabled", true];
		}, {
			_res = profileNamespace getVariable ["RD501_UTIL_LightsEnabled", true];

			not _res
		}] call ace_interact_menu_fnc_createAction, [], _target]
	];

	_children;
};

_action = ["RD501 Lights", "RD501 Lights", "", {}, {true}, _insertChildren] call ace_interact_menu_fnc_createAction;

[(typeOf player), 1, ["ACE_SelfActions"], _action] call ace_interact_menu_fnc_addActionToClass;
