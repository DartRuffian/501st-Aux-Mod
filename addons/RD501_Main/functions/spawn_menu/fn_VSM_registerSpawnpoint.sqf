params ["_pad", "_disp", "_rot", "_filter"];

if (isNil "RD501_VSM_registered_positions") then {
	RD501_VSM_registered_positions = createHashMap;
	publicVariable "RD501_VSM_registered_positions";
};

item_list = RD501_VSM_registered_positions getOrDefault ["pads", []];
item_list append [[_pad, _disp, _rot, _filter]];
RD501_VSM_registered_positions set ["pads", item_list];

diag_log text (["[SVLN]", "[VIC SPAWNER]", "DEBUG:", "Registered Pad", _pad, "(", _disp, ")", "at", getPos _pad] joinString " ");