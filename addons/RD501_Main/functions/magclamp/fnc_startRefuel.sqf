params["_vehicle"];

diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Start refuel script entered for vehicle:", _vehicle] joinString " ");

if (not (isNull _vehicle)) then {
    private _rate = 10;

    private _attached = _vehicle getVariable["RD501_mc_attached_large", objNull];
    diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Found attached vehicle:", _attached] joinString " ");

    if(not (isNull _attached)) then {
        _vehicle setVariable["RD501_mc_is_refueling", true, true];
        _vehicle setVariable["RD501_mc_stop_refuel", false, true];

        _maxFuelSource = getNumber (configFile >> "CfgVehicles" >> typeOf _vehicle >> "fuelCapacity");
        _maxFuelTarget = getNumber (configFile >> "CfgVehicles" >> typeOf _attached >> "fuelCapacity");

        private _firstTick = 0;
        if (isMultiplayer) then {
            _firstTick = serverTime;
        } else {
            _firstTick = time;
        };

        diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Refueling started at tick:", _firstTick] joinString " ");
        hint "Refueling started";

        _vehicle setVariable["RD501_mc_lastRefuelTick", _firstTick, true];
        _vehicle setVariable["RD501_mc_nextRefuelUpdate", 0.1, true];

        [{
            params ["_args", "_pfID"];
            _args params ["_target", "_source", "_rate", "_maxFuelSource", "_maxFuelTarget"];

            if (!alive _source || {!alive _sink}) exitWith  {
                diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Source or sink invalid. Stopping refuel.", _source, _sink] joinString " ");

                _source setVariable["RD501_mc_lastRefuelTick", nil, true];
                _source setVariable["RD501_mc_nextRefuelUpdate", nil, true];
                _source setVariable["RD501_mc_lastTargetFuelAmount", nil, true];
                _source setVariable["RD501_mc_is_refueling", false, true];
                [_pfID] call CBA_fnc_removePerFrameHandler;
            };

            if (_source getVariable["RD501_mc_stop_refuel", false]) exitWith {
                hint "Refueling stopped";
                diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Refueling stopped by manual exit."] joinString " ");

                _source setVariable["RD501_mc_lastRefuelTick", nil, true];
                _source setVariable["RD501_mc_nextRefuelUpdate", nil, true];
                _source setVariable["RD501_mc_lastTargetFuelAmount", nil, true];
                _source setVariable["RD501_mc_is_refueling", false, true];
                [_pfID] call CBA_fnc_removePerFrameHandler;
            };

            private _currentTime = 0;
            if (isMultiplayer) then {
                _currentTime = serverTime;
            } else {
                _currentTime = time;
            };
            
            private _target_fuel_raw = _source getVariable["RD501_mc_lastTargetFuelAmount", nil];

            if (isNil '_target_fuel_raw') then {
                _target_fuel_raw = fuel _target;
                diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Got new fuel value as last did not exist:", _target_fuel_raw] joinString " ");
            };

            private _target_fuel = _target_fuel_raw * _maxFuelTarget;
            private _source_fuel = (fuel _source) * _maxFuelSource;
            
            diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Got current fuel values. Target:", _target_fuel, "Source:", _source_fuel] joinString " ");

            // if last ticks have not been applied yet, act as if they have
            // private _last_target_fuel = (_source getVariable["RD501_mc_lastTargetFuelAmount", 0]) * _maxFuelTarget;
            // if (_last_target_fuel > _target_fuel) then {
            //     _target_fuel = _last_target_fuel;
            // };

            private _deltaT = _currentTime - (_source getVariable["RD501_mc_lastRefuelTick", 0]);
            private _transfer = _rate * _deltaT;

            private _target_new_fuel = ((_target_fuel + _transfer) / _maxFuelTarget);
            private _source_new_fuel = ((_source_fuel - _transfer) / _maxFuelSource);
            
            diag_log text (["[RD501]", "[TRACE]", "[MC-REFUEL]", "Delta T", _deltaT, "Transfer", _transfer, "New Fuel (T)", _target_new_fuel, "New Fuel (S)", _source_new_fuel] joinString " ");

            private _last_update = _source getVariable["RD501_mc_nextRefuelUpdate", 0];

            if (_target_new_fuel >= 1.0) exitWith {
                _target_new_fuel = 1.0;
                hint "Refueling complete";
                diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Refueling stopped by full target tank."] joinString " ");
                
                _source setVariable["RD501_mc_lastRefuelTick", nil, true];
                _source setVariable["RD501_mc_nextRefuelUpdate", nil, true];
                _source setVariable["RD501_mc_lastTargetFuelAmount", nil, true];
                _source setVariable["RD501_mc_is_refueling", false, true];
                [_pfID] call CBA_fnc_removePerFrameHandler;
            };

            if (_target_new_fuel >= _last_update) then {
                _last_update = round(_target_new_fuel * 10) / 10;
                hint format ["Refueling at %1 percent", str (_last_update*100)];
                _source setVariable["RD501_mc_nextRefuelUpdate", _last_update+0.1, true];
            };

            diag_log text (["[RD501]", "[DEBUG]", "[MC-REFUEL]", "Setting detach fuel variable to", _target_new_fuel] joinString " ");

            // This wont work properly due to problems with attachTo and fuel registration, but its still needed.
            // ["RD501_mc_set_fuel", [_target_new_fuel], _target] call CBA_fnc_targetEvent;

            _target setVariable ["RD501_mc_detachFeulAmmount", _target_new_fuel, true];

            _source setVariable ["RD501_mc_lastTargetFuelAmount", _target_new_fuel, true];

            _source setFuel _source_new_fuel;
            _source setVariable["RD501_mc_lastRefuelTick", _currentTime, true];

        }, 1, [_attached, _vehicle, _rate, _maxFuelSource, _maxFuelTarget]] call CBA_fnc_addPerFrameHandler;
    };
};