#include "..\loglevel.hh"

// Registers a missile launcher
params [
	// The launcher object.
	["_launcher", objNull],
	// True if this launcher should never run out of ammo.
	["_refreshAmmo", true],
	// The # of seconds between each allowed missile launch.
	["_reloadSpeed", 15]
];

["RD501_event_internal_CRML_registerLauncher", [_launcher, _refreshAmmo, _reloadSpeed]] call cba_fnc_serverEvent;

["Client requested launcher registration from server.", LOG_DEBUG, "CRML"] call RD501_fnc_logMessage;
