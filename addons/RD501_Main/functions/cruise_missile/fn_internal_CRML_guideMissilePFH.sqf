#include "..\loglevel.hh";

params["_args", "_handle"];

//here we switch to secondary target at random position if the laser dot no longer exists
//if the designator is turned on again, the missile will return to it's original target (providing it hadn't flown too far)
ARG_MISSILE = 0;
ARG_ONDEATH = 1;
ARG_TARGET = 2;
ARG_TRACKING = 3;
ARG_LAST_POS = 4;
ARG_WAYPOINT = 5;
_args params [
	// 0
	// The missile object
	"_missile",
	// 1
	// The script to run on death. All values form _args are
	// passed to this function.
	["_onDeath", {}],
	// 2
	// Target
	["_target", objNull],
	// 3
	// The tracking settings for this missile.
	["_tracking", [false, objNull, ""]],
	// 4
	// The last know location of the missile.
	["_lastPos", [0, 0, 0]],
	// 5
	// Tracking waypoint
	["_waypoint", objNull]

];

if (isNull _missile) exitWith {
	[["Destroying PFH for", _missile, "- Object is no longer alive at position:", _lastPos] joinString " ", LOG_INFO, "CRML GUIDENCE"] call RD501_fnc_logMessage;
	[_handle] call CBA_fnc_removePerFrameHandler;
	
	// Do final cleanup.
	[_target, _lastPos] call _onDeath;
	if (not (isNull _waypoint)) then {
		deleteVehicle _waypoint;
	};

	if (not (isNull (_deleteLauncher select 0))) then {
		["RD501_event_internal_CRML_deleteVLSLauncher", [_deleteLauncher select 0], _deleteLauncher select 0] call CBA_fnc_targetEvent;

		{
			deleteVehicle _x;
		} forEach (_deleteLauncher select 1);
	};
};

private _missilePos = getPosATL _missile;

_args set [ARG_LAST_POS, _missilePos];

if(isNull _waypoint and (_tracking select 0)) then {
	try {
		private _waypointObject = switch (_tracking select 1) do {
			case west: {"RD501_GuidenceHelper_W"};
			case east: {"RD501_GuidenceHelper_E"};
			default {""};
		};

		if (not (_waypointObject isEqualTo "")) then {
			private _waypointObjectRes = [_missilePos, 0, _waypointObject, _tracking select 1] call BIS_fnc_spawnVehicle;
			_waypoint = _waypointObjectRes select 0;
			_args set [ARG_WAYPOINT, _waypoint];

			_waypoint setGroupIdGlobal [[_tracking select 2, random 1001] joinString "-"];
			_waypoint attachTo [_missile, [0,0,0]];
		};
	} catch {
		// Disable tracking, there was an error.
		_tracking set [0, false];
		[["Tracking failed to initalize for:", _missile] joinString " ", LOG_WARN, "CRML GUIDENCE"] call RD501_fnc_logMessage;
	};
};
