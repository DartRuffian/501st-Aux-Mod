// launches a mash missile.

params [
	// The caller of this action.
	"_caller",
	// The target to aim at.
	"_target", 
	// The side of the tracking information. Set to objNull
	// to disable. Defaults to the players side.
	["_side", side player]
];

private _trackingSettings = [true, _side, "MASH"];
try {
	if (not (east isEqualType _side)) then {
		_trackingSettings = [false, objNull, objNull];
	};
} catch {
	_trackingSettings = [false, objNull, objNull];
};

[_caller, _target, "RD501_Mash_Missile_Ammo", {
	_this call RD501_fnc_CRML_mashMissileOnDeath;
}, _trackingSettings, true] call RD501_fnc_CRML_launchMissile;