#include "..\loglevel.hh"

params [
	// The launcher object to remove.
	"_launcher"
];

index = RD501_CRML_LauncherNodes findIf {
	(_x select 0) == _launcher
};

if (index != -1) then {
	removed = RD501_CRML_LauncherNodes deleteAt index;
	[["Removed launcher registration", removed] joinString " ", LOG_INFO, "CRML"] call RD501_fnc_logMessage;
} else {
	// Cleanup this list.
	private _toRemove = [];
	{
		if (isNull (_x select 0)) then {
			_toRemove pushBack _forEachIndex;
		};
	} forEach RD501_CRML_LauncherNodes;

	if ((count _toRemove) > 0) then {
		{
			RD501_CRML_LauncherNodes deleteAt _x;
		} forEach _toRemove;
	} else {
		[["Failed to remove", _launcher, "from the launcher nodes."] joinString " ", LOG_WARN, "CRML"] call RD501_fnc_logMessage;
	};
};