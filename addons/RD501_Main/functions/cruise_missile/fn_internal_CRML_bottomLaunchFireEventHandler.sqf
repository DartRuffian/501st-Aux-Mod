params ["_unit", "_weapon", "_muzzle", "_node", "_ammo", "_magazine", "_projectile"];

_this call (uinamespace getvariable 'BIS_fnc_effectFired');

private _offset = _unit distance _projectile;
private _newPos = (getPosATL _projectile) vectorAdd [0, 0, 0 - _offset];
_projectile setPosATL _newPos;
_projectile setVectorUp [0, 0, -1];