#include "..\loglevel.hh"

// Removes a registerd launcher.
params [
	// The launcher object to remove.
	"_launcher"
];

["RD501_event_internal_CRML_removeLauncher", [_launcher]] call cba_fnc_serverEvent;

["Client requested launcher removal from server.", LOG_DEBUG, "CRML"] call RD501_fnc_logMessage;
