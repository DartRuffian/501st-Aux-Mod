#include "functions\function_macros.hpp"

LOG("PreInit Begin");
private _addonConfig = configFile >> "CfgPatches" >> QUOTE(ADDON);
private _version = getArray(_addonConfig >> "version");
LOG(format["Version: %1", _verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

GVAR(jumpTypes) = [[]] call FUNC(loadJumpTypesFromConfig);
GVAR(validBackpacks) = [GVAR(jumpTypes)] call FUNC(loadJumppacksFromConfig);
GVAR(jumpTypeHideSettings) = [];
GVAR(dispensers) = [GVAR(validBackpacks)] call FUNC(loadJumpDispenserVehiclesFromConfig);

LOG("PreInit Complete");