/*
 * Author: M3ales
 * Executes a jump for a unit with a jumppack.
 *
 * Arguments:
 * 0: Unit <OBJECT>
 * 1: Forward velocity <NUMBER> (default: current forward velocity)
 * 2: Vertical velocity <NUMBER> (default: current vertical velocity)
 * 3: Relative to camera <BOOL> (default: false)
 *
 * Return Value:
 * None
 *
 * Example:
 * [player, 10, 5, true] call rd501_jumppack_fnc_executeJump;
 *
 * Public: No
 */

#include "function_macros.hpp"

params ["_unit", "_forward", "_vertical", "_relativeToCamera"];

velocityModelSpace _unit params ["_currentSide", "_currentForward", "_currentVertical"];

if (_forward isEqualType false) then {
	_forward = _currentForward;
};

if (_vertical isEqualType false) then {
	_vertical = _currentVertical;
};

if (isTouchingGround _unit) then {
	private _pos = getPosASL _unit;
	_unit setPosASL (_pos vectorAdd [0, 0, 0.1]);
};

// Toward the Camera (up/down)
if (_relativeToCamera) exitWith {
	(getCameraViewDirection _unit) params ["_viewX", "_viewY", "_viewZ"];
	_unit setVelocity [
		_viewX * _forward,
		_viewY * _forward,
		(_viewZ * _forward) + (_vertical/2)
	];
};

// Relative to the unit (no up/down)
_unit setVelocityModelSpace [0, _forward, _vertical];
