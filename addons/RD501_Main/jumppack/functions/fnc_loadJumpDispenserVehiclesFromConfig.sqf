/*
 * Author: M3ales
 * Loads jump dispenser vehicles from config and logs the information.
 *
 * Arguments:
 * _jumppacks: List of jumppacks to search for in the config. <ARRAY>
 *
 * Return Value:
 * _store: List of vehicles with jumppack configs. <ARRAY>
 *
 * Example:
 * [["rd501_test_jumppack"]] call rd501_jumppack_fnc_loadJumpDispenserVehiclesFromConfig;
 *
 * Public: Yes
 */

#include "function_macros.hpp"

#define DISPENSER_CLASS rd501_jumppackDispenser

params["_jumppacks"];

private _classes = configProperties [
	configFile >> "CfgVehicles",
	QUOTE(isClass(_x) && {
		isClass(_x >> QUOTE(QUOTE(DISPENSER_CLASS)))
	} && {
		!([ARR_2(_x, QUOTE(QUOTE(Bag_Base)))] call FUNC(inheritsFromConfig))
	}),
	true
];

private _store = [];

{
	private _config = _x >> QUOTE(DISPENSER_CLASS);
	private _jumppack = [_config, "jumppack", ""] call BIS_fnc_returnConfigEntry;

	if!(_jumppacks findIf {
		_x params["_class"];
		_class isEqualTo _jumppack
	} != -1) then {
		LOG_ERRORF_2("%1 has a jumppack dispenser that requests '%2', but '%2' is not a jumppack.", configName _x, _jumppack);
		continue;
	};

	_store pushBack [configName _x, _jumppack];
} forEach(_classes);

LOGF_1("Found %1 vehicles with jumppack configs", count _store);

{
	LOGF_1("%1", _x select 0);
} forEach _store;

_store
