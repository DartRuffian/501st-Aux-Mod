/*
 * Author: M3ales
 * Wraps jump type index when cycling through jump types.
 *
 * Arguments:
 * 0: Jump type index <NUMBER>
 * 1: Number of jump types <NUMBER>
 *
 * Return Value:
 * Wrapped jump type index <NUMBER>
 *
 * Example:
 * [3, 4] call rd501_jumppack_fnc_cycleWrap;
 *
 * Public: No
 */

#include "function_macros.hpp"

params ["_jumpType", "_jumpTypeCount"];

LOGF_2("Cycling jump type from index %1 within range 0-%2", _jumpType, _jumpTypeCount);

if (_jumpType >= _jumpTypeCount) then {
	_jumpType = 0;
	LOG("Jump type index wrapped to the beginning");
};

if (_jumpType < 0) then {
	_jumpType = _jumpTypeCount - 1;
	LOG("Jump type index wrapped to the end");
};

_jumpType
