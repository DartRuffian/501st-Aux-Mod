/*
 * Author: M3ales
 * Function to determine if a unit can jump.
 *
 * Arguments:
 * 0: Unit to check if can jump <OBJECT>
 *
 * Return Value:
 * Can the unit jump <BOOL>
 *
 * Example:
 * [player] call fnc_canJump;
 *
 * Public: No
 */

params["_unit"];

if (_unit isEqualTo objNull) exitWith {
	false
};
if!(alive _unit) exitWith {
	false
};

private _isInWater = surfaceIsWater position _unit;
private _isHeadUnderwater = eyePos _unit select 2 < 0;
private _isUnconscious = _unit getVariable ["ace_medical_isUnconscious", false];

!(_isInWater && _isHeadUnderwater) || _isUnconscious
