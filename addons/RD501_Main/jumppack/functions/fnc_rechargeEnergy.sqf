/*
 * Author: M3ales
 * This function recharges the energy of a rd501_jumppack for a given unit by a specified amount.
 *
 * Arguments:
 * 0: Unit who is using the jumppack <OBJECT>
 * 1: Amount of energy to recharge <NUMBER>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_unit, _amount] call rd501_jumppack_fnc_rechargeEnergy
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_unit", "_amount"];

// Get the unit's backpack and jumppack
private _backpack = unitBackpack _unit;
private _jumppack = [_unit] call FUNC(getJumppack);

if (_jumppack isEqualTo false) exitWith {};

_jumppack params ["_class", "_capacity", "_rechargeRate"];

// Get current energy, calculate new value, and update the energy variable
private _currentEnergy = _backpack getVariable[QGVAR(energy), 0];
private _value = [_currentEnergy + _amount, 0, _capacity] call BIS_fnc_clamp;
_backpack setVariable [QGVAR(energy), _value, true];
