/*
 * Author: M3ales
 * Adds a "Get Jumppack" self action to a specified class.
 *
 * Arguments:
 * _type: The className of the vehicle/object to add this action to. <STRING>
 * _jumppack: The className of the jumppack to give the person using the "Get Jumppack" action. <STRING>
 *
 * Return Value:
 * None
 *
 * Example:
 * ["rd501_laatmk2", "rd501_test_jumppack"] call rd501_jumppack_fnc_addDispenserActionsToClass;
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_type", "_jumppack"];

private _rootAction = [
	QGVAR(dispenser_root),
	"Jumppack Dispenser",
	"",
	{},
	{
		true
	}
] call ACE_interact_menu_fnc_createAction;
[_type, 1, ["ACE_SelfActions"], _rootAction, false] call ACE_interact_menu_fnc_addActionToClass;

_jumppack params ["_class", "_capacity", "_rechargeRate", "_displayName", "_icon"];

private _action = [
	QGVAR(dispenser_get),
	format["Get %1", _displayName],
	_icon,
	{
		params["_target", "_player", "_params"];
		_params params ["_jumppackClass"];
		[_player, _jumppackClass] call FUNC(getJumppackFromDispenser);
	},
	{
		params["_target", "_player", "_params"];
		_params params ["_jumppackClass"];
		(backpack _player) isNotEqualTo _jumppackClass
	},
	{
		[]
	},
	[_class]
] call ACE_interact_menu_fnc_createAction;

[_type, 1, ["ACE_SelfActions", QGVAR(dispenser_root)], _action, false] call ACE_interact_menu_fnc_addActionToClass;
