#include "function_macros.hpp"

params["_unit", ["_jumpIndex", -1]];

private _jumppack = [_unit] call FUNC(getJumppack);

if (_jumppack isEqualTo false) exitWith {
	LOG("No jumppack equipped");
	[_unit] call FUNC(showState);
};

_jumppack params ["_name", "_capacity", "_rechargeRate", "_displayName", "_picture", "_allowedJumpTypes"];

if!([_unit] call FUNC(canJump)) exitWith {
	LOG("Conditions to jump not met");
	[_unit, "Unable to jump from here", _jumpIndex] call FUNC(showState);
};

if (_jumpIndex isEqualTo -1) then {
	_jumpIndex = GVAR(selectedJumpType);
};

if!(_jumpIndex in _allowedJumpTypes) exitWith {
	LOGF_1("Jump type %1 not allowed", _jumpIndex);
	[_unit, "This jumppack does not support the requested jump type", _jumpIndex] call FUNC(showState);
};

(GVAR(jumpTypes) select _jumpIndex) params ["_class", "_displayName", "_forward", "_vertical", "_cost", "_relativeToCamera"];

if!([_unit, _cost] call FUNC(tryUseEnergy)) exitWith {
	[_unit, format["Need %1 energy", _cost], _jumpIndex] call FUNC(showState);
};

[_unit] call FUNC(playIgniteSound);

[_unit, _forward, _vertical, _relativeToCamera] call FUNC(executeJump);

[_unit, "", _jumpIndex] call FUNC(showState);
