/*
 * Author: M3ales
 * Gets a jumppack from a dispenser for the given target.
 *
 * Arguments:
 * 0: Target unit <OBJECT> (default: player)
 * 1: Jumppack type <STRING> (default: "")
 *
 * Return Value:
 * None
 *
 * Example:
 * [player, "my_jumppack_type"] call rd501_jumppack_fnc_getJumppackFromDispenser;
 *
 * Public: No
 */

#include "function_macros.hpp"
params [["_target", player, [player]], ["_jumppackType", ""]];

// if the target already has the specified jumppack, set energy and exit
if (backpack _target == _jumppackType) exitWith {
	[_target, -1] call FUNC(setEnergy);
};

// if the target has a backpack
if (backpack _target != "") then {
	// get the chestpack of the target
	private _chestpack = [_target] call zade_boc_fnc_chestpack;

	// if the target has no chestpack, perform action on chest and remove backpack
	if (_chestpack == "") then {
		[_target] call zade_boc_fnc_actionOnChest;
		removeBackpack _target;
	} else {
		// if the target has a chestpack, perform action swap
		[_target] call zade_boc_fnc_actionSwap;
	};

	// if the target now has the specified jumppack, set energy and exit
	if (backpack _target == _jumppackType) exitWith {};

	// Add the specified jumppack to the target and set energy
	_target addBackpack _jumppackType;
	[_target, -1] call FUNC(setEnergy);
} else {
	// if the target has no backpack, add the specified jumppack and set energy
	_target addBackpack _jumppackType;
	[_target, -1] call FUNC(setEnergy);
}
