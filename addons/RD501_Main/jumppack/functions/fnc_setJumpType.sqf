/*
 * Author: M3ales
 * This function sets the jump type of the rd501_jumppack for the player.
 *
 * Arguments:
 * 0: Jump type index to set <NUMBER>
 *
 * Return Value:
 * None
 *
 * Example:
 * [2] call rd501_jumppack_fnc_setJumpType
 *
 * Public: No
 */

#include "function_macros.hpp"

params["_jumpTypeIndex"];

private _unit = player;
private _max = count GVAR(jumpTypes);
private _jumppack = [_unit] call FUNC(getJumppack);

if (_jumppack isEqualTo false) exitWith {
	LOG("No jumppack equipped");
	[_unit] call FUNC(showState);
};

_jumppack params ["_name", "_capacity", "_rechargeRate", "_displayName", "_picture", "_allowedJumpTypes"];

private _resolvedIndex = [_jumpTypeIndex, 0, _max] call BIS_fnc_clamp;

// Exit if the resolved jump type index is not allowed
if !(_resolvedIndex in _allowedJumpTypes) exitWith {
	LOGF_1("Jump type %1 not allowed", _resolvedIndex);
	[_unit, "This jumppack does not support the requested jump type", _resolvedIndex] call FUNC(showState);
};

GVAR(selectedJumpType) = _resolvedIndex;

LOGF_1("Set jump type to %1", GVAR(selectedJumpType));

[_unit] call FUNC(showState);
