/*
 * Author: M3ales
 * This function plays the ignite sound effect for the rd501_jumppack.
 *
 * Arguments:
 * 0: Unit who triggers the sound <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_unit] call rd501_jumppack_fnc_playIgniteSound
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_unit"];

// Retrieve the selected sound from the config and play it in 3D space at the unit's position
private _selectedSound = getText(configFile >> "CfgVehicles" >> (backpack _unit) >> QUOTE(ADDON) >> "igniteSound");
playSound3D [
    _selectedSound,
    _unit,
    false,
    getPosASL _unit,
    2,
    1,
    50
];
