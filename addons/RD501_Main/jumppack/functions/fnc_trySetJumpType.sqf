/*
 * Author: M3ales
 * This function attempts to set the jump type of a jumppack.
 * It returns true if the jump type is successfully set or false if not.
 *
 * Arguments:
 * 0: Unit to set the jump type for <OBJECT>
 * 1: Jump type index <NUMBER>
 *
 * Return Value:
 * A boolean indicating whether the jump type was set successfully or not <BOOLEAN>
 *
 * Example:
 * [player, 2] call rd501_jumppack_fnc_trySetJumpType;
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params ["_unit", "_jumpTypeIndex"];

private _jumppack = [_unit] call FUNC(getJumppack);
if (_jumppack isEqualTo false) exitWith {
	false
};
_jumppack params ["_name", "_capacity", "_rechargeRate", "_displayName", "_picture", "_allowedJumpTypes"];

if (_jumpTypeIndex < 0 || _jumpTypeIndex >= count GVAR(jumpTypes)) exitWith {
	false
};

private _disallowed = !(_jumpTypeIndex in _allowedJumpTypes);

if (!_disallowed) exitWith {
	GVAR(selectedJumpType) = _jumpTypeIndex;
	true
};

false
