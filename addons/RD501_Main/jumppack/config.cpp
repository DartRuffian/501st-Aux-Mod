#include "config_macros.hpp"
class CfgPatches {
	class ADDON
	{
        addonRootClass = "rd501_main";
		version[] = { 1, 0, 0, 0 }; //Negative last digit indicates alpha/beta
		requiredAddons[] = {"ace_medical", "cba_settings", "A3_UI_F"};
		units[] = {};
		weapons[] = {};
	};
};

class Extended_PreInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_preInit));
    };
};

class Extended_PostInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_postInit));
    };
};

#include "ACE_Medical_Injuries.hpp"
#include "HUD.hpp"
#include "JumpTypes.hpp"

/* 
class CfgVehicles
{
	class B_AssaultPack_blk;
	class JLTS_Clone_jumppack_mc;
	class JLTS_Clone_jumppack;
	class JLTS_Clone_jumppack_JT12;

	class rd501_test_jumppack_01: JLTS_Clone_jumppack_JT12
	{
		scope=2;
		displayName = "000 - TEST PACK";
		maximumLoad=700;
        JLTS_isJumppack = 0;
        class rd501_jumppack {
            rechargeRateSecond = 6;
            capacity = 150;
            allowedJumpTypes[] = {
                "Forward",
                "Short",
                "Cancel"
            };
        };
	};

    class B_Heli_Light_01_F;
    class rd501_test_fake_laat : B_Heli_Light_01_F {
        class rd501_jumppackDispenser {
            jumppack="rd501_test_jumppack_01";
        };
    };
};
*/
