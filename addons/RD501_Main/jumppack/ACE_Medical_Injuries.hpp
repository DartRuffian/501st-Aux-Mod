#include "config_macros.hpp"

class ACE_Medical_Injuries {
    class damageTypes {
        class woundHandlers;
        class falling {
            class woundHandlers: woundHandlers {
                ADDON = QUOTE({ _this call FUNC(fallDamageHandler) });
            };
        };
        class collision {
            class woundHandlers: woundHandlers {
                ADDON = QUOTE({ _this call FUNC(fallDamageHandler) });
            };
        };
    };
};