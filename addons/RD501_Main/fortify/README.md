# Fortify Tool

Custom cost logic using items instead of money. By default uses 

## Placable Items

Any object in CfgVehicles can be registered as eligable for placement, by including:

`rd501_fortify_tool_placeable=1`

Specifying `rd501_fortify_tool_placeable=0` will remove it if previously specified as 1.
It does not inherit properties from parent, all leaf nodes will need to have it specified. [Citation Needed]

Alternatively this can be specified as a list of classnames within `CfgPatches > rd501_fortify`.

`externalPlaceables[] = { "class1", "class2", "class3" };

The final list of placeables is the sum of these two arrays. No uniqueness checking is made at present.

## Currency Item

Defined in the CfgPatches entry for `rd501_fortify_tool`, `currencyItem` specifies the classname of the item to consume when using the fortify tool.

```cpp
	class ADDON
	{
        addonRootClass = "rd501_main";
		version[] = { 1, 0, 0, 0 }; //Negative last digit indicates alpha/beta
		requiredAddons[] = {"acex_fortify_tool", "cba_settings"};
		units[] = {};
		weapons[] = {};
        currencyItem = "RD501_fortify_nanobots";
	};
```

```cpp
	class rd501_fortify_tool
	{
        addonRootClass = "rd501_main";
		version[] = { 1, 0, 0, 0 }; //Negative last digit indicates alpha/beta
		requiredAddons[] = {"acex_fortify_tool", "cba_settings"};
		units[] = {};
		weapons[] = {};
        currencyItem = "RD501_fortify_nanobots";
	};
```
