#include "function_macros.hpp"
params["_placeableObjects"];

if (GVAR(usePreset)) then
{
	[west, 0, _placeableObjects] call acex_fortify_fnc_registerObjects;
} else
{
	[west, 0, []] call acex_fortify_fnc_registerObjects;
}