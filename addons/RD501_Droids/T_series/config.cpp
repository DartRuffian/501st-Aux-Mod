class CfgPatches
{
	class rd501_patch_name_tseries_units
	{
		requiredAddons[]=
		{
			RD501_patch_units
		};
		requiredVersion=0.1;
		units[]=
		{
			"RD501_opfor_unit_tseries_standard",
			"RD501_opfor_unit_tseries_green",
            "RD501_opfor_unit_tseries_red"
		};
		weapons[]=
		{
			
		};
	};
};
class CfgWeapons
{
	class U_I_CombatUniform;
	class UniformItem;
	class RD501_Tseries: U_I_CombatUniform
	{
		JLTS_isDroid = 0;
		JLTS_hasEMPProtection = 0;
		JLTS_deathSounds = "DeathDroid";
		picture = "\RD501_Droids\T_series\Tseries_armor_ui.paa";
		scope = 2;
		displayName = "[CIS] T-Series Chassis";
		model = "\3AS\3AS_Characters\Droids\TSeries\3AS_TS.p3d";
		hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_CO.paa"};
		hiddenSelections[] = {"camo"};
		class ItemInfo: UniformItem
		{
			uniformModel = "-";
			uniformClass = "RD501_opfor_unit_tseries_standard";
			containerClass = "Supply150";
			mass = 60;
			hiddenSelections[] = {"camo"};
		};
	};
	class RD501_Tseries_green: RD501_Tseries
	{
		JLTS_hasEMPProtection = 0;
		JLTS_deathSounds = "DeathDroid";
		scope = 2;
		displayName = "[CIS] T-Series Green Chassis";
		model = "\3AS\3AS_Characters\Droids\TSeries\3AS_TS.p3d";
		hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_Green_CO.paa"};
		hiddenSelections[] = {"camo"};
		class ItemInfo: UniformItem
		{
			uniformModel = "-";
			uniformClass = "RD501_opfor_unit_tseries_green";
			containerClass = "Supply150";
			mass = 60;
			hiddenSelections[] = {"camo"};
		};
	};
	class RD501_Tseries_red: RD501_Tseries
	{
		JLTS_hasEMPProtection = 0;
		JLTS_deathSounds = "DeathDroid";
		scope = 2;
		displayName = "[CIS] T-Series Red Chassis";
		model = "\3AS\3AS_Characters\Droids\TSeries\3AS_TS.p3d";
		hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_Red_CO.paa"};
		hiddenSelections[] = {"camo"};
		class ItemInfo: UniformItem
		{
			uniformModel = "-";
			uniformClass = "RD501_opfor_unit_tseries_red";
			containerClass = "Supply150";
			mass = 60;
			hiddenSelections[] = {"camo"};
		};
	};
};
class CfgVehicles
{
	class O_soldier_base_F;
	class O_Soldier_F: O_soldier_base_F
	{
		class HitPoints;
	};
	class RD501_opfor_unit_tseries_base: O_Soldier_F
    {
		author = "RD501";
		scope = 1;
		scopeCurator = 0;
		faction="RD501_CIS_Faction";
		editorSubcategory="RD501_Editor_Category_CIS_SpecOps";
		vehicleClass ="RD501_Editor_Category_CIS_SpecOps";
		model = "\3AS\3AS_Characters\Droids\TSeries\3AS_TS.p3d";
		identityTypes[] = {"lsd_voice_b1Droid"};
		threat[] = {1.2,0.1,0.1};
		role = "Rifleman";
        speaker = "Male01ENGVR";
		genericNames = "3ASDroidNames";
		accuracy = 0.9;
		precision = 6;
		sensitivity = 1.5;
		sensitivityEar = 0.13;
		camouflage = 0;
		minFireTime = 20;
		AnimEffectShortExhaust = 0.005;
		AnimEffectShortRest = 0.05;
		oxygenCapacity = 10800;
		aimingAccuracy = 0.2;
		aimingSpeed = 0.6;
		commanding = 0.6;
		courage = 1;
		general = 0.8;
		reloadSpeed = 0.6;
		spotDistance = 0.4;
		spotTime = 0.4;
		aimingShake = 0.6;
		irTarget = 0.5;
		hiddenSelections[] = {"Camo"};
		uniformClass = "RD501_Tseries";
		hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_CO.paa"};
		hideProxySelections[] = {"ghillie_hide"};
		backpack = "";
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		Items[] = {""};
		RespawnItems[] = {""};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
		    "Aux501_Weapons_Mags_CISDetonator"
        };
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
			"Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
		    "Aux501_Weapons_Mags_CISDetonator"
        };
		linkedItems[]=
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		respawnLinkedItems[] = 
		{
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"JLTS_droid_comlink",
			"JLTS_NVG_droid_chip_1"
		};
		cost = 600000;
		ISmaxTurn = 20;
		class HitPoints: HitPoints
		{
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 5;
				material = -1;
				name = "neck";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 5;
				material = -1;
				name = "head";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 15;
				material = -1;
				name = "pelvis";
				passThrough = 0.1;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 15;
				material = -1;
				name = "spine1";
				passThrough = 0.1;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 15;
				material = -1;
				name = "spine2";
				passThrough = 1;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 15;
				material = -1;
				name = "spine3";
				passThrough = 0.1;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 0.1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 15;
				material = -1;
				name = "arms";
				passThrough = 0.5;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 15;
				material = -1;
				name = "hands";
				passThrough = 0.5;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 15;
				material = -1;
				name = "legs";
				passThrough = 0.5;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		armor = 2;
		armorLights = 0.4;
		armorStructural = 3;
		explosionShielding = 0.4;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.5;
		canDeactivateMines = 0;
		engineer = 0;
		attendant = 0;		
		class SoundEquipment 
		{	
		    soldier[] = 
			{
             {"run", {"\WebKnightsRobotics\sounds\dirt1.wav", 2, 1, 30}}, 
             {"run", {"\WebKnightsRobotics\sounds\dirt2.wav", 2, 1, 30}},
             {"run", {"\WebKnightsRobotics\sounds\dirt3.wav", 2, 1, 30}},
			 {"run", {"\WebKnightsRobotics\sounds\dirt4.wav", 2, 1, 30}},
			 {"walk", {"\WebKnightsRobotics\sounds\dirt1.wav", 2, 1, 15}}, 
             {"walk", {"\WebKnightsRobotics\sounds\dirt2.wav", 2, 1, 15}},
             {"walk", {"\WebKnightsRobotics\sounds\dirt3.wav", 2, 1, 15}},
			 {"walk", {"\WebKnightsRobotics\sounds\dirt4.wav", 2, 1, 15}},
			 {"sprint", {"\WebKnightsRobotics\sounds\dirt1.wav", 2, 1, 45}}, 
             {"sprint", {"\WebKnightsRobotics\sounds\dirt2.wav", 2, 1, 45}},
             {"sprint", {"\WebKnightsRobotics\sounds\dirt3.wav", 2, 1, 45}},
			 {"sprint", {"\WebKnightsRobotics\sounds\dirt4.wav", 2, 1, 45}},
			 {"Tactical", {"\WebKnightsRobotics\sounds\dirt1.wav", 2, 1, 15}}, 
             {"Tactical", {"\WebKnightsRobotics\sounds\dirt2.wav", 2, 1, 15}},
             {"Tactical", {"\WebKnightsRobotics\sounds\dirt3.wav", 2, 1, 15}},
			 {"Tactical", {"\WebKnightsRobotics\sounds\dirt4.wav", 2, 1, 15}}
            };
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
        impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
    };
	class RD501_opfor_unit_tseries_standard:RD501_opfor_unit_tseries_base
    {
		scope = 2;
		scopeCurator = 2;
		displayName = "T-Series (Blue)";
	};
	class RD501_opfor_unit_tseries_green:RD501_opfor_unit_tseries_standard
    {
		scope = 2;
		scopeCurator = 2;
		displayName = "T-Series (Green)";
		uniformClass = "RD501_Tseries_green"; 
		hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_Green_CO.paa"};
	};
	class RD501_opfor_unit_tseries_red:RD501_opfor_unit_tseries_standard
    {
		scope = 2;
		scopeCurator = 2;
		displayName = "T-Series (Red)";
		uniformClass = "RD501_Tseries_red"; 
		hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_Red_CO.paa"};
	};        
};



	