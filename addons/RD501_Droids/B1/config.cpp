#include "../../RD501_main/config_macros.hpp"
#include "../droid_macros.hpp"


class CfgPatches
{
	class RD501_patch_B1
	{
		addonRootClass=RD501_patch_droids_config;
		requiredAddons[] = {
			RD501_patch_droids_config,
			"JLTS_characters_DroidArmor",
   			"JLTS_weapons_RPS6",
			"JLTS_weapons_E5",
			"JLTS_weapons_E5S",
			"JLTS_weapons_E60R"
		};
		requiredVersion = 0.1;
		units[] = {
			macro_new_unit_class(opfor,B1),
			macro_new_unit_class(opfor,B1_marine),
			macro_new_unit_class(opfor,B1_security),
			macro_new_unit_class(opfor,B1_commander),
			macro_new_unit_class(opfor,B1_pilot),
			macro_new_unit_class(opfor,B1_crew),
			macro_new_unit_class(opfor,B1_prototype),
			macro_new_unit_class(opfor,B1_heavy),
			macro_new_unit_class(opfor,B1_AT),
			macro_new_unit_class(opfor,B1_AA),
			macro_new_unit_class(opfor,B1_shotgun),
			macro_new_unit_class(opfor,B1_marksman),
			macro_new_unit_class(opfor,B1_jammer),
			macro_new_unit_class(opfor,B1_E_Web),
			macro_new_unit_class(opfor,B1_grenadier)
		};
	};
};

class CfgWeapons
{
	class UniformItem;
	class JLTS_DroidB1;

	// custom uniforms
	class macro_new_uniform_class(opfor,B1_jammer): JLTS_DroidB1
	{
		displayName="[CIS] B1 Jammer Chassis";
		class ItemInfo: UniformItem
		{
			uniformModel="-";
			uniformClass=macro_new_unit_class(opfor,B1_jammer);
			containerClass="Supply150";
			mass=40;
		};
	};
};

class CfgVehicles
{
	class JLTS_B1_backpack;
	class JLTS_B1_backpack_prototype;
	class JLTS_B1_antenna;
	class RD501_opfor_B1_backpack: JLTS_B1_backpack
	{
		author = "RD501";
		scope = 1;
	};
	class RD501_opfor_B1_prototype_backpack: JLTS_B1_backpack_prototype
	{
		author = "RD501";
		scope = 1;
	};
	class RD501_opfor_B1_antenna_backpack: JLTS_B1_antenna
	{
		author = "RD501";
		scope = 1;
	};
	class RD501_opfor_B1_AT_backpack: JLTS_B1_backpack
	{
		author = "RD501";
		scope = 1;
		class TransportMagazines
		{
			class _xx_at_mag
			{
				count = 4;
				magazine = "Aux501_Weapons_Mags_e60r_at";
			};
			class _xx_he_mag
			{
				count = 2;
				magazine = "Aux501_Weapons_Mags_e60r_he";
			};
		};
	};
	class RD501_opfor_B1_AA_backpack: JLTS_B1_backpack
	{
		author = "RD501";
		scope = 1;
		class TransportMagazines
		{
			class _xx_aa_mag
			{
				count = 3;
				magazine = "Aux501_Weapons_Mags_e60r_aa";
			};
		};
	};
	class RD501_opfor_B1_jammer_backpack: JLTS_B1_backpack
	{
		author = "RD501";
		displayName = "[CIS] B1 Jammer backpack";
		scope = 1;
		hiddenSelectionsTextures[] = {"\RD501_Droids\data\b1_jammer_backpack.paa"};
	};
	class B_UAV_01_backpack_F;
	class EWEB_Bag: B_UAV_01_backpack_F
	{
		class assembleInfo;
	};
	class JLTS_Droid_B1_E5;
	class JLTS_Droid_B1_Marine;
	class JLTS_Droid_B1_Security;
	class JLTS_Droid_B1_Commander;
	class JLTS_Droid_B1_Pilot;
	class JLTS_Droid_B1_Crew;
	class JLTS_Droid_B1_Prototype;
	class RD501_opfor_unit_B1: JLTS_Droid_B1_E5
	{
		displayName = "B1 Battledroid";
		editorSubcategory = "RD501_Editor_Category_B1";
		genericNames = "ls_droid_b1";
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
		respawnlinkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        backpack = "RD501_opfor_B1_backpack";
		identityTypes[] = {"lsd_voice_b1Droid"};
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		cost = 1;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_marine: JLTS_Droid_B1_Marine
	{
		displayName = "B1 Battledroid (Marine)";
		genericNames = "ls_droid_b1";
		editorSubcategory = "RD501_Editor_Category_B1";
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
		respawnlinkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        backpack = "RD501_opfor_B1_backpack";
		identityTypes[] = {"lsd_voice_b1Droid"};
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		cost = 1;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_security: JLTS_Droid_B1_Security
	{
		displayName = "B1 Battledroid (Security)";
		editorSubcategory = "RD501_Editor_Category_B1";
		genericNames = "ls_droid_b1";
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
		respawnlinkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        identityTypes[] = {"lsd_voice_b1Droid"};
		backpack = "RD501_opfor_B1_backpack";
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		cost = 1;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_commander: JLTS_Droid_B1_Commander
	{
		displayName = "B1 Battledroid (Commander)";
		editorSubcategory = "RD501_Editor_Category_B1";
		identityTypes[] = {"lsd_voice_b1Droid"};
		genericNames = "ls_droid_b1";
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		respawnlinkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		backpack = "RD501_opfor_B1_antenna_backpack";
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		cost = 5;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_pilot: JLTS_Droid_B1_Pilot
	{
		displayName = "B1 Battledroid (Pilot)";
		genericNames = "ls_droid_b1";
		editorSubcategory = "RD501_Editor_Category_B1";
		identityTypes[] = {"lsd_voice_b1Droid"};
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		respawnlinkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		backpack = "RD501_opfor_B1_backpack";
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		cost = 2;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_crew: JLTS_Droid_B1_Crew
	{
		displayName = "B1 Battledroid (Crew)";
		editorSubcategory = "RD501_Editor_Category_B1";
		genericNames = "ls_droid_b1";
		identityTypes[] = {"lsd_voice_b1Droid"};
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		respawnlinkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		backpack = "RD501_opfor_B1_backpack";
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		cost = 1;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_prototype: JLTS_Droid_B1_Prototype
	{
		displayName = "B1 Battledroid (SpecOps)";
		editorSubcategory = "RD501_Editor_Category_CIS_SpecOps";
		genericNames = "ls_droid_b1";
		author = "RD501";
		scope = 2;
		faction = "RD501_CIS_Faction";
		linkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		respawnlinkeditems[] = {"ItemGPS","ItemMap","ItemCompass","ItemWatch","JLTS_droid_comlink","JLTS_NVG_droid_chip_1"};
		class HitPoints
		{
			class ACE_HDBracket
			{
				armor = 1;
				depends = "HitHead";
				explosionShielding = 1;
				material = -1;
				minimalHit = 0;
				name = "head";
				passThrough = 0;
				radius = 1;
				visual = "";
			};
			class HitFace
			{
				armor = 1;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 1;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 1;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 8;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 6;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 3;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 6;
				material = -1;
				name = "spine2";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 8;
				material = -1;
				name = "spine3";
				passThrough = 0.33;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 6;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 6;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 6;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 3;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 6;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 3;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 6;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 3;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
		identityTypes[] = {"lsd_voice_b1Droid"};
		impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
		canBleed = 0;
		weapons[] = {"Aux501_Weaps_E5_Special","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_flashnade",
            "Aux501_Weapons_Mags_flashnade"
        };
		respawnWeapons[] = {"Aux501_Weaps_E5_Special","Throw","Put"};
		respawnMagazines[] =
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_flashnade",
            "Aux501_Weapons_Mags_flashnade"
        };
        items[] = {"ACE_CableTie","ACE_CableTie","ACE_CableTie"};
		respawnItems[] = {"ACE_CableTie","ACE_CableTie","ACE_CableTie"};
		backpack = "RD501_opfor_B1_prototype_backpack";
		cost = 3;
		class SoundEnvironExt
		{
			generic[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundEquipment
		{
			soldier[] = {{"run",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,30}},{"run",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,30}},{"walk",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"walk",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}},{"sprint",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,45}},{"sprint",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,45}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt1.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt2.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt3.wav",2,1,15}},{"Tactical",{"\WebKnightsRobotics\sounds\dirt4.wav",2,1,15}}};
		};
		class SoundBreath
		{
			breath[] = {};
		};
		class SoundDrown
		{
			breath[] = {};
		};
		class SoundInjured
		{
			breath[] = {};
		};
		class SoundBleeding
		{
			breath[] = {};
		};
		class SoundBurning
		{
			breath[] = {};
		};
		class SoundChoke
		{
			breath[] = {};
		};
		class SoundRecovered
		{
			breath[] = {};
		};
	};
	class RD501_opfor_unit_B1_heavy: RD501_opfor_unit_B1_security
	{
		displayName = "B1 Battledroid (Heavy)";
		icon = "JLTS_iconManSupportGunner";
		weapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		respawnWeapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
		respawnMagazines[] =
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        cost = 2;
	};
	class RD501_opfor_unit_B1_AT: RD501_opfor_unit_B1_pilot
	{
		displayName = "B1 Battledroid (Anti-Tank)";
		threat[] = {0.6,1,0.3};
		weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
		respawnWeapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        backpack = "RD501_opfor_B1_AT_backpack";
		cost = 2;
	};
	class RD501_opfor_unit_B1_AA: RD501_opfor_unit_B1_pilot
	{
		displayName = "B1 Battledroid (AA)";
		threat[] = {0.2,0.1,1};
		weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_aa","Throw","Put"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_aa","Throw","Put"};
		respawnMagazines[] =
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		backpack = "RD501_opfor_B1_AA_backpack";
		cost = 2;
	};
	class RD501_opfor_unit_B1_shotgun: RD501_opfor_unit_B1_security
	{
		displayName = "B1 Battledroid (Shotgun)";
		weapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
		respawnWeapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
		magazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
		respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        cost = 2;
	};
	class RD501_opfor_unit_B1_marksman: RD501_opfor_unit_B1_marine
	{
		displayName = "B1 Battledroid (Marksman)";
		icon = "JLTS_iconManSniper";
		weapons[]=
		{
			"Aux501_Weaps_E5S",
			"Throw",
			"Put"
		};
		respawnWeapons[]=
		{
			"Aux501_Weaps_E5S",
			"Throw",
			"Put"
		};
		magazines[]=
		{
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		respawnMagazines[]=
		{
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_E5S15",
			"Aux501_Weapons_Mags_CISDetonator"
		};
		cost=2;
	};
	class RD501_opfor_unit_B1_jammer: RD501_opfor_unit_B1
	{
		displayName = "B1 Battledroid (Jammer)";
		icon = "JLTS_iconManMarshalCMDR";
		backpack = "RD501_opfor_B1_jammer_backpack";
		uniformClass = "RD501_opfor_uniform_B1_jammer";
		hiddenSelectionsTextures[] = {"\RD501_Droids\data\b1_jammer.paa"};
		cost = 3;
	};
};